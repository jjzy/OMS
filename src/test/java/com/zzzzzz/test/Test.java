package com.zzzzzz.test;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.validator.routines.DateValidator;
import org.apache.commons.validator.routines.LongValidator;

import com.zzzzzz.plugins.jdbctemplate.DaoUtils;

public class Test {
	public static void main(String[] args){
		Long id = LongValidator.getInstance().validate(null);
		System.out.println(id);
		
		Date addDt = DateValidator.getInstance().validate("20130101", "yyyyMMdd");
		System.out.println(addDt);
		
		Map<String, Object> map = new HashMap<String, Object>();
		String[] strs = new String[]{};
		map.put("strs", strs);
		
		Object oo = map.get("strs");
		
		
		
		if(DaoUtils.isNotNull(oo)){
			System.out.println(oo);
		}
		
		
		map.put("1", "1.1");
		map.put("2", "2.2");
		System.out.println(map);
		
	}
}
