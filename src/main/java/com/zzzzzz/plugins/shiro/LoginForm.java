package com.zzzzzz.plugins.shiro;

import org.hibernate.validator.constraints.NotBlank;

public class LoginForm {
	@NotBlank
	private String username;
	@NotBlank
	private String password;
	private boolean rememberMe;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isRememberMe() {
		return rememberMe;
	}
	public void setRememberMe(boolean rememberMe) {
		this.rememberMe = rememberMe;
	}
	
}
