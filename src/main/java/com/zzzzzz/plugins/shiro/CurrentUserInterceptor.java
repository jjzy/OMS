package com.zzzzzz.plugins.shiro;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.zzzzzz.sys.menu.ZTree;
import com.zzzzzz.sys.role.menu.RoleMenuDao;
import com.zzzzzz.sys.role.menu.RoleMenuService;
import com.zzzzzz.sys.user.role.UserRoleDao;

public class CurrentUserInterceptor extends HandlerInterceptorAdapter {

	@Resource
	public RoleMenuService roleMenuService;
	@Resource
	public RoleMenuDao roleMenuDao;
	@Resource
	private UserRoleDao userRoleDao;

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
		try {
			if (SecurityUtils.getSubject().isAuthenticated()) {
				I i = ShiroUtils.findUser();
				request.setAttribute("i", i);

				if (ShiroUtils.isSignedIn() && i.getPlatformId() == null) {
					List<Map<String, Object>> list = userRoleDao.findPlatformListByUserId(ShiroUtils.findUserId());
					i.setPlatformId(Long.valueOf(list.get(0).get("id").toString()));
					i.setPlatformName(String.valueOf(list.get(0).get("name")));
				}

				String uri = request.getRequestURI();
				// List<Map<String, Object>> pemisiList = roleMenuDao.findListForPemisi(i.getId(), i.getPlatformId());
				// if(!pemisiList.contains(uri)){
				// 没有权限
				// }
			}

		} catch (Exception e) {
			request.setAttribute("i", null);
			e.printStackTrace();
		}
	}
}
