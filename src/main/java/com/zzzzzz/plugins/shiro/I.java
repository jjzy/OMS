package com.zzzzzz.plugins.shiro;

import java.io.Serializable;

import com.zzzzzz.sys.user.User;

public class I implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String name;
	private String account;
	private Long platformId;
	private String platformName;

	

	public I(User user) {
		this.id = user.getId();
		this.name = user.getName();
		this.account = user.getAccount();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getPlatformId() {
		return platformId;
	}

	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

}
