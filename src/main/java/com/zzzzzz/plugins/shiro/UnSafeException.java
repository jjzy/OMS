package com.zzzzzz.plugins.shiro;

public class UnSafeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public UnSafeException() {
		super();
	}

	public UnSafeException(String message) {
		super(message);
	}

	public UnSafeException(Throwable cause) {
		super(cause);
	}

	public UnSafeException(String message, Throwable cause) {
		super(message, cause);
	}
}
