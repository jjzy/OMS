package com.zzzzzz.plugins.shiro;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import com.zzzzzz.oms.GCS;

public class ShiroUtils {
	private final static Long SU_ID = 1l;//超级管理员ID
	
	public static Boolean isSignedIn() {
		try {
			Subject currentUser = SecurityUtils.getSubject();
			return currentUser.isAuthenticated();
		} catch (Exception e) {
			return false;
		}
	}
	
	public static I findUser() {
		Object obj = SecurityUtils.getSubject().getPrincipal();
		return (I) obj;
	}
	
	public static Long findUserId() {
		return findUser().getId();
	}
	
	public static Long findPlatformId() {
		Long plarformId = findUser().getPlatformId();
		if(plarformId == null){
			throw new UnSafeException();
		}
		return plarformId;
	}
	
	public static Boolean ifSuperAdministrator(){
		I i = findUser();
		return i.getId().longValue() == SU_ID.longValue();
	}
	
	public static Boolean ifSuperAdministrator(Long userId){
		return userId.longValue() == SU_ID.longValue();
	}
}
