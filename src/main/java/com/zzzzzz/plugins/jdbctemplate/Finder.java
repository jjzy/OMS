package com.zzzzzz.plugins.jdbctemplate;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Finder {
	private Map<String,Object> params = null;
    private StringBuilder sql = new StringBuilder();
    private String limitSql;
    
    public Finder(String sql){
    	this.sql.append(sql);
    }
    
	public Finder append(String sql) {
		this.sql.append(" ").append(sql);
		return this;
	}

	public Finder setParam(String param, Object value) {
		if(params==null){
			params=new HashMap<String,Object>();
		}
		params.put(param, value);
		return this;
	}
	
	public Finder limit(Integer start, Integer offset){
		setParam("start", start);
		setParam("offset", offset);
		
		this.limitSql = " limit :start,:offset";
    	return this;
    }

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	public String getSql() {
		if(sql==null)
			return null;
		
		if(StringUtils.isNotBlank(limitSql))
			this.append(limitSql);
		
		return sql.toString();
	}

	public void setSql(String sql) {
		this.sql = new StringBuilder(sql);
		this.limitSql = null;
	}
	
}
