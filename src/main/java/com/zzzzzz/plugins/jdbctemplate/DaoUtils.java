package com.zzzzzz.plugins.jdbctemplate;

import java.util.List;

import org.apache.commons.lang.StringUtils;

public class DaoUtils {
	public static Boolean isNull(Object param){
		if(param==null||StringUtils.isBlank(param.toString())){
			return true;
		}else if(param instanceof Object[]){
			Object[] paramArray = (Object[])param;
			if(paramArray.length==0){
				return true;
			}
		}else if(param instanceof List){
			List<?> list = (List<?>)param;
			if(list.size()==0){
				return true;
			}
		}
		
		return false;
	}
	
	public static Boolean isNotNull(Object param){
		return !isNull(param);
	}
}
