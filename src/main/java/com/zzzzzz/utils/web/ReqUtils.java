package com.zzzzzz.utils.web;

import javax.servlet.http.HttpServletRequest;

public class ReqUtils {
	public static String getServerAndContextPath(HttpServletRequest request){
		return new StringBuilder("http://").append(request.getServerName()).append(":").append(request.getServerPort()).append(request.getContextPath()).toString();
	}
}
