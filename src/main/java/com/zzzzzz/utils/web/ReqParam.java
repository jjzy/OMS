package com.zzzzzz.utils.web;

import javax.servlet.ServletRequest;

import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;

public class ReqParam {
	
	public static String getString(ServletRequest request, String name) throws ServletRequestBindingException {
		return ServletRequestUtils.getStringParameter(request, name);
	}
	public static String getString(ServletRequest request, String name, String defaultVal) {
		return ServletRequestUtils.getStringParameter(request, name, defaultVal);
	}
	
	public static Integer getInt(ServletRequest request, String name) throws ServletRequestBindingException {
		return ServletRequestUtils.getIntParameter(request, name);
	}
	public static int getInt(ServletRequest request, String name, int defaultVal) {
		return ServletRequestUtils.getIntParameter(request, name, defaultVal);
	}
	
	public static Long getLong(ServletRequest request, String name) throws ServletRequestBindingException {
		return ServletRequestUtils.getLongParameter(request, name);
	}
	public static long getString(ServletRequest request, String name, long defaultVal) {
		return ServletRequestUtils.getLongParameter(request, name, defaultVal);
	}
	
	public static Double getDouble(ServletRequest request, String name) throws ServletRequestBindingException {
		return ServletRequestUtils.getDoubleParameter(request, name);
	}
	public static double getDouble(ServletRequest request, String name, double defaultVal) {
		return ServletRequestUtils.getDoubleParameter(request, name, defaultVal);
	}
	
	public static Boolean getBoolean(ServletRequest request, String name) throws ServletRequestBindingException {
		return ServletRequestUtils.getBooleanParameter(request, name);
	}
	public static boolean getBoolean(ServletRequest request, String name, boolean defaultVal) {
		return ServletRequestUtils.getBooleanParameter(request, name, defaultVal);
	}
}
