package com.zzzzzz.utils.page;


public class Page {
	private Integer start = 1;
	private Integer offset = -1;
	private String url;
	private String urlEnd;
	
	public static final Integer DEFAULT_OFFSET = 20;
	
	//-- 返回结果 --//
	private Integer total = -1;
	
	public Page(Integer start, Integer offset){
		this.start = start;
		this.offset = offset;
	}
	
	public Page(Integer start, Integer offset, String url){
		this.start = start;
		this.offset = offset;
		this.url = url;
	}
	
	public Page(Integer start,Integer offset, String url, String urlEnd){
		this.start = start;
		this.offset = offset;
		this.url = url;
		this.urlEnd = urlEnd;
	}
	
	public Page(Integer start, Integer offset, Integer total, String url){
		this.start = start;
		this.offset = offset;
		this.url = url;
		this.total = total;
	}
	
	//-- 分页参数访问函数 --//
	/**
	 * 获得当前页的页号,序号从1开始,默认为1.
	 */
	public Integer getStart() {
		return start;
	}

	/**
	 * 设置当前页的页号,序号从1开始,低于1时自动调整为1.
	 */
	public void setStart(final Integer start) {
		this.start = start;

		if (start < 1) {
			this.start = 1;
		}
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	/**
	 * 获得每页的记录数量, 默认为-1.
	 */
	public Integer getOffset() {
		return offset;
	}

	/**
	 * 设置每页的记录数量.
	 */
	public void setOffset(final Integer offset) {
		this.offset = offset;
	}
	
	//-- 访问查询结果函数 --//
	/**
	 * 获得总记录数, 默认值为-1.
	 */
	public Integer getTotal() {
		return total;
	}

	/**
	 * 设置总记录数.
	 */
	public void setTotal(final Integer total) {
		this.total = total;
	}

	/**
	 * 根据pageSize与totalCount计算总页数, 默认值为-1.
	 */
	public Integer getTotalPages() {
		if (total < 0) {
			return -1;
		}

		Integer count = total / offset;
		if (total % offset > 0) {
			count++;
		}
		return count;
	}

	/**
	 * 是否还有下一页.
	 */
	public boolean hasNext() {
		return start + offset <= total;
	}

	/**
	 * 取得下页的页号, 序号从1开始.
	 * 当前页为尾页时仍返回尾页序号.
	 */
	public Integer getNextStart() {
		if (hasNext()) {
			return start + offset;
		} else {
			return start;
		}
	}

	/**
	 * 是否还有上一页.
	 */
	public boolean hasPre() {
		return start+1 > offset;
	}

	/**
	 * 取得上页的页号, 序号从1开始.
	 * 当前页为首页时返回首页序号.
	 */
	public Integer getPreStart() {
		if (hasPre()) {
			return start - offset;
		} else {
			return 1;
		}
	}

	public String getUrlEnd() {
		return urlEnd == null ? "" : urlEnd;
	}

	public void setUrlEnd(String urlEnd) {
		this.urlEnd = urlEnd;
	}
}
