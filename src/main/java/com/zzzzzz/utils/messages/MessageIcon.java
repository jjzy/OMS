package com.zzzzzz.utils.messages;

public interface MessageIcon {
	String OK = "<i class='icon-ok'></i>&nbsp;";
	String WARNING = "<i class='icon-warning-sign'></i>&nbsp;";
}
