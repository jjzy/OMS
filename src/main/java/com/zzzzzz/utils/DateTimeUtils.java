package com.zzzzzz.utils;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Seconds;
import org.joda.time.Years;

public class DateTimeUtils {
	public final static String FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
	public final static String FORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	public final static String FORMAT_HH_MM_SS = "HH:mm:ss";
	public final static String FORMAT_YYYYMMDD = "yyyyMMdd";
	public final static String FORMAT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	public final static String FORMAT_HHMMSS = "HHmmss";
	public final static String FORMAT_MMMDDYYYY_AT_HHMM_A = "MMM dd, yyyy 'at' hh:mm a";
	
	public static Date str2Date(String dateStr, String dateFormat) throws ParseException {
		return DateUtils.parseDate(dateStr, dateFormat);
	}
	
	public static Date str2Date(String dateStr, String dateFormat, Date defaultValue) throws ParseException {
		if (StringUtils.isBlank(dateStr))
			return defaultValue;
		
		return DateUtils.parseDate(dateStr, dateFormat);
	}
	
	public static String date2Str(Date date, String dateFormat) {
		return DateFormatUtils.format(date, dateFormat);
	}
	
	public static String date2Str(Date date, String dateFormat, String defaultValue) {
		if(date == null)
			return defaultValue;
		
		return DateFormatUtils.format(date, dateFormat);
	}
	
	public static long getTimeStamp(){
		return new Date().getTime();
	}
	
	public static String getTimeDifferent(Date d1, Date d2) {
		String timeDiff = "";

		DateTime dt1 = new DateTime(d1);
		DateTime dt2 = new DateTime(d2);
		
		Years yearDiff = Years.yearsBetween(dt1, dt2);
		int years = yearDiff.getYears();
		if(years == 0) {
			Months monthDiff = Months.monthsBetween(dt1, dt2);
			int months = monthDiff.getMonths();
			if(months == 0) {
				Days dayDiff = Days.daysBetween(dt1, dt2);
				int days = dayDiff.getDays();
				if(days == 0) {
					Hours hourDiff = Hours.hoursBetween(dt1, dt2);
					int hours = hourDiff.getHours();
					if(hours == 0) {
						Minutes minDiff = Minutes.minutesBetween(dt1, dt2);
						int minutes = minDiff.getMinutes(); 
						if(minutes == 0) {
							Seconds secDiff = Seconds.secondsBetween(dt1, dt2);
							int seconds = secDiff.getSeconds();
							if(seconds == 0) {
								timeDiff = "0 second ago";
							}else {
								if(seconds == 1) {
									timeDiff = "1 second ago";
								}else {
									timeDiff = seconds + " seconds ago";
								}
							}
						}else {
							if(minutes == 1) {
								timeDiff = "1 minute ago";
							}else {
								timeDiff = minutes + " minutes ago";
							}
						}
					}else {
						if(hours == 1) {
							timeDiff = "1 hour ago";
						}else {
							timeDiff = hours + " hours ago";
						}
					}
				}else {
					if(days == 1) {
						timeDiff = "1 day ago";
					}else {
						timeDiff = days + " days ago";
					}
				}
			}else {
				if(months == 1) {
					timeDiff = "1 month ago";
				}else {
					timeDiff = months + " months ago";
				}
			}
		}else {
			if(years == 1) {
				timeDiff = "1 year ago";
			}else {
				timeDiff = years + " years ago";
			}
		}
		
		return timeDiff;
	}
	
}
