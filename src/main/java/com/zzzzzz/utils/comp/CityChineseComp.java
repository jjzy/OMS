package com.zzzzzz.utils.comp;

import java.text.Collator;
import java.util.Comparator;

import com.zzzzzz.sys.city.City;

public class CityChineseComp implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		City c1=(City)o1;
		City c2=(City)o2;
		Collator collator=Collator.getInstance(java.util.Locale.CHINA);
		if(collator.compare(c1.getName(), c2.getName())<0){
			return -1;
		}else if(collator.compare(c1.getName(), c2.getName())>0){
			return 1;
		}else{
		return 0;
		}
	}

}
