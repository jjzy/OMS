package com.zzzzzz.utils.comp;

import java.text.Collator;
import java.util.Comparator;

import com.zzzzzz.oms.translocation.TransLocation;


public class TranslocationChineseComp implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		TransLocation c1=(TransLocation)o1;
		TransLocation c2=(TransLocation)o2;
		Collator collator=Collator.getInstance(java.util.Locale.CHINA);
		if(collator.compare(c1.getName(), c2.getName())<0){
			return -1;
		}else if(collator.compare(c1.getName(), c2.getName())>0){
			return 1;
		}else{
		return 0;
		}
	}

}
