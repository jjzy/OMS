package com.zzzzzz.utils.comp;

import java.text.Collator;
import java.util.Comparator;

import com.zzzzzz.sys.client.Client;

public class ClientChineseComp implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		Client c1=(Client)o1;
		Client c2=(Client)o2;
		Collator collator=Collator.getInstance(java.util.Locale.CHINA);
		if(collator.compare(c1.getShortName(), c2.getShortName())<0){
			return -1;
		}else if(collator.compare(c1.getShortName(), c2.getShortName())>0){
			return 1;
		}else{
		return 0;
		}
	}

}
