package ${packageName};

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;
import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author ${author}
 * @version ${version}
 */
public class ${ClassName} implements Serializable {
	
	<#list daoConfig.tableColumns as row>
	<#if row.excelField??>@ExcelField(title = "${row.label}", align = ${row.excelField.align}, sort = ${row.excelField.sort}<#if row.excelField.dictType??>, dictType = ${row.excelField.dictType}</#if>)</#if>
	<#if row.validator??>${row.validator}</#if>
  	private ${row.javaType} ${row.name};
 	</#list>
	
	<#list daoConfig.tableColumns as row>
  	public ${row.javaType} get${row.name?cap_first}(){
  		return ${row.name};
  	}
  	public void set${row.name?cap_first}(${row.javaType} ${row.name}) {
		this.${row.name} = ${row.name};
	}
 	</#list>
 	
 	public static ${ClassName} newTest() {
		${ClassName} ${className} = new ${ClassName}();
		<#list daoConfig.tableColumns as row><#t>
		<#if row.excelField??>//${className}.set${row.name?cap_first}("");</#if>
	 	</#list>
		return ${className};
	}
}


