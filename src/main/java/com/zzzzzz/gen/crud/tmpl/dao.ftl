package ${packageName};

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import javax.annotation.Resource;
import org.springframework.stereotype.Repository;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.utils.page.Page;

import com.zzzzzz.plugins.shiro.I;

/**
 * @author ${author}
 * @version ${version}
 */
@Repository
public class ${ClassName}Dao {
	
	@Resource
	private BaseDao baseDao;
			
	<#list daoConfig.daoFunctions as daoFunction>
	<#if daoFunction.type=="default">
		<#switch daoFunction.name>
		  <#case "add">
	private final static String sql_add = "insert into ${daoConfig.tableName}(<#assign firstCol=true /><#list daoConfig.tableColumns as row><#if row.dbField.insertable??><#if !firstCol>, </#if><#assign firstCol=false />${row.name}</#if></#list>) values(<#assign firstCol=true /><#list daoConfig.tableColumns as row><#if row.dbField.insertable??><#if !firstCol>, </#if><#assign firstCol=false />:${row.name}</#if></#list>)";
		     <#break>
		  <#case "delById">
	private final static String sql_delById = "delete from ${daoConfig.tableName} where ${daoConfig.tablePk} = :${daoConfig.tablePk}";
		     <#break>
		  <#case "delByIds">
	private final static String sql_delByIds = "delete from ${daoConfig.tableName} where ${daoConfig.tablePk} in (:${daoConfig.tablePk}s)";
		     <#break>
		  <#case "updStById">
	private final static String sql_updStById = "update ${daoConfig.tableName} set st = :st, updDt = :updDt, updBy = :updBy where ${daoConfig.tablePk} = :${daoConfig.tablePk}";
		     <#break>
		  <#case "updStByIds">
	private final static String sql_updStByIds = "update ${daoConfig.tableName} set st = :st, updDt = :updDt, updBy = :updBy where ${daoConfig.tablePk} in (:${daoConfig.tablePk}s)";
		     <#break>
		  <#case "updById">
	private final static String sql_upd = "update ${daoConfig.tableName} set <#assign firstCol=true /><#list daoConfig.tableColumns as row><#if row.dbField.updatable??><#if !firstCol>, </#if><#assign firstCol=false />${row.name}=:${row.name}</#if></#list> where ${daoConfig.tablePk} = :${daoConfig.tablePk}";
		     <#break>
		  <#case "findById">
	private final static String sql_findById = "select <#assign firstCol=true /><#list daoConfig.tableColumns as row><#if row.dbField.selectable??><#if !firstCol>, </#if><#assign firstCol=false />${row.name}</#if></#list> from ${daoConfig.tableName} where ${daoConfig.tablePk} = :${daoConfig.tablePk}";
		     <#break>
		  <#case "findList">
	private final static String sql_findList = "select <#assign firstCol=true /><#list daoConfig.tableColumns as row><#if row.dbField.selectable??><#if !firstCol>, </#if><#assign firstCol=false />${row.name}</#if></#list> from ${daoConfig.tableName}";
	private final static String sql_findCount = "select count(1) as ct from ${daoConfig.tableName}";
		     <#break>
		  <#default>
	No daoFunctions!!!
		</#switch>
	</#if>
	</#list>
	
	<#list daoConfig.daoFunctions as daoFunction>
	<#if daoFunction.type=="default">
		<#switch daoFunction.name>
		  <#case "add">
	public Long add(${ClassName} ${className}){
		return baseDao.updateGetLongKey(sql_add, ${className});
	}
		     <#break>
		  <#case "delById">
	public int delById(Long ${daoConfig.tablePk}){
		Finder finder = new Finder(sql_delById).setParam("${daoConfig.tablePk}", ${daoConfig.tablePk});
		return baseDao.update(finder);
	}
		     <#break>
		  <#case "delByIds">
	public int delByIds(List<Long> ${daoConfig.tablePk}s){
		Finder finder = new Finder(sql_delByIds).setParam("${daoConfig.tablePk}s", ${daoConfig.tablePk}s);
		return baseDao.update(finder);
	}
		     <#break>
		  <#case "updStById">
	public int updStById(Long ${daoConfig.tablePk}, Integer st, I i){
		Finder finder = new Finder(sql_updStById).setParam("${daoConfig.tablePk}", ${daoConfig.tablePk}).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}	  
		     <#break>
		  <#case "updStByIds">
	public int updStByIds(List<Long> ${daoConfig.tablePk}s, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("${daoConfig.tablePk}s", ${daoConfig.tablePk}s).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}		  
		     <#break>
		  <#case "updById">
	public int updById(${ClassName} ${className}){
		return baseDao.update(sql_upd, ${className});
	}
		     <#break>   
		  <#case "findById">
	public ${ClassName} findById(Long ${daoConfig.tablePk}){
		Finder finder = new Finder(sql_findById).setParam("${daoConfig.tablePk}", ${daoConfig.tablePk});
		return baseDao.findOne(finder, ${ClassName}.class);
	}
		     <#break>
		  <#default>
	No daoFunctions!!!
		</#switch> 
	</#if>
	</#list>
	
	<#list daoConfig.daoFunctions as daoFunction>
	<#if daoFunction.type=="baseQuery">
		<#if daoFunction.comment??>
	/**
	 * ${daoFunction.comment}
	 */
		</#if>	
	public ${daoFunction.return} ${daoFunction.name}(<#if daoFunction.pageable??&&daoFunction.hasParams??>Page page, Map<String, Object> ffMap<#elseif daoFunction.pageable??>Page page<#elseif daoFunction.hasParams??>Map<String, Object> ffMap</#if>){
		Finder finder = new Finder("");
			<#assign dynamicSqlFlag=false />
			<#list daoFunction.sql as sqlPart>
				<#if sqlPart == "#dynamicSqlEnd">
					<#assign dynamicSqlFlag=false />
				</#if>
				<#if dynamicSqlFlag>
		if(<#rt>
					<#list sqlPart?split(":") as s>
						<#if s_index gt 0>
							<#assign dynamicField=s?split(" ")[0]?replace(")", "") />
							DaoUtils.isNotNull(ffMap.get("${dynamicField}"))<#t>
							<#if s_has_next> && </#if><#t>
						</#if><#t>
					</#list>){<#lt>
		finder.append(" ${sqlPart}")<#rt>
					<#list sqlPart?split(":") as s>
						<#if s_index gt 0>
							<#assign dynamicField=s?split(" ")[0]?replace(")", "") />
							<#if sqlPart?contains("like")>
								.setParam("${dynamicField}", "%" + ffMap.get("${dynamicField}") + "%")<#t>
							<#else>
								.setParam("${dynamicField}", ffMap.get("${dynamicField}"))<#t>
							</#if>
						</#if>
					</#list>
					;<#lt>
		}
				<#else>
					<#if !sqlPart?starts_with("#")>
		finder.append(" ${sqlPart}")<#rt>
						<#list sqlPart?split(":") as s>
							<#if s_index gt 0>
								<#assign dynamicField=s?split(" ")[0]?replace(")", "") />
								<#if sqlPart?contains("like")>
									.setParam("${dynamicField}", "%" + ffMap.get("${dynamicField}") + "%")<#t>
								<#else>
									.setParam("${dynamicField}", ffMap.get("${dynamicField}"))<#t>
								</#if>
							</#if>
						</#list>
						;<#lt>
					</#if>
				</#if>
				<#if sqlPart == "#dynamicSqlStart">
					<#assign dynamicSqlFlag=true />
				</#if>
			</#list>
				
			<#if daoFunction.pageable??>		
		Integer total = baseDao.findInt(finder);
		page.setTotal(total);
		
		String sql = finder.getSql().replace("count(1) as tt", "${daoFunction.sql_select}");
		finder.setSql(sql);
		finder.limit(page.getStart(), page.getOffset());
			</#if>
		${daoFunction.return} list = baseDao.${daoFunction.jdbcFunc}(finder<#if daoFunction.jdbcFuncClass??>, ${daoFunction.jdbcFuncClass}.class</#if>);
				
		return list;
	}
	</#if>
	</#list>
}
