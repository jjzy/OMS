package ${packageName};

import java.util.List;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.utils.page.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzzzzz.oms.GCS;
import com.google.common.collect.Lists;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.plugins.poi.ExportExcel;
import com.zzzzzz.plugins.poi.ImportExcel;

/**
 * @author ${author}
 * @version ${version}
 */
@Controller
public class ${ClassName}Web {

	@Resource
	public ${ClassName}Service ${className}Service;
	@Resource
	public ${ClassName}Dao ${className}Dao;

	@RequestMapping(value = "/${className}/list", method = RequestMethod.GET)
	public String list() {
		return "${basePath}";
	}

	<#list daoConfig.daoFunctions as daoFunction>
	<#if daoFunction.type=="default">
		<#switch daoFunction.name>
		  <#case "add">
	@RequestMapping(value = "/${className}/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid ${ClassName} ${className}, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}

		${className}Service.save(${className}, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
		     <#break>
		  <#case "delById">
	@RequestMapping(value = "/${className}/delById", method = RequestMethod.POST)
	@ResponseBody
	public BaseData delById(@RequestParam(value = "id", required = true) Long id) throws Exception {
		${className}Service.delById(id);
		return new BaseData("0");
	}
	
		     <#break>
		  <#case "delByIds">
	@RequestMapping(value = "/${className}/delByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData delByIds(@RequestParam(value = "ids", required = true) List<Long> ids) throws Exception {
		${className}Dao.delByIds(ids);
		return new BaseData("0");
	}
	
		     <#break>
		  <#case "updStById">
	@RequestMapping(value = "/${className}/updStById", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStById(@RequestParam(value = "id", required = true) Long id, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		${className}Dao.updStById(id, st, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
		     <#break>
		  <#case "updStByIds">
	@RequestMapping(value = "/${className}/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		${className}Dao.updStByIds(ids, st, ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	
		     <#break>
		  <#case "updById">
	
		     <#break>   
		  <#case "findById">
	@RequestMapping(value = "/${className}/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		${ClassName} ${className} = ${className}Dao.findById(id);
		return new BaseData(${className}, null);
	}
	
		     <#break>
		  <#case "findList">
	
		     <#break>   
		  <#default>
	No daoFunctions!!!
		</#switch> 
	</#if>
	</#list>

	<#list daoConfig.daoFunctions as daoFunction>
	<#if daoFunction.type=="baseQuery">
	@RequestMapping(value = "/${className}/${daoFunction.name}", method = RequestMethod.POST)
	@ResponseBody
	public BaseData ${daoFunction.name}(<#if daoFunction.hasParams??>@RequestBody BaseQueryForm baseQueryForm</#if>) {
		<#if daoFunction.pageable??>
			<#if !daoFunction.hasParams??>
		BaseQueryForm baseQueryForm = new BaseQueryForm();
			</#if>
		Page page = baseQueryForm.getPage();
		${daoFunction.return} list = ${className}Dao.${daoFunction.name}(page<#if daoFunction.hasParams??>, baseQueryForm.getFfMap()</#if>);
		return new BaseData(list, page.getTotal());
		<#else>
		${daoFunction.return} list = ${className}Dao.${daoFunction.name}(<#if daoFunction.hasParams??>baseQueryForm.getFfMap()</#if>);
		return new BaseData(list, null);
		</#if>
	}
	
	</#if>
	</#list>
	
	@RequestMapping(value = "/${className}/import", method=RequestMethod.POST)
    public String importExcel(@RequestParam MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			ImportExcel importExcel = new ImportExcel(file, 1, 0);
			List<${ClassName}> list = importExcel.getDataList(${ClassName}.class);
			
			BaseData baseData = ${className}Service.batchAdd(list, ShiroUtils.findUser());
			redirectAttributes.addFlashAttribute("msg", baseData.getErrMsg());
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "导入数据保存失败。");
			logger.warn("importExcel", e);
		}
		return "redirect:/msg";
    }
    
    @RequestMapping(value = "${className}/import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "${daoConfig.excelName}导入模板.xlsx";
    		List<${ClassName}> list = Lists.newArrayList();
    		list.add(${ClassName}.newTest());
    		new ExportExcel("${daoConfig.excelName}导入模板", ${ClassName}.class, 2).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "${daoConfig.excelName}导入模板下载失败。");
			logger.warn("importFileTemplate", e);
		}
		return "redirect:/msg";
    }
    
    private static final Logger logger = LoggerFactory.getLogger(${ClassName}Web.class);
}
