CREATE TABLE ${daoConfig.tableName} (
	<#list daoConfig.tableColumns as row>
	${row.name} ${row.dbField.sqlType}<#if !row.dbField.nullable??> not null</#if><#if row.dbField.default??> default ${row.dbField.default}</#if><#if row.dbField.comment??> comment '${row.dbField.comment}'</#if><#if row.dbField.ai??> AUTO_INCREMENT</#if>,
	</#list>
	PRIMARY KEY (${daoConfig.tablePk})<#if daoConfig.tableAttr??>, ${daoConfig.tableAttr}</#if>
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='${daoConfig.tableComment}';