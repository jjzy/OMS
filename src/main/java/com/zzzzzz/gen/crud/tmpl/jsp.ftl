<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>${functionName}</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="${className}Ctrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span> 查询
			</button>
			<shiro:hasPermission name="sys:${className}:add">
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('add')">
				<span class="glyphicon glyphicon-plus"></span> 增加
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:${className}:add">
			<button type="button" class="btn btn-default" ng-click="onOpenDelFormModal()">
				<span class="glyphicon glyphicon-minus"></span> 删除
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:${className}:add">
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('upd')">
				<span class="glyphicon glyphicon-pencil"></span> 编辑
			</button>
			</shiro:hasPermission>
		</div>
	</div>
</body>

<#if viewConfig.queryForm??>
<div id="queryFormModal" class="modal" tabindex="-1" data-width="500px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="${viewConfig.queryForm.formName}Form" class="form-inline" role="form">
					<#list viewConfig.queryForm.fields as field>
					<div class="form-group">
						<label for="${field.name}" class="sr-only">${field.label}</label> 
						<#if field.tag=="select">
						<select name="${field.name}"<#if field.attr??> ${field.attr}</#if><#if field.required??> ng-required="true"</#if> ng-model="${viewConfig.queryForm.formName}.${field.name}" class="form-control"<#if field.placeholder??> data-placeholder="${field.placeholder}"</#if>>
							<option value=""></option>
							${field.selectOption}
						</select>
						<#elseif field.tag=="input">
						<input type="${field.tagType}" name="${field.name}" ng-model="${viewConfig.queryForm.formName}.${field.name}"<#if field.attr??> ${field.attr}</#if><#if field.required??> required</#if> class="form-control"<#if field.placeholder??> placeholder="${field.placeholder}"</#if>>		
						</#if>
					</div>	
					</#list>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>
</#if>

<div id="editFormModal" class="modal" tabindex="-1" data-width="500px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">编辑</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" class="form-horizontal" role="form" novalidate>
					<#list daoConfig.tableColumns as field>
					<#if field.formField??>
					<div class="form-group<#if field.formField.tagType??&&field.formField.tagType=="hidden"> hidden</#if>">
						<label for="${field.name}" class="col-sm-4 control-label">${field.label}</label> 
						<div class="col-sm-5">
						<#if field.formField.tag=="select">
							<select name="${field.name}"<#if field.attr??> ${field.formField.attr}</#if><#if field.formField.required??> ng-required="true"</#if> ng-model="editForm.${field.name}" class="form-control"<#if field.formField.placeholder??> data-placeholder="${field.formField.placeholder}"</#if>>
								<option value=""></option>
								${field.formField.selectOption}
							</select>
						<#elseif field.formField.tag=="input">
							<input type="${field.formField.tagType}" name="${field.name}" ng-model="editForm.${field.name}"<#if field.formField.attr??> ${field.attr}</#if><#if field.formField.required??> required</#if> class="form-control"<#if field.formField.placeholder??> placeholder="${field.formField.placeholder}"</#if>>		
						</#if>
						</div>
					</div>
					</#if>
					</#list>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${r"${ctx}"}/res_${projectName}/${basePath}.js"></script>
</html>
