package com.zzzzzz.gen.crud.model;

import java.util.List;

public class GenForm {
	private String tableName;
	private List<GenField> entityFieldList;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<GenField> getEntityFieldList() {
		return entityFieldList;
	}

	public void setEntityFieldList(List<GenField> entityFieldList) {
		this.entityFieldList = entityFieldList;
	}
}
