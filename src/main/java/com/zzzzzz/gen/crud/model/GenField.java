package com.zzzzzz.gen.crud.model;

import org.apache.commons.lang3.StringUtils;

public class GenField {

	private String name;
	private String javaType;
	private String sqlType;

	public GenField(String name, String javaType, String dbType) {
		this.name = name;
		this.javaType = javaType;
		this.sqlType = javaType2SqlType(javaType, dbType);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJavaType() {
		return javaType;
	}

	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}

	public String getSqlType() {
		return sqlType;
	}

	public void setSqlType(String sqlType) {
		this.sqlType = sqlType;
	}

	private String javaType2SqlType(String javaType, String dbType) throws RuntimeException {
		if (StringUtils.equalsIgnoreCase(dbType, "mysql")) {
			if (StringUtils.equalsIgnoreCase(javaType, "Long")) {
				return "bigint";
			} else if (StringUtils.equalsIgnoreCase(javaType, "Integer")) {
				return "int";
			} else if (StringUtils.equalsIgnoreCase(javaType, "Double")) {
				return "double";
			} else if (StringUtils.equalsIgnoreCase(javaType, "String")) {
				return "varchar";
			} else if (StringUtils.equalsIgnoreCase(javaType, "Date")) {
				return "datetime";
			} else {
				throw new RuntimeException("invalid javaType");
			}
		} else {
			throw new RuntimeException("invalid dbType");
		}

	}
}
