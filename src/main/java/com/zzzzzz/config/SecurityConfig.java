package com.zzzzzz.config;


import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.shiro.ShiroDbRealm;

@Configuration
public class SecurityConfig {
//	public static void main(String[] args){
//		DefaultPasswordService passwordService = new DefaultPasswordService();
//		DefaultHashService hashService = new DefaultHashService();
//        hashService.setHashAlgorithmName("SHA-512");
//        hashService.setHashIterations(1024);
//        hashService.setGeneratePublicSalt(true);
//        passwordService.setHashService(hashService);
//        System.out.println(passwordService.encryptPassword("111111"));
//	}
	
	@Bean(name = "passwordService")
	public DefaultPasswordService configPasswordService() {
		DefaultPasswordService passwordService = new DefaultPasswordService();
		DefaultHashService hashService = new DefaultHashService();
        hashService.setHashAlgorithmName("SHA-512");
        hashService.setHashIterations(1024);
        hashService.setGeneratePublicSalt(true);
        passwordService.setHashService(hashService);
		return passwordService;
	}
	
	@Bean(name = "passwordMatcher")
	public PasswordMatcher configPasswordMatcher(){
		PasswordMatcher passwordMatcher = new PasswordMatcher();
		passwordMatcher.setPasswordService(configPasswordService());
		return passwordMatcher;
	}
	
	@Bean
	public BaseDao baseDao() {
		BaseDao baseDao = new BaseDao();
		return baseDao;
	}
	
	@Bean
	public Realm configureRealm() {
		return new ShiroDbRealm();
	}
	
	@Bean(name = "securityManager")
	public DefaultWebSecurityManager configSecurityManager() {
		DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
		securityManager.setRealm(configureRealm());
		//securityManager.setCacheManager(configEhCacheManager());
		return securityManager;
	}
	
	/*private EhCacheManager configEhCacheManager() {
		EhCacheManager ehCacheManager = new EhCacheManager();
		ehCacheManager.setCacheManagerConfigFile("classpath:ehcache/ehcache-shiro.xml");
		return ehCacheManager;
	}*/
	
	
	@Bean(name = "shiroFilter")
	public ShiroFilterFactoryBean configShiroFilterFactoryBean() {
		ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
		shiroFilterFactoryBean.setSecurityManager(configSecurityManager());
		shiroFilterFactoryBean.setLoginUrl("/login");
		shiroFilterFactoryBean.setUnauthorizedUrl("/unauthorized");
		shiroFilterFactoryBean.setSuccessUrl("/");
		shiroFilterFactoryBean.setFilterChainDefinitionMap(getFilterChainDefinitionMap());
		return shiroFilterFactoryBean;
	}
	
	private Map<String, String> getFilterChainDefinitionMap(){
		Map<String, String> filterChainDefinitionMap = new HashMap<String, String>();
		filterChainDefinitionMap.put("/login", "authc");
		filterChainDefinitionMap.put("/logout", "logout");
		filterChainDefinitionMap.put("/res/**", "anon");
		
		filterChainDefinitionMap.put("/favicon.ico", "anon");
		filterChainDefinitionMap.put("/*", "user");
		filterChainDefinitionMap.put("/*/*", "user");
		
		//filterChainDefinitionMap.put("/excel/import", "anon");
		
		return filterChainDefinitionMap;
	}
	
	@Bean(name = "lifecycleBeanPostProcessor")
	public LifecycleBeanPostProcessor configLifecycleBeanPostProcessor() {
		LifecycleBeanPostProcessor lifecycleBeanPostProcessor = new LifecycleBeanPostProcessor();
		return lifecycleBeanPostProcessor;
	}
	
}
