package com.zzzzzz.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages={"com.zzzzzz.plugins", "com.zzzzzz.sys", "com.zzzzzz.oms"})
@PropertySource(value={"classpath:config.properties","classpath:app.properties"})
public class ComponentConfig {
	
}
