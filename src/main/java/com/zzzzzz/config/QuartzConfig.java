package com.zzzzzz.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath*:/quartz-context.xml")
public class QuartzConfig {
	
}
