package com.zzzzzz.core.model;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.util.WebUtils;

import com.zzzzzz.utils.page.Page;

public class DataTablesBeanMap {
	private Map<String, Object> formFiledMap;
	private Page page;
	private DataTablesView dataTablesView;
	
	public DataTablesBeanMap(HttpServletRequest request){
		this.formFiledMap = WebUtils.getParametersStartingWith(request, "formField_");
		
		Integer iDisplayStart = ServletRequestUtils.getIntParameter(request, "iDisplayStart", 0);
		Integer iDisplayLength = ServletRequestUtils.getIntParameter(request, "iDisplayLength", 10);
		Integer sEcho = ServletRequestUtils.getIntParameter(request, "sEcho", 0);
		
		this.page =  new Page(iDisplayStart, iDisplayLength);
		formFiledMap.put("start", this.page.getStart());
		formFiledMap.put("end", this.page.getStart() + this.page.getOffset());
		
		dataTablesView = new DataTablesView();
		dataTablesView.setsEcho(sEcho);
	}
	
	public Map<String, Object> getFormFiledMap() {
		return formFiledMap;
	}
	public void setFormFiledMap(Map<String, Object> formFiledMap) {
		this.formFiledMap = formFiledMap;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	public DataTablesView getDataTablesView(Object obj) {
		this.dataTablesView.setiTotalDisplayRecords(page.getTotal()+"");
		this.dataTablesView.setiTotalRecords(page.getTotal()+"");
		this.dataTablesView.setAaData(obj);
		return dataTablesView;
	}
	public void setDataTablesView(DataTablesView dataTablesView) {
		this.dataTablesView = dataTablesView;
	}
}
