package com.zzzzzz.core.model;

public class BaseResult {
	private String resultCode;
	private String resultMsg;
	
	public BaseResult(String resultCode){
		this.resultCode = resultCode;
	}
	public BaseResult(String resultCode, String resultMsg){
		this.resultCode = resultCode;
		this.resultMsg = resultMsg;
	}
	
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
}
