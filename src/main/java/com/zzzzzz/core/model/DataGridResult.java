package com.zzzzzz.core.model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

public class DataGridResult {
	private Integer total;
	private Object result;
	private Map<String, String> formErrorMap;
	
	public DataGridResult(){}
	public DataGridResult(Object result, Integer total){
		this.result = result;
		this.total = total;
	}
	public DataGridResult(BindingResult bindingResult){
		this.formErrorMap = getMap(bindingResult);
	}
	
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}
	public Map<String, String> getFormErrorMap() {
		return formErrorMap;
	}
	public void setFormErrorMap(Map<String, String> formErrorMap) {
		this.formErrorMap = formErrorMap;
	}
	
	private Map<String, String> getMap(BindingResult bindingResult){
		Map<String, String> map = new LinkedHashMap<String, String>();
		if(bindingResult.hasErrors()){
			List<ObjectError> list = bindingResult.getAllErrors();
			for(ObjectError objectError : list){
				FieldError fieldError = (FieldError)objectError;
				map.put("#"+fieldError.getObjectName()+" #"+fieldError.getField(), objectError.getDefaultMessage());
			}
			return map;
		}else{
			return null;
		}
	}
}
