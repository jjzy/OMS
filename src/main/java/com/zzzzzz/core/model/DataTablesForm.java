package com.zzzzzz.core.model;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.ServletRequestUtils;

import com.zzzzzz.utils.page.Page;

public class DataTablesForm {
	
	private Integer sEcho;
	private Integer iDisplayStart;
	private Integer iDisplayLength;
	
	public DataTablesForm(HttpServletRequest request){
		Integer iDisplayStart = ServletRequestUtils.getIntParameter(request, "iDisplayStart", 0);
		Integer iDisplayLength = ServletRequestUtils.getIntParameter(request, "iDisplayLength", 10);
		Integer sEcho = ServletRequestUtils.getIntParameter(request, "sEcho", 0);
		
		this.sEcho = sEcho;
		this.iDisplayStart = iDisplayStart;
		this.iDisplayLength = iDisplayLength;
	}
	
	public Page getPage(){
		Page page = new Page(getiDisplayStart(), getiDisplayLength());
		return page;
	}

	public Integer getsEcho() {
		return sEcho;
	}
	public void setsEcho(Integer sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiDisplayStart() {
		return iDisplayStart;
	}
	public void setiDisplayStart(Integer iDisplayStart) {
		this.iDisplayStart = iDisplayStart;
	}
	public Integer getiDisplayLength() {
		return iDisplayLength;
	}
	public void setiDisplayLength(Integer iDisplayLength) {
		this.iDisplayLength = iDisplayLength;
	}
	
}
