package com.zzzzzz.core.model;

public class BaseForm {
	private final static int MAX_OFFSET = 1000;
	private final static int DEFAULT_OFFSET = 20;
	
	private Integer pageNumber;
	private String sortBy;
	private String sortDir;
	private Integer start;
	private Integer end;
	private Integer offset;//pagePerSize
	
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getSortDir() {
		return sortDir;
	}
	public void setSortDir(String sortDir) {
		this.sortDir = sortDir;
	}
	public Integer getStart() {
		if(getPageNumber() <= 1){
			return 0;
		}
		setStart(getOffset()*(getPageNumber() - 1));
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getOffset() {
		if(offset == null || offset > MAX_OFFSET){
			return DEFAULT_OFFSET;
		}
		return offset;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	public Integer getEnd() {
		setEnd(getStart() + getOffset());
		if(end < 0) end = 0;
		return end;
	}
	public void setEnd(Integer end) {
		this.end = end;
	}
	
}
