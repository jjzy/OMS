package com.zzzzzz.core.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class ExcelUploadValidator implements Validator {

	public boolean supports(Class clazz) {
		// just validate the FileUpload instances
		return CommonsMultipartFile.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) {

		CommonsMultipartFile file = (CommonsMultipartFile) target;

		if (!"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet".equals(file.getContentType())
				&& !"application/vnd.ms-excel".equals(file.getContentType())) {
			
			errors.rejectValue("fileData", "error.fileData.contentType", "required contentType is xlsx or xls");
		}
	}
}
