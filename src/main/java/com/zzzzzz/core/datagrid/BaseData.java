package com.zzzzzz.core.datagrid;

import com.zzzzzz.oms.GCS;

public class BaseData {
	private Object dt;
	private Integer tt;
	private String errCd;
	private String errMsg;

	public BaseData(Object dt, Integer tt) {
		this.errCd = GCS.ERRCD_SUS;
		this.dt = dt;
		this.tt = tt;
	}

	public BaseData() {
	}

	public BaseData(String errCd) {
		this.errCd = errCd;
	}

	public BaseData(String errCd, String errMsg) {
		this.errCd = errCd;
		this.errMsg = errMsg;
	}

	public Object getDt() {
		return dt;
	}

	public void setDt(Object dt) {
		this.dt = dt;
	}

	public Integer getTt() {
		return tt;
	}

	public void setTt(Integer tt) {
		this.tt = tt;
	}

	public String getErrCd() {
		return errCd;
	}

	public void setErrCd(String errCd) {
		this.errCd = errCd;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

}
