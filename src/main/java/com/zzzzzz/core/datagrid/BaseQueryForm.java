package com.zzzzzz.core.datagrid;

import java.util.HashMap;
import java.util.Map;

import com.zzzzzz.utils.page.Page;

public class BaseQueryForm {
	private Integer pageNb;
	private Integer pageSize;
	private Map<String, Object> ffMap = new HashMap<String, Object>();;
	
	private Page page;
	
	public Page getPage() {
		page = new Page(pageSize*(pageNb-1), pageSize);
		return page;
	}
	
	public Integer getPageNb() {
		return pageNb;
	}

	public void setPageNb(Integer pageNb) {
		this.pageNb = pageNb;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Map<String, Object> getFfMap() {
		return ffMap;
	}

	public void setFfMap(Map<String, Object> ffMap) {
		this.ffMap = ffMap;
	}
	
}
