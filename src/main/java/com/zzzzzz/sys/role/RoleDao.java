package com.zzzzzz.sys.role;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import javax.annotation.Resource;
import org.springframework.stereotype.Repository;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.utils.page.Page;

import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class RoleDao {
	
	@Resource
	private BaseDao baseDao;
			
	private final static String sql_add = "insert into sys_role(name, platformId, remark, sortNb, addDt, addBy, st) values(:name, :platformId, :remark, :sortNb, :addDt, :addBy, :st)";
	private final static String sql_updStByIds = "update sys_role set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update sys_role set name=:name, platformId=:platformId, remark=:remark, sortNb=:sortNb, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, name, platformId, remark, sortNb, addDt, addBy, updDt, updBy, st from sys_role where id = :id";
	
	public Long add(Role role){
		return baseDao.updateGetLongKey(sql_add, role);
	}
	public int updStByIds(List<Long> ids, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}		  
	public int updById(Role role){
		return baseDao.update(sql_upd, role);
	}
	public Role findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, Role.class);
	}
	
	/**
	 * 动态查询
	 */
	public List<Map<String, Object>> findListBy(Map<String, Object> ffMap){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append(" r.id, r.name, r.platformId, r.remark, r.sortNb, r.addDt, r.addBy, r.updDt, r.updBy, r.st, p.name as platformName");
		finder.append(" from sys_role r inner join sys_platform p on r.platformId = p.id");
		finder.append(" where 1=1");
		if(DaoUtils.isNotNull(ffMap.get("name"))){
		finder.append(" and r.name like :name").setParam("name", "%" + ffMap.get("name") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("st"))){
		finder.append(" and r.st = :st").setParam("st", ffMap.get("st"));
		}else{
			finder.append(" and r.st = 0");
		}
		if(DaoUtils.isNotNull(ffMap.get("platformId"))){
		finder.append(" and r.platformId = :platformId").setParam("platformId", ffMap.get("platformId"));
		}
		finder.append("order by r.id desc");		
		List<Map<String, Object>> list = baseDao.findList(finder);
				
		return list;
	}
}
