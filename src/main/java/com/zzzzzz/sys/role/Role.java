package com.zzzzzz.sys.role;

import java.util.Date;

/**
 * @author hing
 * @version 1.0.0
 */
public class Role {
	
  	private Long id;
  	private String name;
  	private Long platformId;
  	private String remark;
  	private Integer sortNb;
  	private Date addDt;
  	private Long addBy;
  	private Date updDt;
  	private Long updBy;
  	private Integer st;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public String getName(){
  		return name;
  	}
  	public void setName(String name) {
		this.name = name;
	}
  	public Long getPlatformId(){
  		return platformId;
  	}
  	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
  	public String getRemark(){
  		return remark;
  	}
  	public void setRemark(String remark) {
		this.remark = remark;
	}
  	public Integer getSortNb(){
  		return sortNb;
  	}
  	public void setSortNb(Integer sortNb) {
		this.sortNb = sortNb;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
  	public Integer getSt(){
  		return st;
  	}
  	public void setSt(Integer st) {
		this.st = st;
	}
}


