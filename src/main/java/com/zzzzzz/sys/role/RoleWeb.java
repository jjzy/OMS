package com.zzzzzz.sys.role;

import java.util.List;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.utils.page.Page;

import com.zzzzzz.oms.GCS;

import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class RoleWeb {

	@Resource
	public RoleService roleService;
	@Resource
	public RoleDao roleDao;

	@RequestMapping(value = "/role/list", method = RequestMethod.GET)
	public String list() {
		return "sys/role";
	}

	@RequestMapping(value = "/role/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid Role role, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}

		roleService.save(role, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/role/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		roleDao.updStByIds(ids, st, ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	
	@RequestMapping(value = "/role/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		Role role = roleDao.findById(id);
		return new BaseData(role, null);
	}

	@RequestMapping(value = "/role/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		if(!ShiroUtils.ifSuperAdministrator()){
			Long platformId = ShiroUtils.findPlatformId();
			baseQueryForm.getFfMap().put(GCS.COL_PLATFORM_ID, platformId);
		}
		
		List<Map<String, Object>> list = roleDao.findListBy(baseQueryForm.getFfMap());
		return new BaseData(list, null);
	}
}
