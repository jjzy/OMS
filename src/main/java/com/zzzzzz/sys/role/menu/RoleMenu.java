package com.zzzzzz.sys.role.menu;

public class RoleMenu {
	private Long roleId;
	private Long menuId;
	
	public RoleMenu(Long roleId, Long menuId){
		this.roleId = roleId;
		this.menuId = menuId;
	}
	
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getMenuId() {
		return menuId;
	}
	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}
	
}
