package com.zzzzzz.sys.role.menu;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.sys.menu.ZTree;

@Controller
public class RoleMenuWeb {
	
	@Resource
	public RoleMenuService roleMenuService;
	@Resource
	public RoleMenuDao roleMenuDao;
	
	@RequestMapping(value="/role/menu/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestParam(value = "roleId", required = true) Long roleId, @RequestParam(value = "menuIds", required = false) List<Long> menuIds) throws Exception{
		roleMenuService.save(roleId, menuIds);
		return new BaseData("0");
	}
	
	@RequestMapping(value="/role/menu/view", method = RequestMethod.GET)
	public String  view(){
		return "sys/roleMenu";
	}
	
}
