package com.zzzzzz.sys.role.menu;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.sys.menu.ZTree;

@Repository
public class RoleMenuDao {
	
	@Resource
	private BaseDao baseDao;
	
	private final static String sql_add = "insert into sys_role_menu(roleId, menuId) values(:roleId, :menuId)";
	private final static String sql_delByRoleId = "delete from sys_role_menu where roleId = :roleId";
	private final static String sql_delByMenuId = "delete from sys_role_menu where menuId = :menuId";
	private final static String sql_delByRoleIdAndMenuId = "delete from sys_role_menu where roleId = :roleId and menuId = :menuId";
	
	public int add(RoleMenu roleMenu){
		return baseDao.update(sql_add, roleMenu);
	}
	
	public int delByRoleId(Long roleId){
		Finder finder = new Finder(sql_delByRoleId).setParam("roleId", roleId);
		return baseDao.update(finder);
	}
	
	public int delByMenuId(Long menuId){
		Finder finder = new Finder(sql_delByMenuId).setParam("menuId", menuId);
		return baseDao.update(finder);
	}
	
	public int delByRoleIdAndMenuId(Long roleId, Long menuId){
		Finder finder = new Finder(sql_delByRoleIdAndMenuId).setParam("roleId", roleId).setParam("menuId", menuId);
		return baseDao.update(finder);
	}
	
	/*
	 * 场景：角色管理界面中的菜单管理。
	 * 角色所在平台的所有菜单和角色本身对应的菜单(checked)
	 */
	public List<ZTree> findListByPidForEdit(Long roleId, Long pid){
		Finder finder = new Finder("select")
		.append("m.id, m.name, m.pid, m.href as url, m.target, (case when rm.menuId is null then false else true end) as checked")
		.append("from sys_menu m")
		.append("inner join sys_platform_menu pm on m.id = pm.menuId")
		.append("inner join sys_role r on pm.platformId = r.platformId and r.id = :roleId")
		.append("left join sys_role_menu rm on m.id = rm.menuId and rm.roleId = :roleId")
		.append("where m.st = 0 and m.pid=:pid")
		.append("order by m.sortNb")
		.setParam("roleId", roleId)
		.setParam("pid", pid);
		
		return baseDao.findList(finder, ZTree.class);
	}
	/*
	 * 场景：角色管理界面中的菜单管理。
	 * 角色所在平台的所有菜单和角色本身对应的菜单(checked)
	 */
	public List<ZTree> findListByNullPidForEdit(Long roleId){
		Finder finder = new Finder("select")
		.append("m.id, m.name, m.pid, m.href as url, m.target, (case when rm.menuId is null then false else true end) as checked")
		.append("from sys_menu m")
		.append("inner join sys_platform_menu pm on m.id = pm.menuId")
		.append("inner join sys_role r on pm.platformId = r.platformId and r.id = :roleId")
		.append("left join sys_role_menu rm on m.id = rm.menuId and rm.roleId = :roleId")
		.append("where m.st = 0 and m.pid is null")
		.append("order by m.sortNb")
		.setParam("roleId", roleId);		
				
		return baseDao.findList(finder, ZTree.class);
	}
	
	public List<ZTree> findListForChildSidebar(Long userId, Long platformId, Long pid){
		Finder finder = new Finder("select")
		.append("distinct m.id, m.name, m.pid, m.href as url, m.target, m.pemisi, m.icon, m.ifShow")
		.append("from sys_menu m")
		.append("inner join sys_role_menu rm on m.id = rm.menuId")
		.append("inner join sys_role r on rm.roleId = r.id and r.platformId = :platformId")
		.append("inner join sys_user_role ur on r.id = ur.roleId and ur.userId = :userId")
		.append("where m.st = 0 and m.pid = :pid")
		.append("and r.st = 0 and m.ifShow = 1")
		.append("order by m.sortNb")
		.setParam("pid", pid)
		.setParam("platformId", platformId)
		.setParam("userId", userId);		
				
		return baseDao.findList(finder, ZTree.class);
	}
	
	public List<ZTree> findListForSidebar(Long userId, Long platformId){
		Finder finder = new Finder("select")
		.append("distinct m.id, m.name, m.pid, m.href as url, m.target, m.pemisi, m.icon, m.ifShow")
		.append("from sys_menu m")
		.append("inner join sys_role_menu rm on m.id = rm.menuId")
		.append("inner join sys_role r on rm.roleId = r.id and r.platformId = :platformId")
		.append("inner join sys_user_role ur on r.id = ur.roleId and ur.userId = :userId")
		.append("where m.st = 0 and m.pid is null")
		.append("and r.st = 0 and m.ifShow = 1")
		.append("order by m.sortNb")
		.setParam("platformId", platformId)
		.setParam("userId", userId);
				
		return baseDao.findList(finder, ZTree.class);
	}
	
	public List<Map<String, Object>> findListForPemisi(Long userId, Long platformId){
		Finder finder = new Finder("select")
		.append("distinct m.id, m.pemisi")
		.append("from sys_menu m")
		.append("inner join sys_role_menu rm on m.id = rm.menuId")
		.append("inner join sys_role r on rm.roleId = r.id and r.platformId = :platformId")
		.append("inner join sys_user_role ur on r.id = ur.roleId and ur.userId = :userId")
		.append("where m.st = 0")
		.append("and r.st = 0")
		.append("order by m.sortNb")
		.setParam("platformId", platformId)
		.setParam("userId", userId);
				
		return baseDao.findList(finder);
	}
	
	public List<Map<String, Object>> findImportList(Long userId, Long platformId){
		Finder finder = new Finder("select")
		.append("distinct m.href, d.lb")
		.append("from sys_menu m")
		.append("inner join sys_role_menu rm on m.id = rm.menuId")
		.append("inner join sys_role r on rm.roleId = r.id and r.platformId = :platformId")
		.append("inner join sys_user_role ur on r.id = ur.roleId and ur.userId = :userId")
		.append("inner join sys_dict d on m.href = d.val and d.cd = 'oms:import:url' and d.st = 0")
		.append("where m.st = 0")
		.append("and r.st = 0")
		.append("order by d.sortNb")
		.setParam("platformId", platformId)
		.setParam("userId", userId);
				
		return baseDao.findList(finder);
	}
}
