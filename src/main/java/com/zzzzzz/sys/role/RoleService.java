package com.zzzzzz.sys.role;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zzzzzz.plugins.shiro.I;

@Service
public class RoleService {
	@Resource
	public RoleDao roleDao;

	@Transactional
	public int save(Role role, I i) {
		role.setUpdDt(new Date());
		role.setUpdBy(i.getId());
		if (role.getId() == null) {
			role.setAddDt(new Date());
			role.setAddBy(i.getId());
			roleDao.add(role);
		} else {
			roleDao.updById(role);
		}

		return 1;
	}
}
