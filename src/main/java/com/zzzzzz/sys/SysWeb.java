package com.zzzzzz.sys;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.sys.menu.ZTree;
import com.zzzzzz.sys.platform.menu.PlatformMenuService;
import com.zzzzzz.sys.role.menu.RoleMenuDao;
import com.zzzzzz.sys.role.menu.RoleMenuService;
import com.zzzzzz.sys.user.client.UserClientDao;
import com.zzzzzz.sys.user.role.UserRoleDao;

@Controller
public class SysWeb {
	
	@Resource
	public UserRoleDao userRoleDao;
	@Resource
	public UserClientDao userClientDao;
	@Resource
	public RoleMenuService roleMenuService;
	@Resource
	public RoleMenuDao roleMenuDao;
	@Resource
	public PlatformMenuService platformMenuService;
	@Resource
	public BaseDao baseDao;
	
	@RequestMapping(value="/user/role/select/{userId}", method = RequestMethod.GET)
	@ResponseBody
	public BaseData findRoleListByUserIdForSelect(Model model, @PathVariable Long userId){
		List<Map<String, Object>> list = userRoleDao.findRoleListByUserIdForSelect(userId);
		return new BaseData(list, null);
	}
	
	@RequestMapping(value="/user/role/platforms", method = RequestMethod.GET)
	@ResponseBody
	public BaseData findPlatformListByI(Model model){
		List<Map<String, Object>> list = userRoleDao.findPlatformListByUserId(ShiroUtils.findUserId());
		return new BaseData(list, null);
	}
	
	@RequestMapping(value="/role/menu/tree/{roleId}", method = RequestMethod.GET)
	@ResponseBody
	public BaseData treeByRoleId(@PathVariable Long roleId){
		List<ZTree> list = roleMenuService.findListByIdForEdit(roleId, null);
		return new BaseData(list, null);
	}
	
	@RequestMapping(value="/user/role/menu/tree/{userId}/{platformId}", method = RequestMethod.GET)
	@ResponseBody
	public BaseData tree(@PathVariable Long userId, @PathVariable Long platformId){
		List<ZTree> list = roleMenuService.findListForSidebar(userId, platformId, null);
		return new BaseData(list, null);
	}
	
	@RequestMapping(value="/platform/menu/tree/{platformId}", method = RequestMethod.GET)
	@ResponseBody
	public BaseData treeByPlatformId(@PathVariable Long platformId){
		List<ZTree> list = platformMenuService.findListByIdForEdit(platformId, null);
		return new BaseData(list, null);
	}
	
	@RequestMapping(value="/user/client/select/{userId}", method = RequestMethod.GET)
	@ResponseBody
	public BaseData findClientListByUserIdForSelect(Model model, @PathVariable Long userId){
		List<Map<String, Object>> list = userClientDao.findClientListByUserIdForSelect(userId);
		return new BaseData(list, null);
	}
	
	@RequestMapping(value="/findImportList", method = RequestMethod.GET)
	@ResponseBody
	public BaseData findImportList(){
		I i = ShiroUtils.findUser();
		List<Map<String, Object>> list = roleMenuDao.findImportList(i.getId(), i.getPlatformId());
		return new BaseData(list, null);
	}
}
