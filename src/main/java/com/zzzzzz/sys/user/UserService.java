package com.zzzzzz.sys.user;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.plugins.shiro.ChangePasswordForm;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.utils.BeanValidators;
import com.zzzzzz.utils.DateTimeUtils;

@Service
public class UserService {
	@Resource
	public UserDao userDao;
	@Resource
	public Validator validator;

	public int save(User user, I i) {
		user.setUpdDt(new Date());
		user.setUpdBy(i.getId());
		if (user.getId() == null) {
			user.setAddDt(new Date());
			user.setAddBy(i.getId());
			userDao.add(user);
		} else {
			userDao.updById(user);
		}

		return 1;
	}

	@Transactional
	public BaseData batchAdd(List<User> list, I i) {
		logger.info("user batchAdd validate start:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		//start validate
		int errTt = 0;
		StringBuilder errMsg = new StringBuilder();
		for (User user : list) {
			try {
				BeanValidators.validateWithException(validator, user);
			} catch (ConstraintViolationException e) {
				List<String> msgList = BeanValidators.extractPropertyAndMessageAsList(e, ": ");
				for (String msg : msgList) {
					errTt++;
					errMsg.append("<br/>").append(msg);
				}
			} catch (Exception e) {
				logger.warn("", e);
				errTt++;
				errMsg.append("<br/>验证失败：").append(e.getMessage());
			}
		}
		if(errTt > 0){
			errMsg.insert(0, "共有"+errTt+"笔记录验证失败：");
			return new BaseData("101", errMsg.toString());
		}
		logger.info("user batchAdd validate end:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		logger.info("user batchAdd save start:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		//state save
		int susTt = 0;
		for (User user : list) {
			user.setSt(0);
			save(user, i);
			susTt++;
		}
		logger.info("user batchAdd save end:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		return new BaseData("0", "共有"+susTt+"笔记录导入成功。");
	}

	public BaseData changePassword(ChangePasswordForm changePasswordForm) {
		if(!StringUtils.equals(changePasswordForm.getNewPassword(), changePasswordForm.getConfirmNewPassword())){
			return new BaseData("1", "两次输入的新密码不相同，请重新输入。");
		}else if(StringUtils.equals(changePasswordForm.getNewPassword(), changePasswordForm.getCurrentPassword())){
			return new BaseData("1", "新密码与旧密码相同。");
		}
		
		int result = userDao.changePassword(ShiroUtils.findUserId(), changePasswordForm.getCurrentPassword(), changePasswordForm.getNewPassword());
		if(result == 1){
			return new BaseData("0");
		}
		return new BaseData("1", "请确认当前密码是否正确。");
	}
	
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
}
