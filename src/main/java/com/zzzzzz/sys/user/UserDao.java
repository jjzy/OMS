package com.zzzzzz.sys.user;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.credential.PasswordService;
import org.springframework.stereotype.Repository;

import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.utils.page.Page;
import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class UserDao {
	
	@Resource
	private BaseDao baseDao;
	@Resource
	private PasswordService passwordService;
			
	private final static String sql_add = "insert into sys_user(account, password, name, email, homePage, remark, sortNb, addDt, addBy, st) values(:account, :password, :name, :email, :homePage, :remark, :sortNb, :addDt, :addBy, :st)";
	private final static String sql_updStByIds = "update sys_user set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update sys_user set account=:account, password=:password, name=:name, email=:email, homePage=:homePage, remark=:remark, sortNb=:sortNb, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, account, password, name, email, homePage, remark, sortNb, addDt, addBy, updDt, updBy, st from sys_user where id = :id";
	
	public Long add(User user){
		user.setPassword(passwordService.encryptPassword(user.getPassword()));
		return baseDao.updateGetLongKey(sql_add, user);
	}
	
	public int updStByIds(List<Long> ids, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}		  
	public int updById(User user){
		String password = findById(user.getId()).getPassword();
		if(!StringUtils.equals(password, user.getPassword())){
			user.setPassword(passwordService.encryptPassword(user.getPassword()));
		}
		return baseDao.update(sql_upd, user);
	}
	public User findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, User.class);
	}
	
	/**
	 * 动态查询
	 */
	public List<Map<String, Object>> findListBy(Map<String, Object> ffMap){
		Finder finder = new Finder("");
		finder.append("select");
		finder.append("id, account, password, name, email,homePage, remark, sortNb, addDt, addBy, updDt, updBy, st");
		finder.append("from sys_user");
		finder.append("where 1=1");
		if(DaoUtils.isNotNull(ffMap.get("account"))){
			finder.append("and account like :account").setParam("account", "%" + ffMap.get("account") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and st = :st").setParam("st", ffMap.get("st"));
		}else{
			finder.append(" and st = 0");
		}		
		List<Map<String, Object>> list = baseDao.findList(finder);
				
		return list;
	}
	/**
	 * 用于登陆时验证
	 */
	public User findByAccount(String account){
		Finder finder = new Finder("");
		finder.append("select id, account, password, name, email, remark, st");
		finder.append("from sys_user");
		finder.append("where");
		finder.append("account=:account and st=0").setParam("account", account);
				
		User user = baseDao.findOne(finder, User.class);
		return user;
	}

	public int changePassword(Long userId, String currentPassword, String newPassword) {
		Finder finder = new Finder("select password from sys_user where id = :id and st=0").setParam("id", userId);
		String encryptedPassword = baseDao.findString(finder);
		if(passwordService.passwordsMatch(currentPassword, encryptedPassword)){
			String encryptedNewPassword = passwordService.encryptPassword(newPassword);
			finder = new Finder("update sys_user set password = :encryptedNewPassword where id = :id and st = 0");
			finder.setParam("id", userId).setParam("encryptedNewPassword", encryptedNewPassword);
			return baseDao.update(finder);
		}else{
			return -1;
		}
	}
}
