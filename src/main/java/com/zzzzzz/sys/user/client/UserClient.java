package com.zzzzzz.sys.user.client;

public class UserClient {
	private Long userId;
	private Long clientId;

	public UserClient() {}
	
	public UserClient(Long userId, Long clientId) {
		this.userId = userId;
		this.clientId = clientId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

}
