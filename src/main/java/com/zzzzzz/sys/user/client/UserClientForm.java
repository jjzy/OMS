package com.zzzzzz.sys.user.client;

import javax.validation.constraints.NotNull;

public class UserClientForm {
	@NotNull
	private Long userId;
	@NotNull
	private Long[] clientIds;
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long[] getClientIds() {
		return clientIds;
	}
	public void setClientIds(Long[] clientIds) {
		this.clientIds = clientIds;
	}
}
