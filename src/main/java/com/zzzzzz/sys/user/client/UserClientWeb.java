package com.zzzzzz.sys.user.client;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.plugins.shiro.ShiroUtils;

@Controller
public class UserClientWeb {
	
	@Resource
	public UserClientService userClientService;
	@Resource
	public UserClientDao userClientDao;
	
	@RequestMapping(value="/user/client/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestParam(value = "userId", required = true) Long userId, @RequestParam(value = "clientIds", required = false) List<Long> clientIds) throws Exception{
		userClientService.save(userId, clientIds);
		return new BaseData("0");
	}
	
	@RequestMapping(value="/user/client/{userId}", method = RequestMethod.GET)
	@ResponseBody
	public BaseData findClientListByUserId(Model model, @PathVariable Long userId){
		List<Long> list = userClientDao.findClientIdListByUserId(userId);
		return new BaseData(list, null);
	}
	
}
