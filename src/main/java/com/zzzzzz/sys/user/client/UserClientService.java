package com.zzzzzz.sys.user.client;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserClientService {
	@Resource
	public UserClientDao userClientDao;
	
	@Transactional
	public int save(Long userId, List<Long> clientIds){
		userClientDao.delByUserId(userId);
		if(clientIds != null){
			for(Long clientId : clientIds){
				userClientDao.add(new UserClient(userId, clientId));
			}
		}
		return 1;
	}
}
