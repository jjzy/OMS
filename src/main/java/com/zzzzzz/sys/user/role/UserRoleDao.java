package com.zzzzzz.sys.user.role;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.zzzzzz.oms.GCS;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.sys.role.Role;

@Repository
public class UserRoleDao {
	
	@Resource
	private BaseDao baseDao;
	
	private final static String sql_add = "insert into sys_user_role(userId, roleId) values(:userId, :roleId)";
	private final static String sql_delByUserId = "delete from sys_user_role where userId = :userId";
	private final static String sql_delByRoleId = "delete from sys_user_role where roleId = :roleId";
	
	private final static String sql_findRoleIdListByUserId = "select roleId from sys_user_role where userId=:userId";
	private final static String sql_findRoleListByUserId = "select r.* from sys_user_role ur inner join sys_role r on ur.roleId = r.id where ur.userId = :userId and r.st = 0 order by r.updDt";
	
	public int add(UserRole userRole){
		return baseDao.update(sql_add, userRole);
	}
	
	public int delByUserId(Long userId){
		Finder finder = new Finder(sql_delByUserId).setParam("userId", userId);
		return baseDao.update(finder);
	}
	
	public int delByRoleId(Long roleId){
		Finder finder = new Finder(sql_delByRoleId).setParam("roleId", roleId);
		return baseDao.update(finder);
	}
	
	public List<Long> findRoleIdListByUserId(Long userId){
		Finder finder = new Finder(sql_findRoleIdListByUserId).setParam("userId", userId);
		List<Long> roleList = baseDao.findListSingleColumn(finder, Long.class);
		return roleList;
	}
	
	public List<Role> findRoleListByUserId(Long userId){
		Finder finder = new Finder(sql_findRoleListByUserId).setParam("userId", userId);
		List<Role> roleList = baseDao.findList(finder, Role.class);
		return roleList;
	}
	
	/*
	 * 场景：用户管理界面的角色管理。
	 * 根据所选择的用户，查看此用户所在平台的所有角色，和用户所对应的所有角色(checked)
	 * 如果是超级管理员，则可查看到所有平台的所有角色
	 */
	public List<Map<String, Object>> findRoleListByUserIdForSelect(Long userId){
		Finder finder = new Finder("select r.*, ur.userId")
		.append("from");
		if(ShiroUtils.ifSuperAdministrator()){
			finder.append("sys_role r");
		}else{
			finder.append("(select * from sys_role where platformId = :platformId)r").setParam(GCS.COL_PLATFORM_ID, ShiroUtils.findPlatformId());
		}
		finder.append("left join sys_user_role ur on ur.roleId = r.id and ur.userId = :userId")
		.append("where r.st = 0 order by r.updDt")
		.setParam("userId", userId);
		
		List<Map<String, Object>> list = baseDao.findList(finder);
		return list;
	}
	
	/*
	 * 根据用户查询其所拥有角色对应的所有平台列表。
	 * 如果是超级管理员则可查看到所有平台。
	 * 场景：编辑角色时的可选择平台列表
	 */
	public List<Map<String, Object>> findPlatformListByUserId(Long userId){
		Finder finder = null;
		if(ShiroUtils.ifSuperAdministrator(userId)){
			finder = new Finder("select")
			.append("*")
			.append("from sys_platform")
			.append("where st = 0 order by sortNb, updDt");
		}else{
			finder = new Finder("select")
			.append("distinct r.platformId, p.*")
			.append("from sys_user_role ur")
			.append("inner join sys_role r on ur.roleId = r.id and r.st = 0")
			.append("inner join sys_platform p on r.platformId = p.id and p.st = 0")
			.append("where ur.userId = :userId")
			.append("order by p.sortNb, p.updDt")
			.setParam("userId", userId);
		}
		
		List<Map<String, Object>> list = baseDao.findList(finder);
		return list;
	}
}
