package com.zzzzzz.sys.user.role;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.plugins.shiro.ShiroUtils;

@Controller
public class UserRoleWeb {
	
	@Resource
	public UserRoleService userRoleService;
	@Resource
	public UserRoleDao userRoleDao;
	
	@RequestMapping(value="/user/role/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestParam(value = "userId", required = true) Long userId, @RequestParam(value = "roleIds", required = false) List<Long> roleIds) throws Exception{
		userRoleService.save(userId, roleIds);
		return new BaseData("0");
	}
	
	@RequestMapping(value="/user/role/{userId}", method = RequestMethod.GET)
	@ResponseBody
	public BaseData findRoleListByUserId(Model model, @PathVariable Long userId){
		List<Long> list = userRoleDao.findRoleIdListByUserId(userId);
		return new BaseData(list, null);
	}
	
}
