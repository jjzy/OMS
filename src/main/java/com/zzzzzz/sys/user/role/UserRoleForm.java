package com.zzzzzz.sys.user.role;

import javax.validation.constraints.NotNull;

public class UserRoleForm {
	@NotNull
	private Long userId;
	@NotNull
	private Long[] roleIds;
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long[] getRoleIds() {
		return roleIds;
	}
	public void setRoleIds(Long[] roleIds) {
		this.roleIds = roleIds;
	}
}
