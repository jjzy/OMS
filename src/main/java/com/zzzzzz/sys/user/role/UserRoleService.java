package com.zzzzzz.sys.user.role;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserRoleService {
	@Resource
	public UserRoleDao userRoleDao;
	
	@Transactional
	public int save(Long userId, List<Long> roleIds){
		userRoleDao.delByUserId(userId);
		if(roleIds != null){
			for(Long roleId : roleIds){
				userRoleDao.add(new UserRole(userId, roleId));
			}
		}
		return 1;
	}
}
