package com.zzzzzz.sys.user;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class User implements Serializable {

	private Long id;

	@ExcelField(title = "登录名", align = 2, sort = 10)
	@Length(min = 1, max = 30)
	private String account;

	@ExcelField(title = "密码", align = 2, sort = 20)
	
	private String password;

	@ExcelField(title = "姓名", align = 2, sort = 30)
	@Length(min = 1, max = 30)
	private String name;

	@ExcelField(title = "邮箱", align = 2, sort = 40)
	@Email
	private String email;

	@ExcelField(title = "主页", align = 2, sort = 50)
	private String homePage;

	@ExcelField(title = "备注", align = 2, sort = 60)
	private String remark;

	@ExcelField(title = "排序", align = 2, sort = 70)
	@NotNull
	private Integer sortNb;

	private Date addDt;
	private Long addBy;
	private Date updDt;
	private Long updBy;
	private Integer st;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHomePage() {
		return homePage;
	}

	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getSortNb() {
		return sortNb;
	}

	public void setSortNb(Integer sortNb) {
		this.sortNb = sortNb;
	}

	public Date getAddDt() {
		return addDt;
	}

	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}

	public Long getAddBy() {
		return addBy;
	}

	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}

	public Date getUpdDt() {
		return updDt;
	}

	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}

	public Long getUpdBy() {
		return updBy;
	}

	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}

	public Integer getSt() {
		return st;
	}

	public void setSt(Integer st) {
		this.st = st;
	}

	public static User newTest() {
		User user = new User();
		user.setAccount("测试帐号");
		user.setPassword("测试密码");
		user.setName("测试姓名");
		user.setEmail("test@email.com");
		user.setHomePage("测试主页");
		user.setRemark("测试备注");
		user.setSortNb(1);
		return user;
	}
}
