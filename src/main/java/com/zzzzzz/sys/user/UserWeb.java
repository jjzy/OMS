package com.zzzzzz.sys.user;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.oms.GCS;
import com.zzzzzz.plugins.poi.ExcelField;
import com.zzzzzz.plugins.poi.ExportExcel;
import com.zzzzzz.plugins.poi.ImportExcel;
import com.zzzzzz.plugins.shiro.ShiroUtils;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class UserWeb {

	@Resource
	public UserService userService;
	@Resource
	public UserDao userDao;

	@RequestMapping(value = "/user/list", method = RequestMethod.GET)
	public String list() {
		return "sys/user";
	}

	@RequestMapping(value = "/user/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid User user, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}
		userService.save(user, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/user/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		userDao.updStByIds(ids, st, ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	
	@RequestMapping(value = "/user/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		User user = userDao.findById(id);
		return new BaseData(user, null);
	}

	@RequestMapping(value = "/user/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		List<Map<String, Object>> list = userDao.findListBy(baseQueryForm.getFfMap());
		return new BaseData(list, null);
	}
	
	//@RequiresPermissions("sys:user:import")
    @RequestMapping(value = "/user/import", method=RequestMethod.POST)
    public String importExcel(@RequestParam MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			ImportExcel importExcel = new ImportExcel(file, 1, 0);
			List<User> list = importExcel.getDataList(User.class);
			
			BaseData baseData = userService.batchAdd(list, ShiroUtils.findUser());
			redirectAttributes.addFlashAttribute("msg", baseData.getErrMsg());
		} catch (Exception e) {
			logger.warn("importExcel", e);
			redirectAttributes.addFlashAttribute("msg", "导入数据保存失败。");
		}
		return "redirect:/msg";
    }
    
    @RequestMapping(value = "user/import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "用户数据导入模板.xlsx";
    		List<User> list = Lists.newArrayList();
    		list.add(User.newTest());
    		new ExportExcel("用户数据导入模板", User.class, 2).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "用户数据导入模板下载失败。");
		}
		return "redirect:/msg";
    }
    
    private static final Logger logger = LoggerFactory.getLogger(UserWeb.class);
}
