package com.zzzzzz.sys.menu;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.oms.GCS;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.sys.dict.Dict;
import com.zzzzzz.utils.page.Page;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class MenuWeb {

	@Resource
	public MenuService menuService;
	@Resource
	public MenuDao menuDao;

	@RequestMapping(value = "/menu/list", method = RequestMethod.GET)
	public String list() {
		return "sys/menu";
	}

	@RequestMapping(value = "/menu/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid Menu menu, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}

		menuService.save(menu, ShiroUtils.findUser());
		return new BaseData("0");
	}

	@RequestMapping(value = "/menu/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		menuDao.updStByIds(ids, st, ShiroUtils.findUser());
		return new BaseData("0");
	}

	@RequestMapping(value = "/menu/id/{id}", method = RequestMethod.GET)
	@ResponseBody
	public BaseData findById(@PathVariable Long id) {
		Menu menu = menuDao.findById(id);
		return new BaseData(menu, null);
	}

	@RequestMapping(value = "/menu/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		Page page = baseQueryForm.getPage();
		List<Map<String, Object>> list = menuDao.findListBy(page, baseQueryForm.getFfMap());
		return new BaseData(list, page.getTotal());
	}

	@RequestMapping(value = "/menu/findListByPid", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListByPid(@RequestBody BaseQueryForm baseQueryForm) {
		List<ZTree> list = menuDao.findListByPid(baseQueryForm.getFfMap());
		return new BaseData(list, null);
	}

	@RequestMapping(value = "/menu/findListByNullPid", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListByNullPid() {
		List<ZTree> list = menuDao.findListByNullPid();
		return new BaseData(list, null);
	}

	@RequestMapping(value = "/menu/tree", method = RequestMethod.GET)
	@ResponseBody
	public BaseData tree() {
		List<ZTree> list = menuService.findListById(null);
		return new BaseData(list, null);
	}
}
