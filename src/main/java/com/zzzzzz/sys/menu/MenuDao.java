package com.zzzzzz.sys.menu;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.utils.page.Page;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class MenuDao {
	
	@Resource
	private BaseDao baseDao;
			
	private final static String sql_add = "insert into sys_menu(name, pid, href, target, icon, pemisi, ifShow, remark, sortNb, addDt, addBy, st) values(:name, :pid, :href, :target, :icon, :pemisi, :ifShow, :remark, :sortNb, :addDt, :addBy, :st)";
	private final static String sql_updStByIds = "update sys_menu set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update sys_menu set name=:name, pid=:pid, href=:href, target=:target, icon=:icon, pemisi=:pemisi, ifShow=:ifShow, remark=:remark, sortNb=:sortNb, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, name, pid, href, target, icon, pemisi, ifShow, remark, sortNb, addDt, addBy, updDt, updBy, st from sys_menu where id = :id order by sortNb";
	
	public Long add(Menu menu){
		return baseDao.updateGetLongKey(sql_add, menu);
	}
	public int updStByIds(List<Long> ids, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}		  
	public int updById(Menu menu){
		return baseDao.update(sql_upd, menu);
	}
	public Menu findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, Menu.class);
	}
	
	/**
	 * 增删改查列表查询，包括按钮在内和已删除的列表
	 */
	public List<Map<String, Object>> findListBy(Page page, Map<String, Object> ffMap){
		Finder finder = new Finder("select");
		finder.append("count(1) as tt");
		finder.append("from sys_menu m1 left join sys_menu m2 on m1.pid = m2.id");
		finder.append("where 1=1");
		if(DaoUtils.isNotNull(ffMap.get("name"))){
			finder.append(" and (m1.name like :name or m2.name like :name)").setParam("name", "%"+ffMap.get("name")+"%");
		}
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and m1.st = :st").setParam("st", ffMap.get("st"));
		}else{
			finder.append(" and m1.st = 0");
		}
		finder.append(" order by m1.id desc");
		
		if(page != null){
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}

		String sql = finder.getSql().replace("count(1) as tt", "m1.*, m2.name as pidName");
		finder.setSql(sql);
		List<Map<String, Object>> list = baseDao.findList(finder);
				
		return list;
	}
	/**
	 * 包括按钮在内的在用列表的树结构
	 */
	public List<ZTree> findListByPid(Map<String, Object> ffMap){
		Finder finder = new Finder("");
		finder.append(" select id, name, pid, href from sys_menu where pid=:pid and st=0 order by sortNb").setParam("pid", ffMap.get("pid"));
				
		List<ZTree> list = baseDao.findList(finder, ZTree.class);
				
		return list;
	}
	/**
	 * 包括按钮在内的在用列表的树结构
	 */
	public List<ZTree> findListByNullPid(){
		Finder finder = new Finder("");
		finder.append(" select id, name, pid, href from sys_menu where pid is null and st=0 order by sortNb");
				
		List<ZTree> list = baseDao.findList(finder, ZTree.class);
				
		return list;
	}
}
