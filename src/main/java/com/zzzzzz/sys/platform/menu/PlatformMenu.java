package com.zzzzzz.sys.platform.menu;

public class PlatformMenu {
	private Long platformId;
	private Long menuId;
	
	public PlatformMenu(Long platformId, Long menuId){
		this.platformId = platformId;
		this.menuId = menuId;
	}
	
	public Long getPlatformId() {
		return platformId;
	}
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
	public Long getMenuId() {
		return menuId;
	}
	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}
	
}
