package com.zzzzzz.sys.platform.menu;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.sys.menu.ZTree;

@Repository
public class PlatformMenuDao {
	
	@Resource
	private BaseDao baseDao;
	
	private final static String sql_add = "insert into sys_platform_menu(platformId, menuId) values(:platformId, :menuId)";
	private final static String sql_delByPlatformId = "delete from sys_platform_menu where platformId = :platformId";
	private final static String sql_delByMenuId = "delete from sys_platform_menu where menuId = :menuId";
	private final static String sql_delByPlatformIdAndMenuId = "delete from sys_platform_menu where platformId = :platformId and menuId = :menuId";
	
	private final static String sql_findListByPid_for_edit = "select m.id, m.name, m.pid, m.href, (case when pm.menuId is null then false else true end) as checked from sys_menu m left join sys_platform_menu pm on m.id = pm.menuId and pm.platformId = :platformId where m.st = 0 and m.pid=:pid order by m.sortNb";
	private final static String sql_findListByNullPid_for_edit = "select m.id, m.name, m.pid, m.href, (case when pm.menuId is null then false else true end) as checked from sys_menu m left join sys_platform_menu pm on m.id = pm.menuId and pm.platformId = :platformId where m.st = 0 and m.pid is null order by m.sortNb";
	
	private final static String sql_findListByPid_for_sidebar = "select m.id, m.name, m.pid, m.href, m.icon from sys_menu m inner join sys_platform_menu pm on m.id = pm.menuId where m.st = 0 and m.pid=:pid and pm.platformId = :platformId order by (case when m.sortNb is null then 999999 else m.sortNb end)";
	private final static String sql_findListByNullPid_for_sidebar = "select m.id, m.name, m.pid, m.href, m.icon from sys_menu m inner join sys_platform_menu pm on m.id = pm.menuId where m.st = 0 and m.pid is null and pm.platformId = :platformId order by (case when m.sortNb is null then 999999 else m.sortNb end)";
	
	public int add(PlatformMenu platformMenu){
		return baseDao.update(sql_add, platformMenu);
	}
	
	public int delByPlatformId(Long platformId){
		Finder finder = new Finder(sql_delByPlatformId).setParam("platformId", platformId);
		return baseDao.update(finder);
	}
	
	public int delByMenuId(Long menuId){
		Finder finder = new Finder(sql_delByMenuId).setParam("menuId", menuId);
		return baseDao.update(finder);
	}
	
	public int delByPlatformIdAndMenuId(Long platformId, Long menuId){
		Finder finder = new Finder(sql_delByPlatformIdAndMenuId).setParam("platformId", platformId).setParam("menuId", menuId);
		return baseDao.update(finder);
	}
	
	public List<ZTree> findListByPidForEdit(Long platformId, Long pid){
		Finder finder = new Finder(sql_findListByPid_for_edit).setParam("platformId", platformId).setParam("pid", pid);
		return baseDao.findList(finder, ZTree.class);
	}
	
	public List<ZTree> findListByNullPidForEdit(Long platformId){
		Finder finder = new Finder(sql_findListByNullPid_for_edit).setParam("platformId", platformId);
		return baseDao.findList(finder, ZTree.class);
	}
	
	public List<ZTree> findListByPidForSidebar(Long platformId, Long pid){
		Finder finder = new Finder(sql_findListByPid_for_sidebar).setParam("platformId", platformId).setParam("pid", pid);
		return baseDao.findList(finder, ZTree.class);
	}
	
	public List<ZTree> findListByNullPidForSidebar(Long platformId){
		Finder finder = new Finder(sql_findListByNullPid_for_sidebar).setParam("platformId", platformId);
		return baseDao.findList(finder, ZTree.class);
	}
	
}
