package com.zzzzzz.sys.platform.menu;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.sys.menu.ZTree;

@Controller
public class PlatformMenuWeb {
	
	@Resource
	public PlatformMenuService platformMenuService;
	@Resource
	public PlatformMenuDao platformMenuDao;
	
	@RequestMapping(value="/platform/menu/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestParam(value = "platformId", required = true) Long platformId, @RequestParam(value = "menuIds", required = false) List<Long> menuIds) throws Exception{
		platformMenuService.save(platformId, menuIds);
		return new BaseData("0");
	}
	
	@RequestMapping(value="/platform/menu/view", method = RequestMethod.GET)
	public String  view(){
		return "sys/platformMenu";
	}
	
	
}
