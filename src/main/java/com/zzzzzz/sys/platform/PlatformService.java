package com.zzzzzz.sys.platform;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zzzzzz.plugins.shiro.I;

@Service
public class PlatformService {
	@Resource
	public PlatformDao platformDao;

	@Transactional
	public int save(Platform platform, I i) {
		platform.setUpdDt(new Date());
		platform.setUpdBy(i.getId());
		if (platform.getId() == null) {
			platform.setAddDt(new Date());
			platform.setAddBy(i.getId());
			platformDao.add(platform);
		} else {
			platformDao.updById(platform);
		}

		return 1;
	}
}
