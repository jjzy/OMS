package com.zzzzzz.sys.platform;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import javax.annotation.Resource;
import org.springframework.stereotype.Repository;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.utils.page.Page;

import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class PlatformDao {
	
	@Resource
	private BaseDao baseDao;
			
	private final static String sql_add = "insert into sys_platform(cd, name, shortName, descr, cityId, linkMan, phone, fax, email, addr, postCd, sortNb, addDt, addBy, st) values(:cd, :name, :shortName, :descr, :cityId, :linkMan, :phone, :fax, :email, :addr, :postCd, :sortNb, :addDt, :addBy, :st)";
	private final static String sql_updStByIds = "update sys_platform set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update sys_platform set cd=:cd, name=:name, shortName=:shortName, descr=:descr, cityId=:cityId, linkMan=:linkMan, phone=:phone, fax=:fax, email=:email, addr=:addr, postCd=:postCd, sortNb=:sortNb, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, cd, name, shortName, descr, cityId, linkMan, phone, fax, email, addr, postCd, sortNb, addDt, addBy, updDt, updBy, st from sys_platform where id = :id";
	
	public Long add(Platform platform){
		return baseDao.updateGetLongKey(sql_add, platform);
	}
	public int updStByIds(List<Long> ids, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}		  
	public int updById(Platform platform){
		return baseDao.update(sql_upd, platform);
	}
	public Platform findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, Platform.class);
	}
	
	/**
	 * 动态查询
	 */
	public List<Map<String, Object>> findListBy(Map<String, Object> ffMap){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append(" p.id, p.cd, p.name, p.shortName, p.descr, p.cityId, p.linkMan, p.phone, p.fax, p.email, p.addr, p.postCd, p.sortNb, p.addDt, p.addBy, p.updDt, p.updBy, p.st, c.name as cityName");
		finder.append(" from sys_platform p inner join sys_city c on p.cityId = c.id");
		finder.append(" where 1=1");
		if(DaoUtils.isNotNull(ffMap.get("cd"))){
			finder.append(" and p.cd = :cd").setParam("cd", ffMap.get("cd"));
		}
		if(DaoUtils.isNotNull(ffMap.get("name"))){
			finder.append(" and p.name like :name").setParam("name", "%" + ffMap.get("name") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("cityId"))){
			finder.append(" and p.cityId = :cityId").setParam("cityId", ffMap.get("cityId"));
		}
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and p.st = :st").setParam("st", ffMap.get("st"));
		}else{
			finder.append(" and p.st = 0");
		}
		finder.append("order by p.id desc");		
		List<Map<String, Object>> list = baseDao.findList(finder);
				
		return list;
	}
}
