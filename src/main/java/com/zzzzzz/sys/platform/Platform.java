package com.zzzzzz.sys.platform;

import java.util.Date;

/**
 * @author hing
 * @version 1.0.0
 */
public class Platform {
	
  	private Long id;
  	private String cd;
  	private String name;
  	private String shortName;
  	private String descr;
  	private Long cityId;
  	private String linkMan;
  	private String phone;
  	private String fax;
  	private String email;
  	private String addr;
  	private String postCd;
  	private Integer sortNb;
  	private Date addDt;
  	private Long addBy;
  	private Date updDt;
  	private Long updBy;
  	private Integer st;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public String getCd(){
  		return cd;
  	}
  	public void setCd(String cd) {
		this.cd = cd;
	}
  	public String getName(){
  		return name;
  	}
  	public void setName(String name) {
		this.name = name;
	}
  	public String getShortName(){
  		return shortName;
  	}
  	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
  	public String getDescr(){
  		return descr;
  	}
  	public void setDescr(String descr) {
		this.descr = descr;
	}
  	public Long getCityId(){
  		return cityId;
  	}
  	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
  	public String getLinkMan(){
  		return linkMan;
  	}
  	public void setLinkMan(String linkMan) {
		this.linkMan = linkMan;
	}
  	public String getPhone(){
  		return phone;
  	}
  	public void setPhone(String phone) {
		this.phone = phone;
	}
  	public String getFax(){
  		return fax;
  	}
  	public void setFax(String fax) {
		this.fax = fax;
	}
  	public String getEmail(){
  		return email;
  	}
  	public void setEmail(String email) {
		this.email = email;
	}
  	public String getAddr(){
  		return addr;
  	}
  	public void setAddr(String addr) {
		this.addr = addr;
	}
  	public String getPostCd(){
  		return postCd;
  	}
  	public void setPostCd(String postCd) {
		this.postCd = postCd;
	}
  	public Integer getSortNb(){
  		return sortNb;
  	}
  	public void setSortNb(Integer sortNb) {
		this.sortNb = sortNb;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
  	public Integer getSt(){
  		return st;
  	}
  	public void setSt(Integer st) {
		this.st = st;
	}
}


