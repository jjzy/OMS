package com.zzzzzz.sys.platform;

import java.util.List;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.utils.page.Page;

import com.zzzzzz.oms.GCS;

import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class PlatformWeb {

	@Resource
	public PlatformService platformService;
	@Resource
	public PlatformDao platformDao;

	@RequestMapping(value = "/platform/list", method = RequestMethod.GET)
	public String list() {
		return "sys/platform";
	}

	@RequestMapping(value = "/platform/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid Platform platform, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}

		platformService.save(platform, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/platform/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		platformDao.updStByIds(ids, st, ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	
	
	@RequestMapping(value = "/platform/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		Platform platform = platformDao.findById(id);
		return new BaseData(platform, null);
	}
	

	@RequestMapping(value = "/platform/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		List<Map<String, Object>> list = platformDao.findListBy(baseQueryForm.getFfMap());
		return new BaseData(list, null);
	}
	
}
