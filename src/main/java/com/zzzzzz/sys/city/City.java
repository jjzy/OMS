package com.zzzzzz.sys.city;

import java.io.Serializable;
import java.util.Date;

import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class City implements Serializable {
	
	
	
  	private Long id;
	
  	@ExcelField(title = "代码*", align = 2, sort = 10)
  	private String cd;
	
  	@ExcelField(title = "名称*", align = 2, sort = 20)
  	private String name;
	
  	@ExcelField(title = "省份", align = 2, sort = 30)
  	private String province;
	
  	@ExcelField(title = "描述", align = 2, sort = 40)
  	private String descr;
	
  	private Integer sortNb;
	
  	private Date addDt;
	
	
  	private Long addBy;
	
	
  	private Date updDt;
	
	
  	private Long updBy;
	
	
  	private Integer st;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public String getCd(){
  		return cd;
  	}
  	public void setCd(String cd) {
		this.cd = cd;
	}
  	public String getName(){
  		return name;
  	}
  	public void setName(String name) {
		this.name = name;
	}
  	public String getProvince(){
  		return province;
  	}
  	public void setProvince(String province) {
		this.province = province;
	}
  	public String getDescr(){
  		return descr;
  	}
  	public void setDescr(String descr) {
		this.descr = descr;
	}
  	public Integer getSortNb(){
  		return sortNb;
  	}
  	public void setSortNb(Integer sortNb) {
		this.sortNb = sortNb;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
  	public Integer getSt(){
  		return st;
  	}
  	public void setSt(Integer st) {
		this.st = st;
	}
 	
}


