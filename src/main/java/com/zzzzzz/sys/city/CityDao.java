package com.zzzzzz.sys.city;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.sys.client.Client;
import com.zzzzzz.utils.comp.CityChineseComp;
import com.zzzzzz.utils.comp.ClientChineseComp;
import com.zzzzzz.utils.page.Page;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class CityDao {
	
	@Resource
	private BaseDao baseDao;
			
	private final static String sql_add = "insert into sys_city(cd, name, province, descr, sortNb, addDt, addBy, st) values(:cd, :name, :province, :descr, :sortNb, :addDt, :addBy, :st)";
	private final static String sql_updStByIds = "update sys_city set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update sys_city set cd=:cd, name=:name, province=:province, descr=:descr, sortNb=:sortNb, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, cd, name, province, descr, sortNb, addDt, addBy, updDt, updBy, st from sys_city where id = :id";
	
	@CacheEvict(value = "cityCache", allEntries = true)
	public Long add(City city){
		return baseDao.updateGetLongKey(sql_add, city);
	}
	
	@CacheEvict(value = "cityCache", allEntries = true)
	public int updStByIds(List<Long> ids, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}
	
	@CacheEvict(value = "cityCache", allEntries = true)
	public int updById(City city){
		return baseDao.update(sql_upd, city);
	}
	public City findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, City.class);
	}
	
	/**
	 * 动态查询
	 */
	public List<City> findListBy(Page page,Map<String, Object> ffMap){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("count(1) as tt");
		finder.append(" from sys_city");
		finder.append(" where 1=1");
		if(DaoUtils.isNotNull(ffMap.get("cd"))){
			finder.append(" and cd = :cd").setParam("cd", ffMap.get("cd"));
		}
		if(DaoUtils.isNotNull(ffMap.get("name"))){
			finder.append(" and name like :name").setParam("name", "%" + ffMap.get("name") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("province"))){
			finder.append(" and province like :province").setParam("province", "%" +ffMap.get("province") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and st = :st").setParam("st", ffMap.get("st"));
		}else{
			finder.append(" and st = 0");
		}
		finder.append("order by id desc");
		if (page != null) {
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}
		String sql = finder.getSql().replace("count(1) as tt","id, cd, name, province, descr, sortNb, addDt, addBy, updDt, updBy, st");
		finder.setSql(sql);			
		List<City> list = baseDao.findList(finder, City.class);
				
		return list;
	}
	
	/*
	 * 实现按照中文首字母排序
	 */
	public List<City> findListByFirst(Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("id, cd, name, province, descr, sortNb, addDt, addBy, updDt, updBy, st");
		finder.append(" from sys_city");
		finder.append(" where 1=1");
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and st = :st").setParam("st", ffMap.get("st"));
		}		
		List<City> list = baseDao.findList(finder, City.class);
		Comparator cm=new CityChineseComp(); 
		Collections.sort(list, cm);
		return list;
	}
	/*
	 * 城市缓存，把所有城市缓存。在没有缓存的情况下才查询数据库。
	 */
	@Cacheable(value = "cityCache")
	public Map<String, City> getAllCityMap() {
		Map<String, City> cityMap= new HashMap<String, City>();
		List<City> allCityList = findAllBy();
		for (City city : allCityList) {
			cityMap.put(city.getCd(), city);
		}
		return cityMap;
	}
	
	/*
	 * 缓存
	 */
	
	public List<City> findAllBy() {
		Finder finder = new Finder("");
		finder.append("select");
		finder.append("id, cd, name, province, descr, sortNb, addDt, addBy, updDt, updBy, st");
		finder.append("from sys_city");
		List<City> list = baseDao.findList(finder, City.class);
		return list;
	}
}
