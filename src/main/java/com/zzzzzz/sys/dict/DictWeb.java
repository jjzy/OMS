package com.zzzzzz.sys.dict;

import java.util.List;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.sys.client.Client;
import com.zzzzzz.utils.page.Page;
import com.zzzzzz.oms.GCS;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class DictWeb {

	@Resource
	public DictService dictService;
	@Resource
	public DictDao dictDao;

	@RequestMapping(value = "/dict/list", method = RequestMethod.GET)
	public String list() {
		return "sys/dict";
	}

	@RequestMapping(value = "/dict/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid Dict dict, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}

		dictService.save(dict, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/dict/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		dictDao.updStByIds(ids, st, ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	
	
	@RequestMapping(value = "/dict/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		Dict dict = dictDao.findById(id);
		return new BaseData(dict, null);
	}
	

	@RequestMapping(value = "/dict/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		Page page;
		List<Dict> list;
		BaseData baseData;
		if(baseQueryForm.getPageNb()==null&&baseQueryForm.getPageSize()==null){
			list = dictDao.findListBy(null,baseQueryForm.getFfMap());
			baseData=new BaseData(list, null);
		}else{
			page=baseQueryForm.getPage();
			list = dictDao.findListBy(page,baseQueryForm.getFfMap());
			baseData=new BaseData(list, page.getTotal());
		}
		return baseData;
	}
	
	@RequestMapping(value = "/dict/findGroupByCd", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findGroupByCd() {
		List<String> list = dictDao.findGroupByCd();
		return new BaseData(list, null);
	}
	
}
