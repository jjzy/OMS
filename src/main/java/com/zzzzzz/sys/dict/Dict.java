package com.zzzzzz.sys.dict;

import java.util.Date;

/**
 * @author hing
 * @version 1.0.0
 */
public class Dict {
	
  	private Long id;
  	private String cd;
  	private String lb;
  	private String val;
  	private String descr;
  	private String remark;
  	private Integer sortNb;
  	private Date addDt;
  	private Long addBy;
  	private Date updDt;
  	private Long updBy;
  	private Integer st;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public String getCd(){
  		return cd;
  	}
  	public void setCd(String cd) {
		this.cd = cd;
	}
  	public String getLb(){
  		return lb;
  	}
  	public void setLb(String lb) {
		this.lb = lb;
	}
  	public String getVal(){
  		return val;
  	}
  	public void setVal(String val) {
		this.val = val;
	}
  	public String getDescr(){
  		return descr;
  	}
  	public void setDescr(String descr) {
		this.descr = descr;
	}
  	public String getRemark(){
  		return remark;
  	}
  	public void setRemark(String remark) {
		this.remark = remark;
	}
  	public Integer getSortNb(){
  		return sortNb;
  	}
  	public void setSortNb(Integer sortNb) {
		this.sortNb = sortNb;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
  	public Integer getSt(){
  		return st;
  	}
  	public void setSt(Integer st) {
		this.st = st;
	}
}


