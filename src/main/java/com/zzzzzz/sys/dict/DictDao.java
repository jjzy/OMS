package com.zzzzzz.sys.dict;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Repository;

import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.sys.client.Client;
import com.zzzzzz.utils.page.Page;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class DictDao {

	@Resource
	private BaseDao baseDao;

	private final static String sql_add = "insert into sys_dict(cd, lb, val, descr, remark, sortNb, addDt, addBy, updDt, updBy, st) values(:cd, :lb, :val, :descr, :remark, :sortNb, :addDt, :addBy, :updDt, :updBy, :st)";
	private final static String sql_updStByIds = "update sys_dict set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update sys_dict set cd=:cd, lb=:lb, val=:val, descr=:descr, remark=:remark, sortNb=:sortNb, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, cd, lb, val, descr, remark, sortNb, addDt, addBy, updDt, updBy, st from sys_dict where id = :id";
	private final static String sql_findByName="select*from sys_dict where val=:val";

	
	public Long add(Dict dict) {
		return baseDao.updateGetLongKey(sql_add, dict);
	}

	
	public int updStByIds(List<Long> ids, Integer st, I i) {
		Finder finder = new Finder(sql_updStByIds)
		.setParam("ids", ids)
		.setParam("st", st)
		.setParam("updDt", new Date())
		.setParam("updBy", i.getId());
		return baseDao.update(finder);
	}

	public int updById(Dict dict) {
		return baseDao.update(sql_upd, dict);
	}

	public Dict findById(Long id) {
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, Dict.class);
	}

	/**
	 * 动态查询
	 */
	public List<Dict> findListBy(Page page, Map<String, Object> ffMap) {
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append(" count(1) as tt");
		finder.append(" from sys_dict");
		finder.append(" where 1=1");
		if (ffMap != null) {
			if (DaoUtils.isNotNull(ffMap.get("cd"))) {
				finder.append(" and cd = :cd").setParam("cd", ffMap.get("cd"));
			}
			if (DaoUtils.isNotNull(ffMap.get("lb"))) {
				finder.append(" and lb like :lb").setParam("lb", "%" + ffMap.get("lb") + "%");
			}
			if (DaoUtils.isNotNull(ffMap.get("st"))) {
				finder.append(" and st = :st").setParam("st", ffMap.get("st"));
			}
		}
		finder.append(" order by sortNb");

		if(page != null){
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}
		String sql = finder.getSql().replace("count(1) as tt", "id, cd, lb, val, descr, remark, sortNb, addDt, addBy, updDt, updBy, st");
		finder.setSql(sql);
		List<Dict> list = baseDao.findList(finder, Dict.class);

		return list;
	}
	/**
	 * 查找字典表中所有标签
	 */
	public List<String> findGroupByCd() {
		Finder finder = new Finder("");
		finder.append(" select cd from sys_dict group by cd");

		List<String> list = baseDao.findListSingleColumn(finder, String.class);

		return list;
	}
	
	public String findDictNameBy(String val){
		Finder finder = new Finder(sql_findByName).setParam("val", val);
		Dict dict= baseDao.findOne(finder, Dict.class);
		return dict.getDescr();
	}
}
