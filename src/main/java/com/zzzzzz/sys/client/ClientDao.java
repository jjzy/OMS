package com.zzzzzz.sys.client;

import java.text.Collator;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.zzzzzz.oms.ordertype.OrderType;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.sys.city.City;
import com.zzzzzz.sys.client.Client;
import com.zzzzzz.sys.dict.Dict;
import com.zzzzzz.utils.comp.ClientChineseComp;
import com.zzzzzz.utils.page.Page;
import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class ClientDao {

	@Resource
	private BaseDao baseDao;

	private final static String sql_add = "insert into t_client(cd, name, platformId, shortName, descr, cityId, linkMan, phone, fax, email, addr, postCd, sortNb, addDt, addBy, st) values(:cd, :name, :platformId, :shortName, :descr, :cityId, :linkMan, :phone, :fax, :email, :addr, :postCd, :sortNb, :addDt, :addBy, :st)";
	private final static String sql_updStByIds = "update t_client set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update t_client set cd=:cd, name=:name, platformId=:platformId, shortName=:shortName, descr=:descr, cityId=:cityId, linkMan=:linkMan, phone=:phone, fax=:fax, email=:email, addr=:addr, postCd=:postCd, sortNb=:sortNb, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, cd, name, platformId, shortName, descr, cityId, linkMan, phone, fax, email, addr, postCd, sortNb, addDt, addBy, updDt, updBy, st from t_client where id = :id";
	@CacheEvict(value = "clientCache", allEntries = true)
	public Long add(Client client) {
		return baseDao.updateGetLongKey(sql_add, client);
	}
	@CacheEvict(value = "clientCache", allEntries = true)
	public int updStByIds(List<Long> ids, Integer st, I i) {
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st).setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}
	@CacheEvict(value = "clientCache", allEntries = true)
	public int updById(Client client) {
		return baseDao.update(sql_upd, client);
	}

	public Client findById(Long id) {
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, Client.class);
	}

	/**
	 * 动态查询
	 */
	public List<Client> findListBy(Page page,Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("count(1) as tt");
		finder.append(" from t_client c inner join sys_platform p on c.platformId = p.id  left join sys_city s on s.id=c.cityId");
		finder.append(" where 1=1");
		finder.append("and c.platformId=:platformId").setParam("platformId", i.getPlatformId());
		if(DaoUtils.isNotNull(ffMap.get("name"))){
			finder.append(" and c.name like :name").setParam("name", "%" + ffMap.get("name") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("shortName"))){
			finder.append(" and c.shortName like :shortName").setParam("shortName", "%" + ffMap.get("shortName") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and c.st = :st").setParam("st", ffMap.get("st"));
		}else{
			finder.append(" and c.st = 0");
		}
		if(DaoUtils.isNotNull(ffMap.get("platformId"))){
			finder.append(" and c.platformId = :platformId").setParam("platformId", ffMap.get("platformId"));
		}
		if(DaoUtils.isNotNull(ffMap.get("cd"))){
			finder.append(" and c.cd = :cd").setParam("cd", ffMap.get("cd"));
		}
		finder.append("order by c.id desc");
		if (page != null) {
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}
		String sql = finder.getSql().replace("count(1) as tt"," c.*, p.name as platformName,s.name as cityName");
		finder.setSql(sql);		
		List<Client> list = baseDao.findList(finder, Client.class);
		return list;
	}
	/*
	 * 实现按照中文首字母排序
	 */
	public List<Client> findListByFirst(Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("id, cd, name, platformId, shortName, descr, cityId, linkMan, phone, fax, email, addr, postCd, sortNb, addDt, addBy, updDt, updBy, st");
		finder.append(" from t_client c left join t_user_client u on c.id=u.clientId");
		finder.append(" where 1=1");
		finder.append("and c.platformId=:platformId").setParam("platformId", i.getPlatformId());
		finder.append("and u.userId=:userId").setParam("userId", i.getId());
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and c.st = :st").setParam("st", ffMap.get("st"));
		}		
		List<Client> list = baseDao.findList(finder, Client.class);
		Comparator cm=new ClientChineseComp(); 
		Collections.sort(list, cm);
		return list;
	}
	/*
	 * 缓存
	 */
	public List<Client> findListBy(){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("id, cd, name, platformId, shortName, descr, cityId, linkMan, phone, fax, email, addr, postCd, sortNb, addDt, addBy, updDt, updBy, st");
		finder.append("from t_client");
		List<Client> list = baseDao.findList(finder, Client.class);
		return list;
	}
	
	@Cacheable(value = "clientCache")
	public Map<String, List<Client>> getAllClientMap() {
		Map<String, List<Client>> clientMap = new HashMap<String, List<Client>>();
		List<Client> allClientList = findListBy();
		for (Client client : allClientList) {
			List<Client> clientList = clientMap.get(client.getCd());
			if (clientList != null) {
				clientList.add(client);
			} else {
				clientMap.put(client.getCd(), Lists.newArrayList(client));
			}
		}
		return clientMap;
	}
}
