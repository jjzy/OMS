package com.zzzzzz.sys.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.WebUtils;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.sys.city.City;
import com.zzzzzz.utils.page.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzzzzz.oms.GCS;
import com.zzzzzz.oms.receiver.Receiver;
import com.google.common.collect.Lists;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.plugins.poi.ExportExcel;
import com.zzzzzz.plugins.poi.ImportExcel;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class ClientWeb {

	@Resource
	public ClientService clientService;
	@Resource
	public ClientDao clientDao;

	@RequestMapping(value = "/client/list", method = RequestMethod.GET)
	public String list() {
		return "sys/client";
	}

	@RequestMapping(value = "/client/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid Client client, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}

		clientService.save(client, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/client/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		clientDao.updStByIds(ids, st, ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	
	
	@RequestMapping(value = "/client/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		Client client = clientDao.findById(id);
		return new BaseData(client, null);
	}
	

	@RequestMapping(value = "/client/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		Page page;
		List<Client> list;
		BaseData baseData;
		if(baseQueryForm.getPageNb()==null&&baseQueryForm.getPageSize()==null){
			list = clientDao.findListBy(null,baseQueryForm.getFfMap(),ShiroUtils.findUser());
			baseData=new BaseData(list, null);
		}else{
			page=baseQueryForm.getPage();
			list = clientDao.findListBy(page,baseQueryForm.getFfMap(),ShiroUtils.findUser());
			baseData=new BaseData(list, page.getTotal());
		}
		return baseData;
	}
	//按照字母排序
	@RequestMapping(value = "/client/findListByFirst", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListByFirst(@RequestBody BaseQueryForm baseQueryForm) {
			List<Client> list = clientDao.findListByFirst(baseQueryForm.getFfMap(), ShiroUtils.findUser());
			BaseData baseData=new BaseData(list, null);
	
		return baseData;
	}
	
	@RequestMapping(value = "/client/import", method=RequestMethod.POST)
    public String importExcel(@RequestParam MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			if(StringUtils.isBlank(file.getOriginalFilename())){
				List<BaseData> list3=new ArrayList<BaseData>();
				BaseData baseData=new BaseData();
				baseData.setErrMsg("您还没选择文件，请先选择需要导入的文件！");
				list3.add(baseData);
				redirectAttributes.addFlashAttribute("msg",list3);
			}else{
				ImportExcel importExcel = new ImportExcel(file, 1, 0);
				if(file.getOriginalFilename().contains("客户")){
					List<Client> list = importExcel.getDataList(Client.class);
					List<BaseData> list1 =clientService.batchAdd(list, ShiroUtils.findUser());
					redirectAttributes.addFlashAttribute("msg", list1);
				}else{
					List<BaseData> list2=new ArrayList<BaseData>();
					BaseData baseData=new BaseData();
					baseData.setErrMsg("你导入模板有误，请重新选择模板导入。");
					list2.add(baseData);
					redirectAttributes.addFlashAttribute("msg",list2);
				}
			}			
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "导入数据保存失败。");
			logger.warn("importExcel", e);
		}
		return "redirect:/msg";
    }
    
    @RequestMapping(value = "client/import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "客户数据导入模板.xlsx";
    		List<Client> list = Lists.newArrayList();
    		list.add(Client.newTest());
    		new ExportExcel("客户数据导入模板", Client.class, 2).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "客户数据导入模板下载失败。");
			logger.warn("importFileTemplate", e);
		}
		return "redirect:/msg";
    }
    @RequestMapping(value = "/client/export")
    public String exportFileTemplate(HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
  		try {
  			
  			Map<String, Object> params = WebUtils.getParametersStartingWith(request, null);
  			
  			//Page page=baseQueryForm.getPage();
              String fileName = "客户数据.xlsx";
              
            List<Client> list = clientDao.findListBy(null, params,ShiroUtils.findUser());
            List<Client> list1=new ArrayList<Client>();
      		for(Client client:list){
      			client.setCityCd(client.getCityName());
      			list1.add(client);
      		}
              
              new ExportExcel("客户数据", Client.class, 1).setDataList(list1).write(response, fileName).dispose();
      		return null;
  		} catch (Exception e) {
  			redirectAttributes.addFlashAttribute("msg", "客户数据导出失败。");
  			logger.warn("importFileTemplate", e);
  		}
  		return "redirect:/msg";
      }
    private static final Logger logger = LoggerFactory.getLogger(ClientWeb.class);
}
