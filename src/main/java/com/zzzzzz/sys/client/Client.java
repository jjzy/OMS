package com.zzzzzz.sys.client;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;
import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class Client implements Serializable {

	private Long id;
	@ExcelField(title = "代码*", align = 2, sort = 10)
	@Length(min = 1, max = 30)
	private String cd;
	@ExcelField(title = "名称*", align = 2, sort = 20)
	private String name;
	private Long platformId;
	@ExcelField(title = "缩写*", align = 2, sort = 30)
	private String shortName;
	@ExcelField(title = "城市", align = 2, sort = 40)
	private String cityCd;
	private Long cityId;
	@ExcelField(title = "描述", align = 2, sort = 50)
	private String descr;
	@ExcelField(title = "联系人", align = 2, sort = 60)
	private String linkMan;
	@ExcelField(title = "电话", align = 2, sort = 70)
	private String phone;
	@ExcelField(title = "传真", align = 2, sort = 80)
	private String fax;
	@ExcelField(title = "邮件", align  = 2, sort = 90)
	private String email;
	@ExcelField(title = "地址", align = 2, sort = 100)
	private String addr;
	@ExcelField(title = "邮编", align = 2, sort = 110)
	private String postCd;
	private Integer sortNb;

	private Date addDt;

	private Long addBy;

	private Date updDt;

	private Long updBy;
	
	private Integer st;
	
	private String cityName;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCd() {
		return cd;
	}

	public void setCd(String cd) {
		this.cd = cd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public String getCityCd() {
		return cityCd;
	}

	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}

	public String getLinkMan() {
		return linkMan;
	}

	public void setLinkMan(String linkMan) {
		this.linkMan = linkMan;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getPostCd() {
		return postCd;
	}

	public void setPostCd(String postCd) {
		this.postCd = postCd;
	}

	public Integer getSortNb() {
		return sortNb;
	}

	public void setSortNb(Integer sortNb) {
		this.sortNb = sortNb;
	}

	public Date getAddDt() {
		return addDt;
	}

	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}

	public Long getAddBy() {
		return addBy;
	}

	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}

	public Date getUpdDt() {
		return updDt;
	}

	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}

	public Long getUpdBy() {
		return updBy;
	}

	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}

	public Integer getSt() {
		return st;
	}

	public void setSt(Integer st) {
		this.st = st;
	}

	public Long getPlatformId() {
		return platformId;
	}

	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
	
	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public static Client newTest() {
		Client client = new Client();

		// client.setCd("");
		// client.setName("");
		// client.setShortName("");
		// client.setDescr("");
		// client.setCityId("");
		// client.setLinkMan("");
		// client.setPhone("");
		// client.setFax("");
		// client.setEmail("");
		// client.setAddr("");
		// client.setPostCd("");
		// client.setSortNb("");

		// client.setSt("");
		return client;
	}
}
