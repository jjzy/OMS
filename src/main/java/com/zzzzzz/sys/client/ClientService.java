package com.zzzzzz.sys.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.sys.city.City;
import com.zzzzzz.sys.city.CityService;
import com.zzzzzz.utils.BeanValidators;
import com.zzzzzz.utils.DateTimeUtils;

@Service
public class ClientService {
	@Resource
	public ClientDao clientDao;
	@Resource
	public Validator validator;
	@Resource
	public CityService cityService;
	@Transactional
	public int save(Client client, I i) {
		client.setUpdDt(new Date());
		client.setUpdBy(i.getId());
		if (client.getId() == null) {
			client.setAddDt(new Date());
			client.setAddBy(i.getId());
			client.setPlatformId(i.getPlatformId());
			client.setSortNb(1);
			clientDao.add(client);
			}
		else {
			clientDao.updById(client);
		}
		return 1;
	}
	
	//找cd
	public Client getclientByCd(String cd,I i) {
		Map<String, List<Client>> allClientMap=clientDao.getAllClientMap();
		List<Client> clientList=allClientMap.get(cd);
		if(clientList!=null){
			for(Client client:clientList){
				if(client.getPlatformId().equals(i.getPlatformId())){
					return client;
				}
			}
		}
		return null;
	}
	@Transactional
	public List<BaseData> batchAdd(List<Client> list, I i) {
		logger.info("client batchAdd validate start:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		//start validate
		int errTt = 0;// 错误信息条数
		int susTt = 0;// 正确信息条数
		BaseData baseData;
		//用于存放信息
		List<BaseData> listMsg=new ArrayList<BaseData>();
		for (Client client : list) {
			//验证是否为空值
			if(StringUtils.isBlank(client.getCd())){
				baseData=new BaseData();
				baseData.setErrMsg("代码"+client.getCd()+"为无效值");
				listMsg.add(baseData);
				errTt++;
			}else{									
					if(StringUtils.isBlank(client.getCityCd())){
						//如果没存在就导入
						client.setSt(0);
						if(client.getDescr()==null||client.getDescr().equals("")){
							client.setDescr(null);
						}
						if(client.getEmail()==null||client.getEmail().equals("")){
							client.setEmail(null);
						}
						if(client.getFax()==null||client.getFax().equals("")){
							client.setFax(null);
						}
						if(client.getLinkMan()==null||client.getLinkMan().equals("")){
							client.setLinkMan(null);
						}
						if(client.getPhone()==null||client.getPhone().equals("")){
							client.setPhone(null);
						}
						if(client.getPostCd()==null||client.getPostCd().equals("")){
							client.setPostCd(null);
						}
						if(client.getAddr()==null||client.getAddr().equals("")){
							client.setAddr(null);
						}
						try{
							save(client, i);
							susTt++;
						}catch(DuplicateKeyException e){
							baseData=new BaseData("-1", String.format("客户代码"+client.getCd()+"已经存在"));
							listMsg.add(baseData);
							errTt++;
						}
					
					}else{
						if(cityService.getCityByCd(client.getCityCd())==null){
							baseData=new BaseData();
							baseData.setErrMsg("城市代码"+client.getCityCd()+"不存在");
							listMsg.add(baseData);
							errTt++;
						}else{
							//如果没存在就导入
							client.setSt(0);
							client.setCityId(cityService.getCityByCd(client.getCityCd()).getId());
							if(client.getDescr()==null||client.getDescr().equals("")){
								client.setDescr(null);
							}
							if(client.getEmail()==null||client.getEmail().equals("")){
								client.setEmail(null);
							}
							if(client.getFax()==null||client.getFax().equals("")){
								client.setFax(null);
							}
							if(client.getLinkMan()==null||client.getLinkMan().equals("")){
								client.setLinkMan(null);
							}
							if(client.getPhone()==null||client.getPhone().equals("")){
								client.setPhone(null);
							}
							if(client.getPostCd()==null||client.getPostCd().equals("")){
								client.setPostCd(null);
							}
							if(client.getAddr()==null||client.getAddr().equals("")){
								client.setAddr(null);
							}
							try{
								save(client, i);
								susTt++;
							}catch(DuplicateKeyException e){
								baseData=new BaseData("-1", String.format("客户代码"+client.getCd()+"已经存在"));
								listMsg.add(baseData);
								errTt++;
							}
						}
					}
			}
		}
		baseData = new BaseData();
		baseData.setErrMsg("总共有" + (susTt + errTt) + "行记录；共有" + susTt+ "行记录导入成功；共有" + errTt + "行记录导入失败；");
		listMsg.add(baseData);
		logger.info("client batchAdd save end:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		return listMsg;
	}
	private static final Logger logger = LoggerFactory.getLogger(Client.class);
}
