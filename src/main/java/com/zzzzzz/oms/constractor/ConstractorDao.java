package com.zzzzzz.oms.constractor;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.zzzzzz.oms.receiver.Receiver;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.utils.page.Page;
import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class ConstractorDao {
	
	@Resource
	private BaseDao baseDao;
			
	private final static String sql_add = "insert into t_constractor(cd, name, shortName,platformId, descr, cityId, shipment_method,linkMan, phone, fax, email, addr, postCd, addDt, addBy, st) values(:cd, :name, :shortName, :platformId,:descr, :cityId,:shipment_method, :linkMan, :phone, :fax, :email, :addr, :postCd, :addDt, :addBy, :st)";
	private final static String sql_updStByIds = "update t_constractor set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update t_constractor set cd=:cd, name=:name, shortName=:shortName,platformId=:platformId, descr=:descr, cityId=:cityId, shipment_method=:shipment_method,linkMan=:linkMan, phone=:phone, fax=:fax, email=:email, addr=:addr, postCd=:postCd, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, cd, name, shortName,platformId, descr, cityId,shipment_method, linkMan, phone, fax, email, addr, postCd, addDt, addBy, updDt, updBy, st from t_constractor where id = :id";
	
	public Long add(Constractor constractor){
		return baseDao.updateGetLongKey(sql_add, constractor);
	}
	public int updStByIds(List<Long> ids, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}		  
	public int updById(Constractor constractor){
		return baseDao.update(sql_upd, constractor);
	}
	public Constractor findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, Constractor.class);
	}
	
	/**
	 * 动态查询
	 */
	public List<Constractor> findListBy(Page page,Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("count(1) as tt");
		finder.append(" from t_constractor p inner join sys_city c on p.cityId = c.id inner join sys_dict d on d.descr=p.shipment_method");
		finder.append(" where 1=1");
		finder.append("and p.platformId=:platformId").setParam("platformId", i.getPlatformId());
		if(DaoUtils.isNotNull(ffMap.get("cd"))){
		finder.append(" and p.cd = :cd").setParam("cd", ffMap.get("cd"));
		}
		if(DaoUtils.isNotNull(ffMap.get("name"))){
		finder.append(" and p.name like :name").setParam("name", "%" + ffMap.get("name") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("cityId"))){
		finder.append(" and p.cityId = :cityId").setParam("cityId", ffMap.get("cityId"));
		}
		if(DaoUtils.isNotNull(ffMap.get("st"))){
		finder.append(" and p.st = :st").setParam("st", ffMap.get("st"));
		}else{
			finder.append(" and p.st = 0");
		}
		finder.append("order by p.id desc");	
		if (page != null) {
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}
		String sql = finder.getSql().replace("count(1) as tt","p.id, p.cd, p.name, p.shortName, p.descr, p.cityId, p.linkMan, p.phone, p.fax, p.email, p.addr, p.postCd, p.addDt, p.addBy, p.updDt, p.updBy, p.st, c.name as cityName,d.val as shipment_methodName");
		finder.setSql(sql);
		List<Constractor> list = baseDao.findList(finder, Constractor.class);
		return list;
	}
}
