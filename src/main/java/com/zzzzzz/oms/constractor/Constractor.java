package com.zzzzzz.oms.constractor;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;
import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class Constractor implements Serializable {
	
	
	
  	private Long id;
	@ExcelField(title = "代码", align = 2, sort = 10)
	@Length(min = 1, max = 30)
  	private String cd;
	@ExcelField(title = "名称", align = 2, sort = 20)
	
  	private String name;
	@ExcelField(title = "简称", align = 2, sort = 30)
	
  	private String shortName;
	@ExcelField(title = "描述", align = 2, sort = 40)
	
  	private String descr;
	@ExcelField(title = "城市", align = 2, sort = 50)
	
  	private Long cityId;
	@ExcelField(title = "联系人", align = 2, sort = 60)
	
  	private String linkMan;
	@ExcelField(title = "电话", align = 2, sort = 70)
	
  	private String phone;
	@ExcelField(title = "传真", align = 2, sort = 80)
	
  	private String fax;
	@ExcelField(title = "邮件", align = 2, sort = 90)
	
  	private String email;
	@ExcelField(title = "地址", align = 2, sort = 100)
	
  	private String addr;
	@ExcelField(title = "邮编", align = 2, sort = 110)
	
  	private String postCd;
	@ExcelField(title = "排序值", align = 2, sort = 120)
	private String shipment_method;
	private String shipment_methodName;
	
	private Long platformId;
	
	private String cityName;

	private Date addDt;
	
	
  	private Long addBy;
	
	
  	private Date updDt;
	
	
  	private Long updBy;
	@ExcelField(title = "状态", align = 2, sort = 130)
	
  	private Integer st;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public String getCd(){
  		return cd;
  	}
  	public void setCd(String cd) {
		this.cd = cd;
	}
  	public String getName(){
  		return name;
  	}
  	public void setName(String name) {
		this.name = name;
	}
  	public String getShortName(){
  		return shortName;
  	}
  	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
  	public String getDescr(){
  		return descr;
  	}
  	public void setDescr(String descr) {
		this.descr = descr;
	}
  	public Long getCityId(){
  		return cityId;
  	}
  	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
  	public String getLinkMan(){
  		return linkMan;
  	}
  	public void setLinkMan(String linkMan) {
		this.linkMan = linkMan;
	}
  	public String getPhone(){
  		return phone;
  	}
  	public void setPhone(String phone) {
		this.phone = phone;
	}
  	public String getFax(){
  		return fax;
  	}
  	public void setFax(String fax) {
		this.fax = fax;
	}
  	public String getEmail(){
  		return email;
  	}
  	public void setEmail(String email) {
		this.email = email;
	}
  	public String getAddr(){
  		return addr;
  	}
  	public void setAddr(String addr) {
		this.addr = addr;
	}
  	public String getPostCd(){
  		return postCd;
  	}
  	public void setPostCd(String postCd) {
		this.postCd = postCd;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
  	public Integer getSt(){
  		return st;
  	}
  	public void setSt(Integer st) {
		this.st = st;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public Long getPlatformId() {
		return platformId;
	}
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
	
 	public String getShipment_method() {
		return shipment_method;
	}
	public void setShipment_method(String shipment_method) {
		this.shipment_method = shipment_method;
	}
	
	public String getShipment_methodName() {
		return shipment_methodName;
	}
	public void setShipment_methodName(String shipment_methodName) {
		this.shipment_methodName = shipment_methodName;
	}
	public static Constractor newTest() {
		Constractor constractor = new Constractor();
		
		//constractor.setCd("");
		//constractor.setName("");
		//constractor.setShortName("");
		//constractor.setDescr("");
		//constractor.setCityId("");
		//constractor.setLinkMan("");
		//constractor.setPhone("");
		//constractor.setFax("");
		//constractor.setEmail("");
		//constractor.setAddr("");
		//constractor.setPostCd("");
		//constractor.setSortNb("");
		
		
		
		
		//constractor.setSt("");
		return constractor;
	}
}


