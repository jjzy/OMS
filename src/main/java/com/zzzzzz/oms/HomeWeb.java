package com.zzzzzz.oms;

import java.util.Locale;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.sys.platform.Platform;
import com.zzzzzz.sys.platform.PlatformDao;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeWeb {

	@Resource
	private PlatformDao platformDao;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String test(Locale locale, Model model) {
		return "home";
	}
	
	@RequestMapping(value = "/blank", method = RequestMethod.GET)
	public String blank(Locale locale, Model model) {
		return "blank";
	}

	@RequestMapping(value = "/msg", method = RequestMethod.GET)
	public String msg() {
		return "/msg";
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String home(Model model) {
		Long userId = ShiroUtils.findUserId();

		return "home";
	}

	@RequestMapping(value = "/switchPlatform/{platformId}", method = RequestMethod.GET)
	public String switchPlatform(@PathVariable Long platformId) {
		I i = ShiroUtils.findUser();
		i.setPlatformId(platformId);

		Platform platform = platformDao.findById(platformId);
		i.setPlatformName(platform.getName());
		
		return "redirect:/";
	}

	private static final Logger logger = LoggerFactory.getLogger(HomeWeb.class);
}
