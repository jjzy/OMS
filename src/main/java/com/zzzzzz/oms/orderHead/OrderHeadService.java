package com.zzzzzz.oms.orderHead;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.zzzzzz.oms.legs.Legs;
import com.zzzzzz.oms.legs.LegsDao;
import com.zzzzzz.oms.legs.LegsService;
import com.zzzzzz.oms.ordertype.OrderTypeDao;
import com.zzzzzz.oms.orderHead.OrderHead;
import com.zzzzzz.oms.receiver.Receiver;
import com.zzzzzz.oms.shipment.Shipment;
import com.zzzzzz.oms.tempShipment.TempShipment;
import com.zzzzzz.oms.tempShipment.TempShipmentDao;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.sys.client.ClientDao;
import com.zzzzzz.sys.client.ClientService;
import com.zzzzzz.sys.dict.Dict;
import com.zzzzzz.sys.dict.DictDao;

@Service
public class OrderHeadService {
	@Resource
	public OrderHeadDao orderHeadDao;
	@Resource
	public Validator validator;
	@Resource
	public ClientDao clientDao;
	@Resource
	public OrderTypeDao orderTypeDao;
	@Resource
	public DictDao dictDao;
	@Resource
	public ClientService clientService;
	@Resource
	public LegsService legsService;
	@Resource
	public LegsDao legsDao;
	@Resource
	public TempShipmentDao tempShipmentDao;
	public OrderHeadService() {
	}
	@Transactional
	public Long save(OrderHead orderHead, I i) {
		orderHead.setUpdDt(new Date());
		orderHead.setUpdBy(i.getId());
		if (orderHead.getId() == null) {
			orderHead.setAddDt(new Date());
			orderHead.setAddBy(i.getId());
			orderHead.setPlatformId(i.getPlatformId());
			Long id=orderHeadDao.add(orderHead);
			return id;
		} else {
			orderHeadDao.updById(orderHead);
		}
		return (long) 1;
	}
	//找cd
	public OrderHead getOrderHeadByCd(String cd,Long clientId) {
		Map<String, List<OrderHead>> allOrderHeadMap=orderHeadDao.getAllOrderHeadMap();
		List<OrderHead> orderHeadList=allOrderHeadMap.get(cd);
		if(orderHeadList!=null){
			for(OrderHead orderHead:orderHeadList){
				if(orderHead.getClientId().equals(clientId)){
					return orderHead;
				}
			}
		}
		return null;
	}
	//处理生效之后的数据
	public void arrived(List<OrderHead> list,List<Long> ids,List<String> listVocation,I i,String code){
		//List<OrderHead> list =orderHeadDao.findByIds(ids);
		saveLegs(list, i);
		saveTemp(list, listVocation, i, code);
	}
	
	//订单失效
	public void back(List<Long> ids,List<Long> tempShipmentIds,List<Legs> legsList,I i){
		//List<OrderHead> list =orderHeadDao.findByIds(ids);
		legsDao.delByIds(ids);
		changeTemp(tempShipmentIds, legsList, i);
	}
	//生成分段订单
	public void saveLegs(List<OrderHead> listOrder,I i){
		List<Legs> legsList=new ArrayList<Legs>();
		for(OrderHead orderHead:listOrder){
			Legs legs=legsService.add(orderHead, i);
			legsList.add(legs);
		}
		legsDao.saveOrder(legsList);
	}
	//生成高级调度
	public void saveTemp(List<OrderHead> listOrder,List<String> listVocation,I i,String code){
		List<TempShipment> list=tempShipmentDao.findByVocations(code, listVocation, i);
		for(TempShipment tempShipment:list){
			String vocationType=tempShipment.getVocationType();
			Double quantity=0.0;
			Double weight=0.0;
			Double volume=0.0;
			Integer points=0;
			for(OrderHead orderHead:listOrder){
				if(orderHead.getVocation().equals(vocationType)){
					quantity+=orderHead.getQuantity();
					weight+=orderHead.getWeight();
					volume+=orderHead.getVolume();
					points++;
				}
			}
			Integer count=legsDao.findCount(tempShipment.getId());
			tempShipmentDao.updStById(tempShipment.getId(), tempShipment.getQuantity()+quantity, tempShipment.getPoints()+points, tempShipment.getWeight()+weight, tempShipment.getVolume()+volume, count, i);
		}
	}
	
	public void changeTemp(List<Long> ids,List<Legs> legsList,I i){
		List<TempShipment> list=tempShipmentDao.findByIds(ids);
		for(TempShipment tempShipment:list){
			Double quantity=0.0;
			Double weight=0.0;
			Double volume=0.0;
			Integer points=0;
			for(Legs legs:legsList){
				if(tempShipment.getId().equals(legs.getTempshipmentId())){
					quantity+=legs.getQuantity();
					weight+=legs.getWeight();
					volume+=legs.getVolume();
					points++;
				}
			}
			Integer count=legsDao.findCount(tempShipment.getId());
			tempShipmentDao.updStById(tempShipment.getId(), tempShipment.getQuantity()-quantity, tempShipment.getPoints()-points, tempShipment.getWeight()-weight, tempShipment.getVolume()-volume, count, i);
		}
	}
	
	private static final Logger logger = LoggerFactory.getLogger(OrderHead.class);
}
