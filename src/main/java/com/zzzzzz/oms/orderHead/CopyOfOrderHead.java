package com.zzzzzz.oms.orderHead;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class CopyOfOrderHead implements Serializable {

	@ExcelField(title = "客户", align = 2, sort = 10)
	private String clientName;
	@ExcelField(title = "订单号", align = 2, sort = 20)
	private String cd;
	@ExcelField(title = "运输方式", align = 2, sort = 30)
	private String shipmentMethodName;
	@ExcelField(title = "业务类型", align = 2, sort = 40)
	private String vocationName;
	@ExcelField(title = "状态", align = 2, sort = 50)
	private String statusName;
	@ExcelField(title = "订单类型", align = 2, sort = 60)
	private String typeName;
	@ExcelField(title = "开单时间", align = 2, sort = 70)
	private Date orderDt;
	@ExcelField(title = "计划出发时间", align = 2, sort = 80)
	private Date planlDt;
	@ExcelField(title = "计划到达时间", align = 2, sort = 90)
	private Date planaDt;
	@ExcelField(title = "计费周期", align = 2, sort = 120)
	private Date billingDt;
	@ExcelField(title = "数量", align = 2, sort = 130)
	private Double quantity;
	@ExcelField(title = "重量", align = 2, sort = 140)
	private Double weight;
	@ExcelField(title = "体积", align = 2, sort = 150)
	private Double volume;
	@ExcelField(title = "托数", align = 2, sort = 160)
	private Double palletsum;
	@ExcelField(title = "一口价", align = 2, sort = 170)
	private Double expense;
	@ExcelField(title = "发货方代码", align = 2, sort = 180)
	private String freceiverCd;
	@ExcelField(title = "发货方名称", align = 2, sort = 190)
	private String freceiverName;
	@ExcelField(title = "出发地", align = 2, sort = 210)
	private String ftranslocationName;
	@ExcelField(title = "发货方联系人", align = 2, sort = 220)
	private String freceiverLikename;
	@ExcelField(title = "发货方联系电话", align = 2, sort = 230)
	private String freceiverPhone;
	@ExcelField(title = "发货方传真", align = 2, sort = 240)
	private String freceiverFax;
	@ExcelField(title = "发货方邮箱", align = 2, sort = 250)
	private String freceiverEmail;
	@ExcelField(title = "发货方邮编", align = 2, sort = 260)
	private String freceiverPostcode;
	@ExcelField(title = "发货方地址", align = 2, sort = 270)
	private String freceiverAddress;
	@ExcelField(title = "收货方代码", align = 2, sort = 310)
	private String treceiverCd;
	@ExcelField(title = "收货方名称", align = 2, sort = 320)
	private String treceiverName;
	@ExcelField(title = "目的地", align = 2, sort = 320)
	private String ttranslocationName;
	@ExcelField(title = "收货方联系人", align = 2, sort = 340)
	private String treceiverLikename;
	@ExcelField(title = "收货方联系电话", align = 2, sort = 350)
	private String treceiverPhone;
	@ExcelField(title = "收货方传真", align = 2, sort = 360)
	private String treceiverFax;
	@ExcelField(title = "收货方邮箱", align = 2, sort = 370)
	private String treceiverEmail;
	@ExcelField(title = "收货方邮编", align = 2, sort = 380)
	private String treceiverPostcode;
	@ExcelField(title = "收货方地址", align = 2, sort = 390)
	private String treceiverAddress;
	private String descr;
	private Long id;
	private Long platformId;
	private String clientCd;
	private String orderTypeCd;
	private String productCd;
	private String lot;
	private String unit;
	private Long lineNo;
	private String shipment_method;
	private String ftranslocationCd;
	private String ttranslocationCd;
	private Long ftranslocationId;
	private Long ttranslocationId;
	private Date addDt;

	private Long addBy;

	private Date updDt;

	private Long updBy;
	// @ExcelField(title = "扩展属性01", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield01;
	// @ExcelField(title = "扩展属性02", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield02;
	// @ExcelField(title = "扩展属性03", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield03;
	// @ExcelField(title = "扩展属性04", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield04;
	// @ExcelField(title = "扩展属性05", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield05;
	// @ExcelField(title = "扩展属性06", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield06;
	// @ExcelField(title = "扩展属性07", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield07;
	// @ExcelField(title = "扩展属性08", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield08;
	// @ExcelField(title = "扩展属性09", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield09;
	// @ExcelField(title = "扩展属性10", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield10;
	// @ExcelField(title = "扩展属性11", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield11;
	// @ExcelField(title = "扩展属性12", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield12;
	// @ExcelField(title = "扩展属性13", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield13;
	// @ExcelField(title = "扩展属性14", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield14;
	// @ExcelField(title = "扩展属性15", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield15;
	// @ExcelField(title = "扩展属性16", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield16;
	// @ExcelField(title = "扩展属性17", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield17;
	// @ExcelField(title = "扩展属性18", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield18;
	// @ExcelField(title = "扩展属性19", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield19;
	// @ExcelField(title = "扩展属性20", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield20;
	// @ExcelField(title = "扩展属性21", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield21;
	// @ExcelField(title = "扩展属性22", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield22;
	// @ExcelField(title = "扩展属性23", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield23;
	// @ExcelField(title = "扩展属性24", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield24;
	// @ExcelField(title = "扩展属性25", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield25;
	// @ExcelField(title = "扩展属性26", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield26;
	// @ExcelField(title = "扩展属性27", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield27;
	// @ExcelField(title = "扩展属性28", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield28;
	// @ExcelField(title = "扩展属性29", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield29;
	// @ExcelField(title = "扩展属性30", align = 2, sort = 60)
	@Length(min = 1, max = 100)
	private String extendfield30;
	private String status;

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getProductCd() {
		return productCd;
	}

	public void setProductCd(String productCd) {
		this.productCd = productCd;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public Long getLineNo() {
		return lineNo;
	}

	public void setLineNo(Long lineNo) {
		this.lineNo = lineNo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCd() {
		return cd;
	}

	public void setCd(String cd) {
		this.cd = cd;
	}

	public Date getOrderDt() {
		return orderDt;
	}

	public void setOrderDt(Date orderDt) {
		this.orderDt = orderDt;
	}

	public Date getPlanlDt() {
		return planlDt;
	}

	public void setPlanlDt(Date planlDt) {
		this.planlDt = planlDt;
	}

	public Date getPlanaDt() {
		return planaDt;
	}

	public void setPlanaDt(Date planaDt) {
		this.planaDt = planaDt;
	}

	public Date getBillingDt() {
		return billingDt;
	}

	public void setBillingDt(Date billingDt) {
		this.billingDt = billingDt;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Double getPalletsum() {
		return palletsum;
	}

	public void setPalletsum(Double palletsum) {
		this.palletsum = palletsum;
	}

	public Double getExpense() {
		return expense;
	}

	public void setExpense(Double expense) {
		this.expense = expense;
	}

	public String getFreceiverCd() {
		return freceiverCd;
	}

	public void setFreceiverCd(String freceiverCd) {
		this.freceiverCd = freceiverCd;
	}

	public String getFreceiverName() {
		return freceiverName;
	}

	public void setFreceiverName(String freceiverName) {
		this.freceiverName = freceiverName;
	}

	public String getFtranslocationCd() {
		return ftranslocationCd;
	}

	public void setFtranslocationCd(String ftranslocationCd) {
		this.ftranslocationCd = ftranslocationCd;
	}

	public String getTtranslocationCd() {
		return ttranslocationCd;
	}

	public void setTtranslocationCd(String ttranslocationCd) {
		this.ttranslocationCd = ttranslocationCd;
	}

	public String getFreceiverLikename() {
		return freceiverLikename;
	}

	public void setFreceiverLikename(String freceiverLikename) {
		this.freceiverLikename = freceiverLikename;
	}

	public String getFreceiverPhone() {
		return freceiverPhone;
	}

	public void setFreceiverPhone(String freceiverPhone) {
		this.freceiverPhone = freceiverPhone;
	}
	
	public String getFtranslocationName() {
		return ftranslocationName;
	}

	public void setFtranslocationName(String ftranslocationName) {
		this.ftranslocationName = ftranslocationName;
	}

	public String getTtranslocationName() {
		return ttranslocationName;
	}

	public void setTtranslocationName(String ttranslocationName) {
		this.ttranslocationName = ttranslocationName;
	}

	public String getFreceiverFax() {
		return freceiverFax;
	}

	public void setFreceiverFax(String freceiverFax) {
		this.freceiverFax = freceiverFax;
	}

	public String getFreceiverEmail() {
		return freceiverEmail;
	}

	public void setFreceiverEmail(String freceiverEmail) {
		this.freceiverEmail = freceiverEmail;
	}

	public String getFreceiverPostcode() {
		return freceiverPostcode;
	}

	public void setFreceiverPostcode(String freceiverPostcode) {
		this.freceiverPostcode = freceiverPostcode;
	}

	public String getFreceiverAddress() {
		return freceiverAddress;
	}

	public void setFreceiverAddress(String freceiverAddress) {
		this.freceiverAddress = freceiverAddress;
	}

	public String getTreceiverCd() {
		return treceiverCd;
	}

	public void setTreceiverCd(String treceiverCd) {
		this.treceiverCd = treceiverCd;
	}

	public String getTreceiverName() {
		return treceiverName;
	}

	public void setTreceiverName(String treceiverName) {
		this.treceiverName = treceiverName;
	}

	public String getVocationName() {
		return vocationName;
	}

	public void setVocationName(String vocationName) {
		this.vocationName = vocationName;
	}

	public Long getFtranslocationId() {
		return ftranslocationId;
	}

	public void setFtranslocationId(Long ftranslocationId) {
		this.ftranslocationId = ftranslocationId;
	}

	public Long getTtranslocationId() {
		return ttranslocationId;
	}

	public void setTtranslocationId(Long ttranslocationId) {
		this.ttranslocationId = ttranslocationId;
	}

	public String getTreceiverLikename() {
		return treceiverLikename;
	}

	public void setTreceiverLikename(String treceiverLikename) {
		this.treceiverLikename = treceiverLikename;
	}

	public String getTreceiverPhone() {
		return treceiverPhone;
	}

	public void setTreceiverPhone(String treceiverPhone) {
		this.treceiverPhone = treceiverPhone;
	}

	public String getTreceiverFax() {
		return treceiverFax;
	}

	public void setTreceiverFax(String treceiverFax) {
		this.treceiverFax = treceiverFax;
	}

	public String getTreceiverEmail() {
		return treceiverEmail;
	}

	public void setTreceiverEmail(String treceiverEmail) {
		this.treceiverEmail = treceiverEmail;
	}

	public String getTreceiverPostcode() {
		return treceiverPostcode;
	}

	public void setTreceiverPostcode(String treceiverPostcode) {
		this.treceiverPostcode = treceiverPostcode;
	}

	public String getTreceiverAddress() {
		return treceiverAddress;
	}

	public void setTreceiverAddress(String treceiverAddress) {
		this.treceiverAddress = treceiverAddress;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getClientCd() {
		return clientCd;
	}

	public void setClientCd(String clientCd) {
		this.clientCd = clientCd;
	}

	public Long getPlatformId() {
		return platformId;
	}

	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}

	public String getShipment_method() {
		return shipment_method;
	}

	public void setShipment_method(String shipment_method) {
		this.shipment_method = shipment_method;
	}
	public String getOrderTypeCd() {
		return orderTypeCd;
	}

	public void setOrderTypeCd(String orderTypeCd) {
		this.orderTypeCd = orderTypeCd;
	}

	public Date getAddDt() {
		return addDt;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}

	public Long getAddBy() {
		return addBy;
	}

	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}

	public Date getUpdDt() {
		return updDt;
	}

	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}

	public Long getUpdBy() {
		return updBy;
	}

	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getShipmentMethodName() {
		return shipmentMethodName;
	}

	public void setShipmentMethodName(String shipmentMethodName) {
		this.shipmentMethodName = shipmentMethodName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getExtendfield01() {
		return extendfield01;
	}

	public void setExtendfield01(String extendfield01) {
		this.extendfield01 = extendfield01;
	}

	public String getExtendfield02() {
		return extendfield02;
	}

	public void setExtendfield02(String extendfield02) {
		this.extendfield02 = extendfield02;
	}

	public String getExtendfield03() {
		return extendfield03;
	}

	public void setExtendfield03(String extendfield03) {
		this.extendfield03 = extendfield03;
	}

	public String getExtendfield04() {
		return extendfield04;
	}

	public void setExtendfield04(String extendfield04) {
		this.extendfield04 = extendfield04;
	}

	public String getExtendfield05() {
		return extendfield05;
	}

	public void setExtendfield05(String extendfield05) {
		this.extendfield05 = extendfield05;
	}

	public String getExtendfield06() {
		return extendfield06;
	}

	public void setExtendfield06(String extendfield06) {
		this.extendfield06 = extendfield06;
	}

	public String getExtendfield07() {
		return extendfield07;
	}

	public void setExtendfield07(String extendfield07) {
		this.extendfield07 = extendfield07;
	}

	public String getExtendfield08() {
		return extendfield08;
	}

	public void setExtendfield08(String extendfield08) {
		this.extendfield08 = extendfield08;
	}

	public String getExtendfield09() {
		return extendfield09;
	}

	public void setExtendfield09(String extendfield09) {
		this.extendfield09 = extendfield09;
	}

	public String getExtendfield10() {
		return extendfield10;
	}

	public void setExtendfield10(String extendfield10) {
		this.extendfield10 = extendfield10;
	}

	public String getExtendfield11() {
		return extendfield11;
	}

	public void setExtendfield11(String extendfield11) {
		this.extendfield11 = extendfield11;
	}

	public String getExtendfield12() {
		return extendfield12;
	}

	public void setExtendfield12(String extendfield12) {
		this.extendfield12 = extendfield12;
	}

	public String getExtendfield13() {
		return extendfield13;
	}

	public void setExtendfield13(String extendfield13) {
		this.extendfield13 = extendfield13;
	}

	public String getExtendfield14() {
		return extendfield14;
	}

	public void setExtendfield14(String extendfield14) {
		this.extendfield14 = extendfield14;
	}

	public String getExtendfield15() {
		return extendfield15;
	}

	public void setExtendfield15(String extendfield15) {
		this.extendfield15 = extendfield15;
	}

	public String getExtendfield16() {
		return extendfield16;
	}

	public void setExtendfield16(String extendfield16) {
		this.extendfield16 = extendfield16;
	}

	public String getExtendfield17() {
		return extendfield17;
	}

	public void setExtendfield17(String extendfield17) {
		this.extendfield17 = extendfield17;
	}

	public String getExtendfield18() {
		return extendfield18;
	}

	public void setExtendfield18(String extendfield18) {
		this.extendfield18 = extendfield18;
	}

	public String getExtendfield19() {
		return extendfield19;
	}

	public void setExtendfield19(String extendfield19) {
		this.extendfield19 = extendfield19;
	}

	public String getExtendfield20() {
		return extendfield20;
	}

	public void setExtendfield20(String extendfield20) {
		this.extendfield20 = extendfield20;
	}

	public String getExtendfield21() {
		return extendfield21;
	}

	public void setExtendfield21(String extendfield21) {
		this.extendfield21 = extendfield21;
	}

	public String getExtendfield22() {
		return extendfield22;
	}

	public void setExtendfield22(String extendfield22) {
		this.extendfield22 = extendfield22;
	}

	public String getExtendfield23() {
		return extendfield23;
	}

	public void setExtendfield23(String extendfield23) {
		this.extendfield23 = extendfield23;
	}

	public String getExtendfield24() {
		return extendfield24;
	}

	public void setExtendfield24(String extendfield24) {
		this.extendfield24 = extendfield24;
	}

	public String getExtendfield25() {
		return extendfield25;
	}

	public void setExtendfield25(String extendfield25) {
		this.extendfield25 = extendfield25;
	}

	public String getExtendfield26() {
		return extendfield26;
	}

	public void setExtendfield26(String extendfield26) {
		this.extendfield26 = extendfield26;
	}

	public String getExtendfield27() {
		return extendfield27;
	}

	public void setExtendfield27(String extendfield27) {
		this.extendfield27 = extendfield27;
	}

	public String getExtendfield28() {
		return extendfield28;
	}

	public void setExtendfield28(String extendfield28) {
		this.extendfield28 = extendfield28;
	}

	public String getExtendfield29() {
		return extendfield29;
	}

	public void setExtendfield29(String extendfield29) {
		this.extendfield29 = extendfield29;
	}

	public String getExtendfield30() {
		return extendfield30;
	}

	public void setExtendfield30(String extendfield30) {
		this.extendfield30 = extendfield30;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static CopyOfOrderHead newTest() {
		CopyOfOrderHead orderHead = new CopyOfOrderHead();

		// orderHead.setClientCd("");
		// orderHead.setPlatformCd("");
		// orderHead.setCd("");
		// orderHead.setShipmentmethodCd("");
		// orderHead.setOrdertypeCd("");
		// orderHead.setOrderDt("");
		// orderHead.setPlanlDt("");
		// orderHead.setPlanaDt("");
		// orderHead.setBillingDt("");
		// orderHead.setQuantity("");
		// orderHead.setWeight("");
		// orderHead.setVolume("");
		// orderHead.setPalletsum("");
		// orderHead.setExpense("");
		// orderHead.setFreceiverCd("");
		// orderHead.setFreceiverName("");
		// orderHead.setFtranslocationCd("");
		// orderHead.setFreceiverLikename("");
		// orderHead.setFreceiverPhone("");
		// orderHead.setFreceiverFax("");
		// orderHead.setFreceiverEmail("");
		// orderHead.setFreceiverPostcode("");
		// orderHead.setFreceiverAddress("");
		// orderHead.setTreceiverCd("");
		// orderHead.setTreceiverName("");
		// orderHead.setTtranslocationCd("");
		// orderHead.setTreceiverLikename("");
		// orderHead.setTreceiverPhone("");
		// orderHead.setTreceiverFax("");
		// orderHead.setTreceiverEmail("");
		// orderHead.setTreceiverPostcode("");
		// orderHead.setTreceiverAddress("");
		// orderHead.setDescr("");
		// orderHead.setOperatorCd("");
		// orderHead.setOperatorname("");

		// orderHead.setExtendfield01("");
		// orderHead.setExtendfield02("");
		// orderHead.setExtendfield03("");
		// orderHead.setExtendfield04("");
		// orderHead.setExtendfield05("");
		// orderHead.setExtendfield06("");
		// orderHead.setExtendfield07("");
		// orderHead.setExtendfield08("");
		// orderHead.setExtendfield09("");
		// orderHead.setExtendfield10("");
		// orderHead.setExtendfield11("");
		// orderHead.setExtendfield12("");
		// orderHead.setExtendfield13("");
		// orderHead.setExtendfield14("");
		// orderHead.setExtendfield15("");
		// orderHead.setExtendfield16("");
		// orderHead.setExtendfield17("");
		// orderHead.setExtendfield18("");
		// orderHead.setExtendfield19("");
		// orderHead.setExtendfield20("");
		// orderHead.setExtendfield21("");
		// orderHead.setExtendfield22("");
		// orderHead.setExtendfield23("");
		// orderHead.setExtendfield24("");
		// orderHead.setExtendfield25("");
		// orderHead.setExtendfield26("");
		// orderHead.setExtendfield27("");
		// orderHead.setExtendfield28("");
		// orderHead.setExtendfield29("");
		// orderHead.setExtendfield30("");
		// orderHead.setSt("");
		return orderHead;
	}
}
