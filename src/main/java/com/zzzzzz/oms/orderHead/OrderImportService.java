package com.zzzzzz.oms.orderHead;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.oms.orderDetail.OrderDetail;
import com.zzzzzz.oms.orderDetail.OrderDetailDao;
import com.zzzzzz.oms.orderDetail.OrderDetailService;
import com.zzzzzz.oms.ordertype.OrderType;
import com.zzzzzz.oms.ordertype.OrderTypeDao;
import com.zzzzzz.oms.ordertype.OrderTypeService;
import com.zzzzzz.oms.product.Product;
import com.zzzzzz.oms.product.ProductDao;
import com.zzzzzz.oms.product.ProductService;
import com.zzzzzz.oms.receiver.Receiver;
import com.zzzzzz.oms.receiver.ReceiverDao;
import com.zzzzzz.oms.receiver.ReceiverService;
import com.zzzzzz.oms.translocation.TransLocationService;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.sys.client.Client;
import com.zzzzzz.sys.client.ClientDao;
import com.zzzzzz.sys.client.ClientService;
import com.zzzzzz.sys.dict.Dict;
import com.zzzzzz.sys.dict.DictDao;
import com.zzzzzz.sys.dict.DictService;
import com.zzzzzz.utils.DateTimeUtils;

@Service
public class OrderImportService {
	@Resource
	public OrderHeadDao orderHeadDao;
	@Resource
	public ClientDao clientDao;
	@Resource
	public OrderTypeDao orderTypeDao;
	@Resource
	public ProductDao productDao;
	@Resource
	public DictDao dictDao;
	@Resource
	public OrderDetailDao orderDetailDao;
	@Resource
	public ClientService clientService;
	@Resource
	public OrderHeadService orderHeadService;
	@Resource
	public ReceiverService receiverService;
	@Resource
	public OrderDetailService orderDetailService;
	@Resource
	public ProductService productService;
	@Resource
	public TransLocationService transLocationService;
	@Resource
	public ReceiverDao receiverDao;
	@Resource
	public OrderTypeService orderTypeService;
	@Resource
	public DictService dictService;
	@Transactional
	public List<BaseData> batchAdd(List<OrderHead> list, I i) throws Exception {
		logger.info("orderHead batchAdd validate start:"+ DateTimeUtils.date2Str(new Date(),
						DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		// start validate
		int errTt = 0;// 错误信息条数
		int susTt = 0;// 正确信息条数
		BaseData baseData = null;
		// 用于存放信息的集合
		List<BaseData> listMsg = new ArrayList<BaseData>();
		// 用于存放导入失败的单号
		Set<String> set = new HashSet<String>();
		List<Dict> listDict=dictService.getDictListByCd("vocation");
		List<Dict> listUom=dictService.getDictListByCd("uom");
		// 用于存放通过单号
		List<OrderHead> listOrderHead = new ArrayList<OrderHead>();
		int count=3;
		for (OrderHead orderHead : list) {
			String descr=null;
			String uom=null;
			//验证输入订单号是否为空值
			if(StringUtils.isBlank(orderHead.getCd())){
				baseData = new BaseData();
				errTt++;
				baseData.setErrMsg("第"+(count++)+"行： 订单号"+ orderHead.getCd()+"为无效值");
				listMsg.add(baseData);
			}else{
				// 客户验证
				Client client=clientService.getclientByCd(orderHead.getClientCd(),i);
				if (client!=null) {
				//验证订单是否存在
				if (orderHeadDao.findByClientIdAndCd(orderHead.getCd(), client.getId())!=null) {
					set.add(orderHead.getCd()+orderHead.getClientCd());
					baseData = new BaseData();
					errTt++;
					baseData.setErrMsg("第"+(count++)+"行：订单号" + orderHead.getCd() + "已存在");
					listMsg.add(baseData);
				} else {
						//把clientId set到OrderHead中
						orderHead.setClientId(client.getId());
						// 订单类型验证
						OrderType orderType=orderTypeService.getOrderTypeByCd(orderHead.getOrderTypeCd(),client.getId());
						if (orderType!=null) {
							//把orderTypeId set到OrderHead中
							orderHead.setOrderTypeId(orderType.getId());
							orderHead.setShipment_method(orderType.getShipment_method());
							//发货方验证
							Receiver freceiver=receiverService.getReceiverByCd(orderHead.getFreceiverCd(),client.getId());
								if (freceiver!=null) {		
									//把收货方id set到OrderHead 中
									orderHead.setFreceiverId(freceiver.getId());
									//收发方验证
									Receiver treceiver=receiverService.getReceiverByCd(orderHead.getTreceiverCd(),client.getId());
										if (treceiver!=null) {
											//把发后方id set 到OrderHead中
											orderHead.setTreceiverId(treceiver.getId());
												//数量合法性验证
												if(orderHead.getQuantity()==null||orderHead.getQuantity()>=0){
													//重量合法性验证
													if(orderHead.getWeight()==null||orderHead.getWeight()>=0){
														//体积合法性验证
														if(orderHead.getVolume()==null||orderHead.getVolume()>=0){
															//托数合法性验证
															if(orderHead.getPalletsum()==null||orderHead.getPalletsum()>=0){
																//一口价合法性验证
																if(orderHead.getExpense()==null||orderHead.getExpense()>=0){
																	//业务类型验证
																	for(Dict dict:listDict){
																		if(dict.getDescr().equals(orderHead.getVocation())){
																			descr=dict.getDescr();
																			break;
																		}
																	}
																	if(descr!=null){
																		//物料验证
																		if(StringUtils.isBlank(orderHead.getProductCd())){
																			if(StringUtils.isBlank(orderHead.getFtranslocationCd())){
																				if(StringUtils.isBlank(orderHead.getTtranslocationCd())){
																					//如果都成功则通过
																					orderHead.setFtranslocationId(freceiver.getTranslocationId());
																					orderHead.setTtranslocationId(treceiver.getTranslocationId());
																					listOrderHead.add(orderHead);
																					susTt++;
																					count++;
																				}else{
																					if(transLocationService.getTransLocationByCd(i, orderHead.getTtranslocationCd())!=null){
																						orderHead.setFtranslocationId(freceiver.getTranslocationId());
																						orderHead.setTtranslocationId(transLocationService.getTransLocationByCd(i, orderHead.getTtranslocationCd()).getId());
																						listOrderHead.add(orderHead);
																						susTt++;
																						count++;
																					}else{
																						set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
																						baseData = new BaseData();
																						errTt++;
																						baseData.setErrMsg("第"+(count++)+"行：目的地城市"+ orderHead.getTtranslocationCd()+"不存在");
																						listMsg.add(baseData);
																					}
																				}
																			}else{
																				if(transLocationService.getTransLocationByCd(i, orderHead.getFtranslocationCd())!=null){
																					if(StringUtils.isBlank(orderHead.getTtranslocationCd())){
																						orderHead.setFtranslocationId(transLocationService.getTransLocationByCd(i, orderHead.getFtranslocationCd()).getId());
																						orderHead.setTtranslocationId(treceiver.getTranslocationId());
																						//如果都成功则通过
																						listOrderHead.add(orderHead);
																						susTt++;
																						count++;
																					}else{
																						if(transLocationService.getTransLocationByCd(i, orderHead.getTtranslocationCd())!=null){
																							orderHead.setTtranslocationId(transLocationService.getTransLocationByCd(i, orderHead.getTtranslocationCd()).getId());
																							orderHead.setFtranslocationId(transLocationService.getTransLocationByCd(i, orderHead.getFtranslocationCd()).getId());
																							listOrderHead.add(orderHead);
																							susTt++;
																							count++;
																						}else{
																							set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
																							baseData = new BaseData();
																							errTt++;
																							baseData.setErrMsg("第"+(count++)+"行：目的地城市"+ orderHead.getTtranslocationCd()+"不存在");
																							listMsg.add(baseData);
																						}
																					}
																				}else{
																					set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
																					baseData = new BaseData();
																					errTt++;
																					baseData.setErrMsg("第"+(count++)+"行：出发地城市"+ orderHead.getFtranslocationCd()+"不存在");
																					listMsg.add(baseData);
																				}
																			}											
																		}else{
																			if(productService.getProductByCd(orderHead.getProductCd(),client.getId())!=null){
																				for(Dict dict:listUom){
																					if(dict.getDescr().equals(orderHead.getUnit())){
																						uom=dict.getDescr();
																						break;
																					}
																				}
																				if(uom!=null){
																					if(StringUtils.isBlank(orderHead.getFtranslocationCd())){
																						if(StringUtils.isBlank(orderHead.getTtranslocationCd())){
																							//如果都成功则通过
																							orderHead.setFtranslocationId(freceiver.getTranslocationId());
																							orderHead.setTtranslocationId(treceiver.getTranslocationId());
																							listOrderHead.add(orderHead);
																							susTt++;
																							count++;
																						}else{
																							if(transLocationService.getTransLocationByCd(i, orderHead.getTtranslocationCd())!=null){
																								orderHead.setFtranslocationId(freceiver.getTranslocationId());
																								orderHead.setTtranslocationId(transLocationService.getTransLocationByCd(i, orderHead.getTtranslocationCd()).getId());
																								listOrderHead.add(orderHead);
																								susTt++;
																								count++;
																							}else{
																								set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
																								baseData = new BaseData();
																								errTt++;
																								baseData.setErrMsg("第"+(count++)+"行：目的地城市"+ orderHead.getTtranslocationCd()+"不存在");
																								listMsg.add(baseData);
																							}
																						}
																					}else{
																						if(transLocationService.getTransLocationByCd(i, orderHead.getFtranslocationCd())!=null){
																							if(StringUtils.isBlank(orderHead.getTtranslocationCd())){
																								orderHead.setFtranslocationId(transLocationService.getTransLocationByCd(i, orderHead.getFtranslocationCd()).getId());
																								orderHead.setTtranslocationId(treceiver.getTranslocationId());
																								//如果都成功则通过
																								listOrderHead.add(orderHead);
																								susTt++;
																								count++;
																							}else{
																								if(transLocationService.getTransLocationByCd(i, orderHead.getTtranslocationCd())!=null){
																									orderHead.setTtranslocationId(transLocationService.getTransLocationByCd(i, orderHead.getTtranslocationCd()).getId());
																									orderHead.setFtranslocationId(transLocationService.getTransLocationByCd(i, orderHead.getFtranslocationCd()).getId());
																									listOrderHead.add(orderHead);
																									susTt++;
																									count++;
																								}else{
																									set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
																									baseData = new BaseData();
																									errTt++;
																									baseData.setErrMsg("第"+(count++)+"行：目的地城市"+ orderHead.getTtranslocationCd()+"不存在");
																									listMsg.add(baseData);
																								}
																							}
																						}else{
																							set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
																							baseData = new BaseData();
																							errTt++;
																							baseData.setErrMsg("第"+(count++)+"行：出发地城市"+ orderHead.getFtranslocationCd()+"不存在");
																							listMsg.add(baseData);
																						}
																					}
																				}else{
																					set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
																					baseData = new BaseData();
																					errTt++;
																					baseData.setErrMsg("第"+(count++)+"行：包装单位"+ orderHead.getUnit()+"不存在");
																					listMsg.add(baseData);
																				}
																			}else{
																				set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
																				baseData = new BaseData();
																				errTt++;
																				baseData.setErrMsg("第"+(count++)+"行：物料"+ orderHead.getProductCd()+"不存在");
																				listMsg.add(baseData);
																			}
																		}
																	}else{
																		set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
																		baseData = new BaseData();
																		errTt++;
																		baseData.setErrMsg("第"+(count++)+"行：业务类型"+ orderHead.getVocation()+"不存在");
																		listMsg.add(baseData);
																	}																	
																}else{
																	set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
																	baseData = new BaseData();
																	errTt++;
																	baseData.setErrMsg("第"+(count++)+"行：一口价"+ orderHead.getExpense()+"有误");
																	listMsg.add(baseData);
																}														
															}else{
																set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
																baseData = new BaseData();
																errTt++;
																baseData.setErrMsg("第"+(count++)+"行：托数"+ orderHead.getPalletsum()+"有误");
																listMsg.add(baseData);
															}
														}else{
															set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
															baseData = new BaseData();
															errTt++;
															baseData.setErrMsg("第"+(count++)+"行：体积"+ orderHead.getVolume()+"有误");
															listMsg.add(baseData);
														}												
													}else{
														set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
														baseData = new BaseData();
														errTt++;
														baseData.setErrMsg("第"+(count++)+"行：重量"+ orderHead.getWeight()+"有误");
														listMsg.add(baseData);
													}						
												}else{
													set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
													baseData = new BaseData();
													errTt++;
													baseData.setErrMsg("第"+(count++)+"行：数量"+ orderHead.getQuantity()+"有误");
													listMsg.add(baseData);
												}
										} else {
											set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
											baseData = new BaseData();
											errTt++;
											baseData.setErrMsg("第"+(count++)+"行：收货方代码"+ orderHead.getTreceiverCd()+"不存在");
											listMsg.add(baseData);
										}
								} else {
									set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
									baseData = new BaseData();
									errTt++;
									baseData.setErrMsg("第"+(count++)+"行：发货方代码"+orderHead.getFreceiverCd()+"不存在");
									listMsg.add(baseData);
								}
						} else {
							set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
							baseData = new BaseData();
							errTt++;
							baseData.setErrMsg("第"+(count++)+"行：订单类型" + orderHead.getOrderTypeCd()+"不存在");
							listMsg.add(baseData);
							}
					} 
					}else {
						set.add(orderHead.getClientCd()+"--"+orderHead.getCd());
						baseData = new BaseData();
						errTt++;
						baseData.setErrMsg("第"+(count++)+"行：客户" + orderHead.getClientCd() + "不存在");
						listMsg.add(baseData);
				}		
			}
		}
		baseData = new BaseData();
		baseData.setErrMsg("总共有" + (susTt + errTt) + "行记录；共有" + susTt+"行记录读取成功；共有" + errTt + "行记录读取失败；");
		listMsg.add(baseData);
		Map<String, OrderHead> map = new HashMap<String, OrderHead>();
		// 保存可以批量插入的明细
		List<OrderDetail> detail = new ArrayList<OrderDetail>();
		// 保存可以批量出入的订单
		List<OrderHead> orderhead = new ArrayList<OrderHead>();
		
		Set<String> set1=new HashSet<String>();
		
		boolean flag = false;
		/*
		 * 生成订单
		 */
		for (OrderHead order : listOrderHead) {
			for (String scd : set) {
				if ((order.getClientCd()+"--"+order.getCd()).equals(scd)) {
					flag = true;
					break;
				}
			}
			if (flag == false) {
				OrderHead orderCd = map.get(order.getClientCd()+"--"+order.getCd());
				//判断客户是否一样
				if (orderCd != null) {
					//判断有些数据的统一性
						
					if(orderCd.getVocation().equals(order.getVocation()))
					{
						if(orderCd.getOrderTypeCd().equals(order.getOrderTypeCd()))
						{
							if(checkTime(orderCd.getOrderDt(), order.getOrderDt()))
							{				
								if(checkTime(orderCd.getPlanlDt(), order.getPlanlDt()))
								{
									if(checkTime(orderCd.getPlanaDt(), order.getPlanaDt()))
									{
										if(checkTime(orderCd.getBillingDt(), order.getBillingDt()))
										{
											if(orderCd.getFreceiverCd().equals(order.getFreceiverCd()))
											{
												Receiver freceiver =receiverService.getReceiverByCd(order.getFreceiverCd(),clientService.getclientByCd(order.getClientCd(),i).getId());
												if(check(order.getFreceiverName(),orderCd.getFreceiverName(),freceiver.getName()))
												{
													if(check(order.getFreceiverLikename(), orderCd.getFreceiverLikename(), freceiver.getLikeman()))
													{
														if(check(order.getFreceiverPhone(), orderCd.getFreceiverPhone(), freceiver.getPhone()))
														{
															if(check(order.getFreceiverFax(), orderCd.getFreceiverFax(), freceiver.getFax()))
															{
																if(check(order.getFreceiverEmail(), orderCd.getFreceiverEmail(), freceiver.getEmail()))
																{
																	if(check(order.getFreceiverPostcode(), orderCd.getFreceiverPostcode(), freceiver.getPostcode()))
																	{
																		if(check(order.getFreceiverAddress(), orderCd.getFreceiverAddress(), freceiver.getAddress()))
																		{
																			//验证收货方信息
																			if(orderCd.getTreceiverCd().equals(order.getTreceiverCd()))
																			{
																				Receiver treceiver =receiverService.getReceiverByCd(order.getTreceiverCd(),clientService.getclientByCd(order.getClientCd(),i).getId());
																				if(check(order.getTreceiverName(), orderCd.getTreceiverName(),treceiver.getName()))
																				{
																					if(check(order.getTreceiverLikename(), orderCd.getTreceiverLikename(), treceiver.getLikeman()))
																					{
																						if(check(order.getTreceiverPhone(), orderCd.getTreceiverPhone(), treceiver.getPhone()))
																						{
																							if(check(order.getTreceiverFax(), orderCd.getTreceiverFax(), treceiver.getFax()))
																							{
																								if(check(order.getTreceiverEmail(), orderCd.getTreceiverEmail(), treceiver.getEmail()))
																								{
																									if(check(order.getTreceiverPostcode(), orderCd.getTreceiverPostcode(), treceiver.getPostcode()))
																									{
																										if(check(order.getTreceiverAddress(), orderCd.getTreceiverAddress(), treceiver.getAddress()))
																										{
																											if(orderCd.getFtranslocationId()==order.getFtranslocationId()){
																												if(orderCd.getTtranslocationId()==order.getTtranslocationId()){
																													OrderHead order2=new OrderHead();
																													if(order.getProductCd()!=null&&!order.getProductCd().equals("")){
																														order2=isProduct(order, i);
																														detail.add(saveDetail(order2));
																													}else{
																														order2=isNotProduct(order);
																													}					
																													orderCd.setQuantity(orderCd.getQuantity()+ order2.getQuantity());
																													orderCd.setWeight(orderCd.getWeight() + order2.getWeight());
																													orderCd.setVolume(orderCd.getVolume() + order2.getVolume());
																													orderCd.setPalletsum(orderCd.getPalletsum()+order2.getPalletsum());
																													orderCd.setExpense(orderCd.getExpense()+order2.getExpense());
																												}else{
																													set1.add(order.getClientCd()+"--"+order.getCd());
																													baseData = new BaseData();
																													//errTt++;
																													baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-目的地城市存在不统一情况");
																													listMsg.add(baseData);
																												}
																											}else{
																												set1.add(order.getClientCd()+"--"+order.getCd());
																												baseData = new BaseData();
																												//errTt++;
																												baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-出发地城市存在不统一情况");
																												listMsg.add(baseData);
																											}								
																										}else{
																											set1.add(order.getClientCd()+"--"+order.getCd());
																											baseData = new BaseData();
																											//errTt++;
																											baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-收货方地址存在不统一情况");
																											listMsg.add(baseData);
																										}
																									}else{
																										set1.add(order.getClientCd()+"--"+order.getCd());
																										baseData = new BaseData();
																										//errTt++;
																										baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-收货方邮编存在不统一情况");
																										listMsg.add(baseData);
																									}
																								}else{
																									set1.add(order.getClientCd()+"--"+order.getCd());
																									baseData = new BaseData();
																									//errTt++;
																									baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-收货方邮箱存在不统一情况");
																									listMsg.add(baseData);
																								}
																							}else{
																								set1.add(order.getClientCd()+"--"+order.getCd());
																								baseData = new BaseData();
																								//errTt++;
																								baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-收货方传真存在不统一情况");
																								listMsg.add(baseData);
																							}
																						}else{
																							set1.add(order.getClientCd()+"--"+order.getCd());
																							baseData = new BaseData();
																							//errTt++;
																							baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-收货方联系电话存在不统一情况");
																							listMsg.add(baseData);
																						}
																					}else{
																						set1.add(order.getClientCd()+"--"+order.getCd());
																						baseData = new BaseData();													
																						baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-收货方联系人存在不统一情况");
																						listMsg.add(baseData);
																					}
																				}else{
																					set1.add(order.getClientCd()+"--"+order.getCd());
																					baseData = new BaseData();
																					//errTt++;
																					baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-收货方名称存在不统一情况");
																					listMsg.add(baseData);
																				}			
																			}else{
																				set1.add(order.getClientCd()+"--"+order.getCd());
																				baseData = new BaseData();
																				//errTt++;
																				baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-收货方代码存在不统一情况");
																				listMsg.add(baseData);
																			}
																		}else{
																			set1.add(order.getClientCd()+"--"+order.getCd());
																			baseData = new BaseData();
																			//errTt++;
																			baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-发货方地址存在不统一情况");
																			listMsg.add(baseData);
																		}
																	}else{
																		set1.add(order.getClientCd()+"--"+order.getCd());
																		baseData = new BaseData();
																		//errTt++;
																		baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-发货方邮编存在不统一情况");
																		listMsg.add(baseData);
																	}
																}else{
																	set1.add(order.getClientCd()+"--"+order.getCd());
																	baseData = new BaseData();
																	//errTt++;
																	baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-发货方邮箱存在不统一情况");
																	listMsg.add(baseData);
																}
															}else{
																set1.add(order.getClientCd()+"--"+order.getCd());
																baseData = new BaseData();
																//errTt++;
																baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-发货方传真存在不统一情况");
																listMsg.add(baseData);
															}
														}else{
															set1.add(order.getClientCd()+"--"+order.getCd());
															baseData = new BaseData();
															//errTt++;
															baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-发后方联系电话存在不统一情况");
															listMsg.add(baseData);
														}
													}else{
														set1.add(order.getClientCd()+"--"+order.getCd());
														baseData = new BaseData();
														//errTt++;
														baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-发货方联系人存在不统一情况");
														listMsg.add(baseData);
													}
												}else{
													set1.add(order.getClientCd()+"--"+order.getCd());
													baseData = new BaseData();
													//errTt++;
													baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-发货方名称存在不统一情况");
													listMsg.add(baseData);
												}															
										}else{
											set1.add(order.getClientCd()+"--"+order.getCd());
											baseData = new BaseData();
											//errTt++;
											baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-发货方代码存在不统一情况");
											listMsg.add(baseData);
										}
									}else{
										set1.add(order.getClientCd()+"--"+order.getCd());
										baseData = new BaseData();
										//errTt++;
										baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-计费周期存在不统一情况");
										listMsg.add(baseData);
									}
								}else{
									set1.add(order.getClientCd()+"--"+order.getCd());
									baseData = new BaseData();
									//errTt++;
									baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-计划到达时间存在不统一情况");
									listMsg.add(baseData);
								}
							}else{
								set1.add(order.getClientCd()+"--"+order.getCd());
								baseData = new BaseData();
								//errTt++;
								baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-计划出发时间存在不统一情况");
								listMsg.add(baseData);
							}
						}else{
							set1.add(order.getClientCd()+"--"+order.getCd());
							baseData = new BaseData();
							//errTt++;
							baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-开单时间存在不统一情况");
							listMsg.add(baseData);
						}
					}else{
						set1.add(order.getClientCd()+"--"+order.getCd());
						baseData = new BaseData();
						//errTt++;
						baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-订单类型存在不统一情况");
						listMsg.add(baseData);
					}
				}else{
					set1.add(order.getClientCd()+"--"+order.getCd());
					baseData = new BaseData();
					//errTt++;
					baseData.setErrMsg("客户"+orderCd.getClientCd()+"-订单号"+orderCd.getCd()+"-业务类型存在不统一情况");
					listMsg.add(baseData);
				}							
				}else {
						OrderHead order1=new OrderHead();
						if(order.getProductCd()!=null&&!order.getProductCd().equals("")){
							order1=isProduct(order, i);
							detail.add(saveDetail(order1));
						}else{
							order1=isNotProduct(order);	
						}
					map.put(order.getClientCd()+"--"+order.getCd(), order1);
			}
		}
	}
			for (String key : map.keySet()) {
				Boolean flag1=true;
				OrderHead order = map.get(key);
				baseData = new BaseData();			
				for (String cd : set1) {					
					if(cd.equals(key)){				
						baseData.setErrMsg(" 订单  " +order.getClientCd()+"--"+order.getCd()+" 生成失败");
						listMsg.add(baseData);
						flag1=false; 
						break;
					}
				}
				if(flag1){
						baseData.setErrMsg("订单  " + order.getClientCd()+"--"+order.getCd()+ " 生成成功");
						listMsg.add(baseData);
						OrderHead orderHead=make(order,i);
						orderhead.add(orderHead);
				}
			}
			for (String scd : set) {
				baseData = new BaseData();
				baseData.setErrMsg("订单 " + scd + " 生成失败");
				listMsg.add(baseData);
			}
		// 批量插入订单表
		List<Long> listId = orderHeadDao.saveOrder(orderhead, i);
		if(listId.size()!=0){
			List<OrderHead> listorder = orderHeadDao.findByIds(listId);
			for (OrderHead orderAll : listorder) {
				Long lineNo = (long) 1;
				String ocd = orderAll.getCd();
				Long orderId = orderAll.getId();
				for (OrderDetail orderDt : detail) {
					if (ocd.equals(orderDt.getOrderCd())) {
						Product product = productService.getProductByCd(orderDt.getProductCd(),clientService.getclientByCd(orderDt.getClientCd(), i).getId());
						orderDt.setOrderId(orderId);
						orderDt.setProductId(product.getId());
						orderDt.setLineno(lineNo);
						orderDt.setAddDt(new Date());
						orderDt.setAddBy(i.getId());
						lineNo++;
					}
				}
			}

			// 批量插入明细
			orderDetailDao.saveDetail(detail);
		}
		return listMsg;
	}
	
	
	//生成明细
	public OrderDetail saveDetail(OrderHead order){
		OrderDetail orderDetail=new OrderDetail();
		orderDetail.setProductCd(order.getProductCd());
		orderDetail.setClientCd(order.getClientCd());
		orderDetail.setOrderCd(order.getCd());
		orderDetail.setQuantity(order.getQuantity());
		orderDetail.setWeight(order.getWeight());
		orderDetail.setVolume(order.getVolume());
		orderDetail.setLot(order.getLot());
		orderDetail.setUnit(order.getUnit());
		return orderDetail;
	}
	//判断统一性封装
	public Boolean check(String str1,String str2,String str3){
		Boolean flag=false;
		if(StringUtils.isBlank(str1))
		{
			if(StringUtils.isBlank(str2))
			{
				flag=true;
			}else{
					if(str2.equals(str3))
					{
						flag=true;
					}	
			}
		}else{
			if(StringUtils.isBlank(str2))
			{
				if(str1.equals(str3))
				{
					flag=true;
				}
			}else{
				if(str1.equals(str2))
				{
					flag=true;					
				}
			}
		}
		return flag;
	}
	//对时间的封装
	public Boolean checkTime(Date d1,Date d2){
		Boolean flag=false;
		if(d1==null){
			if(d2==null){
				flag=true;
			}
		}else{
			if(d1.equals(d2)){
				flag=true;
			}
		}
		return flag;
	}
	//物料部分分为两块
	public OrderHead isProduct (OrderHead order,I i){	
		Product product=productService.getProductByCd(order.getProductCd(),clientService.getclientByCd(order.getClientCd(),i).getId());
		if(order.getUnit().equals("pcs")){
			if (order.getQuantity()== null|| order.getQuantity().equals("")) {
				order.setQuantity(0.0);
				if(order.getVolume()==null||order.getVolume().equals("")){
					order.setVolume(0.0);
				}
				if(order.getWeight()==null||order.getWeight().equals("")){
					order.setWeight(0.0);
				}
			}else {
				if (order.getVolume() == null|| order.getVolume().equals("")) {
					if (product.getMuvolume() != null) {
						order.setVolume(order.getQuantity()* product.getMuvolume());
					} else {
						order.setVolume(0.0);
					}
				}
				if (order.getWeight() == null|| order.getWeight().equals("")) {
					if (product.getMuweight() != null) {
						order.setWeight(order.getQuantity()* product.getMuweight());
					} else {
						order.setWeight(0.0);
					}
				}
			}
		}else{
			if (order.getQuantity()== null|| order.getQuantity().equals("")) {
				order.setQuantity(0.0);
				if(order.getVolume()==null||order.getVolume().equals("")){
					order.setVolume(0.0);
				}
				if(order.getWeight()==null||order.getWeight().equals("")){
					order.setWeight(0.0);
				}
			}else {
				if (order.getVolume() == null|| order.getVolume().equals("")) {
					if (product.getCasevolume()!= null) {
						order.setVolume(order.getQuantity()* product.getCasevolume());
					} else {
						order.setVolume(0.0);
					}
				}
				if (order.getWeight() == null|| order.getWeight().equals("")) {
					if (product.getCaseweight() != null) {
						order.setWeight(order.getQuantity()* product.getCaseweight());
					} else {
						order.setWeight(0.0);
					}
				}
			}
		}
		if(order.getPalletsum()==null||order.getPalletsum().equals("")){
			order.setPalletsum(0.0);
		}
		if(order.getExpense()==null||order.getExpense().equals("")){
			order.setExpense(0.0);
		}
		return order;
	}
	public OrderHead isNotProduct(OrderHead order){
		if (order.getQuantity()== null|| order.getQuantity().equals("")) {
			order.setQuantity(0.0);
		}
		if(order.getVolume()==null||order.getVolume().equals("")){
			order.setVolume(0.0);
		}
		if(order.getWeight()==null||order.getWeight().equals("")){
			order.setWeight(0.0);
		}
		if(order.getPalletsum()==null||order.getPalletsum().equals("")){
			order.setPalletsum(0.0);
		}
		if(order.getExpense()==null||order.getExpense().equals("")){
			order.setExpense(0.0);
		}
		return order;
	} 
	//封装收发货方信息
	public OrderHead make(OrderHead order,I i){
		// 发货方
		Receiver freceiver = receiverService.getReceiverByCd(order.getFreceiverCd(),clientService.getclientByCd(order.getClientCd(),i).getId());
				if (StringUtils.isBlank(order.getFreceiverName())) {
					if (freceiver.getName() != null) {
						order.setFreceiverName(freceiver.getName());
						} else {
							order.setFreceiverName(null);
						}
					}
				if (StringUtils.isBlank(order.getFreceiverLikename())) {
					if (freceiver.getLikeman() != null) {
						order.setFreceiverLikename(freceiver.getLikeman());
					} else {
						order.setFreceiverLikename(null);
					}
				}

				if (StringUtils.isBlank(order.getFreceiverEmail())) {
					if (freceiver.getEmail() != null) {
						order.setFreceiverEmail(freceiver.getEmail());
					} else {
						order.setFreceiverEmail(null);
					}
				}

				if (StringUtils.isBlank(order.getFreceiverPhone())) {
					if (freceiver.getPhone() != null) {
						order.setFreceiverPhone(freceiver.getPhone());
					} else {
						order.setFreceiverPhone(null);
					}
				}
				if (StringUtils.isBlank(order.getFreceiverFax())) {
					if (freceiver.getFax() != null) {
						order.setFreceiverFax(freceiver.getFax());
					} else {
						order.setFreceiverFax(null);
					}
				}
				if (StringUtils.isBlank(order.getFreceiverPostcode())) {
					if (freceiver.getPostcode() != null) {
						order.setFreceiverPostcode(freceiver.getPostcode());
					} else {
						order.setFreceiverPostcode(null);
					}
				}
				if (StringUtils.isBlank(order.getFreceiverAddress())) {
					if (freceiver.getAddress() != null) {
						order.setFreceiverAddress(freceiver.getAddress());
					} else {
						order.setFreceiverAddress(null);
					}
				}
				if(StringUtils.isBlank(order.getFtranslocationCd())){
					
				}
				// 收货方
				Receiver treceiver =receiverService.getReceiverByCd(order.getTreceiverCd(),clientService.getclientByCd(order.getClientCd(),i).getId());
				if (StringUtils.isBlank(order.getTreceiverName())) {
					if (treceiver.getName() != null) {
						order.setTreceiverName(treceiver.getName());
					} else {
						order.setTreceiverName(null);
					}
				}
				if (StringUtils.isBlank(order.getTreceiverLikename())) {
					if (treceiver.getLikeman() != null) {
						order.setTreceiverLikename(treceiver.getLikeman());
					} else {
						order.setTreceiverLikename(null);
					}
				}
				if (StringUtils.isBlank(order.getTreceiverEmail())) {
					if (treceiver.getEmail() != null) {
						order.setTreceiverEmail(treceiver.getEmail());
					} else {
						order.setTreceiverEmail(null);
					}
				}

				if (StringUtils.isBlank(order.getTreceiverPhone())) {
					if (treceiver.getPhone() != null) {
						order.setTreceiverPhone(treceiver.getPhone());
					} else {
						order.setTreceiverPhone(null);
					}
				}

				if (StringUtils.isBlank(order.getTreceiverFax())) {
					if (treceiver.getFax() != null) {
						order.setTreceiverFax(treceiver.getFax());
					} else {
						order.setTreceiverFax(null);
					}
				}
				if (StringUtils.isBlank(order.getTreceiverPostcode())) {
					if (treceiver.getPostcode() != null) {
						order.setTreceiverPostcode(treceiver.getPostcode());
					} else {
						order.setTreceiverPostcode(null);
					}
				}
				if (StringUtils.isBlank(order.getTreceiverAddress())) {
					if (treceiver.getAddress() != null) {
						order.setTreceiverAddress(treceiver.getAddress());
					} else {
						order.setTreceiverAddress(null);
					}
				}
		return order;
	}
	private static final Logger logger = LoggerFactory.getLogger(OrderHead.class);
}
