package com.zzzzzz.oms.orderHead;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import com.google.common.collect.Lists;
import com.zzzzzz.oms.translocation.TransLocationService;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.utils.page.Page;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class OrderHeadDao {

	@Resource
	private BaseDao baseDao;
	@Resource
	private TransLocationService transLocationService;
	private final static String sql_add = "insert into t_order_head(clientId, platformId, cd, shipment_method,vocation, orderTypeId, orderDt, planlDt, planaDt, billingDt, quantity, weight, volume, palletsum, expense, freceiverId, freceiverName, ftranslocationId, freceiverLikename, freceiverPhone, freceiverFax, freceiverEmail, freceiverPostcode, freceiverAddress, treceiverId, treceiverName, ttranslocationId, treceiverLikename, treceiverPhone, treceiverFax, treceiverEmail, treceiverPostcode, treceiverAddress, descr,addDt, addBy, status) values(:clientId, :platformId, :cd, :shipment_method,:vocation, :orderTypeId, :orderDt, :planlDt, :planaDt, :billingDt, :quantity, :weight, :volume, :palletsum, :expense, :freceiverId, :freceiverName, :ftranslocationId, :freceiverLikename, :freceiverPhone, :freceiverFax, :freceiverEmail, :freceiverPostcode, :freceiverAddress, :treceiverId, :treceiverName, :ttranslocationId, :treceiverLikename, :treceiverPhone, :treceiverFax, :treceiverEmail, :treceiverPostcode, :treceiverAddress, :descr,:addDt, :addBy,'INPUT')";
	private final static String sql_updStByIds = "update t_order_head set status = :status, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update t_order_head set clientId=:clientId, platformId=:platformId, cd=:cd, vocation=:vocation,shipment_method=:shipment_method, orderTypeId=:orderTypeId, orderDt=:orderDt, planlDt=:planlDt, planaDt=:planaDt, billingDt=:billingDt, quantity=:quantity, weight=:weight, volume=:volume, palletsum=:palletsum, expense=:expense, freceiverId=:freceiverId, freceiverName=:freceiverName, ftranslocationId=:ftranslocationId, freceiverLikename=:freceiverLikename, freceiverPhone=:freceiverPhone, freceiverFax=:freceiverFax, freceiverEmail=:freceiverEmail, freceiverPostcode=:freceiverPostcode, freceiverAddress=:freceiverAddress, treceiverId=:treceiverId, treceiverName=:treceiverName, ttranslocationId=:ttranslocationId, treceiverLikename=:treceiverLikename, treceiverPhone=:treceiverPhone, treceiverFax=:treceiverFax, treceiverEmail=:treceiverEmail, treceiverPostcode=:treceiverPostcode, treceiverAddress=:treceiverAddress, descr=:descr, updDt=:updDt, updBy=:updBy, extendfield01=:extendfield01, extendfield02=:extendfield02, extendfield03=:extendfield03, extendfield04=:extendfield04, extendfield05=:extendfield05, extendfield06=:extendfield06, extendfield07=:extendfield07, extendfield08=:extendfield08, extendfield09=:extendfield09, extendfield10=:extendfield10, extendfield11=:extendfield11, extendfield12=:extendfield12, extendfield13=:extendfield13, extendfield14=:extendfield14, extendfield15=:extendfield15, extendfield16=:extendfield16, extendfield17=:extendfield17, extendfield18=:extendfield18, extendfield19=:extendfield19, extendfield20=:extendfield20, extendfield21=:extendfield21, extendfield22=:extendfield22, extendfield23=:extendfield23, extendfield24=:extendfield24, extendfield25=:extendfield25, extendfield26=:extendfield26, extendfield27=:extendfield27, extendfield28=:extendfield28, extendfield29=:extendfield29, extendfield30=:extendfield30, status=:status where id = :id";
	private final static String sql_findById = "select o.*,l.name as ftranslocationName,ll.name as ttranslocationName from t_order_head o inner join t_trans_location l on l.id=o.ftranslocationId  inner join t_trans_location ll on ll.id=o.ttranslocationId where o.id =:id";
	private final static String sql_findByIds = "select id, clientId, platformId, cd, shipment_method,vocation,orderTypeId, orderDt, planlDt, planaDt, billingDt, quantity, weight, volume, palletsum, expense, freceiverId, freceiverName, ftranslocationId, freceiverLikename, freceiverPhone, freceiverFax, freceiverEmail, freceiverPostcode, freceiverAddress, treceiverId, treceiverName, ttranslocationId, treceiverLikename, treceiverPhone, treceiverFax, treceiverEmail, treceiverPostcode, treceiverAddress, descr, addDt, addBy, updDt, updBy, status from t_order_head where id in (:ids)";
	@CacheEvict(value = "orderHeadCache", allEntries = true)
	public Long add(OrderHead orderHead) {
		return baseDao.updateGetLongKey(sql_add, orderHead);
	}
	@CacheEvict(value = "orderHeadCache", allEntries = true)
	public int updStByIds(List<Long> ids, String status, I i) {
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids)
				.setParam("status", status).setParam("updDt", new Date())
				.setParam("updBy", i.getId());
		return baseDao.update(finder);
	}
	@CacheEvict(value = "orderHeadCache", allEntries = true)
	public int updById(OrderHead orderHead) {
		return baseDao.update(sql_upd, orderHead);
	}

	public OrderHead findById(Long id) {
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, OrderHead.class);
	}

	/*
	 * 通过id查找对象
	 */
	public List<OrderHead> findByIds(List<Long> ids) {  
			Finder finder = new Finder(sql_findByIds).setParam("ids", ids);
            List<OrderHead> list=baseDao.findList(finder, OrderHead.class);
            return list;
	}

	public OrderHead findByClientIdAndCd(String cd,Long clientId){
		Finder finder = new Finder("");
		finder.append("select");
		finder.append("id, clientId, platformId, cd, shipment_method,vocation,orderTypeId, orderDt, planlDt, planaDt, billingDt, quantity, weight, volume, palletsum, expense, freceiverId, freceiverName, ftranslocationId, freceiverLikename, freceiverPhone, freceiverFax, freceiverEmail, freceiverPostcode, freceiverAddress, treceiverId, treceiverName, ttranslocationId, treceiverLikename, treceiverPhone, treceiverFax, treceiverEmail, treceiverPostcode, treceiverAddress, descr, addDt, addBy, updDt, updBy, status from t_order_head");
		finder.append("where cd=:cd").setParam("cd", cd);
		finder.append("and clientId=:clientId").setParam("clientId", clientId);
		List<OrderHead> orderHead=baseDao.findList(finder, OrderHead.class);
		if(orderHead.size()>0){
			return orderHead.get(0);
		}
		return null;
	}
	/**
	 * 动态查询
	 */
	public List<OrderHead> findListBy(Page page, Map<String, Object> ffMap,I i) {
		Finder finder = new Finder("");
		finder.append("select");
		finder.append("count(1) as tt");
		finder.append("from t_order_head o inner join t_client c on o.clientId=c.id inner join t_order_type t on o.orderTypeId=t.id inner join sys_dict d on o.vocation=d.descr left join sys_dict dd on o.shipment_method=dd.descr inner join sys_dict dt on o.status=dt.descr inner join t_trans_location l on l.id=o.ftranslocationId inner join t_trans_location ll on ll.id=o.ttranslocationId inner join t_receiver f on f.id=o.freceiverId inner join t_receiver tt on tt.id=o.treceiverId left join t_user_client u on o.clientId=u.clientId");
		finder.append("where 1=1");
		finder.append("and o.platformId=:platformId").setParam("platformId", i.getPlatformId());
		finder.append("and u.userId=:userId").setParam("userId", i.getId());
		if(DaoUtils.isNotNull(ffMap.get("orderDt1"))){
			finder.append("and orderDt >=date_format(:orderDt1,'%Y-%m-%d')").setParam("orderDt1", ffMap.get("orderDt1"));
		}
		if(DaoUtils.isNotNull(ffMap.get("orderDt2"))){
			finder.append("and orderDt <=date_format(:orderDt2,'%Y-%m-%d')").setParam("orderDt2", ffMap.get("orderDt2"));
		}
		if(DaoUtils.isNotNull(ffMap.get("planaDt1"))){
			finder.append("and planaDt >=date_format(:planaDt1,'%Y-%m-%d')").setParam("planaDt1", ffMap.get("planaDt1"));
		}
		if(DaoUtils.isNotNull(ffMap.get("planaDt2"))){
			finder.append("and planaDt <=date_format(:planaDt2,'%Y-%m-%d')").setParam("planaDt2", ffMap.get("planaDt2"));
		}
		if (DaoUtils.isNotNull(ffMap.get("clientId"))) {
			finder.append("and o.clientId = :clientId").setParam("clientId",
					ffMap.get("clientId"));
		}
		if (DaoUtils.isNotNull(ffMap.get("cd"))) {
			finder.append("and o.cd = :cd").setParam("cd", ffMap.get("cd"));
		}
		if (DaoUtils.isNotNull(ffMap.get("orderTypeId"))) {
			finder.append("and o.orderTypeId = :orderTypeId").setParam("orderTypeId", ffMap.get("orderTypeId"));
		}
		if (DaoUtils.isNotNull(ffMap.get("status"))) {
			finder.append("and o.status = :status").setParam("status", ffMap.get("status"));
		}
		finder.append("order by o.id desc");
		if (page != null) {
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}

		String sql = finder
				.getSql()
				.replace(
						"count(1) as tt",
						"o.*,c.name as clientName ,c.cd as clientCd,t.name as typeName,t.cd as orderTypeCd,d.val as vocationName,dd.val as shipmentMethodName,dt.val as statusName,l.name as ftranslocationName,ll.name as ttranslocationName,f.cd as freceiverCd,tt.cd as treceiverCd");
		finder.setSql(sql);
		List<OrderHead> list = baseDao.findList(finder, OrderHead.class);
		return list;
	}

	/*
	 * 缓存
	 */
	public List<OrderHead> findAllBy() {
		Finder finder = new Finder("");
		finder.append("select");
		finder.append("id, clientId, platformId, cd, shipment_method,vocation,orderTypeId, orderDt, planlDt, planaDt, billingDt, quantity, weight, volume, palletsum, expense, freceiverId, freceiverName, ftranslocationId, freceiverLikename, freceiverPhone, freceiverFax, freceiverEmail, freceiverPostcode, freceiverAddress, treceiverId, treceiverName, ttranslocationId, treceiverLikename, treceiverPhone, treceiverFax, treceiverEmail, treceiverPostcode, treceiverAddress, descr, addDt, addBy, updDt, updBy, status");
		finder.append("from t_order_head");
		List<OrderHead> list = baseDao.findList(finder, OrderHead.class);
		return list;
	}
	
	/*
	 * 订单缓存
	 */
	@Cacheable(value = "orderHeadCache")
	public Map<String, List<OrderHead>> getAllOrderHeadMap() {
		Map<String, List<OrderHead>> orderHeadMap= new HashMap<String, List<OrderHead>>();
		List<OrderHead> allOrderHeadList = findAllBy();
		for (OrderHead orderHead : allOrderHeadList) {
			List<OrderHead> orderHeadList = orderHeadMap.get(orderHead.getCd());
			if (orderHeadList != null) {
				orderHeadList.add(orderHead);
			} else {
				orderHeadMap.put(orderHead.getCd(), Lists.newArrayList(orderHead));
			}
		}
		return orderHeadMap;
	}

	/*
	 * 批量出入数据
	 */
	@CacheEvict(value = "orderHeadCache", allEntries = true)
	public List<Long> saveOrder(List<OrderHead> orderheads,I i) throws Exception {
		List<Long> list=new ArrayList<Long>();
		for(OrderHead orderHead : orderheads){
			orderHead.setPlatformId(i.getPlatformId());
			orderHead.setAddBy(i.getId());
			orderHead.setAddDt(new Date());
			long id = add(orderHead);
			list.add(id);
		}
		return list;
	}
}
