package com.zzzzzz.oms.tempShipment;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class TempShipment implements Serializable {
	
	
	
  	private Long id;
	@ExcelField(title = "代码", align = 2, sort = 10)
	@Length(min = 1, max = 30)
  	private String code;
	@ExcelField(title = "运输工具", align = 2, sort = 40)
	
  	private Long vehicleId;
	private String carNo;
	@ExcelField(title = "平台", align = 2, sort = 40)
	private Long constractorId;
	private Long driverId;
	private String shipment_method;
  	private Long platformId;
	@ExcelField(title = "调度单状态", align = 2, sort = 40)
	
  	private String status;
	@ExcelField(title = "调度单", align = 2, sort = 40)
	
  	private Long shipmentId;
	@ExcelField(title = "件数", align = 2, sort = 80)
	
  	private Double quantity;
	@ExcelField(title = "重量", align = 2, sort = 80)
	
  	private Double weight;
	@ExcelField(title = "体积", align = 2, sort = 80)
	private String name;
  	private Double volume;
	private Double expense;
	private Integer points;
	private String cd;
	private String vehicleNo;
	private Long vehicleTypeId;
	private String vocationType;
	private String vocation;
	private Date planaDt;
	private Date planlDt;
  	public Integer getPoints() {
		return points;
	}
	public void setPoints(Integer points) {
		this.points = points;
	}
	private Integer count;
	
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	private Long addBy;
	
	
  	private Date updDt;
	
	
  	private Date addDt;
	
	
  	private Long updBy;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public String getCode(){
  		return code;
  	}
  	public void setCode(String code) {
		this.code = code;
	}
  	public Long getVehicleId(){
  		return vehicleId;
  	}
  	public void setVehicleId(Long vehicleId) {
		this.vehicleId = vehicleId;
	}
  	
  	public String getCarNo() {
		return carNo;
	}
	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}
	public Long getDriverId() {
		return driverId;
	}
	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}
	
	public String getShipment_method() {
		return shipment_method;
	}
	public void setShipment_method(String shipment_method) {
		this.shipment_method = shipment_method;
	}
	public Long getConstractorId() {
		return constractorId;
	}
	public String getVocation() {
		return vocation;
	}
	public void setVocation(String vocation) {
		this.vocation = vocation;
	}
	public void setConstractorId(Long constractorId) {
		this.constractorId = constractorId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getPlatformId(){
  		return platformId;
  	}
  	public String getVocationType() {
		return vocationType;
	}
	public void setVocationType(String vocationType) {
		this.vocationType = vocationType;
	}
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
  	public String getStatus(){
  		return status;
  	}
  	public void setStatus(String status) {
		this.status = status;
	}
  	public Date getPlanaDt() {
		return planaDt;
	}
	public void setPlanaDt(Date planaDt) {
		this.planaDt = planaDt;
	}
	public Date getPlanlDt() {
		return planlDt;
	}
	public void setPlanlDt(Date planlDt) {
		this.planlDt = planlDt;
	}
	public Long getShipmentId(){
  		return shipmentId;
  	}
  	public void setShipmentId(Long shipmentId) {
		this.shipmentId = shipmentId;
	}
  	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getWeight(){
  		return weight;
  	}
  	public void setWeight(Double weight) {
		this.weight = weight;
	}
  	public Double getVolume(){
  		return volume;
  	}
  	public void setVolume(Double volume) {
		this.volume = volume;
	}
  	
  	public Double getExpense() {
		return expense;
	}
	public void setExpense(Double expense) {
		this.expense = expense;
	}
	public String getCd() {
		return cd;
	}
	public void setCd(String cd) {
		this.cd = cd;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public Long getVehicleTypeId() {
		return vehicleTypeId;
	}
	public void setVehicleTypeId(Long vehicleTypeId) {
		this.vehicleTypeId = vehicleTypeId;
	}
	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
 	
	public static TempShipment newTest() {
		TempShipment tempShipment = new TempShipment();
		
		//tempShipment.setCode("");
		//tempShipment.setVehicleId("");
		//tempShipment.setPlatformId("");
		//tempShipment.setStatus("");
		//tempShipment.setShipmentId("");
		//tempShipment.setQuantity("");
		//tempShipment.setWeight("");
		//tempShipment.setVolume("");
		
		
		
		
		return tempShipment;
	}
}


