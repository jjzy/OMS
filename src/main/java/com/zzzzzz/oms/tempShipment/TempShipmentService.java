package com.zzzzzz.oms.tempShipment;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.oms.legs.Legs;
import com.zzzzzz.oms.legs.LegsDao;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.utils.BeanValidators;
import com.zzzzzz.utils.DateTimeUtils;

@Service
public class TempShipmentService {
	@Resource
	public TempShipmentDao tempShipmentDao;
	@Resource
	public Validator validator;
	@Resource
	public LegsDao legsDao;
	@Transactional
	public TempShipment save(List<Legs> list,String code,List<Long> ids,Long tempshipmentId,I i) {
		TempShipment tempShipment=new TempShipment();
		Double quantity=0.0;
		Double weight=0.0;
		Double volume=0.0;
		Integer points=list.size();
		Integer count=0;
		Set<Long> set=new HashSet<Long>();
		for(Legs legs:list){
			set.add(legs.getTtranslocationId());
			quantity+=legs.getQuantity();
			weight+=legs.getWeight();
			volume+=legs.getVolume();
		}
		count=set.size();
		tempShipment.setCode(code);
		tempShipment.setQuantity(quantity);
		tempShipment.setWeight(weight);
		tempShipment.setVolume(volume);
		tempShipment.setCount(count);
		tempShipment.setPoints(points);
		tempShipment.setVocationType(list.get(0).getVocationType());
		tempShipment.setAddDt(new Date());
		tempShipment.setAddBy(i.getId());
		tempShipment.setPlatformId(i.getPlatformId());
		Long id=tempShipmentDao.add(tempShipment);
		legsDao.updTempshipmenByIds(ids, id, i);
		TempShipment tempShipment2 =udpTemp(tempshipmentId, tempShipment,i);
		return tempShipment2;
	}

	//更行temp的值
	public TempShipment udpTemp(Long id,TempShipment tempShipment,I i){
		TempShipment tempShipment1=tempShipmentDao.findById(id);
		tempShipment1.setQuantity(tempShipment1.getQuantity()-tempShipment.getQuantity());
		tempShipment1.setWeight(tempShipment1.getWeight()-tempShipment.getWeight());
		tempShipment1.setVolume(tempShipment1.getVolume()-tempShipment.getVolume());
		tempShipment1.setPoints(tempShipment1.getPoints()-tempShipment.getPoints());
		tempShipment1.setUpdBy(i.getId());
		tempShipment1.setUpdDt(new Date());
		Integer count=legsDao.findCount(id);
		tempShipment1.setCount(count);
		tempShipmentDao.updById(tempShipment1);
		return tempShipment1;
	}
	
	//取消配载
	public void cancel(List<Long> ids,I i){
		Double quantity=0.0;
		Double weight=0.0;
		Double volume=0.0;
		Integer points=0;
		Integer count=0;
		List<TempShipment> list=tempShipmentDao.findByIds(ids);
		for(TempShipment tempShipment:list){
			quantity+=tempShipment.getQuantity();
			weight+=tempShipment.getWeight();
			volume+=tempShipment.getVolume();
			points+=tempShipment.getPoints();
		}
		//删除temp表中的记录
		tempShipmentDao.delById(ids);
		TempShipment tempShipment=tempShipmentDao.findIdByCd("未配载", list.get(0).getVocationType(), i);
		//更新legs的temp id
		legsDao.updStById(ids, tempShipment.getId(), i);
		count=legsDao.findCount(tempShipment.getId());
		tempShipment.setQuantity(tempShipment.getQuantity()+quantity);
		tempShipment.setWeight(tempShipment.getWeight()+weight);
		tempShipment.setVolume(tempShipment.getVolume()+volume);
		tempShipment.setPoints(points+tempShipment.getPoints());
		tempShipment.setCount(count);
		tempShipment.setUpdBy(i.getId());
		tempShipment.setAddDt(new Date());
		tempShipmentDao.updById(tempShipment);
	}
	private static final Logger logger = LoggerFactory.getLogger(TempShipment.class);
}
