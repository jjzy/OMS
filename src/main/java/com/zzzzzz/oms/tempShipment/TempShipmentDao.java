package com.zzzzzz.oms.tempShipment;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.zzzzzz.oms.driver.Driver;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class TempShipmentDao {
	
	@Resource
	private BaseDao baseDao;		
	private final static String sql_add = "insert into t_tempShipment(code, platformId,vocationType,points,count,quantity, weight, volume ,addBy, addDt) values(:code, :platformId,:vocationType,:points,:count,:quantity, :weight, :volume, :addBy, :addDt)";
	private final static String sql_add1="insert into t_tempShipment(code,platformId,vocationType,points,count,quantity, weight, volume,expense,addBy, addDt) values(:code, :platformId,:vocationType,0,0,0.0,0.0,0.0,0.0,:addBy, :addDt)";
	private final static String sql_updStByIds = "update t_tempShipment set vehicleId=:vehicleId,constractorId=:constractorId, driverId=:driverId,shipment_method=:shipment_method,expense=:expense,planlDt=:planlDt,planaDt=:planaDt,updDt = :updDt, updBy = :updBy where id=:id";
	private final static String sql_updStById = "update t_tempShipment set quantity=:quantity, points=:points,weight=:weight, volume=:volume,count=:count, updDt=:updDt, updBy=:updBy where id = :id";
	private final static String sql_updStatusById = "update t_tempShipment set  updDt=:updDt, updBy=:updBy where id = :id";
	private final static String sql_findById = "select t.*,c.name as name,v.cd,v.vehicleNo,v.vehicleTypeId from t_tempShipment t left join t_constractor c on c.id=t.constractorId left join t_vehicle v on v.id=t.vehicleId where t.id = :id";
	private final static String sql_delById="delete from t_tempShipment where id in (:ids)";	
	private final static String sql_findByIds="select id, code,vocationType, vehicleId,driverId,constractorId, shipment_method,platformId, points, count,quantity, weight, volume,expense,planaDt,planlDt,addBy, updDt, addDt, updBy from t_tempShipment where id in (:ids)";
	public Long add(TempShipment tempShipment){
		return baseDao.updateGetLongKey(sql_add, tempShipment);
	}
	public int add1(String vocationType,I i){
		Finder finder=new Finder(sql_add1).setParam("code", "未配载").setParam("platformId", i.getPlatformId()).setParam("vocationType", vocationType)
				.setParam("addDt", new Date()).setParam("addBy", i.getId());
		return baseDao.update(finder);
	}
	public int updStByIds(Long ids, Long vehicleId, Long constractorId,Long driverId,String shipment_method,String expense,String planlDt,String planaDt,I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("vehicleId", vehicleId).setParam("constractorId", constractorId).setParam("driverId", driverId).setParam("shipment_method", shipment_method).setParam("expense", expense)
		.setParam("planlDt", planlDt).setParam("planaDt", planaDt).setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}	
	
	public int updById(TempShipment tempShipment){
		return baseDao.update(sql_updStById, tempShipment);
	}
	
	public int updStById(Long id, Double quantity,Integer points,Double weight,Double volume,Integer count, I i){
		Finder finder = new Finder(sql_updStById).setParam("id", id).setParam("quantity", quantity)
		.setParam("points", points).setParam("weight", weight).setParam("volume", volume).setParam("count", count).setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}	
	public int updStatusById(Long id,I i){
		Finder finder = new Finder(sql_updStatusById).setParam("id", id)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}	
	public int delById(List<Long> ids){
		Finder finder = new Finder(sql_delById).setParam("ids", ids);
		return baseDao.update(finder);
	}	
	
	public TempShipment findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, TempShipment.class);
	}
	
	public List<TempShipment> findByIds(List<Long> ids){
		Finder finder = new Finder(sql_findByIds).setParam("ids", ids);
		return baseDao.findList(finder, TempShipment.class);
	}
	/**
	 * 动态查询
	 */
	public List<Map<String, Object>> findListBy(Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("t.*,v.cd as vehicleCd");
		finder.append("from t_tempShipment t left join t_vehicle v on v.id=t.vehicleId");
		finder.append(" where 1=1");
		finder.append("and platformId=:platformId").setParam("platformId", i.getPlatformId());
		if(DaoUtils.isNotNull(ffMap.get("code"))){
			if(ffMap.get("code").equals("未配载")){
				finder.append(" and t.code = :code").setParam("code", ffMap.get("code"));
			}else{
				finder.append(" and t.code != :code").setParam("code","未配载");
			}
		}
		if(DaoUtils.isNotNull(ffMap.get("vocation"))){
			finder.append(" and t.vocationType in (:vocations)").setParam("vocations",ffMap.get("vocation"));
		}
		List<Map<String, Object>> list = baseDao.findList(finder);		
		return list;
	}
	
	public TempShipment findIdByCd(String code,String vocationType,I i){
		Finder finder = new Finder("");
		finder.append(" select id, code,vocationType, vehicleId,driverId,constractorId, shipment_method,platformId, points, count,quantity, weight, volume,expense,planaDt,planlDt,addBy, updDt, addDt, updBy from t_tempShipment");
		finder.append("where code=:code").setParam("code", code);
		finder.append("and vocationType=:vocationType").setParam("vocationType", vocationType);
		finder.append("and platformId=:platformId").setParam("platformId", i.getPlatformId());
		List<TempShipment> list=baseDao.findList(finder,TempShipment.class);
		if(list.size()>0){
			return list.get(0);
		}
		return null;
	} 
	
	public List<TempShipment> findByVocations(String code,List<String> vocations,I i){
		Finder finder = new Finder("");
		finder.append(" select id, code,vocationType, vehicleId,driverId,constractorId, shipment_method,platformId, points, count,quantity, weight, volume,expense,planaDt,planlDt,addBy, updDt, addDt, updBy from t_tempShipment");
		finder.append("where code=:code").setParam("code", code);
		finder.append("and vocationType in (:vocations)").setParam("vocations", vocations);
		finder.append("and platformId=:platformId").setParam("platformId", i.getPlatformId());
		List<TempShipment> list=baseDao.findList(finder,TempShipment.class);
		return list;
	} 
	
}
