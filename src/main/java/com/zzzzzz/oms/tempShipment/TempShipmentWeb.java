package com.zzzzzz.oms.tempShipment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzzzzz.oms.GCS;
import com.zzzzzz.oms.legs.Legs;
import com.google.common.collect.Lists;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.plugins.poi.ExportExcel;
import com.zzzzzz.plugins.poi.ImportExcel;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class TempShipmentWeb {

	@Resource
	public TempShipmentService tempShipmentService;
	@Resource
	public TempShipmentDao tempShipmentDao;

	@RequestMapping(value = "/tempShipment/list", method = RequestMethod.GET)
	public String list() {
		return "oms/tempShipment";
	}

	@RequestMapping(value = "/tempShipment/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody List<Legs> list,@RequestParam(value = "code", required = true) String code,@RequestParam(value = "ids", required = true) List<Long> ids,@RequestParam(value = "tempshipmentId", required = true) Long tempshipmentId,
			BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}
		TempShipment tempShipment=tempShipmentService.save(list, code, ids, tempshipmentId, ShiroUtils.findUser());
		return new BaseData(tempShipment, null);
	}
	  
	@RequestMapping(value = "/tempShipment/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestBody @Valid TempShipment tempShipment, BindingResult bindingResult) throws Exception {
		tempShipmentDao.updById(tempShipment);
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/tempShipment/updStById", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStById(@RequestParam(value = "id", required = true) Long id, @RequestParam(value = "quantity", required = true) Double quantity,@RequestParam(value = "points", required = true) Integer points,@RequestParam(value = "weight", required = true) Double weight,@RequestParam(value = "volume", required = true) Double volume,@RequestParam(value = "count", required = true) Integer count) throws Exception {
		tempShipmentDao.updStById(id,quantity,points,weight,volume,count,ShiroUtils.findUser());
		return new BaseData("0");
	}
	@RequestMapping(value = "/tempShipment/updStatusById", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStatusById(@RequestParam(value = "id", required = true) Long id) throws Exception {
		tempShipmentDao.updStatusById(id,ShiroUtils.findUser());
		return new BaseData("0");
	}	
	
	
	@RequestMapping(value = "/tempShipment/delById", method = RequestMethod.POST)
	@ResponseBody
	public BaseData delById(@RequestParam(value = "ids", required = true) List<Long> ids) throws Exception {
		tempShipmentDao.delById(ids);
		return new BaseData("0");
	}	
	
	@RequestMapping(value = "/tempShipment/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		TempShipment tempShipment = tempShipmentDao.findById(id);
		return new BaseData(tempShipment, null);
	}
	@RequestMapping(value = "/tempShipment/findByIds", method = RequestMethod.POST)
	@ResponseBody 
	public BaseData findByIds(@RequestParam(value = "ids", required = true)List<Long> ids) {
		tempShipmentService.cancel(ids, ShiroUtils.findUser());
		return new BaseData("0");
	}
	@RequestMapping(value = "/tempShipment/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		List<Map<String, Object>> list = tempShipmentDao.findListBy(baseQueryForm.getFfMap(),ShiroUtils.findUser());
		return new BaseData(list, null);
	}
	
    
    private static final Logger logger = LoggerFactory.getLogger(TempShipmentWeb.class);
}
