package com.zzzzzz.oms.product;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Validator;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.oms.producttype.ProductTypeDao;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.sys.client.Client;
import com.zzzzzz.sys.client.ClientService;
import com.zzzzzz.sys.dict.Dict;
import com.zzzzzz.sys.dict.DictService;
import com.zzzzzz.utils.DateTimeUtils;

@Service
public class ProductService {
	@Resource
	public ProductDao productDao;
	@Resource
	public Validator validator;
	@Resource
	public ClientService clientService;
	@Resource
	public ProductTypeDao productTypeDao;
	@Resource
	public DictService dictService;
	@Transactional
	public int save(Product product, I i) {
		product.setUpdDt(new Date());
		product.setUpdBy(i.getId());
		if (product.getId() == null) {
			product.setAddDt(new Date());
			product.setAddBy(i.getId());
			productDao.add(product);
		} else {
			productDao.updById(product);
		}

		return 1;
	}
	public Product getProductByCd(String cd,Long productId) {
		Map<String, List<Product>> allProductMap=productDao.getAllProductMap();
		List<Product> productList=allProductMap.get(cd);
		if(productList!=null){
			for(Product product:productList){
				if(product.getClientId().equals(productId)){
					return product;
				}
			}
		}
		return null;
	}
	
		@Transactional
		public List<BaseData> batchAdd(List<Product> list, I i) {
			logger.info("product batchAdd validate start:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
			//start validate
			int errTt = 0;// 错误信息条数
			int susTt = 0;// 正确信息条数
			BaseData baseData;
			String descr=null;
			String val=null;
			//用于存放信息
			List<BaseData> listMsg=new ArrayList<BaseData>();
			List<Dict> listDict=dictService.getDictListByCd("uom");
			for (Product product : list) {
				System.out.println(product.getProducttypeCd()+product.getUom());
				if(StringUtils.isBlank(product.getCd())){
					baseData=new BaseData();
					baseData.setErrMsg("代码"+product.getCd()+"为无效值");
					listMsg.add(baseData);
					errTt++;
				}else{
					Client client=clientService.getclientByCd(product.getClientCd(),i);
					//判断客户代码是否存在
					if(client!=null){
							//判断产品类型，两种情况，一种是没有产品类型的导入，二种是有产品类型的导入；
							//一种，没有导入产品类型
							if(StringUtils.isBlank(product.getProducttypeCd())){
								//判断名称是否存在
								if(!StringUtils.isBlank(product.getName())){
									for(Dict dict:listDict){
										if(dict.getLb().equals(product.getUom())){
											descr=dict.getDescr();
											val=dict.getVal();
											break;
										}
									}
									if(descr!=null){
										if(product.getMuheight()==null){
											product.setMuheight(0.0);
										}
										if(product.getMulength()==null){
											product.setMulength(0.0);
										}
										if(product.getMunetweight()==null){
											product.setMunetweight(0.0);
										}
										if(product.getMuvolume()==null){
											product.setMuvolume(0.0);
										}
										if(product.getMuweight()==null){
											product.setMuweight(0.0);
										}
										if(product.getMuwidth()==null){
											product.setMuwidth(0.0);
										}
										if(product.getCaseheight()==null){
											product.setCaseheight(0.0);
										}
										if(product.getCaselength()==null){
											product.setCaselength(0.0);
										}
										if(product.getCasenetweight()==null){
											product.setCasenetweight(0.0);
										}
										if(product.getCasequantity()==null){
											product.setCasequantity(1.0);
										}
										if(product.getCasevolume()==null){
											product.setCasevolume(0.0);
										}
										if(product.getCaseweight()==null){
											product.setCaseweight(0.0);
										}
										if(product.getCasewidth()==null){
											product.setCasewidth(0.0);
										}
										
										if(product.getMuheight()>=0.0){
											if(product.getMulength()>=0.0){
												if(product.getMunetweight()>=0.0){
													if(product.getMuvolume()>=0.0){
														if(product.getMuweight()>=0.0){
															if(product.getMuwidth()>=0.0){
																if(product.getCaseheight()>=0.0){
																	if(product.getCaselength()>=0.0){
																		if(product.getCasenetweight()>=0.0){
																			if(product.getCasequantity()>=0.0){
																				if(product.getCasevolume()>=0.0){
																					if(product.getCaseweight()>=0.0){
																						if(product.getCasewidth()>=0.0){
																							product.setSt(0);
																							product.setClientId(client.getId());
																							product.setMuCd(descr);
																							product.setMuname(val);
																							product.setCaseCd("case");
																							product.setCasename("箱");
																							try{
																								save(product, i);
																								susTt++;
																							}catch(DuplicateKeyException e){
																								baseData=new BaseData("-1", String.format("物料代码"+product.getCd()+"已经存在"));
																								listMsg.add(baseData);
																								errTt++;
																							}
																							
																						}else{
																							baseData=new BaseData();
																							baseData.setErrMsg("箱宽度"+product.getCasewidth()+"有误");
																							listMsg.add(baseData);
																							errTt++;
																						}
																					}else{
																						baseData=new BaseData();
																						baseData.setErrMsg("箱重量"+product.getCaseweight()+"有误");
																						listMsg.add(baseData);
																						errTt++;
																					}
																				}else{
																					baseData=new BaseData();
																					baseData.setErrMsg("箱体积"+product.getCasevolume()+"有误");
																					listMsg.add(baseData);
																					errTt++;
																				}
																			}else{
																				baseData=new BaseData();
																				baseData.setErrMsg("包含主单位数量"+product.getCasequantity()+"有误");
																				listMsg.add(baseData);
																				errTt++;
																			}
																		}else{
																			baseData=new BaseData();
																			baseData.setErrMsg("箱净重"+product.getCasenetweight()+"有误");
																			listMsg.add(baseData);
																			errTt++;
																		}
																	}else{
																		baseData=new BaseData();
																		baseData.setErrMsg("箱长度"+product.getCaselength()+"有误");
																		listMsg.add(baseData);
																		errTt++;
																	}
																}else{
																	baseData=new BaseData();
																	baseData.setErrMsg("箱高度"+product.getCaseheight()+"有误");
																	listMsg.add(baseData);
																	errTt++;
																}													
															}else{
																baseData=new BaseData();
																baseData.setErrMsg("主宽度"+product.getMuwidth()+"有误");
																listMsg.add(baseData);
																errTt++;
															}													
														}else{
															baseData=new BaseData();
															baseData.setErrMsg("主重量"+product.getMuweight()+"有误");
															listMsg.add(baseData);
															errTt++;
														}
													}else{
														baseData=new BaseData();
														baseData.setErrMsg("主体积"+product.getMuvolume()+"有误");
														listMsg.add(baseData);
														errTt++;
													}
												}else{
													baseData=new BaseData();
													baseData.setErrMsg("主净重"+product.getMunetweight()+"有误");
													listMsg.add(baseData);
													errTt++;
												}
											}else{
												baseData=new BaseData();
												baseData.setErrMsg("主长度"+product.getMulength()+"有误");
												listMsg.add(baseData);
												errTt++;
											}
										}else{
											baseData=new BaseData();
											baseData.setErrMsg("主高度"+product.getMuheight()+"有误");
											listMsg.add(baseData);
											errTt++;
										}	
																		
									}else{
										baseData=new BaseData();
										baseData.setErrMsg("计量单位"+product.getCd()+"有误");
										listMsg.add(baseData);
										errTt++;
									}
								}else{
									baseData=new BaseData();
									baseData.setErrMsg("产品名称"+product.getName()+"不能为空");
									listMsg.add(baseData);
									errTt++;
								}
							}else{
								if(productTypeDao.checkProductType(client.getId(), product.getProducttypeCd())!=null){
									//判断名称是否存在
									if(!StringUtils.isBlank(product.getName())){
										for(Dict dict:listDict){
											if(dict.getLb().equals(product.getUom())){
												descr=dict.getDescr();
												val=dict.getVal();
												break;
											}
										}
										if(descr!=null){
											
											if(product.getMuheight()==null){
												product.setMuheight(0.0);
											}
											if(product.getMulength()==null){
												product.setMulength(0.0);
											}
											if(product.getMunetweight()==null){
												product.setMunetweight(0.0);
											}
											if(product.getMuvolume()==null){
												product.setMuvolume(0.0);
											}
											if(product.getMuweight()==null){
												product.setMuweight(0.0);
											}
											if(product.getMuwidth()==null){
												product.setMuwidth(0.0);
											}
											if(product.getCaseheight()==null){
												product.setCaseheight(0.0);
											}
											if(product.getCaselength()==null){
												product.setCaselength(0.0);
											}
											if(product.getCasenetweight()==null){
												product.setCasenetweight(0.0);
											}
											if(product.getCasequantity()==null){
												product.setCasequantity(1.0);
											}
											if(product.getCasevolume()==null){
												product.setCasevolume(0.0);
											}
											if(product.getCaseweight()==null){
												product.setCaseweight(0.0);
											}
											if(product.getCasewidth()==null){
												product.setCasewidth(0.0);
											}
											
											if(product.getMuheight()>=0.0){
												if(product.getMulength()>=0.0){
													if(product.getMunetweight()>=0.0){
														if(product.getMuvolume()>=0.0){
															if(product.getMuweight()>=0.0){
																if(product.getMuwidth()>=0.0){
																	if(product.getCaseheight()>=0.0){
																		if(product.getCaselength()>=0.0){
																			if(product.getCasenetweight()>=0.0){
																				if(product.getCasequantity()>=0.0){
																					if(product.getCasevolume()>=0.0){
																						if(product.getCaseweight()>=0.0){
																							if(product.getCasewidth()>=0.0){
																								product.setSt(0);
																								product.setClientId(client.getId());
																								product.setMuCd(descr);
																								product.setMuname(val);
																								product.setProducttypeId(productTypeDao.checkProductType(client.getId(), product.getProducttypeCd()).getId());
																								product.setCaseCd("case");
																								product.setCasename("箱");
																								try{
																									save(product, i);
																									susTt++;
																								}catch(DuplicateKeyException e){
																									baseData=new BaseData("-1", String.format("物料代码"+product.getCd()+"已经存在"));
																									listMsg.add(baseData);
																									errTt++;
																								}
																							}else{
																								baseData=new BaseData();
																								baseData.setErrMsg("箱宽度"+product.getCasewidth()+"有误");
																								listMsg.add(baseData);
																								errTt++;
																							}
																						}else{
																							baseData=new BaseData();
																							baseData.setErrMsg("箱重量"+product.getCaseweight()+"有误");
																							listMsg.add(baseData);
																							errTt++;
																						}
																					}else{
																						baseData=new BaseData();
																						baseData.setErrMsg("箱体积"+product.getCasevolume()+"有误");
																						listMsg.add(baseData);
																						errTt++;
																					}
																				}else{
																					baseData=new BaseData();
																					baseData.setErrMsg("包含主单位数量"+product.getCasequantity()+"有误");
																					listMsg.add(baseData);
																					errTt++;
																				}
																			}else{
																				baseData=new BaseData();
																				baseData.setErrMsg("箱净重"+product.getCasenetweight()+"有误");
																				listMsg.add(baseData);
																				errTt++;
																			}
																		}else{
																			baseData=new BaseData();
																			baseData.setErrMsg("箱长度"+product.getCaselength()+"有误");
																			listMsg.add(baseData);
																			errTt++;
																		}
																	}else{
																		baseData=new BaseData();
																		baseData.setErrMsg("箱高度"+product.getCaseheight()+"有误");
																		listMsg.add(baseData);
																		errTt++;
																	}													
																}else{
																	baseData=new BaseData();
																	baseData.setErrMsg("主宽度"+product.getMuwidth()+"有误");
																	listMsg.add(baseData);
																	errTt++;
																}													
															}else{
																baseData=new BaseData();
																baseData.setErrMsg("主重量"+product.getMuweight()+"有误");
																listMsg.add(baseData);
																errTt++;
															}
														}else{
															baseData=new BaseData();
															baseData.setErrMsg("主体积"+product.getMuvolume()+"有误");
															listMsg.add(baseData);
															errTt++;
														}
													}else{
														baseData=new BaseData();
														baseData.setErrMsg("主净重"+product.getMunetweight()+"有误");
														listMsg.add(baseData);
														errTt++;
													}
												}else{
													baseData=new BaseData();
													baseData.setErrMsg("主长度"+product.getMulength()+"有误");
													listMsg.add(baseData);
													errTt++;
												}
											}else{
												baseData=new BaseData();
												baseData.setErrMsg("主高度"+product.getMuheight()+"有误");
												listMsg.add(baseData);
												errTt++;
											}
									
											
										
										}else{
											baseData=new BaseData();
											baseData.setErrMsg("计量单位"+product.getUom()+"有误");
											listMsg.add(baseData);
											errTt++;
										}
									}else{
										baseData=new BaseData();
										baseData.setErrMsg("产品名称"+product.getName()+"不能为空");
										listMsg.add(baseData);
										errTt++;
									}
								}else{
									baseData=new BaseData();
									baseData.setErrMsg("产品类型代码"+product.getProducttypeCd()+"不存在");
									listMsg.add(baseData);
									errTt++;
								}
							}
					}else{
						baseData=new BaseData();
						baseData.setErrMsg("客户代码"+product.getClientCd()+"不存在");
						listMsg.add(baseData);
						errTt++;
					}
				}
			}
			baseData = new BaseData();
			baseData.setErrMsg("总共有" + (susTt + errTt) + "行记录；共有" + susTt+ "行记录导入成功；共有" + errTt + "行记录导入失败；");
			listMsg.add(baseData);
			logger.info("city batchAdd save end:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
			return listMsg;
		}
	private static final Logger logger = LoggerFactory.getLogger(Product.class);
}
