package com.zzzzzz.oms.product;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Min;

import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class Product implements Serializable {
	
	
	
  	private Long id;
	private Long clientId;
	@ExcelField(title = "所属客户", align = 2, sort = 10)
	private String clientCd;
	private String clientName;
	@ExcelField(title = "产品类型", align = 2, sort = 20)
	private String producttypeCd;
	private String typeName;
	private Long producttypeId;
	@ExcelField(title = "代码", align = 2, sort = 30)
  	private String cd;
	@ExcelField(title = "名称", align = 2, sort = 40)
  	private String name;
	@ExcelField(title = "计量单位", align = 2, sort = 50)
  	private String uom;
  	private String muCd;
  	private String muname;
	
  	@ExcelField(title = "主体积", align = 2, sort = 60)
  	@Min(value=0, message="muvolume必须大于0")
  	private Double muvolume;
	@ExcelField(title = "主重量", align = 2, sort = 70)
	@Min(value=0, message="muweight必须大于0")
  	private Double muweight;
	@ExcelField(title = "主长度", align = 2, sort = 80)
	@Min(value=0, message="mulength必须大于0")
	private Double mulength;

	@ExcelField(title = "主宽度", align = 2, sort = 90)
	@Min(value=0, message="muwidth必须大于0")
  	private Double muwidth;
	@ExcelField(title = "主高度", align = 2, sort = 100)
	@Min(value=0, message="muheight必须大于0")
  	private Double muheight;
	@ExcelField(title = "主净重", align = 2, sort = 110)
	@Min(value=0, message="munetweight必须大于0")
  	private Double munetweight;
	
  	private String caseCd;
	
  	private String casename;
  	@ExcelField(title = "包含主单位数量", align = 2, sort = 120)
  	@Min(value=0, message="casequantity必须大于0")
  	private Double casequantity;
  	@ExcelField(title = "箱长度", align = 2, sort = 130)
  	@Min(value=0, message="caselength必须大于0")
  	private Double caselength;
  	@ExcelField(title = "箱宽度", align = 2, sort = 140)
  	@Min(value=0, message="casewidth必须大于0")
  	private Double casewidth;
  	@ExcelField(title = "箱高度", align = 2, sort = 150)
  	@Min(value=0, message="caseheight必须大于0")
  	private Double caseheight;
  	@ExcelField(title = "箱单位重量", align = 2, sort = 160)
  	@Min(value=0, message="caseweight必须大于0")
  	private Double caseweight;
  	@ExcelField(title = "箱净重", align = 2, sort = 180)
  	@Min(value=0, message="casenetweight必须大于0")
  	private Double casenetweight;
  	@ExcelField(title = "箱体积", align = 2, sort = 170)
  	@Min(value=0, message="casevolume必须大于0")
  	private Double casevolume;
	
	
	
  	private Date addDt;
	
	
  	private Long addBy;
	
	
  	private Date updDt;
	
	
  	private Long updBy;
	
  	private Integer st;
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

  	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public Long getClientId(){
  		return clientId;
  	}
  	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}
  	public String getClientCd() {
		return clientCd;
	}
	public void setClientCd(String clientCd) {
		this.clientCd = clientCd;
	}
	public Long getProducttypeId(){
  		return producttypeId;
  	}
  	public void setProducttypeId(Long producttypeId) {
		this.producttypeId = producttypeId;
	}
  	public String getProducttypeCd() {
		return producttypeCd;
	}
	public void setProducttypeCd(String producttypeCd) {
		this.producttypeCd = producttypeCd;
	}
	public String getCd(){
  		return cd;
  	}
  	public void setCd(String cd) {
		this.cd = cd;
	}
  	public String getName(){
  		return name;
  	}
  	public void setName(String name) {
		this.name = name;
	}
  	public String getUom(){
  		return uom;
  	}
  	public void setUom(String uom) {
		this.uom = uom;
	}
  	public String getMuCd(){
  		return muCd;
  	}
  	public void setMuCd(String muCd) {
		this.muCd = muCd;
	}
  	public String getMuname(){
  		return muname;
  	}
  	public void setMuname(String muname) {
		this.muname = muname;
	}
  	public Double getMulength(){
  		return mulength;
  	}
  	public void setMulength(Double mulength) {
		this.mulength = mulength;
	}
  	public Double getMuwidth(){
  		return muwidth;
  	}
  	public void setMuwidth(Double muwidth) {
		this.muwidth = muwidth;
	}
  	public Double getMuheight(){
  		return muheight;
  	}
  	public void setMuheight(Double muheight) {
		this.muheight = muheight;
	}
  	public Double getMuvolume(){
  		return muvolume;
  	}
  	public void setMuvolume(Double muvolume) {
		this.muvolume = muvolume;
	}
  	public Double getMuweight(){
  		return muweight;
  	}
  	public void setMuweight(Double muweight) {
		this.muweight = muweight;
	}
  	public Double getMunetweight(){
  		return munetweight;
  	}
  	public void setMunetweight(Double munetweight) {
		this.munetweight = munetweight;
	}
  	public String getCaseCd(){
  		return caseCd;
  	}
  	public void setCaseCd(String caseCd) {
		this.caseCd = caseCd;
	}
  	public String getCasename(){
  		return casename;
  	}
  	public void setCasename(String casename) {
		this.casename = casename;
	}
  	public Double getCasequantity(){
  		return casequantity;
  	}
  	public void setCasequantity(Double casequantity) {
		this.casequantity = casequantity;
	}
  	public Double getCaselength(){
  		return caselength;
  	}
  	public void setCaselength(Double caselength) {
		this.caselength = caselength;
	}
  	public Double getCasewidth(){
  		return casewidth;
  	}
  	public void setCasewidth(Double casewidth) {
		this.casewidth = casewidth;
	}
  	public Double getCaseheight(){
  		return caseheight;
  	}
  	public void setCaseheight(Double caseheight) {
		this.caseheight = caseheight;
	}
  	public Double getCaseweight(){
  		return caseweight;
  	}
  	public void setCaseweight(Double caseweight) {
		this.caseweight = caseweight;
	}
  	public Double getCasenetweight(){
  		return casenetweight;
  	}
  	public void setCasenetweight(Double casenetweight) {
		this.casenetweight = casenetweight;
	}
  	public Double getCasevolume(){
  		return casevolume;
  	}
  	public void setCasevolume(Double casevolume) {
		this.casevolume = casevolume;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
  	public Integer getSt(){
  		return st;
  	}
  	public void setSt(Integer st) {
		this.st = st;
	}
 	
	public static Product newTest() {
		Product product = new Product();
		
		
		
		//product.setCd("");
		//product.setName("");
		//product.setUom("");
		//product.setMuCd("");
		//product.setMuname("");
		//product.setMulength("");
		//product.setMuwidth("");
		//product.setMuheight("");
		//product.setMuvolume("");
		//product.setMuweight("");
		//product.setMunetweight("");
		//product.setCaseCd("");
		//product.setCasename("");
		//product.setCasequantity("");
		//product.setCaselength("");
		//product.setCasewidth("");
		//product.setCaseheight("");
		//product.setCaseweight("");
		//product.setCasenetweight("");
		//product.setCasevolume("");
		
		
		
		
		//product.setSt("");
		return product;
	}
}


