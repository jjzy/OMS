package com.zzzzzz.oms.product;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.zzzzzz.oms.ordertype.OrderType;
import com.zzzzzz.oms.receiver.Receiver;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.sys.client.Client;
import com.zzzzzz.utils.page.Page;
import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class ProductDao {
	
	@Resource
	private BaseDao baseDao;
			
	private final static String sql_add = "insert into t_product(clientId, producttypeId, cd, name, uom, muCd, muname, mulength, muwidth, muheight, muvolume, muweight, munetweight, caseCd, casename, casequantity, caselength, casewidth, caseheight, caseweight, casenetweight, casevolume, addDt, addBy, st) values(:clientId, :producttypeId, :cd, :name, :uom, :muCd, :muname, :mulength, :muwidth, :muheight, :muvolume, :muweight, :munetweight, :caseCd, :casename, :casequantity, :caselength, :casewidth, :caseheight, :caseweight, :casenetweight, :casevolume, :addDt, :addBy, :st)";
	private final static String sql_updStByIds = "update t_product set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update t_product set clientId=:clientId, producttypeId=:producttypeId, cd=:cd, name=:name, uom=:uom, muCd=:muCd, muname=:muname, mulength=:mulength, muwidth=:muwidth, muheight=:muheight, muvolume=:muvolume, muweight=:muweight, munetweight=:munetweight, caseCd=:caseCd, casename=:casename, casequantity=:casequantity, caselength=:caselength, casewidth=:casewidth, caseheight=:caseheight, caseweight=:caseweight, casenetweight=:casenetweight, casevolume=:casevolume, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, clientId, producttypeId, cd, name, uom, muCd, muname, mulength, muwidth, muheight, muvolume, muweight, munetweight, caseCd, casename, casequantity, caselength, casewidth, caseheight, caseweight, casenetweight, casevolume, addDt, addBy, updDt, updBy, st from t_product where id = :id";
	private final static String sql_findByCd="select*from t_product where cd=:cd";
	@CacheEvict(value = "productCache", allEntries = true)
	public Long add(Product product){
		return baseDao.updateGetLongKey(sql_add, product);
	}
	@CacheEvict(value = "productCache", allEntries = true)
	public int updStByIds(List<Long> ids, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}	
	@CacheEvict(value = "productCache", allEntries = true)
	public int updById(Product product){
		return baseDao.update(sql_upd, product);
	}
	public Product findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, Product.class);
	}
	public Product findByCd(String cd){
		Finder finder = new Finder(sql_findByCd).setParam("cd", cd);
		return baseDao.findOne(finder, Product.class);
	}
	
	/**
	 * 动态查询
	 */
	public List<Product> findListBy(Page page,Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("count(1) as tt");
		finder.append(" from t_product p inner join t_client c on p.clientId=c.id left join t_product_type t on t.id=p.producttypeId left join t_user_client u on u.clientId=p.clientId");
		finder.append(" where 1=1");
		finder.append("and c.platformId=:platformId").setParam("platformId", i.getPlatformId());
		finder.append("and u.userId=:userId").setParam("userId", i.getId());
		if(DaoUtils.isNotNull(ffMap.get("clientId"))){
		finder.append(" and p.clientId = :clientId").setParam("clientId", ffMap.get("clientId"));
		}
		if(DaoUtils.isNotNull(ffMap.get("cd"))){
		finder.append(" and p.cd = :cd").setParam("cd", ffMap.get("cd"));
		}
		if(DaoUtils.isNotNull(ffMap.get("name"))){
		finder.append(" and p.name like :name").setParam("name", "%" + ffMap.get("name") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and p.st = :st").setParam("st", ffMap.get("st"));
			}else{
				finder.append(" and p.st = 0");
			}
		finder.append("order by p.id desc");
		if (page != null) {
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}
		String sql = finder.getSql().replace("count(1) as tt","p.*,c.name as clientName,t.name as typeName");
		finder.setSql(sql);		
		List<Product> list = baseDao.findList(finder, Product.class);			
		return list;
	}
	/*
	 * 缓存
	 */
	public List<Product> findAllBy(){
		Finder finder = new Finder("");
		finder.append("select");
		finder.append("id, clientId, producttypeId, cd, name, uom, muCd, muname, mulength, muwidth, muheight, muvolume, muweight, munetweight, caseCd, casename, casequantity, caselength, casewidth, caseheight, caseweight, casenetweight, casevolume, addDt, addBy, updDt, updBy, st");
		finder.append("from t_product");
		List<Product> list = baseDao.findList(finder, Product.class);
		return list;
	}
	
	@Cacheable(value = "productCache")
	public Map<String, List<Product>> getAllProductMap() {
		Map<String, List<Product>> productMap= new HashMap<String, List<Product>>();
		List<Product> allProductList = findAllBy();
		for (Product product : allProductList) {
			List<Product> productList = productMap.get(product.getCd());
			if (productList != null) {
				productList.add(product);
			} else {
				productMap.put(product.getCd(), Lists.newArrayList(product));
			}
		}
		return productMap;
	}
}
