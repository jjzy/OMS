package com.zzzzzz.oms.product;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.WebUtils;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.sys.client.Client;
import com.zzzzzz.utils.page.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzzzzz.oms.GCS;
import com.zzzzzz.oms.producttype.ProductType;
import com.google.common.collect.Lists;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.plugins.poi.ExportExcel;
import com.zzzzzz.plugins.poi.ImportExcel;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class ProductWeb {

	@Resource
	public ProductService productService;
	@Resource
	public ProductDao productDao;

	@RequestMapping(value = "/product/list", method = RequestMethod.GET)
	public String list() {
		return "oms/product";
	}

	@RequestMapping(value = "/product/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid Product product, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}
		productService.save(product, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/product/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		productDao.updStByIds(ids, st, ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	
	
	@RequestMapping(value = "/product/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		Product product = productDao.findById(id);
		return new BaseData(product, null);
	}
	

	@RequestMapping(value = "/product/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		Page page;
		List<Product> list;
		BaseData baseData;
		if(baseQueryForm.getPageNb()==null&&baseQueryForm.getPageSize()==null){
			list = productDao.findListBy(null,baseQueryForm.getFfMap(),ShiroUtils.findUser());
			baseData=new BaseData(list, null);
		}else{
			page=baseQueryForm.getPage();
			list = productDao.findListBy(page,baseQueryForm.getFfMap(),ShiroUtils.findUser());
			baseData=new BaseData(list, page.getTotal());
		}
		return baseData;
	}
	
	@RequestMapping(value = "/product/import", method=RequestMethod.POST)
    public String importExcel(@RequestParam MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			if(StringUtils.isBlank(file.getOriginalFilename())){
				List<BaseData> list3=new ArrayList<BaseData>();
				BaseData baseData=new BaseData();
				baseData.setErrMsg("您还没选择文件，请先选择需要导入的文件！");
				list3.add(baseData);
				redirectAttributes.addFlashAttribute("msg",list3);
			}else{
				ImportExcel importExcel = new ImportExcel(file, 1, 0);
				if(file.getOriginalFilename().contains("产品信息")){
					List<Product> list = importExcel.getDataList(Product.class);
					List<BaseData> list1 =productService.batchAdd(list, ShiroUtils.findUser());
					redirectAttributes.addFlashAttribute("msg", list1);
				}else{
					List<BaseData> list2=new ArrayList<BaseData>();
					BaseData baseData=new BaseData();
					baseData.setErrMsg("你导入模板有误，请重新选择模板导入。");
					list2.add(baseData);
					redirectAttributes.addFlashAttribute("msg",list2);
				}
			}			
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "导入数据保存失败。");
			logger.warn("importExcel", e);
		}
		return "redirect:/msg";
	}
    @RequestMapping(value = "product/import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "产品信息数据导入模板.xlsx";
            List<Product> list = Lists.newArrayList();
    		list.add(Product.newTest());
    		new ExportExcel("产品信息数据导入模板", Product.class, 2).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "产品信息数据导入模板下载失败。");
			logger.warn("importFileTemplate", e);
		}
		return "redirect:/msg";
    }
    
    @RequestMapping(value = "/product/export")
    public String exportFileTemplate(HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
  		try {
  			
  			Map<String, Object> params = WebUtils.getParametersStartingWith(request, null);
  			
  			//Page page=baseQueryForm.getPage();
              String fileName = "产品信息数据.xlsx";
              
            List<Product> list = productDao.findListBy(null, params,ShiroUtils.findUser());
            List<Product> list1=new ArrayList<Product>();
      		for(Product product:list){
      			product.setClientCd(product.getClientName());
      			product.setProducttypeCd(product.getTypeName());
      			product.setUom(product.getMuname());
      			list1.add(product);
      		} 
              new ExportExcel("产品信息数据", Product.class, 1).setDataList(list).write(response, fileName).dispose();
      		return null;
  		} catch (Exception e) {
  			redirectAttributes.addFlashAttribute("msg", "产品信息数据导出失败。");
  			logger.warn("importFileTemplate", e);
  		}
  		return "redirect:/msg";
      }
    
    private static final Logger logger = LoggerFactory.getLogger(ProductWeb.class);
}
