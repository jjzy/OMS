package com.zzzzzz.oms.translocation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Validator;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.oms.receiver.Receiver;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.sys.city.CityService;
import com.zzzzzz.utils.DateTimeUtils;

@Service
public class TransLocationService {
	@Resource
	public TransLocationDao transLocationDao;
	@Resource
	public Validator validator;
	@Resource
	public CityService cityService;
	@Transactional
	public int save(TransLocation transLocation, I i) {
		transLocation.setUpdDt(new Date());
		transLocation.setUpdBy(i.getId());
		if (transLocation.getId() == null) {
			transLocation.setAddDt(new Date());
			transLocation.setAddBy(i.getId());
			transLocation.setPlatformId(i.getPlatformId());
			transLocationDao.add(transLocation);
		} else {
			transLocationDao.updById(transLocation);
		}

		return 1;
	}
	//找cd
		public TransLocation getTransLocationByCd(I i,String cd) {
			Map<String, List<TransLocation>> allTransLocationMap = transLocationDao.getAllTransLocationMap();
			List<TransLocation> transLocationList=allTransLocationMap.get(cd);
			if(transLocationList!=null){
				for(TransLocation transLocation:transLocationList){
					if(transLocation.getPlatformId().equals(i.getPlatformId())){
						return transLocation;
					}
				}
			}
			return null;
		}
		@Transactional
		public List<BaseData> batchAdd(List<TransLocation> list, I i) {
			logger.info("transLocation batchAdd validate start:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
			//start validate
			int errTt = 0;// 错误信息条数
			int susTt = 0;// 正确信息条数
			BaseData baseData;
			//用于存放信息
			List<BaseData> listMsg=new ArrayList<BaseData>();
			for (TransLocation transLocation : list) {
				//验证城市代码是否为空
				if(StringUtils.isBlank(transLocation.getCityCd())){
					//如果为空
					errTt++;
					baseData =new BaseData();
					baseData.setErrMsg("城市"+transLocation.getCityCd()+"为无效值");
					listMsg.add(baseData);
				}else{
					//验证城市是否存在
					if(cityService.getCityByCd(transLocation.getCityCd())!=null){
					//判断运输地代码是否为空值
						if(StringUtils.isBlank(transLocation.getCd())){
							baseData =new BaseData();
							baseData.setErrMsg("运输地"+transLocation.getCd()+"为无效值");
							listMsg.add(baseData);
							errTt++;
						}else{
								transLocation.setSt(0);
								transLocation.setPlatformId(i.getPlatformId());
								transLocation.setCityId(cityService.getCityByCd(transLocation.getCityCd()).getId());								
								try{
									save(transLocation, i);
									susTt++;
								}catch(DuplicateKeyException e){
									baseData=new BaseData("-1", String.format("收发货方代码"+transLocation.getCd()+"已经存在"));
									listMsg.add(baseData);
									errTt++;
								}
						}
					}else{
						errTt++;
						baseData =new BaseData();
						baseData.setErrMsg("城市"+transLocation.getCityCd()+"不存在");
						listMsg.add(baseData);
					}
				}
			}
			baseData = new BaseData();
			baseData.setErrMsg("总共有" + (susTt + errTt) + "行记录；共有" + susTt+ "行记录导入成功；共有" + errTt + "行记录导入失败；");
			listMsg.add(baseData);
			logger.info("transLocation batchAdd save end:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
			return listMsg;
		}
	private static final Logger logger = LoggerFactory.getLogger(TransLocation.class);
}
