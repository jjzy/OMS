package com.zzzzzz.oms.translocation;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;
import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class TransLocation implements Serializable {
	
	
  	private Long id;
	
	
  	private Long cityId;
	
	private Long platformId;
	@ExcelField(title = "城市", align = 2, sort = 10)
	private String cityCd;
	@ExcelField(title = "运输地代码", align = 2, sort = 20)
  	private String cd;
	@ExcelField(title = "运输地名称", align = 2, sort = 30)
  	private String name;
	@ExcelField(title = "运输地简称", align = 2, sort = 40)
  	private String shortname;
	private String cityName;
  	private Integer st;
	
  	private Date addDt;
	
  	private Long addBy;
	
  	private Date updDt;
	
  	private Long updBy;
  	
	
	private String platformName;
	
  	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getPlatformName() {
		return platformName;
	}
	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public Long getCityId(){
  		return cityId;
  	}
  	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
  	public Long getPlatformId(){
  		return platformId;
  	}
  	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
  	public String getCd(){
  		return cd;
  	}
  	public void setCd(String cd) {
		this.cd = cd;
	}
  	public String getName(){
  		return name;
  	}
  	public void setName(String name) {
		this.name = name;
	}
  	public String getShortname(){
  		return shortname;
  	}
  	public void setShortname(String shortname) {
		this.shortname = shortname;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
  	public Integer getSt(){
  		return st;
  	}
  	public void setSt(Integer st) {
		this.st = st;
	}
 	
  	
 	public String getCityCd() {
		return cityCd;
	}
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	public static TransLocation newTest() {
		TransLocation transLocation = new TransLocation();
		transLocation.setCd("");
		transLocation.setCityCd("");
		transLocation.setName("");
		transLocation.setShortname("");
		return transLocation;
	}
}


