package com.zzzzzz.oms.translocation;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.zzzzzz.oms.product.Product;
import com.zzzzzz.oms.receiver.Receiver;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.sys.client.Client;
import com.zzzzzz.utils.comp.ClientChineseComp;
import com.zzzzzz.utils.comp.TranslocationChineseComp;
import com.zzzzzz.utils.page.Page;
import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class TransLocationDao {
	
	@Resource
	private BaseDao baseDao;
	private final static String sql_add = "insert into t_trans_location(cityId, platformId, cd, name, shortname, addDt, addBy, st) values(:cityId, :platformId, :cd, :name, :shortname, :addDt, :addBy, :st)";
	private final static String sql_updStByIds = "update t_trans_location set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update t_trans_location set cityId=:cityId, platformId=:platformId, cd=:cd, name=:name, shortname=:shortname, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, cityId, platformId, cd, name, shortname, addDt, addBy, updDt, updBy, st from t_trans_location where id = :id";
	@CacheEvict(value = "translocationCache", allEntries = true)
	public Long add(TransLocation transLocation){
		return baseDao.updateGetLongKey(sql_add, transLocation);
	}
	@CacheEvict(value = "translocationCache", allEntries = true)
	public int updStByIds(List<Long> ids, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}		
	@CacheEvict(value = "translocationCache", allEntries = true)
	public int updById(TransLocation transLocation){
		return baseDao.update(sql_upd, transLocation);
	}
	public TransLocation findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, TransLocation.class);
	}
	
	/**
	 * 动态查询
	 */
	public List<TransLocation> findListBy(Page page,Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("count(1) as tt");
		finder.append(" from t_trans_location t inner join sys_city c on t.cityId=c.id inner join sys_platform p on t.platformId=p.id");
		finder.append(" where 1=1");
		finder.append("and t.platformId=:platformId").setParam("platformId", i.getPlatformId());
		if(DaoUtils.isNotNull(ffMap.get("cityId"))){
		finder.append(" and t.cityId = :cityId").setParam("cityId", ffMap.get("cityId"));
		}
		if(DaoUtils.isNotNull(ffMap.get("cd"))){
		finder.append(" and t.cd = :cd").setParam("cd", ffMap.get("cd"));
		}
		if(DaoUtils.isNotNull(ffMap.get("name"))){
		finder.append(" and t.name like :name").setParam("name", "%" + ffMap.get("name") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and t.st = :st").setParam("st", ffMap.get("st"));
			}else{
				finder.append(" and t.st = 0");
			}
		finder.append("order by t.id desc");
		if (page != null) {
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}
		String sql = finder.getSql().replace("count(1) as tt","t.*,c.name as cityName,p.name as platformName");
		finder.setSql(sql);		
		List<TransLocation> list = baseDao.findList(finder, TransLocation.class);
				
		return list;
	}
	
	/*
	 * 实现按照中文首字母排序
	 */
	public List<TransLocation> findListByFirst(Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("t.*,c.name as cityName,p.name as platformName");
		finder.append(" from t_trans_location t inner join sys_city c on t.cityId=c.id inner join sys_platform p on t.platformId=p.id");
		finder.append(" where 1=1");
		finder.append("and t.platformId=:platformId").setParam("platformId", i.getPlatformId());
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and c.st = :st").setParam("st", ffMap.get("st"));
		}		
		List<TransLocation> list = baseDao.findList(finder, TransLocation.class);
		Comparator cm=new TranslocationChineseComp(); 
		Collections.sort(list, cm);
		return list;
	}
	
	/*
	 * 缓存
	 */
	public List<TransLocation> findListBy(){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("id, cityId, platformId, cd, name, shortname, addDt, addBy, updDt, updBy, st");
		finder.append("from t_trans_location");
		List<TransLocation> list = baseDao.findList(finder, TransLocation.class);
		return list;
	}
	
	
	@Cacheable(value = "translocationCache")
	public Map<String, List<TransLocation>> getAllTransLocationMap() {
		Map<String, List<TransLocation>> transLocationMap= new HashMap<String, List<TransLocation>>();
		List<TransLocation> allTransLocationList =findListBy();
		for (TransLocation transLocation : allTransLocationList) {
			List<TransLocation> transLocationList = transLocationMap.get(transLocation.getCd());
			if (transLocationList != null) {
				transLocationList.add(transLocation);
			} else {
				transLocationMap.put(transLocation.getCd(), Lists.newArrayList(transLocation));
			}
		}
		return transLocationMap;
	}
}
