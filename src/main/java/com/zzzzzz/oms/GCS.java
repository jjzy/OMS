package com.zzzzzz.oms;

public class GCS {
	
	public final static String ERRCD_SUS = "0";
	public final static String ERRCD_PARAM = "1";// 入参错误
	public final static String ERRCD_TIMEOUT = "2";// 超時
	public final static String ERRCD_SYS = "9";// 系統錯誤
	
	public final static Integer EXPORT_EXCEL_MAX_ROW = 50000;
	
	public final static String COL_PLATFORM_ID = "platformId";
}
