package com.zzzzzz.oms.receiver;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.WebUtils;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.sys.city.City;
import com.zzzzzz.utils.page.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzzzzz.oms.GCS;
import com.zzzzzz.oms.translocation.TransLocation;
import com.google.common.collect.Lists;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.plugins.poi.ExportExcel;
import com.zzzzzz.plugins.poi.ImportExcel;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class ReceiverWeb {

	@Resource
	public ReceiverService receiverService;
	@Resource
	public ReceiverDao receiverDao;

	@RequestMapping(value = "/receiver/list", method = RequestMethod.GET)
	public String list() {
		return "oms/receiver";
	}

	@RequestMapping(value = "/receiver/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid Receiver receiver, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}
		receiverService.save(receiver, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/receiver/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		receiverDao.updStByIds(ids, st, ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	
	
	@RequestMapping(value = "/receiver/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		if(id==0){
			return null;
		}
		Receiver receiver = receiverDao.findById(id);
		return new BaseData(receiver, null);
	}

	@RequestMapping(value = "/receiver/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		Page page;
		List<Receiver> list;
		BaseData baseData;
		if(baseQueryForm.getPageNb()==null&&baseQueryForm.getPageSize()==null){
			list = receiverDao.findListBy(null,baseQueryForm.getFfMap(),ShiroUtils.findUser());
			baseData=new BaseData(list, null);
		}else{
			page=baseQueryForm.getPage();
			list = receiverDao.findListBy(page,baseQueryForm.getFfMap(),ShiroUtils.findUser());
			baseData=new BaseData(list, page.getTotal());
		}
		return baseData;
	}
		
	@RequestMapping(value = "/receiver/import", method=RequestMethod.POST)
    public String importExcel(@RequestParam MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			if(StringUtils.isBlank(file.getOriginalFilename())){
				List<BaseData> list3=new ArrayList<BaseData>();
				BaseData baseData=new BaseData();
				baseData.setErrMsg("您还没选择文件，请先选择需要导入的文件！");
				list3.add(baseData);
				redirectAttributes.addFlashAttribute("msg",list3);
			}else{
				ImportExcel importExcel = new ImportExcel(file, 1, 0);
				
				if(file.getOriginalFilename().contains("收发货方")){
					List<Receiver> list = importExcel.getDataList(Receiver.class);
					
					List<BaseData> list1 = receiverService.batchAdd(list, ShiroUtils.findUser());
					redirectAttributes.addFlashAttribute("msg",list1);
				}else{
					List<BaseData> list2=new ArrayList<BaseData>();
					BaseData baseData=new BaseData();
					baseData.setErrMsg("你导入模板有误，请重新选择模板导入。");
					list2.add(baseData);
					redirectAttributes.addFlashAttribute("msg",list2);
				}
			}					
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "导入数据保存失败。");
			logger.warn("importExcel", e);
		}
		return "redirect:/msg";
    }
    
    @RequestMapping(value = "receiver/import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "收发货方数据导入模板.xlsx";
    		List<Receiver> list = Lists.newArrayList();
    		list.add(Receiver.newTest());
    		new ExportExcel("收发货方数据导入模板", Receiver.class, 2).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "收发货方数据导入模板下载失败。");
			logger.warn("importFileTemplate", e);
		}
		return "redirect:/msg";
    }
    @RequestMapping(value = "/receiver/export")
    public String exportFileTemplate(HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
			
			Map<String, Object> params = WebUtils.getParametersStartingWith(request, null);
			
			//Page page=baseQueryForm.getPage();
            String fileName = "收发货方数据.xlsx";
            
            List<Receiver> list = receiverDao.findListBy(null, params,ShiroUtils.findUser());
            
    		List<Receiver> list1=new ArrayList<Receiver>();
    		for(Receiver receiver:list){
    			receiver.setClientCd(receiver.getShortName());
    			receiver.setCityCd(receiver.getCityName());
    			receiver.setTranslocationCd(receiver.getTranslocationName());
    			list1.add(receiver);
    		}
    		new ExportExcel("收发货方数据", Receiver.class, 1).setDataList(list1).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "收发货方数据导出失败。");
			logger.warn("importFileTemplate", e);
		}
		return "redirect:/msg";
    }
    private static final Logger logger = LoggerFactory.getLogger(ReceiverWeb.class);
}
