package com.zzzzzz.oms.receiver;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.oms.product.Product;
import com.zzzzzz.oms.translocation.TransLocation;
import com.zzzzzz.oms.translocation.TransLocationService;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.sys.city.CityService;
import com.zzzzzz.sys.client.Client;
import com.zzzzzz.sys.client.ClientService;
import com.zzzzzz.utils.BeanValidators;
import com.zzzzzz.utils.DateTimeUtils;

@Service
public class ReceiverService {
	@Resource
	public ReceiverDao receiverDao;
	@Resource
	public Validator validator;
	@Resource
	public ClientService clientService;
	@Resource
	public TransLocationService transLocationService;
	@Resource
	public CityService cityService;
	@Transactional
	public int save(Receiver receiver, I i) {
		receiver.setUpdDt(new Date());
		receiver.setUpdBy(i.getId());
		if (receiver.getId() == null) {
			receiver.setAddDt(new Date());
			receiver.setAddBy(i.getId());
			receiverDao.add(receiver);
		} else {
			receiverDao.updById(receiver);
		}

		return 1;
	}
	
	//找cd
	public Receiver getReceiverByCd(String cd,Long clientId) {
		Map<String, List<Receiver>> allReceiverMap=receiverDao.getAllReceiverMap();
		List<Receiver> receiverList=allReceiverMap.get(cd);
		if(receiverList!=null){
			for(Receiver receiver:receiverList){
				if(receiver.getClientId().equals(clientId)){
					return receiver;
				}
			}
		}
		return null;
	}
	
	@Transactional
	public List<BaseData> batchAdd(List<Receiver> list, I i) {
		logger.info("receiver batchAdd validate start:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		//start validate
		int errTt = 0;// 错误信息条数
		int susTt = 0;// 正确信息条数
		BaseData baseData;
		//用于存放信息
		List<BaseData> listMsg=new ArrayList<BaseData>();
		for (Receiver receiver : list) {
			//首先判断收发货方代码时候有效
			//验证城市代码是否为空
			if(StringUtils.isBlank(receiver.getCd())){
				//如果为空
				errTt++;
				baseData =new BaseData();
				baseData.setErrMsg("城市"+receiver.getCityCd()+"为无效值");
				listMsg.add(baseData);
			}else{
				//验证客户是否存在
				Client client=clientService.getclientByCd(receiver.getClientCd(),i);
				if(client!=null){
				//验证运输地是否存在
					if(transLocationService.getTransLocationByCd(i,receiver.getTranslocationCd())!=null){
						//验证城市是否存在
						if(cityService.getCityByCd(receiver.getCityCd())!=null){
							//验证输入收发货方代码是否空值
							if(StringUtils.isBlank(receiver.getCd())){
								errTt++;
								baseData =new BaseData();
								baseData.setErrMsg("收发货方"+receiver.getCd()+"为无效值");
								listMsg.add(baseData);
							}else{
									receiver.setSt(0);
									receiver.setClientId(clientService.getclientByCd(receiver.getClientCd(),i).getId());
									receiver.setTranslocationId(transLocationService.getTransLocationByCd(i, receiver.getTranslocationCd()).getId());
									receiver.setCityId(cityService.getCityByCd(receiver.getCityCd()).getId());
									if(StringUtils.isBlank(receiver.getDescr())){
										receiver.setDescr(null);
									}
									if(StringUtils.isBlank(receiver.getEmail())){
										receiver.setEmail(null);
									}
									if(StringUtils.isBlank(receiver.getFax())){
										receiver.setFax(null);
									}
									if(StringUtils.isBlank(receiver.getLikeman())){
										receiver.setLikeman(null);
									}
									if(StringUtils.isBlank(receiver.getName())){
										receiver.setName(null);
									}
									if(StringUtils.isBlank(receiver.getPhone())){
										receiver.setPhone(null);
									}
									if(StringUtils.isBlank(receiver.getPostcode())){
										receiver.setPostcode(null);
									}
									if(StringUtils.isBlank(receiver.getAddress())){
										receiver.setAddress(null);
									}
									try{
										save(receiver, i);
										susTt++;
									}catch(DuplicateKeyException e){
										baseData=new BaseData("-1", String.format("收发货方代码"+receiver.getCd()+"已经存在"));
										listMsg.add(baseData);
										errTt++;
									}
							}
						}else{
							errTt++;
							baseData =new BaseData();
							baseData.setErrMsg("城市"+receiver.getCityCd()+"不存在");
							listMsg.add(baseData);
						}
					}else{
						baseData =new BaseData();
						baseData.setErrMsg("运输地"+receiver.getTranslocationCd()+"不存在");
						listMsg.add(baseData);
						errTt++;
					}
				}else{
					errTt++;
					baseData =new BaseData();
					baseData.setErrMsg("客户"+receiver.getClientCd()+"不存在");
					listMsg.add(baseData);
				}
			}
		}
		baseData = new BaseData();
		baseData.setErrMsg("总共有" + (susTt + errTt) + "行记录；共有" + susTt+ "行记录导入成功；共有" + errTt + "行记录导入失败；");
		listMsg.add(baseData);
		logger.info("receiver batchAdd save end:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		return listMsg;
	}
	private static final Logger logger = LoggerFactory.getLogger(Receiver.class);
}
