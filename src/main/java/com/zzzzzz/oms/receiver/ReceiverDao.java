package com.zzzzzz.oms.receiver;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.zzzzzz.oms.orderHead.OrderHead;
import com.zzzzzz.oms.ordertype.OrderType;
import com.zzzzzz.oms.product.Product;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.sys.client.Client;
import com.zzzzzz.utils.page.Page;
import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class ReceiverDao {
	
	@Resource
	private BaseDao baseDao;
			
	private final static String sql_add = "insert into t_receiver(clientId, cityId,translocationId, cd, name, likeman, phone, fax, email, postcode, address, addDt, addBy, descr, st) values(:clientId, :cityId, :translocationId,:cd, :name, :likeman, :phone, :fax, :email, :postcode, :address, :addDt, :addBy, :descr, :st)";
	private final static String sql_updStByIds = "update t_receiver set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update t_receiver set clientId=:clientId, cityId=:cityId, translocationId=:translocationId,cd=:cd, name=:name, likeman=:likeman, phone=:phone, fax=:fax, email=:email, postcode=:postcode, address=:address, updDt=:updDt, updBy=:updBy, descr=:descr, st=:st where id = :id";
	private final static String sql_findById = "select id, clientId, translocationId,cityId, cd, name, likeman, phone, fax, email, postcode, address, updDt, addDt, addBy, updBy, descr, st from t_receiver where id = :id";
	@CacheEvict(value = "receiverCache", allEntries = true)
	public Long add(Receiver receiver){
		return baseDao.updateGetLongKey(sql_add, receiver);
	}
	@CacheEvict(value = "receiverCache", allEntries = true)
	public int updStByIds(List<Long> ids, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}	
	@CacheEvict(value = "receiverCache", allEntries = true)
	public int updById(Receiver receiver){
		return baseDao.update(sql_upd, receiver);
	}
	public Receiver findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, Receiver.class);
	}
	
	/**
	 * 动态查询
	 */
	public List<Receiver> findListBy(Page page,Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("count(1) as tt");
		finder.append(" from t_receiver r inner join sys_city c on r.cityId=c.id inner join t_client t on r.clientId=t.id inner join t_trans_location l on l.id=r.translocationId left join t_user_client u on t.id=u.clientId");
		finder.append(" where 1=1");
		finder.append("and t.platformId=:platformId").setParam("platformId", i.getPlatformId());
		finder.append("and u.userId=:userId").setParam("userId", i.getId());
		if(DaoUtils.isNotNull(ffMap.get("clientId"))){
		finder.append(" and r.clientId = :clientId").setParam("clientId", ffMap.get("clientId"));
		}
		if(DaoUtils.isNotNull(ffMap.get("id"))){
			finder.append(" and r.id = :id").setParam("id", ffMap.get("id"));
		}
		if(DaoUtils.isNotNull(ffMap.get("cd"))){
		finder.append(" and r.cd = :cd").setParam("cd", ffMap.get("cd"));
		}
		if(DaoUtils.isNotNull(ffMap.get("name"))){
		finder.append(" and r.name like :name").setParam("name", "%" + ffMap.get("name") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and r.st = :st").setParam("st", ffMap.get("st"));
		}else{
			finder.append(" and r.st = 0");
		}
		finder.append("order by r.id desc");
		if (page != null) {
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}
		String sql = finder.getSql().replace("count(1) as tt","r.*,c.name as cityName,c.cd as cityCd,c.name as cityName,t.name as clientName,t.shortName as shortName,t.cd as clientCd,l.name as translocationName,l.cd as translocationCd");
		finder.setSql(sql);
		List<Receiver> list = baseDao.findList(finder, Receiver.class);
				
		return list;
	}
	
	@Cacheable(value = "receiverCache")
	public Map<String, List<Receiver>> getAllReceiverMap() {
		Map<String, List<Receiver>> receiverMap= new HashMap<String, List<Receiver>>();
		List<Receiver> allReceiverList = findAllBy();
		for (Receiver receiver : allReceiverList) {
			List<Receiver> receiverList = receiverMap.get(receiver.getCd());
			if (receiverList != null) {
				receiverList.add(receiver);
			} else {
				receiverMap.put(receiver.getCd(), Lists.newArrayList(receiver));
			}
		}
		return receiverMap;
	}
	/*
	 * 缓存
	 */
	public List<Receiver> findAllBy(){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("id, clientId, translocationId,cityId, cd, name, likeman, phone, fax, email, postcode, address, updDt, addDt, addBy, updBy, descr, st");
		finder.append("from t_receiver");
		List<Receiver> list = baseDao.findList(finder, Receiver.class);
		return list;
	}
}
