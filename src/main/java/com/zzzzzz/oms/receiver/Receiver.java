package com.zzzzzz.oms.receiver;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;
import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class Receiver implements Serializable {
	
	
	
  	private Long id;
	
  	private Long clientId;
	private Long translocationId;
	private String translocationName;
	private String cityName;
	private String clientName;
	private Long cityId;
	@ExcelField(title = "客户", align = 2, sort = 30)
  	private String clientCd;
	@ExcelField(title = "运输地", align = 2, sort = 30)
	private String translocationCd;
	@ExcelField(title = "城市", align = 2, sort = 30)
	private String cityCd;
	@ExcelField(title = "收发货方代码", align = 2, sort = 30)
  	private String cd;
	@ExcelField(title = "名称", align = 2, sort = 40)
  	private String name;
	@ExcelField(title = "联系人", align = 2, sort = 50)
  	private String likeman;
	@ExcelField(title = "联系电话", align = 2, sort = 50)
  	private String phone;
	@ExcelField(title = "传真", align = 2, sort = 50)
  	private String fax;
	@ExcelField(title = "邮件", align = 2, sort = 50)
  	private String email;
	@ExcelField(title = "邮编", align = 2, sort = 50)
  	private String postcode;
	@ExcelField(title = "联系地址", align = 2, sort = 50)
  	private String address;
	private String shortName;
  	private Date updDt;
	
  	private Date addDt;
	
  	private Long addBy;
	
  	private Long updBy;
	@ExcelField(title = "描述", align = 2, sort = 60)
	@Length(min = 1, max = 255)
  	private String descr;
	
  	private Integer st;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public Long getClientId(){
  		return clientId;
  	}
  	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}
  	
  	public String getTranslocationCd() {
		return translocationCd;
	}
	public void setTranslocationCd(String translocationCd) {
		this.translocationCd = translocationCd;
	}
	public Long getTranslocationId() {
		return translocationId;
	}
	public void setTranslocationId(Long translocationId) {
		this.translocationId = translocationId;
	}
	
	public String getTranslocationName() {
		return translocationName;
	}
	public void setTranslocationName(String translocationName) {
		this.translocationName = translocationName;
	}
	
	public String getClientCd() {
		return clientCd;
	}
	public void setClientCd(String clientCd) {
		this.clientCd = clientCd;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getCityCd() {
		return cityCd;
	}
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	public Long getCityId(){
  		return cityId;
  	}
  	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
  	public String getCd(){
  		return cd;
  	}
  	public void setCd(String cd) {
		this.cd = cd;
	}
  	public String getName(){
  		return name;
  	}
  	public void setName(String name) {
		this.name = name;
	}
  	public String getLikeman(){
  		return likeman;
  	}
  	public void setLikeman(String likeman) {
		this.likeman = likeman;
	}
  	public String getPhone(){
  		return phone;
  	}
  	public void setPhone(String phone) {
		this.phone = phone;
	}
  	public String getFax(){
  		return fax;
  	}
  	public void setFax(String fax) {
		this.fax = fax;
	}
  	public String getEmail(){
  		return email;
  	}
  	public void setEmail(String email) {
		this.email = email;
	}
  	public String getPostcode(){
  		return postcode;
  	}
  	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
  	public String getAddress(){
  		return address;
  	}
  	public void setAddress(String address) {
		this.address = address;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
  	public String getDescr(){
  		return descr;
  	}
  	public void setDescr(String descr) {
		this.descr = descr;
	}
  	public Integer getSt(){
  		return st;
  	}
  	public void setSt(Integer st) {
		this.st = st;
	}
  	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	

	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
 	public static Receiver newTest() {
		Receiver receiver = new Receiver();
		
		
		
		//receiver.setCd("");
		//receiver.setName("");
		//receiver.setLikeman("");
		//receiver.setPhone("");
		//receiver.setFax("");
		//receiver.setEmail("");
		//receiver.setPostcode("");
		//receiver.setAddress("");
		
		
		
		
		//receiver.setDescr("");
		//receiver.setSt("");
		return receiver;
	}
}


