package com.zzzzzz.oms.driver;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.zzzzzz.oms.vehicle.Vehicle;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.utils.page.Page;
import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class DriverDao {
	
	@Resource
	private BaseDao baseDao;
			
	private final static String sql_add = "insert into t_driver(cd,platformId, name, idcard,phone, driverType, addDt, addBy, st) values(:cd,:platformId, :name, :idcard,:phone, :driverType, :addDt, :addBy, :st)";
	private final static String sql_updStByIds = "update t_driver set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update t_driver set cd=:cd,platformId=:platformId, name=:name, idcard=:idcard,phone=:phone, driverType=:driverType, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, cd, platformId,name, idcard,phone, driverType, addDt, addBy, updDt, updBy, st from t_driver where id = :id";
	
	public Long add(Driver driver){
		return baseDao.updateGetLongKey(sql_add, driver);
	}
	public int updStByIds(List<Long> ids, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}		  
	public int updById(Driver driver){
		return baseDao.update(sql_upd, driver);
	}
	public Driver findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, Driver.class);
	}
	
	/**
	 * 动态查询
	 */
	public List<Driver> findListBy(Page page,Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("count(1) as tt");
		finder.append(" from t_driver");
		finder.append(" where 1=1");
		finder.append("and platformId=:platformId").setParam("platformId", i.getPlatformId());
		if(DaoUtils.isNotNull(ffMap.get("cd"))){
		finder.append(" and cd = :cd").setParam("cd", ffMap.get("cd"));
		}
		if(DaoUtils.isNotNull(ffMap.get("name"))){
		finder.append(" and name like :name").setParam("name", "%" + ffMap.get("name") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("st"))){
		finder.append(" and st = :st").setParam("st", ffMap.get("st"));
		}else{
			finder.append(" and st = 0");
		}
				
		finder.append("order by id desc");
		if (page != null) {
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}
		String sql = finder.getSql().replace("count(1) as tt","id, cd, platformId,name, idcard,phone, driverType, addDt, addBy, updDt, updBy, st");
		finder.setSql(sql);
		List<Driver> list = baseDao.findList(finder, Driver.class);
				
		return list;
	}
}
