package com.zzzzzz.oms.driver;

import java.util.List;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.utils.page.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzzzzz.oms.GCS;
import com.zzzzzz.oms.vehicle.Vehicle;
import com.google.common.collect.Lists;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.plugins.poi.ExportExcel;
import com.zzzzzz.plugins.poi.ImportExcel;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class DriverWeb {

	@Resource
	public DriverService driverService;
	@Resource
	public DriverDao driverDao;

	@RequestMapping(value = "/driver/list", method = RequestMethod.GET)
	public String list() {
		return "oms/driver";
	}

	@RequestMapping(value = "/driver/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid Driver driver, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}

		driverService.save(driver, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/driver/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		driverDao.updStByIds(ids, st, ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	
	
	@RequestMapping(value = "/driver/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		Driver driver = driverDao.findById(id);
		return new BaseData(driver, null);
	}
	

	@RequestMapping(value = "/driver/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		Page page;
		List<Driver> list;
		BaseData baseData;
		if(baseQueryForm.getPageNb()==null&&baseQueryForm.getPageSize()==null){
			list = driverDao.findListBy(null,baseQueryForm.getFfMap(),ShiroUtils.findUser());
			baseData=new BaseData(list, null);
		}else{
			page=baseQueryForm.getPage();
			list = driverDao.findListBy(page,baseQueryForm.getFfMap(),ShiroUtils.findUser());
			baseData=new BaseData(list, page.getTotal());
		}
		return baseData;
	}
	
	
	@RequestMapping(value = "/driver/import", method=RequestMethod.POST)
    public String importExcel(@RequestParam MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			ImportExcel importExcel = new ImportExcel(file, 1, 0);
			List<Driver> list = importExcel.getDataList(Driver.class);
			
			BaseData baseData = driverService.batchAdd(list, ShiroUtils.findUser());
			redirectAttributes.addFlashAttribute("msg", baseData.getErrMsg());
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "导入数据保存失败。");
			logger.warn("importExcel", e);
		}
		return "redirect:/msg";
    }
    
    @RequestMapping(value = "driver/import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "司机信息数据导入模板.xlsx";
    		List<Driver> list = Lists.newArrayList();
    		list.add(Driver.newTest());
    		new ExportExcel("司机信息数据导入模板", Driver.class, 2).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "司机信息数据导入模板下载失败。");
			logger.warn("importFileTemplate", e);
		}
		return "redirect:/msg";
    }
    
    private static final Logger logger = LoggerFactory.getLogger(DriverWeb.class);
}
