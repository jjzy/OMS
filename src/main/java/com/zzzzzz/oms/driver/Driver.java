package com.zzzzzz.oms.driver;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;
import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class Driver implements Serializable {
	
	
	
  	private Long id;
	@ExcelField(title = "工号", align = 2, sort = 10)
	@Length(min = 1, max = 30)
  	private String cd;
	@ExcelField(title = "名称", align = 2, sort = 40)
	
  	private String name;
	@ExcelField(title = "身份证", align = 2, sort = 80)
	
  	private String idcard;
	@ExcelField(title = "驾照类型", align = 2, sort = 80)
	
	private String phone;
  	private String driverType;
	
	private Long platformId;
	
  	private Date addDt;
	
	
  	private Long addBy;
	
	
  	private Date updDt;
	
	
  	private Long updBy;
	@ExcelField(title = "状态", align = 2, sort = 130)
	
  	private Integer st;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public String getCd(){
  		return cd;
  	}
  	public void setCd(String cd) {
		this.cd = cd;
	}
  	public String getName(){
  		return name;
  	}
  	public void setName(String name) {
		this.name = name;
	}
  	public String getIdcard(){
  		return idcard;
  	}
  	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
  	public String getDriverType(){
  		return driverType;
  	}
  	public void setDriverType(String driverType) {
		this.driverType = driverType;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
  	public Integer getSt(){
  		return st;
  	}
  	public void setSt(Integer st) {
		this.st = st;
	}
 	
  	
  	
 	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Long getPlatformId() {
		return platformId;
	}
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
	public static Driver newTest() {
		Driver driver = new Driver();
		
		//driver.setCd("");
		//driver.setName("");
		//driver.setIdcard("");
		//driver.setDriverType("");
		
		
		
		
		//driver.setSt("");
		return driver;
	}
}


