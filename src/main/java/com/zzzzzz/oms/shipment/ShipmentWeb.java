package com.zzzzzz.oms.shipment;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.utils.page.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzzzzz.oms.GCS;
import com.zzzzzz.oms.translocation.TransLocation;
import com.google.common.collect.Lists;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.plugins.poi.ExportExcel;
import com.zzzzzz.plugins.poi.ImportExcel;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class ShipmentWeb {

	@Resource
	public ShipmentService shipmentService;
	@Resource
	public ShipmentDao shipmentDao;

	@RequestMapping(value = "/shipment/list", method = RequestMethod.GET)
	public String list() {
		return "oms/shipment";
	}

	@RequestMapping(value = "/shipment/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid Shipment shipment, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}
		Long id=shipmentService.save(shipment, ShiroUtils.findUser());
		List<Long> list=new ArrayList<Long>();
		list.add(id);
		return new BaseData(list,null);
	}
	
	@RequestMapping(value = "/shipment/updStatusByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "status", required = true) String status) throws Exception {
		shipmentDao.updStByIds(ids, status, ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	@RequestMapping(value = "/shipment/upds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData upds(@RequestParam(value = "id", required = true) Long id, @RequestParam(value = "quantity", required = true) Double quantity,@RequestParam(value = "weight", required = true) Double weight,@RequestParam(value = "volume", required = true) Double volume,@RequestParam(value = "points", required = true) Integer points) throws Exception {
		shipmentDao.upds(id, quantity,weight,volume,points,ShiroUtils.findUser());
		return new BaseData("0");
	}	
	
	@RequestMapping(value = "/shipment/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		Shipment shipment = shipmentDao.findById(id);
		return new BaseData(shipment, null);
	}
	
	@RequestMapping(value = "/shipment/updTime", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updTime(@RequestBody BaseQueryForm baseQueryForm) throws Exception {
		shipmentDao.updTime(baseQueryForm.getFfMap(), ShiroUtils.findUser());
		return new BaseData("0");
	}
	@RequestMapping(value = "/shipment/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		Page page;
		List<Shipment> list;
		BaseData baseData;
		if(baseQueryForm.getPageNb()==null&&baseQueryForm.getPageSize()==null){
			list = shipmentDao.findListBy(null,baseQueryForm.getFfMap(),ShiroUtils.findUser());
			baseData=new BaseData(list, null);
		}else{
			page=baseQueryForm.getPage();
			list = shipmentDao.findListBy(page,baseQueryForm.getFfMap(),ShiroUtils.findUser());
			baseData=new BaseData(list, page.getTotal());
		}
		return baseData;
	}
	
	
	
	@RequestMapping(value = "/shipment/import", method=RequestMethod.POST)
    public String importExcel(@RequestParam MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			ImportExcel importExcel = new ImportExcel(file, 1, 0);
			List<Shipment> list = importExcel.getDataList(Shipment.class);
			
			BaseData baseData = shipmentService.batchAdd(list, ShiroUtils.findUser());
			redirectAttributes.addFlashAttribute("msg", baseData.getErrMsg());
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "导入数据保存失败。");
			logger.warn("importExcel", e);
		}
		return "redirect:/msg";
    }
    
    @RequestMapping(value = "shipment/import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "调度单数据导入模板.xlsx";
    		List<Shipment> list = Lists.newArrayList();
    		list.add(Shipment.newTest());
    		new ExportExcel("调度单数据导入模板", Shipment.class, 2).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "调度单数据导入模板下载失败。");
			logger.warn("importFileTemplate", e);
		}
		return "redirect:/msg";
    }
    
    private static final Logger logger = LoggerFactory.getLogger(ShipmentWeb.class);
}
