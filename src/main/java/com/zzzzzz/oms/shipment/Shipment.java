package com.zzzzzz.oms.shipment;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;
import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class Shipment implements Serializable {
	
	
	
  	private Long id;
	@ExcelField(title = "调度单号", align = 2, sort = 10)
	@Length(min = 1, max = 30)
  	private String cd;
	@ExcelField(title = "承运商", align = 2, sort = 40)
	private Long platformId;
  	private Long constractorId;
  	private Long ftranslocationId;
  	private Long ttranslocationId;
	@ExcelField(title = "状态", align = 2, sort = 40)
	
  	private String status;
	@ExcelField(title = "交通工具", align = 2, sort = 40)
	
  	private Long vehicleId;
	@ExcelField(title = "司机", align = 2, sort = 40)
	
  	private Long driverId;
	@ExcelField(title = "司机名称", align = 2, sort = 40)
	
  	private String driverName;
	@ExcelField(title = "联系电话", align = 2, sort = 40)
	
  	private String phone;
	@ExcelField(title = "运输方式", align = 2, sort = 80)
	
  	private String shipment_method;
	@ExcelField(title = "配载方式", align = 2, sort = 80)
	
  	private String shipmentType;
	@ExcelField(title = "件数", align = 2, sort = 80)
	
  	private Integer quantity;
	@ExcelField(title = "重量", align = 2, sort = 80)
	
  	private Double weight;
	@ExcelField(title = "体积", align = 2, sort = 80)
	
  	private Double volume;
	@ExcelField(title = "托数", align = 2, sort = 80)
	
  	private Double palletsum;
	@ExcelField(title = "一口价", align = 2, sort = 80)
	
  	private Double expense;
	@ExcelField(title = "描述", align = 2, sort = 80)
	
  	private String descr;
	@ExcelField(title = "计划离开时间", align = 2, sort = 80)
	
  	private Date planltime;
	@ExcelField(title = "出发时间", align = 2, sort = 80)
	private Date planatime;
	
  	private Date leavetime;
	@ExcelField(title = "到达时间", align = 2, sort = 80)
	
  	private Date arrivetime;
	
	private String carNo;
	
	
	private String idcard;
	
	private Integer points;
	
	private Double timeconsuming;
	
	private String gzNo;
	
	private String expectMoney;
	
	private String constractorName;
	private String ftranslocationName;
	private String ttranslocationName;
	private String shipment_methodName;
  	private Date addDt;
	private Long TempShipmentId;
	private String statusName;
	private String vocationType;
	private String vocationTypeName;
  	private Long addBy;
	
	
  	private Date updDt;
	
	
  	private Long updBy;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public String getCd(){
  		return cd;
  	}
  	public void setCd(String cd) {
		this.cd = cd;
	}
  	public Long getPlatformId() {
		return platformId;
	}
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
	public Long getConstractorId(){
  		return constractorId;
  	}
  	public void setConstractorId(Long constractorId) {
		this.constractorId = constractorId;
	}
  	public Long getFtranslocationId() {
		return ftranslocationId;
	}
	public void setFtranslocationId(Long ftranslocationId) {
		this.ftranslocationId = ftranslocationId;
	}
	public Long getTtranslocationId() {
		return ttranslocationId;
	}
	public void setTtranslocationId(Long ttranslocationId) {
		this.ttranslocationId = ttranslocationId;
	}
	public String getStatus(){
  		return status;
  	}
  	public void setStatus(String status) {
		this.status = status;
	}
  	public Long getVehicleId(){
  		return vehicleId;
  	}
  	public void setVehicleId(Long vehicleId) {
		this.vehicleId = vehicleId;
	}
  	public Long getDriverId(){
  		return driverId;
  	}
  	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}
  	public String getDriverName(){
  		return driverName;
  	}
  	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
  	public String getPhone(){
  		return phone;
  	}
  	public String getVocationTypeName() {
		return vocationTypeName;
	}
	public void setVocationTypeName(String vocationTypeName) {
		this.vocationTypeName = vocationTypeName;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
  	public String getShipment_method(){
  		return shipment_method;
  	}
  	public void setShipment_method(String shipment_method) {
		this.shipment_method = shipment_method;
	}
  	public String getVocationType() {
		return vocationType;
	}
	public void setVocationType(String vocationType) {
		this.vocationType = vocationType;
	}
	public String getShipmentType(){
  		return shipmentType;
  	}
  	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public void setShipmentType(String shipmentType) {
		this.shipmentType = shipmentType;
	}
  	public Integer getQuantity(){
  		return quantity;
  	}
  	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
  	public Double getWeight(){
  		return weight;
  	}
  	public void setWeight(Double weight) {
		this.weight = weight;
	}
  	public Double getVolume(){
  		return volume;
  	}
  	public void setVolume(Double volume) {
		this.volume = volume;
	}
  	public Double getPalletsum(){
  		return palletsum;
  	}
  	public void setPalletsum(Double palletsum) {
		this.palletsum = palletsum;
	}
  	public Double getExpense(){
  		return expense;
  	}
  	public void setExpense(Double expense) {
		this.expense = expense;
	}
  	public String getDescr(){
  		return descr;
  	}
  	public void setDescr(String descr) {
		this.descr = descr;
	}
  	public Date getPlanltime(){
  		return planltime;
  	}
  	public void setPlanltime(Date planltime) {
		this.planltime = planltime;
	}
  	public Date getPlanatime() {
		return planatime;
	}
	public void setPlanatime(Date planatime) {
		this.planatime = planatime;
	}
	public Date getLeavetime(){
  		return leavetime;
  	}
  	public void setLeavetime(Date leavetime) {
		this.leavetime = leavetime;
	}
  	public Date getArrivetime(){
  		return arrivetime;
  	}
  	public void setArrivetime(Date arrivetime) {
		this.arrivetime = arrivetime;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}

  	
 	public String getCarNo() {
		return carNo;
	}
	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}
	public String getIdcard() {
		return idcard;
	}
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	public Integer getPoints() {
		return points;
	}
	public void setPoints(Integer points) {
		this.points = points;
	}
	public Double getTimeconsuming() {
		return timeconsuming;
	}
	public void setTimeconsuming(Double timeconsuming) {
		this.timeconsuming = timeconsuming;
	}
	public String getGzNo() {
		return gzNo;
	}
	public void setGzNo(String gzNo) {
		this.gzNo = gzNo;
	}
	public String getExpectMoney() {
		return expectMoney;
	}
	public void setExpectMoney(String expectMoney) {
		this.expectMoney = expectMoney;
	}
	public String getConstractorName() {
		return constractorName;
	}
	public void setConstractorName(String constractorName) {
		this.constractorName = constractorName;
	}
	public String getFtranslocationName() {
		return ftranslocationName;
	}
	public void setFtranslocationName(String ftranslocationName) {
		this.ftranslocationName = ftranslocationName;
	}
	public String getTtranslocationName() {
		return ttranslocationName;
	}
	public void setTtranslocationName(String ttranslocationName) {
		this.ttranslocationName = ttranslocationName;
	}
	public String getShipment_methodName() {
		return shipment_methodName;
	}
	public void setShipment_methodName(String shipment_methodName) {
		this.shipment_methodName = shipment_methodName;
	}
	public Long getTempShipmentId() {
		return TempShipmentId;
	}
	public void setTempShipmentId(Long tempShipmentId) {
		TempShipmentId = tempShipmentId;
	}
	public static Shipment newTest() {
		Shipment shipment = new Shipment();
		
		//shipment.setCd("");
		//shipment.setConstractorId("");
		//shipment.setStatus("");
		//shipment.setVehicleId("");
		//shipment.setCityId("");
		//shipment.setDriverId("");
		//shipment.setDriverName("");
		//shipment.setPhone("");
		//shipment.setShipment_method("");
		//shipment.setShipmentType("");
		//shipment.setQuantity("");
		//shipment.setWeight("");
		//shipment.setVolume("");
		//shipment.setPalletsum("");
		//shipment.setExpense("");
		//shipment.setDescr("");
		//shipment.setPlanltime("");
		//shipment.setLeavetime("");
		//shipment.setArrivetime("");
		
		
		
		
		return shipment;
	}
}


