package com.zzzzzz.oms.shipment;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.zzzzzz.oms.driver.Driver;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.utils.page.Page;
import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class ShipmentDao {
	
	@Resource
	private BaseDao baseDao;			
	private final static String sql_add = "insert into t_shipment(cd,platformId, constractorId,ftranslocationId,ttranslocationId, status, vehicleId,carNo, driverId, driverName, phone,idcard,points,timeconsuming,shipment_method, shipmentType,vocationType,gzNo,expectMoney, quantity, weight, volume, palletsum, expense, descr, planltime,planatime, leavetime, arrivetime, addDt, addBy) values(:cd, :platformId, :constractorId,:ftranslocationId,:ttranslocationId, :status, :vehicleId,:carNo, :driverId, :driverName, :phone,:idcard,:points,:timeconsuming, :shipment_method, :shipmentType,:vocationType,:gzNo,:expectMoney, :quantity, :weight, :volume, :palletsum, :expense, :descr, :planltime,:planatime, :leavetime, :arrivetime, :addDt, :addBy)";
	private final static String sql_upd = "update t_shipment set cd=:cd, constractorId=:constractorId,ftranslocationId=:ftranslocationId,ttranslocationId=:ttranslocationId, status=:status, vehicleId=:vehicleId, carNo=:carNo, driverId=:driverId, driverName=:driverName, phone=:phone, idcard=:idcard,points=:points,timeconsuming=:timeconsuming,shipment_method=:shipment_method, shipmentType=:shipmentType,vocationType=:vocationType,gzNo=:gzNo,expectMoney=:expectMoney, quantity=:quantity, weight=:weight, volume=:volume, palletsum=:palletsum, expense=:expense, descr=:descr, planltime=:planltime,planatime=:planatime, leavetime=:leavetime, arrivetime=:arrivetime, updDt=:updDt, updBy=:updBy where id = :id";
	private final static String sql_findById = "select id, cd, constractorId, ftranslocationId,ttranslocationId,status, vehicleId, carNo, driverId, driverName, phone,idcard, points,timeconsuming,shipment_method, shipmentType,vocationType,gzNo,expectMoney, quantity, weight, volume, palletsum, expense, descr, planltime,planatime, leavetime, arrivetime, addDt, addBy, updDt, updBy from t_shipment where id = :id";
	private final static String sql_max="select cd from t_shipment where id=(select max(id) from t_shipment where platformId=:platformId)";
	private final static String sql_upds="update t_shipment set quantity = :quantity,weight = :weight,volume = :volume,points=:points, updDt = :updDt, updBy = :updBy where id =:id";
	private final static String sql_uddstatusByIds="update t_shipment set status=:status, updDt = :updDt, updBy = :updBy where id in (:ids)";
	public Long add(Shipment shipment){
		return baseDao.updateGetLongKey(sql_add, shipment);
	}
	public int updStByIds(List<Long> ids, String status, I i){
		Finder finder = new Finder(sql_uddstatusByIds).setParam("ids", ids).setParam("status", status)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}	
	public int upds(Long id,Double quantity,Double weight,Double volume,Integer points, I i){
		Finder finder = new Finder(sql_upds).setParam("id", id).setParam("quantity", quantity).setParam("weight", weight).setParam("volume", volume).setParam("points", points)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}
	public int updById(Shipment shipment){
		return baseDao.update(sql_upd, shipment);
	}
	public Shipment findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, Shipment.class);
	}
	
	public String findByMax(I i){
		Finder finder = new Finder(sql_max).setParam("platformId",i.getPlatformId());
		List<Shipment> list=baseDao.findList(finder, Shipment.class);
		if(list.size()==0){
			return null;
		}
		return list.get(0).getCd();
	}
	
	
	public int updTime(Map<String, Object> ffMap, I i){
		Finder finder=new Finder("");
		finder.append("update t_shipment set");
		if(DaoUtils.isNotNull(ffMap.get("fyTime"))){
			finder.append("leavetime=:leavetime").setParam("leavetime", ffMap.get("fyTime"));
		}
		if(DaoUtils.isNotNull(ffMap.get("ydTime"))){
			finder.append("arrivetime=:arrivetime").setParam("arrivetime", ffMap.get("ydTime"));
		}
		if(DaoUtils.isNotNull(ffMap.get("ids"))){
			finder.append("where id in (:ids)").setParam("ids", ffMap.get("ids"));
		}
		return baseDao.update(finder);
	}
	/**
	 * 动态查询
	 */
	public List<Shipment> findListBy(Page page,Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("count(1) as tt");
		finder.append(" from t_shipment s inner join t_constractor c on c.id=s.constractorId inner join t_trans_location ft on ft.id=s.ftranslocationId inner join "
				+ "t_trans_location tt on tt.id=s.ttranslocationId left join sys_dict d on d.descr=s.shipment_method inner join sys_dict ss on s.status=ss.descr left join sys_dict st on st.descr=s.shipmentType inner join sys_dict dv on dv.descr=s.vocationType");
		finder.append(" where 1=1");
		finder.append("and s.platformId=:platformId").setParam("platformId", i.getPlatformId());
		if(DaoUtils.isNotNull(ffMap.get("cd"))){
			finder.append(" and s.cd = :cd").setParam("cd", ffMap.get("cd"));
		}
		if(DaoUtils.isNotNull(ffMap.get("name"))){
			finder.append(" and s.name like :name").setParam("name", "%" + ffMap.get("name") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("constractorId"))){
			finder.append(" and s.constractorId=:constractorId").setParam("constractorId",ffMap.get("constractorId"));
		}	
		if(DaoUtils.isNotNull(ffMap.get("carNo"))){
			finder.append(" and s.carNo =:carNo").setParam("carNo",ffMap.get("carNo"));
		}
		finder.append("order by s.id desc");
		if (page != null) {
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}
		String sql = finder.getSql().replace("count(1) as tt","s.*,c.name as constractorName,dv.val as vocationTypeName,ft.name as ftranslocationName,tt.name as ttranslocationName,d.val as shipment_methodName,ss.val as statusName,st.val as shipmentType");
		finder.setSql(sql);
		List<Shipment> list = baseDao.findList(finder, Shipment.class);
				
		return list;
	}
}
