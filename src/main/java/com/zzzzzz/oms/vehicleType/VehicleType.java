package com.zzzzzz.oms.vehicleType;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;
import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class VehicleType implements Serializable {
	
	
	
  	private Long id;
	@ExcelField(title = "代码", align = 2, sort = 10)
	@Length(min = 1, max = 30)
  	private String cd;
	@ExcelField(title = "名称", align = 2, sort = 40)
	
  	private String name;
	@ExcelField(title = "描述", align = 2, sort = 80)
	
  	private String descr;
	@ExcelField(title = "排序值", align = 2, sort = 120)
	
  	private Integer sortNb;
	
	
  	private Date addDt;
	
	
  	private Long addBy;
	
	
  	private Date updDt;
	
	
  	private Long updBy;
	@ExcelField(title = "状态", align = 2, sort = 130)
	
  	private Integer st;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public String getCd(){
  		return cd;
  	}
  	public void setCd(String cd) {
		this.cd = cd;
	}
  	public String getName(){
  		return name;
  	}
  	public void setName(String name) {
		this.name = name;
	}
  	public String getDescr(){
  		return descr;
  	}
  	public void setDescr(String descr) {
		this.descr = descr;
	}
  	public Integer getSortNb(){
  		return sortNb;
  	}
  	public void setSortNb(Integer sortNb) {
		this.sortNb = sortNb;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
  	public Integer getSt(){
  		return st;
  	}
  	public void setSt(Integer st) {
		this.st = st;
	}
 	
 	public static VehicleType newTest() {
		VehicleType vehicleType = new VehicleType();
		
		//vehicleType.setCd("");
		//vehicleType.setName("");
		//vehicleType.setDescr("");
		//vehicleType.setSortNb("");
		
		
		
		
		//vehicleType.setSt("");
		return vehicleType;
	}
}


