package com.zzzzzz.oms.ordertype;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.sys.client.Client;
import com.zzzzzz.utils.BeanValidators;
import com.zzzzzz.utils.DateTimeUtils;

@Service
public class OrderTypeService {
	@Resource
	public OrderTypeDao orderTypeDao;
	@Resource
	public Validator validator;
	
	@Transactional
	public int save(OrderType orderType, I i) {
		orderType.setUpdDt(new Date());
		orderType.setUpdBy(i.getId());
		if (orderType.getId() == null) {
			orderType.setAddDt(new Date());
			orderType.setAddBy(i.getId());
			orderTypeDao.add(orderType);
		} else {
			orderTypeDao.updById(orderType);
		}

		return 1;
	}
	
	//找cd
	public OrderType getOrderTypeByCd(String cd,Long clientId) {
		Map<String, List<OrderType>> allOrderTypeMap=orderTypeDao.getAllOrderTypeMap();
		List<OrderType> orderTypeList=allOrderTypeMap.get(cd);
		if(orderTypeList!=null){
			for(OrderType orderType:orderTypeList){
				if(orderType.getClientId().equals(clientId)){
					return orderType;
				}
			}
		}
		return null;
	}
	
	@Transactional
	public BaseData batchAdd(List<OrderType> list, I i) {
		logger.info("orderType batchAdd validate start:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		//start validate
		int errTt = 0;
		StringBuilder errMsg = new StringBuilder();
		for (OrderType orderType : list) {
			try {
				BeanValidators.validateWithException(validator, orderType);
			} catch (ConstraintViolationException e) {
				List<String> msgList = BeanValidators.extractPropertyAndMessageAsList(e, ": ");
				for (String msg : msgList) {
					errTt++;
					errMsg.append("<br/>").append(msg);
				}
			} catch (Exception e) {
				logger.warn("", e);
				errTt++;
				errMsg.append("<br/>验证失败：").append(e.getMessage());
			}
		}
		if(errTt > 0){
			errMsg.insert(0, "共有"+errTt+"笔记录验证失败：");
			return new BaseData("101", errMsg.toString());
		}
		logger.info("orderType batchAdd validate end:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		logger.info("orderType batchAdd save start:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		//state save
		int susTt = 0;
		for (OrderType orderType : list) {
			orderType.setSt(0);
			save(orderType, i);
			susTt++;
		}
		logger.info("orderType batchAdd save end:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		return new BaseData("0", "共有"+susTt+"笔记录导入成功。");
	}

	private static final Logger logger = LoggerFactory.getLogger(OrderType.class);
}
