package com.zzzzzz.oms.ordertype;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class OrderType implements Serializable {
	
	
	
  	private Long id;
	@ExcelField(title = "所属客户", align = 2, sort = 10)
	private String clientName;
	private Long clientId;
	@ExcelField(title = "代码", align = 2, sort = 20)
  	private String cd;
	@ExcelField(title = "名称", align = 2, sort = 30)
  	private String name;
	@ExcelField(title = "类型", align = 2, sort = 40)
  	private String typ;
	@ExcelField(title = "运输方式", align = 2, sort = 50)
	private String shipmentMethodName;
  	private String  shipment_method;
	
  	private Date addDt;
	
  	private Long addBy;
	
  	private Date updDt;
	
  	private Long updBy;
	@ExcelField(title = "状态", align = 2, sort = 130)
	
  	private Integer st;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public Long getClientId(){
  		return clientId;
  	}
  	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}
  	public String getCd(){
  		return cd;
  	}
  	public void setCd(String cd) {
		this.cd = cd;
	}
  	public String getName(){
  		return name;
  	}
  	public void setName(String name) {
		this.name = name;
	}
  	public String getTyp(){
  		return typ;
  	}
  	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getShipment_method() {
		return shipment_method;
	}
	public void setShipment_method(String shipment_method) {
		this.shipment_method = shipment_method;
	}
	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
  	public Integer getSt(){
  		return st;
  	}
  	public void setSt(Integer st) {
		this.st = st;
	}
	public String getShipmentMethodName() {
		return shipmentMethodName;
	}
	public void setShipmentMethodName(String shipmentMethodName) {
		this.shipmentMethodName = shipmentMethodName;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
 	public static OrderType newTest() {
		OrderType orderType = new OrderType();
		
		
		//orderType.setCd("");
		//orderType.setName("");
		//orderType.setTyp("");
		//orderType.setDistCd("");
		
		
		
		
		//orderType.setSt("");
		return orderType;
	}
}


