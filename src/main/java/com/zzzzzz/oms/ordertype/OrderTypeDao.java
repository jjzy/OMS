package com.zzzzzz.oms.ordertype;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.sys.client.Client;
import com.zzzzzz.utils.page.Page;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class OrderTypeDao {
	
	@Resource
	private BaseDao baseDao;
			
	private final static String sql_add = "insert into t_order_type(clientId, cd, name, typ, shipment_method, addDt, addBy, st) values(:clientId, :cd, :name, :typ, :shipment_method, :addDt, :addBy, :st)";
	private final static String sql_updStByIds = "update t_order_type set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update t_order_type set clientId=:clientId, cd=:cd, name=:name, typ=:typ, shipment_method=:shipment_method, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, clientId, cd, name, typ, shipment_method, addDt, addBy, updDt, updBy, st from t_order_type where id = :id";
	@CacheEvict(value = "orderTypeCache", allEntries = true)
	public Long add(OrderType orderType){
		return baseDao.updateGetLongKey(sql_add, orderType);
	}
	@CacheEvict(value = "orderTypeCache", allEntries = true)
	public int updStByIds(List<Long> ids, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}		
	@CacheEvict(value = "orderTypeCache", allEntries = true)
	public int updById(OrderType orderType){
		return baseDao.update(sql_upd, orderType);
	}
	public OrderType findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, OrderType.class);
	}
	
	/**
	 * 动态查询
	 */
	public List<OrderType> findListBy(Page page,Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("count(1) as tt");
		finder.append("  from t_order_type o inner join t_client c on o.clientId=c.id inner join sys_dict d on o.shipment_method=d.descr left join t_user_client u on u.clientId=c.id");
		finder.append(" where 1=1");
		finder.append("and c.platformId=:platformId").setParam("platformId", i.getPlatformId());
		finder.append("and u.userId=:userId").setParam("userId", i.getId());
		if(DaoUtils.isNotNull(ffMap.get("clientId"))){
		finder.append(" and o.clientId = :clientId").setParam("clientId", ffMap.get("clientId"));
		}
		if(DaoUtils.isNotNull(ffMap.get("cd"))){
		finder.append(" and o.cd = :cd").setParam("cd", ffMap.get("cd"));
		}
		if(DaoUtils.isNotNull(ffMap.get("name"))){
		finder.append(" and o.name like :name").setParam("name", "%" + ffMap.get("name") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and o.st = :st").setParam("st", ffMap.get("st"));
		}
		else{
			finder.append(" and o.st = 0");
		}
		finder.append("order by o.id desc");
		if (page != null) {
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}
		String sql = finder.getSql().replace("count(1) as tt","o.*,c.name as clientName,d.val as shipmentMethodName");
		finder.setSql(sql);		
		List<OrderType> list = baseDao.findList(finder, OrderType.class);
				
		return list;
	}
	
	/*
	 * 缓存
	 */
	public List<OrderType> findAllBy(){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("id, clientId, cd, name, typ, shipment_method, addDt, addBy, updDt, updBy, st");
		finder.append("from t_order_type");
		List<OrderType> list = baseDao.findList(finder, OrderType.class);
		return list;
	}
	
	@Cacheable(value = "orderTypeCache")
	public Map<String, List<OrderType>> getAllOrderTypeMap() {
		Map<String, List<OrderType>> orderTypeMap = new HashMap<String, List<OrderType>>();
		List<OrderType> allOrderTypeList = findAllBy();
		for (OrderType orderType : allOrderTypeList) {
			List<OrderType> orderTypeList = orderTypeMap.get(orderType.getCd());
			if (orderTypeList != null) {
				orderTypeList.add(orderType);
			} else {
				orderTypeMap.put(orderType.getCd(), Lists.newArrayList(orderType));
			}
		}
		return orderTypeMap;
	}
}
