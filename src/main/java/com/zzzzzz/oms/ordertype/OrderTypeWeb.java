package com.zzzzzz.oms.ordertype;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.WebUtils;

import com.google.common.collect.Lists;
import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.oms.GCS;
import com.zzzzzz.oms.producttype.ProductType;
import com.zzzzzz.plugins.poi.ExportExcel;
import com.zzzzzz.plugins.poi.ImportExcel;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.utils.page.Page;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class OrderTypeWeb {

	@Resource
	public OrderTypeService orderTypeService;
	@Resource
	public OrderTypeDao orderTypeDao;

	@RequestMapping(value = "/orderType/list", method = RequestMethod.GET)
	public String list() {
		return "oms/ordertype";
	}

	@RequestMapping(value = "/orderType/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid OrderType orderType, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}

		orderTypeService.save(orderType, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/orderType/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		orderTypeDao.updStByIds(ids, st, ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	
	
	@RequestMapping(value = "/orderType/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		OrderType orderType = orderTypeDao.findById(id);
		return new BaseData(orderType, null);
	}
	

	@RequestMapping(value = "/orderType/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		Page page;
		List<OrderType> list;
		BaseData baseData;
		if(baseQueryForm.getPageNb()==null&&baseQueryForm.getPageSize()==null){
			list = orderTypeDao.findListBy(null,baseQueryForm.getFfMap(),ShiroUtils.findUser());
			baseData=new BaseData(list, null);
		}else{
			page=baseQueryForm.getPage();
			list = orderTypeDao.findListBy(page,baseQueryForm.getFfMap(),ShiroUtils.findUser());
			baseData=new BaseData(list, page.getTotal());
		}
		return baseData;
	}
	
	
	@RequestMapping(value = "/orderType/import", method=RequestMethod.POST)
    public String importExcel(@RequestParam MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			ImportExcel importExcel = new ImportExcel(file, 1, 0);
			List<OrderType> list = importExcel.getDataList(OrderType.class);
			
			BaseData baseData = orderTypeService.batchAdd(list, ShiroUtils.findUser());
			redirectAttributes.addFlashAttribute("msg", baseData.getErrMsg());
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "导入数据保存失败。");
			logger.warn("importExcel", e);
		}
		return "redirect:/msg";
    }
    
    @RequestMapping(value = "orderType/import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "订单类型数据导入模板.xlsx";
    		List<OrderType> list = Lists.newArrayList();
    		list.add(OrderType.newTest());
    		new ExportExcel("订单类型数据导入模板", OrderType.class, 2).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "订单类型数据导入模板下载失败。");
			logger.warn("importFileTemplate", e);
		}
		return "redirect:/msg";
    }
    
    
    @RequestMapping(value = "/orderType/export")
    public String exportFileTemplate(HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
  		try {
  			
  			Map<String, Object> params = WebUtils.getParametersStartingWith(request, null);
  			
  			//Page page=baseQueryForm.getPage();
              String fileName = "订单类型数据.xlsx";
              
            List<OrderType> list = orderTypeDao.findListBy(null, params,ShiroUtils.findUser());
              
              new ExportExcel("订单类型数据", OrderType.class, 1).setDataList(list).write(response, fileName).dispose();
      		return null;
  		} catch (Exception e) {
  			redirectAttributes.addFlashAttribute("msg", "订单类型数据导出失败。");
  			logger.warn("importFileTemplate", e);
  		}
  		return "redirect:/msg";
      }
    
    private static final Logger logger = LoggerFactory.getLogger(OrderTypeWeb.class);
}
