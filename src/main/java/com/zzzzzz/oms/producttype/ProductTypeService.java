package com.zzzzzz.oms.producttype;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.oms.receiver.Receiver;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.sys.city.City;
import com.zzzzzz.sys.client.ClientService;
import com.zzzzzz.utils.BeanValidators;
import com.zzzzzz.utils.DateTimeUtils;

@Service
public class ProductTypeService {
	@Resource
	public ProductTypeDao productTypeDao;
	@Resource
	public Validator validator;
	@Resource
	public ClientService clientService;
	@Transactional
	public int save(ProductType productType, I i) {
		productType.setUpdDt(new Date());
		productType.setUpdBy(i.getId());
		if (productType.getId() == null) {
			productType.setAddDt(new Date());
			productType.setAddBy(i.getId());
			productTypeDao.add(productType);
		} else {
			productTypeDao.updById(productType);
		}

		return 1;
	}
	
	//找cd
	public ProductType getProductTypeByCd(String cd,Long clientId) {
		Map<String, List<ProductType>> allProductTypeMap=productTypeDao.getAllProductTypeMap();
		List<ProductType> productTypeList=allProductTypeMap.get(cd);
		if(productTypeList!=null){
			for(ProductType productType:productTypeList){
				if(productType.getClientId().equals(clientId)){
					return productType;
				}
			}
		}
		return null;
	}
	
	@Transactional
	public List<BaseData> batchAdd(List<ProductType> list, I i) {
		logger.info("productType batchAdd validate start:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		//start validate
		int errTt = 0;// 错误信息条数
		int susTt = 0;// 正确信息条数
		BaseData baseData;
		//用于存放信息
		List<BaseData> listMsg=new ArrayList<BaseData>();
		for (ProductType productType : list) {
			//验证是否为空值
			if(StringUtils.isBlank(productType.getCd())){
				baseData=new BaseData();
				baseData.setErrMsg("代码"+productType.getCd()+"为无效值");
				listMsg.add(baseData);
				errTt++;
			}else{	
				if(clientService.getclientByCd(productType.getClientCd(),i)!=null){
						productType.setSt(0);	
						productType.setClientId(clientService.getclientByCd(productType.getClientCd(), i).getId());
						if(productType.getDescr()==null||productType.getDescr().equals("")){
							productType.setDescr(null);
						}
						try{
							save(productType, i);
							susTt++;
						}catch(DuplicateKeyException e){
							baseData=new BaseData("-1", String.format("产品类型代码"+productType.getCd()+"已经存在"));
							listMsg.add(baseData);
							errTt++;
						}					
				}else{
					baseData=new BaseData();
					baseData.setErrMsg("客户代码"+productType.getClientCd()+"不存在");
					listMsg.add(baseData);
					errTt++;
				}										
				}	
			}
		baseData = new BaseData();
		baseData.setErrMsg("总共有" + (susTt + errTt) + "行记录；共有" + susTt+ "行记录导入成功；共有" + errTt + "行记录导入失败；");
		listMsg.add(baseData);
		logger.info("city batchAdd save end:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		return listMsg;
	}
	private static final Logger logger = LoggerFactory.getLogger(ProductType.class);
}
