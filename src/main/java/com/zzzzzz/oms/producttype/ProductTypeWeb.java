package com.zzzzzz.oms.producttype;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.WebUtils;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.sys.city.City;
import com.zzzzzz.sys.client.Client;
import com.zzzzzz.utils.page.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzzzzz.oms.GCS;
import com.zzzzzz.oms.receiver.Receiver;
import com.google.common.collect.Lists;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.plugins.poi.ExportExcel;
import com.zzzzzz.plugins.poi.ImportExcel;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class ProductTypeWeb {

	@Resource
	public ProductTypeService productTypeService;
	@Resource
	public ProductTypeDao productTypeDao;

	@RequestMapping(value = "/productType/list", method = RequestMethod.GET)
	public String list() {
		return "oms/productType";
	}

	@RequestMapping(value = "/productType/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid ProductType productType, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}

		productTypeService.save(productType, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/productType/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		productTypeDao.updStByIds(ids, st, ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	
	
	@RequestMapping(value = "/productType/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		ProductType productType = productTypeDao.findById(id);
		return new BaseData(productType, null);
	}
	@RequestMapping(value = "/productType/clientId/{clientId}", method = RequestMethod.POST)
	@ResponseBody 
	public BaseData findByClientId(@PathVariable Long clientId) {
		List<ProductType> list = productTypeDao.findByClientId(clientId);
		return new BaseData(list, null);
	}

	@RequestMapping(value = "/productType/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		Page page;
		List<ProductType> list;
		BaseData baseData;
		if(baseQueryForm.getPageNb()==null&&baseQueryForm.getPageSize()==null){
			list = productTypeDao.findListBy(null,baseQueryForm.getFfMap(),ShiroUtils.findUser());
			baseData=new BaseData(list, null);
		}else{
			page=baseQueryForm.getPage();
			list = productTypeDao.findListBy(page,baseQueryForm.getFfMap(),ShiroUtils.findUser());
			baseData=new BaseData(list, page.getTotal());
		}
		return baseData;
	}
	
	@RequestMapping(value = "/productType/import", method=RequestMethod.POST)
    public String importExcel(@RequestParam MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			if(StringUtils.isBlank(file.getOriginalFilename())){
				List<BaseData> list3=new ArrayList<BaseData>();
				BaseData baseData=new BaseData();
				baseData.setErrMsg("您还没选择文件，请先选择需要导入的文件！");
				list3.add(baseData);
				redirectAttributes.addFlashAttribute("msg",list3);
			}else{
				ImportExcel importExcel = new ImportExcel(file, 1, 0);
				if(file.getOriginalFilename().contains("产品类型")){
					List<ProductType> list = importExcel.getDataList(ProductType.class);
					List<BaseData> list1 =productTypeService.batchAdd(list, ShiroUtils.findUser());
					redirectAttributes.addFlashAttribute("msg", list1);
				}else{
					List<BaseData> list2=new ArrayList<BaseData>();
					BaseData baseData=new BaseData();
					baseData.setErrMsg("你导入模板有误，请重新选择模板导入。");
					list2.add(baseData);
					redirectAttributes.addFlashAttribute("msg",list2);
				}
			}			
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "导入数据保存失败。");
			logger.warn("importExcel", e);
		}
		return "redirect:/msg";
    }
    
    @RequestMapping(value = "productType/import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "产品类型数据导入模板.xlsx";
    		List<ProductType> list = Lists.newArrayList();
    		list.add(ProductType.newTest());
    		new ExportExcel("产品类型数据导入模板", ProductType.class, 2).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "产品类型数据导入模板下载失败。");
			logger.warn("importFileTemplate", e);
		}
		return "redirect:/msg";
    }
    
    @RequestMapping(value = "/productType/export")
    public String exportFileTemplate(HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
  		try {
  			
  			Map<String, Object> params = WebUtils.getParametersStartingWith(request, null);
  			
  			//Page page=baseQueryForm.getPage();
              String fileName = "产品类型数据.xlsx";
              
            List<ProductType> list = productTypeDao.findListBy(null, params,ShiroUtils.findUser());
            List<ProductType> list1=new ArrayList<ProductType>();
      		for(ProductType productType:list){
      			productType.setClientCd(productType.getClientName());
      			list1.add(productType);
      		} 
              new ExportExcel("产品类型数据", ProductType.class, 1).setDataList(list1).write(response, fileName).dispose();
      		return null;
  		} catch (Exception e) {
  			redirectAttributes.addFlashAttribute("msg", "产品类型数据导出失败。");
  			logger.warn("importFileTemplate", e);
  		}
  		return "redirect:/msg";
      }
    
    private static final Logger logger = LoggerFactory.getLogger(ProductTypeWeb.class);
}
