package com.zzzzzz.oms.producttype;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.zzzzzz.oms.product.Product;
import com.zzzzzz.oms.receiver.Receiver;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.sys.city.City;
import com.zzzzzz.utils.page.Page;
import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class ProductTypeDao {
	
	@Resource
	private BaseDao baseDao;
			
	private final static String sql_add = "insert into t_product_type(clientId, cd, name, typ, descr, addDt, addBy, st) values(:clientId, :cd, :name, :typ, :descr, :addDt, :addBy, :st)";
	private final static String sql_updStByIds = "update t_product_type set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update t_product_type set clientId=:clientId, cd=:cd, name=:name, typ=:typ, descr=:descr, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, clientId, cd, name, typ, descr, addDt, addBy, updDt, updBy, st from t_product_type where id = :id";
	private final static String sql_findByClienId="select id, clientId, cd, name, typ, descr, addDt, addBy, updDt, updBy, st from t_product_type where clientId=:clientId and st=0";
	@CacheEvict(value = "productTypeCache", allEntries = true)
	public Long add(ProductType productType){
		return baseDao.updateGetLongKey(sql_add, productType);
	}
	@CacheEvict(value = "productTypeCache", allEntries = true)
	public int updStByIds(List<Long> ids, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}	
	@CacheEvict(value = "productTypeCache", allEntries = true)
	public int updById(ProductType productType){
		return baseDao.update(sql_upd, productType);
	}
	public ProductType findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, ProductType.class);
	}
	public List<ProductType> findByClientId(Long clientId){
		Finder finder = new Finder(sql_findByClienId).setParam("clientId", clientId);
		return baseDao.findList(finder, ProductType.class);
	}
	
	//判断产品类型是否存在
	public ProductType checkProductType(Long clientId,String cd){
		List<ProductType> list=findByClientId(clientId);
		for(ProductType productType:list){
			if(productType.getCd().equals(cd)){
				ProductType productType1=productType;
				return productType1;
			}
		}
		return null;
	}
	/**
	 * 动态查询
	 */
	public List<ProductType> findListBy(Page page,Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("count(1) as tt");
		finder.append(" from t_product_type t inner join t_client c on t.clientId=c.id left join t_user_client u on u.clientId=t.clientId");
		finder.append(" where 1=1");
		finder.append("and c.platformId=:platformId").setParam("platformId", i.getPlatformId());
		finder.append("and u.userId=:userId").setParam("userId", i.getId());
		if(DaoUtils.isNotNull(ffMap.get("clientId"))){
		finder.append(" and t.clientId = :clientId").setParam("clientId", ffMap.get("clientId"));
		}
		if(DaoUtils.isNotNull(ffMap.get("cd"))){
		finder.append(" and t.cd = :cd").setParam("cd", ffMap.get("cd"));
		}
		if(DaoUtils.isNotNull(ffMap.get("name"))){
		finder.append(" and t.name like :name").setParam("name", "%" + ffMap.get("name") + "%");
		}
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and t.st = :st").setParam("st", ffMap.get("st"));
			}else{
				finder.append(" and t.st = 0");
			}
		finder.append("order by t.id desc");
		if (page != null) {
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}
		String sql = finder.getSql().replace("count(1) as tt","t.*,c.name as clientName");
		finder.setSql(sql);	
				
		List<ProductType> list = baseDao.findList(finder, ProductType.class);
				
		return list;
	}
	
	/*
	 * 城市缓存，把所有城市缓存。在没有缓存的情况下才查询数据库。
	 */
	@Cacheable(value = "productTypeCache")
	public Map<String, List<ProductType>> getAllProductTypeMap() {
		Map<String, List<ProductType>> productTypeMap= new HashMap<String, List<ProductType>>();
		List<ProductType> allProductTypeList = findAllBy();
		for (ProductType productType : allProductTypeList) {
			List<ProductType> productTypeList = productTypeMap.get(productType.getCd());
			if (productTypeList != null) {
				productTypeList.add(productType);
			} else {
				productTypeMap.put(productType.getCd(), Lists.newArrayList(productType));
			}
		}
		return productTypeMap;
	}
	
	/*
	 * 缓存
	 */
	
	public List<ProductType> findAllBy() {
		Finder finder = new Finder("");
		finder.append("select");
		finder.append("id, clientId, cd, name, typ, descr, addDt, addBy, updDt, updBy, st");
		finder.append("from t_product_type");
		List<ProductType> list = baseDao.findList(finder, ProductType.class);
		return list;
	}
}
