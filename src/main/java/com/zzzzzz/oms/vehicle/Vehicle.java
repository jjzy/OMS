package com.zzzzzz.oms.vehicle;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;
import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class Vehicle implements Serializable {
	
	
	
  	private Long id;
	@ExcelField(title = "车辆编号", align = 2, sort = 10)
	@Length(min = 1, max = 30)
  	private String cd;
	@ExcelField(title = "承运商", align = 2, sort = 20)
	
  	private Long constractorId;
	@ExcelField(title = "运输工具类型", align = 2, sort = 30)
	
  	private Long vehicleTypeId;
	@ExcelField(title = "描述", align = 2, sort = 40)
	
  	private String descr;
	@ExcelField(title = "车牌号", align = 2, sort = 60)
	
  	private String vehicleNo;
	@ExcelField(title = "体积", align = 2, sort = 70)
	
  	private Double volume;
	@ExcelField(title = "承包类型", align = 2, sort = 80)
	
  	private String contractType;
	@ExcelField(title = "排序值", align = 2, sort = 120)
	
	private Long driverId;
	
	private String driverName;
	
	private String contractTypeName;
	
	private String constractorName;
	
  	private Date addDt;
	
	
  	private Long addBy;
	
	
  	private Date updDt;
	
	
  	private Long updBy;
	@ExcelField(title = "状态", align = 2, sort = 130)
	
  	private Integer st;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public String getCd(){
  		return cd;
  	}
  	public void setCd(String cd) {
		this.cd = cd;
	}
  	public Long getConstractorId(){
  		return constractorId;
  	}
  	public void setConstractorId(Long constractorId) {
		this.constractorId = constractorId;
	}
  	public Long getVehicleTypeId(){
  		return vehicleTypeId;
  	}
  	public void setVehicleTypeId(Long vehicleTypeId) {
		this.vehicleTypeId = vehicleTypeId;
	}
  	public String getDescr(){
  		return descr;
  	}
  	public void setDescr(String descr) {
		this.descr = descr;
	}
  	public String getVehicleNo(){
  		return vehicleNo;
  	}
  	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
  	public Double getVolume(){
  		return volume;
  	}
  	public void setVolume(Double volume) {
		this.volume = volume;
	}
  	public String getContractType(){
  		return contractType;
  	}
  	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
  	public Integer getSt(){
  		return st;
  	}
  	public void setSt(Integer st) {
		this.st = st;
	}
 	
 	public Long getDriverId() {
		return driverId;
	}
	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}
	
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	
	public String getContractTypeName() {
		return contractTypeName;
	}
	public void setContractTypeName(String contractTypeName) {
		this.contractTypeName = contractTypeName;
	}
		
	public String getConstractorName() {
		return constractorName;
	}
	public void setConstractorName(String constractorName) {
		this.constractorName = constractorName;
	}
	public static Vehicle newTest() {
		Vehicle vehicle = new Vehicle();
		
		//vehicle.setCd("");
		//vehicle.setConstractorId("");
		//vehicle.setVehicleTypeId("");
		//vehicle.setDescr("");
		//vehicle.setVehicleNo("");
		//vehicle.setVolume("");
		//vehicle.setContractType("");
		//vehicle.setSortNb("");
		
		
		
		
		//vehicle.setSt("");
		return vehicle;
	}
}


