package com.zzzzzz.oms.vehicle;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.zzzzzz.oms.receiver.Receiver;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.utils.page.Page;
import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class VehicleDao {
	
	@Resource
	private BaseDao baseDao;
			
	private final static String sql_add = "insert into t_vehicle(cd, constractorId, vehicleTypeId, descr, vehicleNo,driverId ,volume, contractType, addDt, addBy, st) values(:cd, :constractorId, :vehicleTypeId, :descr, :vehicleNo,:driverId, :volume, :contractType, :addDt, :addBy, :st)";
	private final static String sql_updStByIds = "update t_vehicle set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update t_vehicle set cd=:cd, constractorId=:constractorId, vehicleTypeId=:vehicleTypeId, descr=:descr, vehicleNo=:vehicleNo, driverId=:driverId,volume=:volume, contractType=:contractType, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, cd, constractorId, vehicleTypeId, descr, vehicleNo,driverId, volume, contractType, addDt, addBy, updDt, updBy, st from t_vehicle where id = :id";
	
	public Long add(Vehicle vehicle){
		return baseDao.updateGetLongKey(sql_add, vehicle);
	}
	public int updStByIds(List<Long> ids, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}		  
	public int updById(Vehicle vehicle){
		return baseDao.update(sql_upd, vehicle);
	}
	public Vehicle findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, Vehicle.class);
	}
	
	/**
	 * 动态查询
	 */
	public List<Vehicle> findListBy(Page page,Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("count(1) as tt");
		finder.append(" from t_vehicle v inner join t_constractor c on v.constractorId=c.id left join t_driver d on v.driverId=d.id inner join sys_dict sd on sd.descr=v.contractType");
		finder.append(" where 1=1");
		finder.append("and c.platformId=:platformId").setParam("platformId", i.getPlatformId());
		if(DaoUtils.isNotNull(ffMap.get("cd"))){
			finder.append(" and v.cd = :cd").setParam("cd", ffMap.get("cd"));
		}
		if(DaoUtils.isNotNull(ffMap.get("constractorId"))){
			finder.append(" and v.constractorId = :constractorId").setParam("constractorId", ffMap.get("constractorId"));
		}
		if(DaoUtils.isNotNull(ffMap.get("contractType"))){
			finder.append(" and v.contractType = :contractType").setParam("contractType", ffMap.get("contractType"));
		}
		if(DaoUtils.isNotNull(ffMap.get("vehicleNo"))){
			finder.append(" and v.vehicleNo = :vehicleNo").setParam("vehicleNo", ffMap.get("vehicleNo"));
		}
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and v.st = :st").setParam("st", ffMap.get("st"));
		}else{
			finder.append(" and v.st = 0");
		}
				
		finder.append("order by v.id desc");
		if (page != null) {
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}
		String sql = finder.getSql().replace("count(1) as tt","v.*,c.name as constractorName,d.name as driverName,sd.val as contractTypeName");
		finder.setSql(sql);
		List<Vehicle> list = baseDao.findList(finder, Vehicle.class);
				
		return list;
	}
}
