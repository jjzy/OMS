package com.zzzzzz.oms.vehicle;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.utils.BeanValidators;
import com.zzzzzz.utils.DateTimeUtils;

@Service
public class VehicleService {
	@Resource
	public VehicleDao vehicleDao;
	@Resource
	public Validator validator;
	
	@Transactional
	public int save(Vehicle vehicle, I i) {
		vehicle.setUpdDt(new Date());
		vehicle.setUpdBy(i.getId());
		if (vehicle.getId() == null) {
			vehicle.setAddDt(new Date());
			vehicle.setAddBy(i.getId());
			vehicleDao.add(vehicle);
		} else {
			vehicleDao.updById(vehicle);
		}

		return 1;
	}
	
	@Transactional
	public BaseData batchAdd(List<Vehicle> list, I i) {
		logger.info("vehicle batchAdd validate start:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		//start validate
		int errTt = 0;
		StringBuilder errMsg = new StringBuilder();
		for (Vehicle vehicle : list) {
			try {
				BeanValidators.validateWithException(validator, vehicle);
			} catch (ConstraintViolationException e) {
				List<String> msgList = BeanValidators.extractPropertyAndMessageAsList(e, ": ");
				for (String msg : msgList) {
					errTt++;
					errMsg.append("<br/>").append(msg);
				}
			} catch (Exception e) {
				logger.warn("", e);
				errTt++;
				errMsg.append("<br/>验证失败：").append(e.getMessage());
			}
		}
		if(errTt > 0){
			errMsg.insert(0, "共有"+errTt+"笔记录验证失败：");
			return new BaseData("101", errMsg.toString());
		}
		logger.info("vehicle batchAdd validate end:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		logger.info("vehicle batchAdd save start:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		//state save
		int susTt = 0;
		for (Vehicle vehicle : list) {
			vehicle.setSt(0);
			save(vehicle, i);
			susTt++;
		}
		logger.info("vehicle batchAdd save end:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		return new BaseData("0", "共有"+susTt+"笔记录导入成功。");
	}

	private static final Logger logger = LoggerFactory.getLogger(Vehicle.class);
}
