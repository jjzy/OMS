package com.zzzzzz.oms.legs;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Repository;

import com.zzzzzz.oms.orderHead.OrderHead;
import com.zzzzzz.oms.receiver.Receiver;
import com.zzzzzz.oms.tempShipment.TempShipmentDao;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.utils.page.Page;
import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class LegsDao {
	
	@Resource
	private BaseDao baseDao;
	@Resource
	private TempShipmentDao tempShipmentDao;
	private final static String sql_add = "insert into t_legs(orderId, orderCd, legs_no, st, platformId,vocationType, clientId, constractorId,shipment_method, formId, toId,ftranslocationId,ttranslocationId,formName, formlikeName, formPhone, formAddr, toName, tolikeName, toPhone, toAddr, shipmentId, tempshipmentId, shipmentType, quantity, weight, volume, palletsum, expense, bag, ordertime, billtime, planltime, planatime,leavetime, arrivetime, addDt, addBy) values(:orderId, :orderCd, :legs_no, 'shipment', :platformId,:vocationType, :clientId, :constractorId, :shipment_method, :formId, :toId,:ftranslocationId,:ttranslocationId, :formName, :formlikeName, :formPhone, :formAddr, :toName, :tolikeName, :toPhone, :toAddr, :shipmentId,:tempshipmentId, :shipmentType, :quantity, :weight, :volume, :palletsum, :expense, :bag, :ordertime, :billtime, :planltime,:planatime, :leavetime, :arrivetime, :addDt, :addBy)";
	private final static String sql_updTempshipmentByIds = "update t_legs set tempshipmentId=:tempshipmentId,updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_updStById = "update t_legs set tempshipmentId=:tempshipmentId,updDt = :updDt, updBy = :updBy where tempshipmentId in (:ids)";
	private final static String sql_upd = "update t_legs set orderId=:orderId, orderCd=:orderCd, legs_no=:legs_no, st=:st, platformId=:platformId,vocationType=:vocationType, clientId=:clientId, constractorId=:constractorId, driverId=:driverId,shipment_method=:shipment_method, formId=:formId, toId=:toId,ftranslocationId=:ftranslocationId,ttranslocationId=:ttranslocationId, formName=:formName, formlikeName=:formlikeName, formPhone=:formPhone, formAddr=:formAddr, toName=:toName, tolikeName=:tolikeName, toPhone=:toPhone, toAddr=:toAddr, shipmentId=:shipmentId, shipmentType=:shipmentType, quantity=:quantity, weight=:weight, volume=:volume, palletsum=:palletsum, expense=:expense, bag=:bag, ordertime=:ordertime, billtime=:billtime, planltime=:planltime, planatime=:planatime,leavetime=:leavetime, arrivetime=:arrivetime, updDt=:updDt, updBy=:updBy where id = :id";
	private final static String sql_findById = "select id, orderId, orderCd, legs_no, st, platformId,vocationType, clientId, constractorId, shipment_method, formId, toId,ftranslocationId,ttranslocationId, formName, formlikeName, formPhone, formAddr, toName, tolikeName, toPhone, toAddr, shipmentId, shipmentType, quantity, weight, volume, palletsum, expense, bag, pcTime,rkTime,ckTime,ordertime, billtime, planltime,planatime, leavetime, arrivetime, addDt, addBy, updDt, updBy from t_legs where id = :id";
	private final static String sql_updByTempShipmentId="update t_legs set constractorId=:constractorId,shipment_method=:shipment_method,st=:st where tempshipmentId=:tempshipmentId";
	private final static String sql_updShipmentId="update t_legs set shipmentId=:shipmentId,updDt = :updDt, updBy = :updBy where tempshipmentId=:tempshipmentId";
	private final static String sql_findCount="select count(ttranslocationId) from (select distinct ttranslocationId from t_legs L where tempShipmentId=:tempShipmentId) L";
	private final static String sql_findCount1="select count(ttranslocationId) from (select distinct ttranslocationId from t_legs L where shipmentId=:shipmentId) L";
	private final static String sql_findByTempshipmentId="select id,orderId, orderCd, legs_no, st, platformId,vocationType, clientId, constractorId,shipment_method, formId, toId,ftranslocationId,ttranslocationId,formName, formlikeName, formPhone, formAddr, toName, tolikeName, toPhone, toAddr, shipmentId, tempshipmentId, shipmentType, quantity, weight, volume, palletsum, expense, bag,pcTime,rkTime,ckTime, ordertime, billtime, planltime,planatime, leavetime, arrivetime, addDt, addBy,updDt,updBy from t_legs where tempshipmentId=:tempshipmentId";
	private final static String sql_findByIds="select id,orderId, orderCd, legs_no, st, platformId,vocationType, clientId, constractorId,shipment_method, formId, toId,ftranslocationId,ttranslocationId,formName, formlikeName, formPhone, formAddr, toName, tolikeName, toPhone, toAddr, shipmentId, tempshipmentId, shipmentType, quantity, weight, volume, palletsum, expense, bag,pcTime,rkTime,ckTime, ordertime, billtime, planltime, planatime,leavetime, arrivetime, addDt, addBy,updDt,updBy from t_legs where  orderId in (:ids)";
	private final static String sql_updByIds="update t_legs set shipmentId=NULL,tempshipmentId=:tempshipmentId,constractorId=NULL,st='shipment',pcTime=NULL,rkTime=NULL,ckTime=NULL,leavetime=NULL,arrivetime=NULL,updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_updByIds1="update t_legs set shipmentId=:shipmentId,tempshipmentId=NULL,constractorId=:constractorId,shipment_method=:shipment_method,st='shipmentEd',updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_delByIds="delete from t_legs where orderId in (:ids)";
	private final static String sql_updStByShipmentIds="update t_legs set st=:st,updDt = :updDt, updBy = :updBy where shipmentId in (:ids)";
	private final static String sql_updStByIds="update t_legs set st=:st,updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_findByShipmentId="select id, orderId, orderCd, legs_no, st, platformId,vocationType, clientId, constractorId, tempshipmentId,shipment_method, formId, toId,ftranslocationId,ttranslocationId, formName, formlikeName, formPhone, formAddr, toName, tolikeName, toPhone, toAddr, shipmentId, shipmentType, quantity, weight, volume, palletsum, expense, bag, pcTime,rkTime,ckTime,ordertime, billtime, planltime, planatime,leavetime, arrivetime, addDt, addBy, updDt, updBy from t_legs where shipmentId in (:shipmentId)";
	private final static String sql_findByTempShipmentId="select id, orderId, orderCd, legs_no, st, platformId,vocationType, clientId, constractorId, shipment_method,tempshipmentId, formId, toId,ftranslocationId,ttranslocationId, formName, formlikeName, formPhone, formAddr, toName, tolikeName, toPhone, toAddr, shipmentId, shipmentType, quantity, weight, volume, palletsum, expense, bag, pcTime,rkTime,ckTime,ordertime, billtime, planltime, planatime,leavetime, arrivetime, addDt, addBy, updDt, updBy from t_legs where tempshipmentId in (:tempshipmentIds)";
	public Long add(Legs legs){
		return baseDao.updateGetLongKey(sql_add, legs);
	}
	public int updTempshipmenByIds(List<Long> ids,Long tempshipmentId, I i){
		Finder finder = new Finder(sql_updTempshipmentByIds).setParam("ids", ids).setParam("tempshipmentId", tempshipmentId)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}
	
	public List<Legs> findByShipmentId(List<Long> shipmentIds){
		Finder finder = new Finder(sql_findByShipmentId).setParam("shipmentId", shipmentIds);
		return baseDao.findList(finder,Legs.class);
	}
	
	public List<Legs> findByTempShipmentId(List<Long> tempshipmentIds){
		Finder finder = new Finder(sql_findByTempShipmentId).setParam("tempshipmentIds", tempshipmentIds);
		return baseDao.findList(finder,Legs.class);
	}
	
	public int updStByShipmentIds(List<Long> ids,String st,I i){
		Finder finder = new Finder(sql_updStByShipmentIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}
	
	public int updStByIds(Object ids,Object st,I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}
	public int updByIds(List<Long> ids,Long tempShipmentId,I i){
		Finder finder = new Finder(sql_updByIds).setParam("ids", ids).setParam("tempshipmentId",tempShipmentId)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}
	public int delByIds(List<Long> ids){
		Finder finder = new Finder(sql_delByIds).setParam("ids", ids);
		return baseDao.update(finder);
	}
	public int updByIds1(List<Long> ids,Long shipmentId,Long constractorId,String shipment_method,I i){
		Finder finder = new Finder(sql_updByIds1).setParam("ids", ids).setParam("shipmentId",shipmentId)
		.setParam("constractorId", constractorId).setParam("shipment_method", shipment_method).setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}
	public int updStById(List<Long> ids,Long tempshipmentId,I i){
		Finder finder = new Finder(sql_updStById).setParam("ids", ids).setParam("tempshipmentId", tempshipmentId)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}	
	public List<Legs> findByTempShipmentId(Long tempShipmentId){
		Finder finder = new Finder(sql_findByTempshipmentId).setParam("tempshipmentId", tempShipmentId);
		return baseDao.findList(finder,Legs.class);

	}
	public int updByTempShipmentId(Long tempShipmentId,Long constractorId,String shipment_method,String st, I i){
		Finder finder = new Finder(sql_updByTempShipmentId).setParam("tempshipmentId", tempShipmentId).setParam("constractorId", constractorId)
		.setParam("shipment_method", shipment_method).setParam("st", st)		
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}
	
	public int updShipmentId(Long tempShipmentId,Long shipmentId, I i){
		Finder finder = new Finder(sql_updShipmentId).setParam("tempshipmentId", tempShipmentId).setParam("shipmentId", shipmentId)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}
	public int updById(Legs legs){
		return baseDao.update(sql_upd, legs);
	}
	public Legs findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, Legs.class);
	}
	public int findCount(Long id){
		Finder finder = new Finder(sql_findCount).setParam("tempShipmentId", id);
		return baseDao.findInt(finder);
	}
	public int findCount1(Long id){
		Finder finder = new Finder(sql_findCount1).setParam("shipmentId", id);
		return baseDao.findInt(finder);
	}
	public List<Legs> findByIds(List<Long> ids){
		Finder finder = new Finder(sql_findByIds).setParam("ids", ids);
		return baseDao.findList(finder,Legs.class);
	}	
	
	public int updTime(Map<String, Object> ffMap, I i){
		Finder finder=new Finder("");
		finder.append("update t_legs set");
		if(DaoUtils.isNotNull(ffMap.get("pcTime"))){
			finder.append("pcTime=:pcTime").setParam("pcTime", ffMap.get("pcTime"));
		}
		if(DaoUtils.isNotNull(ffMap.get("rkTime"))){
			finder.append("rkTime=:rkTime").setParam("rkTime", ffMap.get("rkTime"));
		}
		if(DaoUtils.isNotNull(ffMap.get("ckTime"))){
			finder.append("ckTime=:ckTime").setParam("ckTime", ffMap.get("ckTime"));
		}
		if(DaoUtils.isNotNull(ffMap.get("fyTime"))){
			finder.append("leavetime=:fyTime").setParam("fyTime", ffMap.get("fyTime"));
		}
		if(DaoUtils.isNotNull(ffMap.get("ydTime"))){
			finder.append("arrivetime=:ydTime").setParam("ydTime", ffMap.get("ydTime"));
		}
		if(DaoUtils.isNotNull(ffMap.get("ids"))){
			finder.append("where id in (:ids)").setParam("ids", ffMap.get("ids"));
		}
		if(DaoUtils.isNotNull(ffMap.get("shipmentIds"))){
			finder.append("where shipmentId in (:shipmentIds)").setParam("shipmentIds", ffMap.get("shipmentIds"));	
		}
		return baseDao.update(finder);
	}
	
	//派车的调度单状态改变
	public int updStatusPcDt(Long shipmentId,String pcTime, I i){
		Finder finder=new Finder("");
		finder.append("update t_shipment s");
		if(pcTime!=null){
			finder.append("set status='sendCar'");
			finder.append("where 1=(select count(id)=count(pcTime) from t_legs l  where l.shipmentId=:shipmentId)").setParam("shipmentId", shipmentId);
		}
		finder.append("and s.id=:id").setParam("id", shipmentId);
		return baseDao.update(finder);
	}
	//车辆入库的调度单状态改变
	public int updStatusRkDt(Long shipmentId,String rkTime, I i){
		Finder finder=new Finder("");
		finder.append("update t_shipment s");
		if(rkTime!=null){
			finder.append("set status='instorage'");
			finder.append("where 1=(select count(id)=count(rkTime) from t_legs l  where l.shipmentId=:shipmentId)").setParam("shipmentId", shipmentId);
		}
		finder.append("and s.id=:id").setParam("id", shipmentId);
		return baseDao.update(finder);
	}
		//车辆入库的调度单状态改变
	public int updStatusCkDt(Long shipmentId,String ckTime, I i){
			Finder finder=new Finder("");
			finder.append("update t_shipment s");
			if(ckTime!=null){
				finder.append("set status='outstorage'");
				finder.append("where 1=(select count(id)=count(ckTime) from t_legs l  where l.shipmentId=:shipmentId)").setParam("shipmentId", shipmentId);
			}
			finder.append("and s.id=:id").setParam("id", shipmentId);
			return baseDao.update(finder);
	}
	//发运确认的调度单状态改变
	public int updStatusFyDt(Long shipmentId,String fyTime, I i){
			Finder finder=new Finder("");
			finder.append("update t_shipment s");
			if(fyTime!=null){
				finder.append("set status='onway',leavetime=:leavetime").setParam("leavetime", fyTime);
				finder.append("where 1=(select count(id)=count(leavetime) from t_legs l  where l.shipmentId=:shipmentId)").setParam("shipmentId", shipmentId);
			}
			finder.append("and s.id=:id").setParam("id", shipmentId);
			return baseDao.update(finder);
	}
	//运抵确认的调度单状态改变
		public int updStatusYdDt(Long shipmentId,String ydTime, I i){
				Finder finder=new Finder("");
				finder.append("update t_shipment s");
				if(ydTime!=null){
					finder.append("set status='arrive',arrivetime=:arrivetime").setParam("arrivetime", ydTime);
					finder.append("where 1=(select count(id)=count(arrivetime) from t_legs l  where l.shipmentId=:shipmentId)").setParam("shipmentId", shipmentId);
				}
				finder.append("and s.id=:id").setParam("id", shipmentId);
				return baseDao.update(finder);
		}
	/**
	 * 动态查询
	 */
	public List<Legs> findListBy(Page page,Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("count(1) as tt");
		finder.append("from t_legs l inner join t_order_head o on o.id=l.orderId inner join sys_dict d on o.status=d.descr inner join sys_dict d1 on l.st=d1.descr inner join sys_platform p on l.platformId=p.id inner join t_client c on l.clientId=c.id left join sys_dict d2 on l.shipment_method=d2.descr left join t_constractor cs on l.constractorId=cs.id inner join t_trans_location ft on ft.id=l.ftranslocationId inner join t_trans_location tt on tt.id=l.ttranslocationId "
				+ "left join t_shipment s on s.id=l.shipmentId left join t_vehicle v on v.id=s.vehicleId inner join sys_dict dv on dv.descr=l.vocationType");
		finder.append(" where 1=1");
		finder.append("and l.platformId=:platformId").setParam("platformId", i.getPlatformId());
		if(DaoUtils.isNotNull(ffMap.get("st"))){
			finder.append(" and l.st = :st").setParam("st", ffMap.get("st"));
		}	
		if(DaoUtils.isNotNull(ffMap.get("tempshipmentId"))){	
			finder.append(" and l.tempshipmentId=:tempshipmentId").setParam("tempshipmentId", ffMap.get("tempshipmentId"));
		}
		if(DaoUtils.isNotNull(ffMap.get("shipmentId"))){	
			finder.append(" and l.shipmentId=:shipmentId").setParam("shipmentId", ffMap.get("shipmentId"));
		}
		if(DaoUtils.isNotNull(ffMap.get("shipment_method"))){	
			finder.append(" and l.shipment_method=:shipment_method").setParam("shipment_method", ffMap.get("shipment_method"));
		}
		if(DaoUtils.isNotNull(ffMap.get("orderCd"))){	
			finder.append(" and l.orderCd=:orderCd").setParam("orderCd", ffMap.get("orderCd"));
		}
		if(DaoUtils.isNotNull(ffMap.get("clientId"))){	
			finder.append(" and l.clientId=:clientId").setParam("clientId", ffMap.get("clientId"));
		}
		if(DaoUtils.isNotNull(ffMap.get("constractorId"))){	
			finder.append(" and l.constractorId=:constractorId").setParam("constractorId", ffMap.get("constractorId"));
		}
		if(DaoUtils.isNotNull(ffMap.get("status"))){	
			finder.append(" and o.status=:status").setParam("status", ffMap.get("status"));
		}
		if(DaoUtils.isNotNull(ffMap.get("shipment_method"))){	
			finder.append(" and l.shipment_method=:shipment_method").setParam("shipment_method", ffMap.get("shipment_method"));
		}
		if(DaoUtils.isNotNull(ffMap.get("shipment_method"))){	
			finder.append(" and l.shipment_method=:shipment_method").setParam("shipment_method", ffMap.get("shipment_method"));
		}
		finder.append("order by l.id desc");
		if (page != null) {
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}
		String sql = finder.getSql().replace("count(1) as tt","l.*,d.val as statusName ,d1.val as stName ,p.shortName as platformName,c.name as clientName ,d2.val as shipment_methodName,cs.name as constractorName,ft.name as ftranslocationName,tt.name as ttranslocationName,s.cd as shipmentCd,s.expense as shipmentExpese,v.cd as vehicleCd,dv.val as vocationName");
		finder.setSql(sql);
		List<Legs> list = baseDao.findList(finder, Legs.class);
				
		return list;
	}
	/*
	 * 根据st查找
	 */
	public List<Legs> findBySt(Map<String, Object> ffMap){
		Finder finder=new Finder("");
		finder.append("select");
		finder.append("*");
		finder.append("from t_legs");
		finder.append(" where 1=1");
		if(DaoUtils.isNotNull(ffMap.get("st"))){		
			finder.append(" and st = :st").setParam("st", ffMap.get("st"));
			}
		if(DaoUtils.isNotNull(ffMap.get("shipmentId"))){		
			finder.append(" and st = :st").setParam("st", ffMap.get("st"));
			}	
		List<Legs> list = baseDao.findList(finder,Legs.class);		
		return list;
	}
	/*
	 * 批量出入数据
	 */
	public void saveOrder(List<Legs> legs){
		baseDao.batchUpdate(sql_add, legs);
	}
}
