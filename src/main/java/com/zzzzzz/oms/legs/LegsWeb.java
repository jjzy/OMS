package com.zzzzzz.oms.legs;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.utils.page.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzzzzz.oms.GCS;
import com.zzzzzz.oms.orderHead.OrderHead;
import com.zzzzzz.oms.orderHead.OrderHeadDao;
import com.zzzzzz.oms.shipment.Shipment;
import com.zzzzzz.oms.tempShipment.TempShipmentDao;
import com.google.common.collect.Lists;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.plugins.poi.ExportExcel;
import com.zzzzzz.plugins.poi.ImportExcel;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class LegsWeb {

	@Resource
	public LegsService legsService;
	@Resource
	public LegsDao legsDao;
	@Resource
	public OrderHeadDao orderHeadDao;
	@Resource
	public TempShipmentDao tempShipmentDao;
	@RequestMapping(value = "/legs/list", method = RequestMethod.GET)
	public String list() {
		return "oms/legs";
	}

	
	@RequestMapping(value = "/legs/updTempshipmenByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updTempshipmenByIds(@RequestParam(value = "ids", required = true) List<Long> ids,@RequestParam(value = "tempshipmentId", required = true)Long tempshipmentId) throws Exception {
		legsDao.updTempshipmenByIds(ids, tempshipmentId,ShiroUtils.findUser());
		return new BaseData("0");
	}	
	
	@RequestMapping(value = "/legs/updStByShipmentIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByShipmentIds(@RequestParam(value = "ids", required = true) List<Long> ids,@RequestParam(value = "st", required = true)String st) throws Exception {
		legsDao.updStByShipmentIds(ids, st,ShiroUtils.findUser());
		return new BaseData("0");
	}	
	
	@RequestMapping(value = "/legs/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids,@RequestParam(value = "st", required = true)String st) throws Exception {
		legsDao.updStByIds(ids, st,ShiroUtils.findUser());
		return new BaseData("0");
	}	
	@RequestMapping(value = "/legs/delByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData delByIds(@RequestBody List<OrderHead> orderHead, @RequestParam(value = "ids", required = true) List<Long> ids) throws Exception {
		legsDao.delByIds(ids);
		return new BaseData("0");
	}	
	
	@RequestMapping(value = "/legs/updByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updByIds(@RequestBody List<Legs> list, @RequestParam(value = "ids", required = true) List<Long> ids,@RequestParam(value = "orderId", required = true) List<Long> orderId,@RequestParam(value = "vocationType", required = true) String vocationType,@RequestParam(value = "shipmentId", required = true) Long shipmentId) throws Exception {
		legsService.updByIds(list, ids, vocationType, shipmentId, ShiroUtils.findUser());
		orderHeadDao.updStByIds(orderId, "ARRIVED",ShiroUtils.findUser());
		return new BaseData("0");
	}	
	@RequestMapping(value = "/legs/updByIds1", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updByIds1(@RequestBody List<Legs> list, @RequestParam(value = "ids", required = true) List<Long> ids,@RequestParam(value = "shipmentId", required = true) Long shipmentId,@RequestParam(value = "constractorId", required = true) Long constractorId,@RequestParam(value = "shipment_method", required = true) String shipment_method,@RequestParam(value = "vocationType", required = true) String vocationType) throws Exception {
		legsService.updByIds1(list, ids, shipmentId, constractorId, shipment_method, vocationType, ShiroUtils.findUser());
		return new BaseData("0");
	}
	@RequestMapping(value = "/legs/findCount/{tempShipmentId}", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findcount(@PathVariable Long tempShipmentId) throws Exception {
		int count=legsDao.findCount(tempShipmentId);
		List<Integer> list=new ArrayList<Integer>();
		list.add(count);
		return new BaseData(list,null);
	}
	@RequestMapping(value = "/legs/findCount1/{shipmentId}", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findcount1(@PathVariable Long shipmentId) throws Exception {
		int count=legsDao.findCount1(shipmentId);
		List<Integer> list=new ArrayList<Integer>();
		list.add(count);
		return new BaseData(list,null);
	}
	@RequestMapping(value = "/legs/updByTempShipmentId", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updByTempShipmentId(@RequestParam(value = "tempShipmentId", required = true) Long tempShipmentId,@RequestParam(value = "constractorId", required = true)Long constractorId,@RequestParam(value = "shipment_method", required = true)String shipment_method,@RequestParam(value = "st", required = true)String st) throws Exception {
		legsDao.updByTempShipmentId(tempShipmentId, constractorId,shipment_method,st,ShiroUtils.findUser());
		return new BaseData("0");
	}	
	
	@RequestMapping(value = "/legs/updShipmentId", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updShipmentId(@RequestParam(value = "tempShipmentId", required = true) Long tempShipmentId,@RequestParam(value = "shipmentId", required = true)Long shipmentId) throws Exception {
		legsDao.updShipmentId(tempShipmentId, shipmentId,ShiroUtils.findUser());
		return new BaseData("0");
	}	
	
	@RequestMapping(value = "/legs/updStById", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStById(@RequestParam(value = "ids", required = true)List<Long> ids,@RequestParam(value = "tempshipmentId", required = true)Long tempshipmentId) throws Exception {
		legsDao.updStById(ids,tempshipmentId,ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	
	@RequestMapping(value = "/legs/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		Legs legs = legsDao.findById(id);
		return new BaseData(legs, null);
	}
	@RequestMapping(value = "/legs/findBySt", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findBySt(@RequestBody BaseQueryForm baseQueryForm) throws Exception {
		List<Legs> list = legsDao.findBySt(baseQueryForm.getFfMap());
		return new BaseData(list, null);
	}	
	@RequestMapping(value = "/legs/findByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findByIds(@RequestParam(value = "ids", required = true) List<Long> ids) throws Exception {
		List<Legs> list=legsDao.findByIds(ids);
		return new BaseData(list,null);
	}	
	@RequestMapping(value = "/legs/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		Page page=baseQueryForm.getPage();
		List<Legs> list = legsDao.findListBy(page,baseQueryForm.getFfMap(),ShiroUtils.findUser());
		BaseData baseData=new BaseData(list, page.getTotal());
		return baseData;
	}
	
	@RequestMapping(value = "/legs/updTime", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updTime(@RequestBody BaseQueryForm baseQueryForm) throws Exception {
		legsDao.updTime(baseQueryForm.getFfMap(), ShiroUtils.findUser());
		return new BaseData("0");
	}	
	@RequestMapping(value = "/legs/updStatusPcDt", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStatusPcDt(@RequestParam(value = "ids", required = true) List<Long> shipmentIds,@RequestParam(value = "pcTime", required = true)String pcTime,@RequestParam(value = "orderIds", required = true) List<Long> orderIds) throws Exception {
		for(int i=0;i<shipmentIds.size();i++){
			legsDao.updStatusPcDt(shipmentIds.get(i), pcTime,ShiroUtils.findUser());
		}
		orderHeadDao.updStByIds(orderIds, "osendCar", ShiroUtils.findUser());
		return new BaseData("0");
	}
	@RequestMapping(value = "/legs/updStatusRkDt", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStatusRkDt(@RequestParam(value = "ids", required = true) List<Long> shipmentIds,@RequestParam(value = "rkTime", required = true)String rkTime,@RequestParam(value = "orderIds", required = true) List<Long> orderIds) throws Exception {
		for(int i=0;i<shipmentIds.size();i++){
			legsDao.updStatusRkDt(shipmentIds.get(i), rkTime,ShiroUtils.findUser());
		}
		orderHeadDao.updStByIds(orderIds, "oinstorage", ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/legs/findByShipmentId", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findByShipmentId(@RequestParam(value = "ids", required = true) List<Long> shipmentIds,@RequestParam(value = "status", required = true) String status) throws Exception {
		legsService.findByShipmentId(shipmentIds, status, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/legs/updStatusCkDt", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStatusCkDt(@RequestParam(value = "ids", required = true) List<Long> shipmentIds,@RequestParam(value = "ckTime", required = true)String ckTime,@RequestParam(value = "orderIds", required = true) List<Long> orderIds) throws Exception {
		for(int i=0;i<shipmentIds.size();i++){
			legsDao.updStatusCkDt(shipmentIds.get(i), ckTime,ShiroUtils.findUser());
		}
		orderHeadDao.updStByIds(orderIds, "ooutstorage", ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/legs/updStatusFyDt", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStatusFyDt(@RequestParam(value = "ids", required = true) List<Long> shipmentIds,@RequestParam(value = "fyTime", required = true)String fyTime,@RequestParam(value = "orderIds", required = true) List<Long> orderIds) throws Exception {
		for(int i=0;i<shipmentIds.size();i++){
			legsDao.updStatusFyDt(shipmentIds.get(i), fyTime,ShiroUtils.findUser());
		}
		orderHeadDao.updStByIds(orderIds, "oonway", ShiroUtils.findUser());
		return new BaseData("0");
	}
	@RequestMapping(value = "/legs/updStatusYdDt", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStatusYdDt(@RequestParam(value = "ids", required = true) List<Long> shipmentIds,@RequestParam(value = "ydTime", required = true)String ydTime,@RequestParam(value = "orderIds", required = true) List<Long> orderIds) throws Exception {
		for(int i=0;i<shipmentIds.size();i++){
			legsDao.updStatusYdDt(shipmentIds.get(i), ydTime,ShiroUtils.findUser());
		}
		orderHeadDao.updStByIds(orderIds, "oarrive", ShiroUtils.findUser());
		return new BaseData("0");
	}
	@RequestMapping(value = "/legs/import", method=RequestMethod.POST)
    public String importExcel(@RequestParam MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			ImportExcel importExcel = new ImportExcel(file, 1, 0);
			List<Legs> list = importExcel.getDataList(Legs.class);
			BaseData baseData = legsService.batchAdd(list, ShiroUtils.findUser());
			redirectAttributes.addFlashAttribute("msg", baseData.getErrMsg());
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "导入数据保存失败。");
			logger.warn("importExcel", e);
		}
		return "redirect:/msg";
    }
    
    @RequestMapping(value = "legs/import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "分段订单数据数据导入模板.xlsx";
    		List<Legs> list = Lists.newArrayList();
    		list.add(Legs.newTest());
    		new ExportExcel("分段订单数据数据导入模板", Legs.class, 2).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "分段订单数据数据导入模板下载失败。");
			logger.warn("importFileTemplate", e);
		}
		return "redirect:/msg";
    }  
    private static final Logger logger = LoggerFactory.getLogger(LegsWeb.class);
}
