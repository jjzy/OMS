package com.zzzzzz.oms.legs;

import java.io.Serializable;
import java.util.Date;
import org.hibernate.validator.constraints.Length;
import com.zzzzzz.oms.orderHead.OrderHead;
import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class Legs implements Serializable {
	
	private OrderHead orderHead;
  	private Long id;
  	private Long clientId;
	@ExcelField(title = "订单", align = 2, sort = 10)
	@Length(min = 1, max = 30)
  	private Long orderId;
	@ExcelField(title = "订单编号", align = 2, sort = 40)
	
  	private String orderCd;
	@ExcelField(title = "分段订单号", align = 2, sort = 40)
	
  	private String legs_no;
	@ExcelField(title = "订单状态", align = 2, sort = 40)
	
  	private String status;
	@ExcelField(title = "状态", align = 2, sort = 40)
	
  	private String st;
	@ExcelField(title = "平台", align = 2, sort = 10)
	
  	private Long platformId;
	@ExcelField(title = "客户", align = 2, sort = 10)
	
  	private String clientCd;
	@ExcelField(title = "承运商", align = 2, sort = 10)
	
  	private Long constractorId;
	@ExcelField(title = "运输工具", align = 2, sort = 10)
	
  	private Long vehicleId;
	@ExcelField(title = "运输方式", align = 2, sort = 40)
	private Long driverId;
  	private String shipment_method;
	@ExcelField(title = "出发地", align = 2, sort = 40)
	
  	private String formCd;
	@ExcelField(title = "目的地", align = 2, sort = 40)
	
  	private String toCd;
	@ExcelField(title = "发货方名称", align = 2, sort = 40)
	
  	private String formName;
	@ExcelField(title = "发货方联系人", align = 2, sort = 40)
	
  	private String formlikeName;
	@ExcelField(title = "发货方电话", align = 2, sort = 40)
	
  	private String formPhone;
	@ExcelField(title = "发货方地址", align = 2, sort = 40)
	
  	private String formAddr;
	@ExcelField(title = "收货方名称", align = 2, sort = 40)
	
  	private String toName;
	@ExcelField(title = "收货方联系人", align = 2, sort = 40)
	
  	private String tolikeName;
	@ExcelField(title = "收货方电话", align = 2, sort = 40)
	
  	private String toPhone;
	@ExcelField(title = "收货方地址", align = 2, sort = 40)
	
  	private String toAddr;
	@ExcelField(title = "调度单号", align = 2, sort = 40)
	
  	private Long shipmentId;
	@ExcelField(title = "配载方式", align = 2, sort = 80)
	
  	private String shipmentType;
	@ExcelField(title = "件数", align = 2, sort = 80)
	
  	private Double quantity;
	@ExcelField(title = "重量", align = 2, sort = 80)
	
  	private Double weight;
	@ExcelField(title = "体积", align = 2, sort = 80)
	
  	private Double volume;
	@ExcelField(title = "托数", align = 2, sort = 80)
	
  	private Double palletsum;
	@ExcelField(title = "一口价", align = 2, sort = 80)
	
  	private Double expense;
	@ExcelField(title = "零箱数", align = 2, sort = 60)
	
  	private Integer bag;
	@ExcelField(title = "开单时间", align = 2, sort = 80)
	private Date pcTime;
	private Date rkTime;
	private Date ckTime;
  	private Date ordertime;
	@ExcelField(title = "计费周期", align = 2, sort = 80)
	
  	private Date billtime;
	@ExcelField(title = "计划离开时间", align = 2, sort = 80)
	
  	private Date planltime;
	@ExcelField(title = "出发时间", align = 2, sort = 80)
	private Date planatime;
  	private Date leavetime;
	@ExcelField(title = "到达时间", align = 2, sort = 80)
	private Long formId;
	private Long toId;
	private Long ftranslocationId;
	private Long ttranslocationId;
  	private String arrivetime;
	private Long tempshipmentId;
	private String statusName;
	private String stName;
	private String platformName;
	private String clientName;
	private String constractorName;
	private String vehicleName;
	private String shipment_methodName;
	private String ftranslocationName;
	private String ttranslocationName;
	private String shipmentCd;
	private String vehicleCd;
	private String shipmentExpese;
	private String vocationType;
	private String vocationName;

	private Date addDt;
	
	
  	private Long addBy;
	
	
  	private Date updDt;
	
	
  	private Long updBy;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public Long getClientId() {
		return clientId;
	}
	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}
	public Long getOrderId(){
  		return orderId;
  	}
  	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
  	public String getOrderCd(){
  		return orderCd;
  	}
  	public void setOrderCd(String orderCd) {
		this.orderCd = orderCd;
	}
  	public String getLegs_no(){
  		return legs_no;
  	}
  	public void setLegs_no(String legs_no) {
		this.legs_no = legs_no;
	}
  	public String getStatus(){
  		return status;
  	}
  	public void setStatus(String status) {
		this.status = status;
	}
  	public String getSt(){
  		return st;
  	}
  	public void setSt(String st) {
		this.st = st;
	}
  	public Long getPlatformId(){
  		return platformId;
  	}
  	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
  	
	
  	public String getClientCd() {
		return clientCd;
	}
	public void setClientCd(String clientCd) {
		this.clientCd = clientCd;
	}
	public Long getConstractorId(){
  		return constractorId;
  	}
  	public void setConstractorId(Long constractorId) {
		this.constractorId = constractorId;
	}
  	public Long getVehicleId(){
  		return vehicleId;
  	}
  	public void setVehicleId(Long vehicleId) {
		this.vehicleId = vehicleId;
	}
  	
  	public Long getDriverId() {
		return driverId;
	}
	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}
	public String getShipment_method(){
  		return shipment_method;
  	}
  	public void setShipment_method(String shipment_method) {
		this.shipment_method = shipment_method;
	}
  	public String getFormCd(){
  		return formCd;
  	}
  	public void setFormCd(String formCd) {
		this.formCd = formCd;
	}
  	public String getToCd(){
  		return toCd;
  	}
  	public void setToCd(String toCd) {
		this.toCd = toCd;
	}
  	public String getFormName(){
  		return formName;
  	}
  	public void setFormName(String formName) {
		this.formName = formName;
	}
  	public String getFormlikeName(){
  		return formlikeName;
  	}
  	public void setFormlikeName(String formlikeName) {
		this.formlikeName = formlikeName;
	}
  	public String getFormPhone(){
  		return formPhone;
  	}
  	public void setFormPhone(String formPhone) {
		this.formPhone = formPhone;
	}
  	public String getFormAddr(){
  		return formAddr;
  	}
  	public void setFormAddr(String formAddr) {
		this.formAddr = formAddr;
	}
  	public String getToName(){
  		return toName;
  	}
  	public void setToName(String toName) {
		this.toName = toName;
	}
  	public String getTolikeName(){
  		return tolikeName;
  	}
  	public void setTolikeName(String tolikeName) {
		this.tolikeName = tolikeName;
	}
  	public String getToPhone(){
  		return toPhone;
  	}
  	public void setToPhone(String toPhone) {
		this.toPhone = toPhone;
	}
  	public String getToAddr(){
  		return toAddr;
  	}
  	public void setToAddr(String toAddr) {
		this.toAddr = toAddr;
	}
  	public Long getShipmentId(){
  		return shipmentId;
  	}
  	public String getVocationName() {
		return vocationName;
	}
	public void setVocationName(String vocationName) {
		this.vocationName = vocationName;
	}
	public String getVocationType() {
		return vocationType;
	}
	public void setVocationType(String vocationType) {
		this.vocationType = vocationType;
	}
	public void setShipmentId(Long shipmentId) {
		this.shipmentId = shipmentId;
	}
  	public String getShipmentType(){
  		return shipmentType;
  	}
  	public void setShipmentType(String shipmentType) {
		this.shipmentType = shipmentType;
	}
  	public Double getQuantity(){
  		return quantity;
  	}
  	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
  	public Double getWeight(){
  		return weight;
  	}
  	public void setWeight(Double weight) {
		this.weight = weight;
	}
  	public Double getVolume(){
  		return volume;
  	}
  	public void setVolume(Double volume) {
		this.volume = volume;
	}
  	public Double getPalletsum(){
  		return palletsum;
  	}
  	public void setPalletsum(Double palletsum) {
		this.palletsum = palletsum;
	}
  	public Double getExpense(){
  		return expense;
  	}
  	public void setExpense(Double expense) {
		this.expense = expense;
	}
  	public String getFtranslocationName() {
		return ftranslocationName;
	}
	public void setFtranslocationName(String ftranslocationName) {
		this.ftranslocationName = ftranslocationName;
	}
	public String getTtranslocationName() {
		return ttranslocationName;
	}
	public void setTtranslocationName(String ttranslocationName) {
		this.ttranslocationName = ttranslocationName;
	}
	public String getShipmentCd() {
		return shipmentCd;
	}
	public String getVehicleCd() {
		return vehicleCd;
	}
	public void setVehicleCd(String vehicleCd) {
		this.vehicleCd = vehicleCd;
	}
	public void setShipmentCd(String shipmentCd) {
		this.shipmentCd = shipmentCd;
	}
	
	public String getShipmentExpese() {
		return shipmentExpese;
	}
	public void setShipmentExpese(String shipmentExpese) {
		this.shipmentExpese = shipmentExpese;
	}
	public Integer getBag(){
  		return bag;
  	}
  	public void setBag(Integer bag) {
		this.bag = bag;
	}
  	public Date getPcTime() {
		return pcTime;
	}
	public void setPcTime(Date pcTime) {
		this.pcTime = pcTime;
	}
	public Date getRkTime() {
		return rkTime;
	}
	public void setRkTime(Date rkTime) {
		this.rkTime = rkTime;
	}
	public Date getCkTime() {
		return ckTime;
	}
	public void setCkTime(Date ckTime) {
		this.ckTime = ckTime;
	}
	public Date getOrdertime(){
  		return ordertime;
  	}
  	public void setOrdertime(Date ordertime) {
		this.ordertime = ordertime;
	}
  	public Date getBilltime(){
  		return billtime;
  	}
  	public void setBilltime(Date billtime) {
		this.billtime = billtime;
	}
  	public Date getPlanltime(){
  		return planltime;
  	}
  	public void setPlanltime(Date planltime) {
		this.planltime = planltime;
	}
  	public Date getPlanatime() {
		return planatime;
	}
	public void setPlanatime(Date planatime) {
		this.planatime = planatime;
	}
	public Date getLeavetime(){
  		return leavetime;
  	}
  	public void setLeavetime(Date leavetime) {
		this.leavetime = leavetime;
	}
  	public String getArrivetime(){
  		return arrivetime;
  	}
  	public void setArrivetime(String arrivetime) {
		this.arrivetime = arrivetime;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
 	
 	public OrderHead getOrderHead() {
		return orderHead;
	}
	public void setOrderHead(OrderHead orderHead) {
		this.orderHead = orderHead;
	}
	public Long getTempshipmentId() {
		return tempshipmentId;
	}
	public void setTempshipmentId(Long tempshipmentId) {
		this.tempshipmentId = tempshipmentId;
	}
	
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getStName() {
		return stName;
	}
	public void setStName(String stName) {
		this.stName = stName;
	}
	public String getPlatformName() {
		return platformName;
	}
	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getConstractorName() {
		return constractorName;
	}
	public void setConstractorName(String constractorName) {
		this.constractorName = constractorName;
	}
	public String getVehicleName() {
		return vehicleName;
	}
	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}
	public String getShipment_methodName() {
		return shipment_methodName;
	}
	public void setShipment_methodName(String shipment_methodName) {
		this.shipment_methodName = shipment_methodName;
	}
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public Long getToId() {
		return toId;
	}
	public void setToId(Long toId) {
		this.toId = toId;
	}
	public Long getFtranslocationId() {
		return ftranslocationId;
	}
	public void setFtranslocationId(Long ftranslocationId) {
		this.ftranslocationId = ftranslocationId;
	}
	public Long getTtranslocationId() {
		return ttranslocationId;
	}
	public void setTtranslocationId(Long ttranslocationId) {
		this.ttranslocationId = ttranslocationId;
	}
	public static Legs newTest() {
		Legs legs = new Legs();
		
		//legs.setOrderId("");
		//legs.setOrderCd("");
		//legs.setLegs_no("");
		//legs.setStatus("");
		//legs.setSt("");
		//legs.setPlatformId("");
		//legs.setClientId("");
		//legs.setConstractorId("");
		//legs.setVehicleId("");
		//legs.setShipment_method("");
		//legs.setFormCd("");
		//legs.setToCd("");
		//legs.setFormName("");
		//legs.setFormlikeName("");
		//legs.setFormPhone("");
		//legs.setFormAddr("");
		//legs.setToName("");
		//legs.setTolikeName("");
		//legs.setToPhone("");
		//legs.setToAddr("");
		//legs.setShipmentId("");
		//legs.setShipmentType("");
		//legs.setQuantity("");
		//legs.setWeight("");
		//legs.setVolume("");
		//legs.setPalletsum("");
		//legs.setExpense("");
		//legs.setBag("");
		//legs.setOrdertime("");
		//legs.setBilltime("");
		//legs.setPlanltime("");
		//legs.setLeavetime("");
		//legs.setArrivetime("");
		
		
		
		
		return legs;
	}
}


