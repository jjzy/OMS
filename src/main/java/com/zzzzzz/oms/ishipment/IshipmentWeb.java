package com.zzzzzz.oms.ishipment;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
@Controller
public class IshipmentWeb {
	@RequestMapping(value = "/ishipment/list", method = RequestMethod.GET)
	public String list() {
		return "oms/ishipment";
	}
}
