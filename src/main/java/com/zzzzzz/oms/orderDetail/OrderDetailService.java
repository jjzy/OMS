package com.zzzzzz.oms.orderDetail;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.utils.BeanValidators;
import com.zzzzzz.utils.DateTimeUtils;

@Service
public class OrderDetailService {
	@Resource
	public OrderDetailDao orderDetailDao;
	@Resource
	public Validator validator;
	
	@Transactional
	public int save(OrderDetail orderDetail, I i) {
		orderDetail.setUpdDt(new Date());
		orderDetail.setUpdBy(i.getId());
		if (orderDetail.getId() == null) {
			orderDetail.setAddDt(new Date());
			orderDetail.setAddBy(i.getId());
			orderDetailDao.add(orderDetail);
		} else {
			orderDetailDao.updById(orderDetail);
		}

		return 1;
	}
	
	@Transactional
	public BaseData batchAdd(List<OrderDetail> list, I i) {
		logger.info("orderDetail batchAdd validate start:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		//start validate
		int errTt = 0;
		StringBuilder errMsg = new StringBuilder();
		for (OrderDetail orderDetail : list) {
			try {
				BeanValidators.validateWithException(validator, orderDetail);
			} catch (ConstraintViolationException e) {
				List<String> msgList = BeanValidators.extractPropertyAndMessageAsList(e, ": ");
				for (String msg : msgList) {
					errTt++;
					errMsg.append("<br/>").append(msg);
				}
			} catch (Exception e) {
				logger.warn("", e);
				errTt++;
				errMsg.append("<br/>验证失败：").append(e.getMessage());
			}
		}
		if(errTt > 0){
			errMsg.insert(0, "共有"+errTt+"笔记录验证失败：");
			return new BaseData("101", errMsg.toString());
		}
		logger.info("orderDetail batchAdd validate end:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		logger.info("orderDetail batchAdd save start:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		//state save
		int susTt = 0;
		for (OrderDetail orderDetail : list) {
			save(orderDetail, i);
			susTt++;
		}
		logger.info("orderDetail batchAdd save end:" + DateTimeUtils.date2Str(new Date(), DateTimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS));
		return new BaseData("0", "共有"+susTt+"笔记录导入成功。");
	}

	private static final Logger logger = LoggerFactory.getLogger(OrderDetail.class);
}
