package com.zzzzzz.oms.orderDetail;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.utils.page.Page;
import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class OrderDetailDao {
	
	@Resource
	private BaseDao baseDao;
			
	private final static String sql_add = "insert into t_order_detail(orderId, productId, lineno, lot, unit, quantity, weight, volume, addDt) values(:orderId, :productId, :lineno, :lot, :unit, :quantity, :weight, :volume, :addDt)";
	private final static String sql_updStByIds = "update t_order_detail set, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update t_order_detail set orderId=:orderId, productId=:productId, lineno=:lineno, lot=:lot, unit=:unit, quantity=:quantity, weight=:weight, volume=:volume, updDt=:updDt, updBy=:updBy where id = :id";
	private final static String sql_findById = "select id, orderId, productId, lineno, lot, unit, quantity, weight, volume, addDt, addBy, updDt, updBy from t_order_detail where id = :id";
	
	public Long add(OrderDetail orderDetail){
		return baseDao.updateGetLongKey(sql_add, orderDetail);
	}
	public int updStByIds(List<Long> ids, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}		  
	public int updById(OrderDetail orderDetail){
		return baseDao.update(sql_upd, orderDetail);
	}
	public OrderDetail findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, OrderDetail.class);
	}
	/**
	 * 动态查询
	 */
	public List<OrderDetail> findListBy(Page page,Map<String, Object> ffMap,I i){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append("count(1) as tt");
		finder.append("  from t_order_detail d inner join t_order_head o on d.orderId=o.id inner join t_product p on d.productId=p.id inner join t_order_type t on o.orderTypeId=t.id inner join t_client c on o.clientId=c.id left join t_product_type tt on p.producttypeId=tt.id inner join sys_dict dd on dd.descr=o.status inner join t_trans_location l on l.id=o.ftranslocationId inner join t_trans_location ll on ll.id=o.ttranslocationId inner join t_receiver f on f.id=o.freceiverId inner join t_receiver tr on tr.id=o.treceiverId left join t_user_client u on u.clientId=o.clientId");
		finder.append(" where 1=1");
		finder.append("and o.platformId=:platformId").setParam("platformId", i.getPlatformId());
		finder.append("and u.userId=:userId").setParam("userId", i.getId());
		if(DaoUtils.isNotNull(ffMap.get("clientId"))){
			finder.append(" and o.clientId = :clientId").setParam("clientId", ffMap.get("clientId"));
		}
		if(DaoUtils.isNotNull(ffMap.get("orderCd"))){
			finder.append(" and o.cd = :orderCd").setParam("orderCd", ffMap.get("orderCd"));
		}
		if(DaoUtils.isNotNull(ffMap.get("productCd"))){
			finder.append(" and p.cd = :productCd").setParam("productCd", ffMap.get("productCd"));
		}
		if(DaoUtils.isNotNull(ffMap.get("status"))){
			finder.append(" and o.status = :status").setParam("status", ffMap.get("status"));
		}
		if(DaoUtils.isNotNull(ffMap.get("orderId"))){
			finder.append(" and d.orderId = :orderId").setParam("orderId", ffMap.get("orderId"));
			finder.append("order by d.id asc");
		}else{
			finder.append("order by d.id desc");
		}
		if (page != null) {
			Integer total = baseDao.findInt(finder);
			page.setTotal(total);
			finder.limit(page.getStart(), page.getOffset());
		}

		String sql = finder
				.getSql()
				.replace(
						"count(1) as tt",
						"d.*,t.name as typeName,c.name as clientName,tt.name as productTypeName,dd.val as statusName,o.cd as orderCd,l.name as ftranslocationName,o.orderDt,o.billingDt,o.freceiverName,o.freceiverId,o.treceiverId,o.treceiverName,ll.name as ttranslocationName,o.treceiverAddress,o.planaDt,o.planlDt,p.cd as productCd,p.name as productName,f.cd as freceiverCd,tr.cd as treceiverCd");
		finder.setSql(sql);
		
		List<OrderDetail> list = baseDao.findList(finder, OrderDetail.class);
				
		return list;
	}
	/*
	 * 批量出入数据
	 */
	public void saveDetail(List<OrderDetail> list){
		baseDao.batchUpdate(sql_add, list);
	}
}
