package com.zzzzzz.oms.orderDetail;

import java.util.List;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.utils.page.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzzzzz.oms.GCS;
import com.zzzzzz.oms.orderHead.OrderHead;
import com.google.common.collect.Lists;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.plugins.poi.ExportExcel;
import com.zzzzzz.plugins.poi.ImportExcel;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class OrderDetailWeb {

	@Resource
	public OrderDetailService orderDetailService;
	@Resource
	public OrderDetailDao orderDetailDao;

	@RequestMapping(value = "/orderDetail/list", method = RequestMethod.GET)
	public String list() {
		return "oms/orderDetail";
	}

	@RequestMapping(value = "/orderDetail/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid OrderDetail orderDetail, BindingResult bindingResult) throws Exception {
	
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}

		orderDetailService.save(orderDetail, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/orderDetail/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		orderDetailDao.updStByIds(ids, ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	
	
	@RequestMapping(value = "/orderDetail/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public BaseData findById(@PathVariable Long id) {
		OrderDetail orderDetail = orderDetailDao.findById(id);
		return new BaseData(orderDetail, null);
	}
	

	@RequestMapping(value = "/orderDetail/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		Page page = baseQueryForm.getPage();
		List<OrderDetail> list = orderDetailDao.findListBy(page, baseQueryForm.getFfMap(),ShiroUtils.findUser());
		return new BaseData(list, page.getTotal());
	}
	
	
	@RequestMapping(value = "/orderDetail/import", method=RequestMethod.POST)
    public String importExcel(@RequestParam MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			ImportExcel importExcel = new ImportExcel(file, 1, 0);
			List<OrderDetail> list = importExcel.getDataList(OrderDetail.class);
			
			BaseData baseData = orderDetailService.batchAdd(list, ShiroUtils.findUser());
			redirectAttributes.addFlashAttribute("msg", baseData.getErrMsg());
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "导入数据保存失败。");
			logger.warn("importExcel", e);
		}
		return "redirect:/msg";
    }
    
    @RequestMapping(value = "orderDetail/import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "订单明细数据导入模板.xlsx";
    		List<OrderDetail> list = Lists.newArrayList();
    		list.add(OrderDetail.newTest());
    		new ExportExcel("订单明细数据导入模板", OrderDetail.class, 2).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "订单明细数据导入模板下载失败。");
			logger.warn("importFileTemplate", e);
		}
		return "redirect:/msg";
    }
    
    private static final Logger logger = LoggerFactory.getLogger(OrderDetailWeb.class);
}
