package com.zzzzzz.oms.orderDetail;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class OrderDetail implements Serializable {
  	private Long id;
  	private Long orderId;
  	private String orderCd;
  	private String typeName;
  	private String clientName;
  	private String ftranslocationName;
  	private String freceiverName;
  	private String freceiverCd;
  	private String treceiverCd;
  	private String treceiverName;
  	private String ttranslocationName;
  	private String treceiverAddress;
  	private Date orderDt;
  	private Date planaDt;
  	private Date billingDt;
  	private Date planlDt;
  	private String productName;
  	private String productTypeName;
  	private Long productId;
  	private String productCd;
	@ExcelField(title = "行号", align = 2, sort = 30)
	@Length(min = 1, max = 20)
  	private Long lineno;
	@ExcelField(title = "批号", align = 2, sort = 40)
	@Length(min = 1, max = 100)
  	private String lot;
	@ExcelField(title = "包装单位", align = 2, sort = 40)
	@Length(min = 1, max = 100)
  	private String unit;
	@ExcelField(title = "数量", align = 2, sort = 20)
	@Length(min = 1, max = 20)
  	private Double quantity;
	@ExcelField(title = "重量", align = 2, sort = 20)
	@Length(min = 1, max = 20)
  	private Double weight;
	@ExcelField(title = "体积", align = 2, sort = 20)
	@Length(min = 1, max = 20)
  	private Double volume;
	private String clientCd;
	
  	private Date addDt;
	
	
  	private Long addBy;
	
	
  	private Date updDt;
	
	
  	private Long updBy;
	@ExcelField(title = "状态", align = 2, sort = 130)
	
  	private String statusName;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	
	public String getOrderCd() {
		return orderCd;
	}
	public void setOrderCd(String orderCd) {
		this.orderCd = orderCd;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getProductCd() {
		return productCd;
	}
	
	public void setProductCd(String productCd) {
		this.productCd = productCd;
	}
	public Long getLineno(){
  		return lineno;
  	}
  	public void setLineno(Long lineno) {
		this.lineno = lineno;
	}
  	public String getLot(){
  		return lot;
  	}
  	public void setLot(String lot) {
		this.lot = lot;
	}
  	public String getUnit(){
  		return unit;
  	}
  	public void setUnit(String unit) {
		this.unit = unit;
	}
  	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public Double getVolume() {
		return volume;
	}
	public void setVolume(Double volume) {
		this.volume = volume;
	}
	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
  	public String getStatusName(){
  		return statusName;
  	}
  	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
 	
 	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	
	public String getFreceiverName() {
		return freceiverName;
	}
	public void setFreceiverName(String freceiverName) {
		this.freceiverName = freceiverName;
	}
	public String getFreceiverCd() {
		return freceiverCd;
	}
	public void setFreceiverCd(String freceiverCd) {
		this.freceiverCd = freceiverCd;
	}
	public String getTreceiverCd() {
		return treceiverCd;
	}
	public void setTreceiverCd(String treceiverCd) {
		this.treceiverCd = treceiverCd;
	}
	public String getTreceiverName() {
		return treceiverName;
	}
	public void setTreceiverName(String treceiverName) {
		this.treceiverName = treceiverName;
	}
	
	public String getFtranslocationName() {
		return ftranslocationName;
	}
	public void setFtranslocationName(String ftranslocationName) {
		this.ftranslocationName = ftranslocationName;
	}
	public String getTtranslocationName() {
		return ttranslocationName;
	}
	public void setTtranslocationName(String ttranslocationName) {
		this.ttranslocationName = ttranslocationName;
	}
	public String getTreceiverAddress() {
		return treceiverAddress;
	}
	public void setTreceiverAddress(String treceiverAddress) {
		this.treceiverAddress = treceiverAddress;
	}
	public Date getOrderDt() {
		return orderDt;
	}
	public void setOrderDt(Date orderDt) {
		this.orderDt = orderDt;
	}
	public Date getPlanaDt() {
		return planaDt;
	}
	public void setPlanaDt(Date planaDt) {
		this.planaDt = planaDt;
	}
	public Date getPlanlDt() {
		return planlDt;
	}
	public void setPlanlDt(Date planlDt) {
		this.planlDt = planlDt;
	}
	
	public Date getBillingDt() {
		return billingDt;
	}
	public void setBillingDt(Date billingDt) {
		this.billingDt = billingDt;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductTypeName() {
		return productTypeName;
	}
	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}
	
	
	public String getClientCd() {
		return clientCd;
	}
	public void setClientCd(String clientCd) {
		this.clientCd = clientCd;
	}
	public static OrderDetail newTest() {
		OrderDetail orderDetail = new OrderDetail();
		
		
		
		//orderDetail.setLineno("");
		//orderDetail.setLot("");
		//orderDetail.setUnit("");
		//orderDetail.setQuantity("");
		//orderDetail.setWeight("");
		//orderDetail.setVolume("");
		
		
		
		
		//orderDetail.setSt("");
		return orderDetail;
	}
}


