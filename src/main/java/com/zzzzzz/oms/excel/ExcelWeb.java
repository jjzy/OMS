package com.zzzzzz.oms.excel;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zzzzzz.oms.HomeWeb;
import com.zzzzzz.sys.dict.DictDao;

@Controller
public class ExcelWeb {
	
	@Resource
	private DictDao dictDao;
	
	@RequestMapping(value = "/excel/import", method=RequestMethod.GET)
	public String excelImport(Model model) {
		return "/test/excel/import";
    }
	
	
	private static final Logger logger = LoggerFactory.getLogger(ExcelWeb.class);
}
