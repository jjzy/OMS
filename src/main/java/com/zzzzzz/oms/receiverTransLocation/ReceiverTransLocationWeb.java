package com.zzzzzz.oms.receiverTransLocation;

import java.util.List;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zzzzzz.core.datagrid.BaseData;
import com.zzzzzz.core.datagrid.BaseQueryForm;
import com.zzzzzz.utils.page.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzzzzz.oms.GCS;
import com.google.common.collect.Lists;
import com.zzzzzz.plugins.shiro.I;
import com.zzzzzz.plugins.shiro.ShiroUtils;
import com.zzzzzz.plugins.poi.ExportExcel;
import com.zzzzzz.plugins.poi.ImportExcel;

/**
 * @author hing
 * @version 1.0.0
 */
@Controller
public class ReceiverTransLocationWeb {

	@Resource
	public ReceiverTransLocationService receiverTransLocationService;
	@Resource
	public ReceiverTransLocationDao receiverTransLocationDao;

	@RequestMapping(value = "/receiverTransLocation/list", method = RequestMethod.GET)
	public String list() {
		return "oms/receiverTransLocation";
	}

	@RequestMapping(value = "/receiverTransLocation/save", method = RequestMethod.POST)
	@ResponseBody
	public BaseData save(@RequestBody @Valid ReceiverTransLocation receiverTransLocation, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return new BaseData(GCS.ERRCD_PARAM);
		}

		receiverTransLocationService.save(receiverTransLocation, ShiroUtils.findUser());
		return new BaseData("0");
	}
	
	@RequestMapping(value = "/receiverTransLocation/updStByIds", method = RequestMethod.POST)
	@ResponseBody
	public BaseData updStByIds(@RequestParam(value = "ids", required = true) List<Long> ids, @RequestParam(value = "st", required = true) Integer st) throws Exception {
		receiverTransLocationDao.updStByIds(ids, st, ShiroUtils.findUser());
		return new BaseData("0");
	}	  
	
	
	@RequestMapping(value = "/receiverTransLocation/id/{id}", method = RequestMethod.GET)
	@ResponseBody 
	public ReceiverTransLocation findById(@PathVariable Long id) {
		ReceiverTransLocation receiverTransLocation = receiverTransLocationDao.findById(id);
		return receiverTransLocation;
	}
	

	@RequestMapping(value = "/receiverTransLocation/findListBy", method = RequestMethod.POST)
	@ResponseBody
	public BaseData findListBy(@RequestBody BaseQueryForm baseQueryForm) {
		List<ReceiverTransLocation> list = receiverTransLocationDao.findListBy(baseQueryForm.getFfMap());
		return new BaseData(list, null);
	}
	
	
	@RequestMapping(value = "/receiverTransLocation/import", method=RequestMethod.POST)
    public String importExcel(@RequestParam MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			ImportExcel importExcel = new ImportExcel(file, 1, 0);
			List<ReceiverTransLocation> list = importExcel.getDataList(ReceiverTransLocation.class);
			
			BaseData baseData = receiverTransLocationService.batchAdd(list, ShiroUtils.findUser());
			redirectAttributes.addFlashAttribute("msg", baseData.getErrMsg());
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "导入数据保存失败。");
			logger.warn("importExcel", e);
		}
		return "redirect:/msg";
    }
    
    @RequestMapping(value = "receiverTransLocation/import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "收发货方与运输地表导入模板.xlsx";
    		List<ReceiverTransLocation> list = Lists.newArrayList();
    		list.add(ReceiverTransLocation.newTest());
    		new ExportExcel("收发货方与运输地表导入模板", ReceiverTransLocation.class, 2).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("msg", "收发货方与运输地表导入模板下载失败。");
			logger.warn("importFileTemplate", e);
		}
		return "redirect:/msg";
    }
    
    private static final Logger logger = LoggerFactory.getLogger(ReceiverTransLocationWeb.class);
}
