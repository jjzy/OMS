package com.zzzzzz.oms.receiverTransLocation;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import javax.annotation.Resource;
import org.springframework.stereotype.Repository;
import com.zzzzzz.plugins.jdbctemplate.BaseDao;
import com.zzzzzz.plugins.jdbctemplate.Finder;
import com.zzzzzz.plugins.jdbctemplate.DaoUtils;
import com.zzzzzz.utils.page.Page;

import com.zzzzzz.plugins.shiro.I;

/**
 * @author hing
 * @version 1.0.0
 */
@Repository
public class ReceiverTransLocationDao {
	
	@Resource
	private BaseDao baseDao;
			
	private final static String sql_add = "insert into t_receiver_trans_location(receiverCd, translocationCd, operatorCd, operatorName, addDt, addBy, st) values(:receiverCd, :translocationCd, :operatorCd, :operatorName, :addDt, :addBy, :st)";
	private final static String sql_updStByIds = "update t_receiver_trans_location set st = :st, updDt = :updDt, updBy = :updBy where id in (:ids)";
	private final static String sql_upd = "update t_receiver_trans_location set receiverCd=:receiverCd, translocationCd=:translocationCd, operatorCd=:operatorCd, operatorName=:operatorName, updDt=:updDt, updBy=:updBy, st=:st where id = :id";
	private final static String sql_findById = "select id, receiverCd, translocationCd, operatorCd, operatorName, updDt, addDt, addBy, updBy, st from t_receiver_trans_location where id = :id";
	
	public Long add(ReceiverTransLocation receiverTransLocation){
		return baseDao.updateGetLongKey(sql_add, receiverTransLocation);
	}
	public int updStByIds(List<Long> ids, Integer st, I i){
		Finder finder = new Finder(sql_updStByIds).setParam("ids", ids).setParam("st", st)
		.setParam("updDt", new Date()).setParam("updBy", i.getId());
		return baseDao.update(finder);
	}		  
	public int updById(ReceiverTransLocation receiverTransLocation){
		return baseDao.update(sql_upd, receiverTransLocation);
	}
	public ReceiverTransLocation findById(Long id){
		Finder finder = new Finder(sql_findById).setParam("id", id);
		return baseDao.findOne(finder, ReceiverTransLocation.class);
	}
	
	/**
	 * 动态查询
	 */
	public List<ReceiverTransLocation> findListBy(Map<String, Object> ffMap){
		Finder finder = new Finder("");
		finder.append(" select");
		finder.append(" *");
		finder.append(" from t_receiver_trans_location");
		finder.append(" where 1=1");
		if(DaoUtils.isNotNull(ffMap.get("receiverCd"))){
		finder.append(" and receiverCd = :receiverCd").setParam("receiverCd", ffMap.get("receiverCd"));
		}
				
		List<ReceiverTransLocation> list = baseDao.findList(finder, ReceiverTransLocation.class);
				
		return list;
	}
}
