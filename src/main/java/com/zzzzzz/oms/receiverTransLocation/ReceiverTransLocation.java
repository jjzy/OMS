package com.zzzzzz.oms.receiverTransLocation;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.Length;
import com.zzzzzz.plugins.poi.ExcelField;

/**
 * @author hing
 * @version 1.0.0
 */
public class ReceiverTransLocation implements Serializable {
	
	
	
  	private Long id;
	@ExcelField(title = "收发货方标识", align = 2, sort = 20)
	@Length(min = 1, max = 20)
  	private String receiverCd;
	@ExcelField(title = "出发地标识", align = 2, sort = 20)
	@Length(min = 1, max = 20)
  	private String translocationCd;
	@ExcelField(title = "用户标识", align = 2, sort = 20)
	@Length(min = 1, max = 20)
  	private String operatorCd;
	@ExcelField(title = "客户名称", align = 2, sort = 20)
	@Length(min = 1, max = 20)
  	private String operatorName;
	
	
  	private Date updDt;
	
	
  	private Date addDt;
	
	
  	private Long addBy;
	
	
  	private Long updBy;
	@ExcelField(title = "状态", align = 2, sort = 130)
	
  	private Integer st;
	
  	public Long getId(){
  		return id;
  	}
  	public void setId(Long id) {
		this.id = id;
	}
  	public String getReceiverCd(){
  		return receiverCd;
  	}
  	public void setReceiverCd(String receiverCd) {
		this.receiverCd = receiverCd;
	}
  	public String getTranslocationCd(){
  		return translocationCd;
  	}
  	public void setTranslocationCd(String translocationCd) {
		this.translocationCd = translocationCd;
	}
  	public String getOperatorCd(){
  		return operatorCd;
  	}
  	public void setOperatorCd(String operatorCd) {
		this.operatorCd = operatorCd;
	}
  	public String getOperatorName(){
  		return operatorName;
  	}
  	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
  	public Date getUpdDt(){
  		return updDt;
  	}
  	public void setUpdDt(Date updDt) {
		this.updDt = updDt;
	}
  	public Date getAddDt(){
  		return addDt;
  	}
  	public void setAddDt(Date addDt) {
		this.addDt = addDt;
	}
  	public Long getAddBy(){
  		return addBy;
  	}
  	public void setAddBy(Long addBy) {
		this.addBy = addBy;
	}
  	public Long getUpdBy(){
  		return updBy;
  	}
  	public void setUpdBy(Long updBy) {
		this.updBy = updBy;
	}
  	public Integer getSt(){
  		return st;
  	}
  	public void setSt(Integer st) {
		this.st = st;
	}
 	
 	public static ReceiverTransLocation newTest() {
		ReceiverTransLocation receiverTransLocation = new ReceiverTransLocation();
		
		//receiverTransLocation.setReceiverCd("");
		//receiverTransLocation.setTranslocationCd("");
		//receiverTransLocation.setOperatorCd("");
		//receiverTransLocation.setOperatorName("");
		
		
		
		
		//receiverTransLocation.setSt("");
		return receiverTransLocation;
	}
}


