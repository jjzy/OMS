CREATE SCHEMA `oms` DEFAULT CHARACTER SET utf8 ;

use oms;

CREATE TABLE `t_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT, 
  `account` varchar(40) NOT NULL COMMENT '账号',
  `password` varchar(200) NOT NULL COMMENT '密码',
  `name` varchar(30) NOT NULL COMMENT '姓名',
  `remark` varchar(2000) DEFAULT NULL COMMENT '描述',
  `state` int(3) DEFAULT 0 COMMENT '状态',
  `home_page` VARCHAR(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

insert into t_user(name, account, password, remark) values('系统管理员', 'admin', '$shiro1$SHA-512$1024$dJE1Dtii/suz03aGVPGS4Q==$ISj4TWoJOE2/rXp1e7v5surcSF/SRKGkKxnR0CPUEHPyJlliIJeEO/zOouM5TyuNEvrOj5xOZgdU6fcqc/WnAw==
', '系统管理员');
insert into t_user(name, account, password, remark) values('张三', 'test1', '$shiro1$SHA-512$1024$dJE1Dtii/suz03aGVPGS4Q==$ISj4TWoJOE2/rXp1e7v5surcSF/SRKGkKxnR0CPUEHPyJlliIJeEO/zOouM5TyuNEvrOj5xOZgdU6fcqc/WnAw==
', '用户');
insert into t_user(name, account, password, remark) values('李四', 'test2', '$shiro1$SHA-512$1024$dJE1Dtii/suz03aGVPGS4Q==$ISj4TWoJOE2/rXp1e7v5surcSF/SRKGkKxnR0CPUEHPyJlliIJeEO/zOouM5TyuNEvrOj5xOZgdU6fcqc/WnAw==
', '用户');
insert into t_user(name, account, password, remark) values('陈大文', 'test3', '$shiro1$SHA-512$1024$dJE1Dtii/suz03aGVPGS4Q==$ISj4TWoJOE2/rXp1e7v5surcSF/SRKGkKxnR0CPUEHPyJlliIJeEO/zOouM5TyuNEvrOj5xOZgdU6fcqc/WnAw==
', '用户');


CREATE TABLE `t_org` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT, 
  `name` varchar(60) NOT NULL COMMENT '名称',
  `cd` varchar(40) DEFAULT NULL COMMENT '代码',
  `sortNb` int(11) DEFAULT NULL COMMENT '排序号',
  `remark` varchar(2000) DEFAULT NULL COMMENT '描述',
  `state` int(3) DEFAULT 0 COMMENT '状态',
  `home_page` VARCHAR(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门';

insert into t_org(name, cd, sortNb, remark) values('仓库', 'ST', 0, '');
insert into t_org(name, cd, sortNb, remark) values('财务', 'FI', 1, '');
insert into t_org(name, cd, sortNb, remark) values('技术', 'IT', 2, '');
insert into t_org(name, cd, sortNb, remark) values('管理层', 'ET', 3, '');


CREATE TABLE `t_user_org` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT, 
  `userId` bigint(20) NOT NULL COMMENT '用户编号',
  `orgId` bigint(20) NOT NULL COMMENT '机构编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户部门中间表';

insert into t_user_org(userId, orgId) values(1, 1);
insert into t_user_org(userId, orgId) values(2, 2);
insert into t_user_org(userId, orgId) values(3, 3);
insert into t_user_org(userId, orgId) values(4, 4);


CREATE TABLE `t_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL COMMENT '名称',
  `pid` bigint(20) DEFAULT NULL COMMENT '父ID',
  `remark` varchar(2000) DEFAULT NULL COMMENT '描述',
  `url` varchar(3000) DEFAULT NULL,
  `typ` int(11) DEFAULT NULL COMMENT '0.功能按钮,1.导航菜单',
  `sortNb` double DEFAULT NULL COMMENT '排序号',
  `state` int(3) DEFAULT '0' COMMENT '状态',
  `icon` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单';


insert into t_menu(name, remark, url, typ, sortNb) values('仓库报表', '', '',1, 1);
insert into t_menu(name, remark, url, typ, sortNb) values('运输报表', '', '',1, 2);
insert into t_menu(name, remark, url, typ, sortNb) values('财务报表', '', '',1, 3);
insert into t_menu(name, remark, url, typ, sortNb) values('系统管理', '', '',1, 4);

insert into t_menu(name, pid, remark, url, typ) values('MK即时库存', 1, '', '',1);
insert into t_menu(name, pid, remark, url, typ) values('延迟发车', 2, '', '',1);
insert into t_menu(name, pid, remark, url, typ) values('承运商财务科目对照', 3, '', '',1);


CREATE TABLE `t_user_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT, 
  `userId` bigint(20) NOT NULL COMMENT '用户编号',
  `menuId` bigint(20) NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户菜单中间表';

insert into t_user_menu(userId, menuId) values(1, 1);
insert into t_user_menu(userId, menuId) values(1, 2);
insert into t_user_menu(userId, menuId) values(1, 3);
insert into t_user_menu(userId, menuId) values(1, 4);
insert into t_user_menu(userId, menuId) values(1, 5);
insert into t_user_menu(userId, menuId) values(1, 6);
insert into t_user_menu(userId, menuId) values(1, 7);


CREATE TABLE `t_client` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT, 
  `name` varchar(30) NOT NULL COMMENT '名称',
  `cd` varchar(40) DEFAULT NULL COMMENT '代号',
  `wms` varchar(40) DEFAULT NULL COMMENT '',
  `company` varchar(200) DEFAULT NULL COMMENT '公司',
  `state` int(3) DEFAULT 0 COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户';

insert into t_client(name, cd, wms, company) values('苹果', 'apple', 'INFOR', '');
insert into t_client(name, cd, wms, company) values('三星', 'samsung', 'INFOR', '');


CREATE TABLE `t_user_client` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT, 
  `userId` bigint(20) NOT NULL COMMENT '用户编号',
  `clientId` bigint(20) NOT NULL COMMENT '客户编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户客户中间表';



CREATE  TABLE `oms`.`t_sample_order` (
  `id` BIGINT NOT NULL AUTO_INCREMENT ,
  `orderId` BIGINT NULL ,
  `typId` INT NULL ,
  `name` VARCHAR(45) NULL ,
  `val` DOUBLE NULL ,
  `orderDt` TIMESTAMP NULL ,
  `addDt` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
DEFAULT CHARACTER SET = utf8 COMMENT='測試上傳EXCEL';