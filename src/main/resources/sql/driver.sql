CREATE TABLE t_driver (
	id bigint not null AUTO_INCREMENT,
	cd varchar(20) not null,
	name varchar(45) not null,
	idcard varchar(45) not null,
	driverType varchar(45) not null,
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id), UNIQUE KEY cd_UNIQUE (cd)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='司机信息表';