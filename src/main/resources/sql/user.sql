CREATE TABLE sys_user (
	id bigint not null AUTO_INCREMENT,
	account varchar(100) not null,
	password varchar(200) not null,
	name varchar(100) not null,
	email varchar(100),
	homePage varchar(500),
	remark varchar(500),
	sortNb int not null comment '排序值',
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';
