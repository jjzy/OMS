CREATE TABLE sys_platform (
	id bigint not null AUTO_INCREMENT,
	cd varchar(100) not null,
	name varchar(100) not null,
	shortName varchar(100) not null,
	descr varchar(255),
	cityId bigint not null,
	linkMan varchar(100),
	phone varchar(100),
	fax varchar(100),
	email varchar(100),
	addr varchar(255),
	postCd varchar(100),
	sortNb int not null comment '排序值',
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台表';

CREATE TABLE sys_city (
	id bigint not null AUTO_INCREMENT,
	cd varchar(100) not null,
	name varchar(100) not null,
	province varchar(100) not null,
	descr varchar(255),
	sortNb int not null comment '排序值',
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='城市表';

CREATE TABLE sys_dict (
	id bigint not null AUTO_INCREMENT,
	cd varchar(100) not null comment '类型',
	lb varchar(100) not null comment '标签名',
	val varchar(100) not null comment '数据值',
	descr varchar(100) comment '描述',
	remark varchar(255) comment '备注',
	sortNb int not null comment '排序值',
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';

CREATE TABLE sys_menu (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  pid bigint(20) DEFAULT NULL,
  href varchar(255) DEFAULT NULL,
  target varchar(25) DEFAULT NULL,
  icon varchar(50) DEFAULT NULL,
  pemisi varchar(255) NOT NULL,
  ifShow int(1) NOT NULL,
  remark varchar(255) DEFAULT NULL COMMENT '备注',
  sortNb int(11) NOT NULL COMMENT '排序值',
  addDt datetime NOT NULL,
  addBy bigint(20) NOT NULL,
  updDt datetime DEFAULT NULL,
  updBy bigint(20) DEFAULT NULL,
  st int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (id),
  UNIQUE KEY pemisi_UNIQUE (pemisi)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

CREATE TABLE sys_platform_menu (
  platformId bigint NOT NULL,
  menuId bigint NOT NULL,
  PRIMARY KEY (platformId, menuId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台菜单表';

CREATE TABLE sys_role_menu (
  roleId bigint NOT NULL,
  menuId bigint NOT NULL,
  PRIMARY KEY (roleId, menuId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色菜单表';

CREATE TABLE sys_role (
	id bigint not null AUTO_INCREMENT,
	name varchar(100) not null,
	platformId bigint not null,
	remark varchar(255) comment '备注',
	sortNb int not null comment '排序值',
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

CREATE TABLE sys_user_role (
  userId bigint NOT NULL,
  roleId bigint NOT NULL,
  PRIMARY KEY (userId, roleId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色表';

CREATE TABLE sys_user (
	id bigint not null AUTO_INCREMENT,
	account varchar(100) not null,
	password varchar(200) not null,
	name varchar(100) not null,
	email varchar(100),
	homePage varchar(500),
	remark varchar(500),
	sortNb int not null comment '排序值',
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';
CREATE TABLE t_product_type (
	id bigint not null AUTO_INCREMENT,
	clientId bigint not null,
	cd varchar(20) not null,
	name varchar(100) not null,
	typ varchar(100) not null,
	descr varchar(255),
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id), UNIQUE KEY cd_UNIQUE (cd)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品类型表';

CREATE TABLE t_client (
	id bigint not null AUTO_INCREMENT,
	cd varchar(100) not null,
	name varchar(100) not null,
	shortName varchar(100) not null,
	platformId bigint not null,
	descr varchar(255),
	cityId bigint not null,
	linkMan varchar(100),
	phone varchar(100),
	fax varchar(100),
	email varchar(100),
	addr varchar(255),
	postCd varchar(100),
	sortNb int not null comment '排序值',
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id), UNIQUE KEY cd_UNIQUE (cd)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户表';

CREATE TABLE t_order_detail (
	id bigint not null AUTO_INCREMENT,
	orderId bigint not null,
	productId bigint not null,
	lineno varchar(20) not null,
	lot varchar(20) not null,
	unit varchar(10) not null,
	quantity varchar(20) not null,
	weight varchar(20),
	volume varchar(20),
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单明细表';

CREATE TABLE t_order_head (
	id bigint not null AUTO_INCREMENT,
	clientCd varchar(20) not null,
	platformCd varchar(20),
	cd varchar(20) not null,
	shipmentmethodCd varchar(50),
	ordertypeCd varchar(20),
	orderDt datetime,
	planlDt datetime,
	planaDt datetime,
	billingDt datetime,
	quantity DOUBLE,
	weight DOUBLE,
	volume DOUBLE,
	palletsum DOUBLE,
	expense DOUBLE,
	freceiverCd varchar(20) not null,
	freceiverName varchar(200),
	ftranslocationCd varchar(20) not null,
	freceiverLikename varchar(200),
	freceiverPhone varchar(200),
	freceiverFax varchar(200),
	freceiverEmail varchar(200),
	freceiverPostcode varchar(50),
	freceiverAddress varchar(255),
	treceiverCd varchar(20) not null,
	treceiverName varchar(200),
	ttranslocationCd varchar(20) not null,
	treceiverLikename varchar(200),
	treceiverPhone varchar(200),
	treceiverFax varchar(200),
	treceiverEmail varchar(200),
	treceiverPostcode varchar(50),
	treceiverAddress varchar(255),
	descr varchar(255),
	operatorCd varchar(20),
	operatorname varchar(20),
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	extendfield01 varchar(100),
	extendfield02 varchar(100),
	extendfield03 varchar(100),
	extendfield04 varchar(100),
	extendfield05 varchar(100),
	extendfield06 varchar(100),
	extendfield07 varchar(100),
	extendfield08 varchar(100),
	extendfield09 varchar(100),
	extendfield10 varchar(100),
	extendfield11 varchar(100),
	extendfield12 varchar(100),
	extendfield13 varchar(100),
	extendfield14 varchar(100),
	extendfield15 varchar(100),
	extendfield16 varchar(100),
	extendfield17 varchar(100),
	extendfield18 varchar(100),
	extendfield19 varchar(100),
	extendfield20 varchar(100),
	extendfield21 varchar(100),
	extendfield22 varchar(100),
	extendfield23 varchar(100),
	extendfield24 varchar(100),
	extendfield25 varchar(100),
	extendfield26 varchar(100),
	extendfield27 varchar(100),
	extendfield28 varchar(100),
	extendfield29 varchar(100),
	extendfield30 varchar(100),
	st int not null default 0,
	PRIMARY KEY (id), UNIQUE KEY cd_UNIQUE (cd)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';

CREATE TABLE t_order_type (
	id bigint not null AUTO_INCREMENT,
	clientCd varchar(20) not null,
	cd varchar(20) not null,
	name varchar(100) not null,
	typ varchar(10) not null,
	shipmentMethodCd varchar(50),
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id), UNIQUE KEY cd_UNIQUE (cd)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单类型表';

CREATE TABLE t_product (
	id bigint not null AUTO_INCREMENT,
	clientId bigint not null,
	producttypeId bigint,
	cd varchar(20) not null,
	name varchar(100) not null,
	uom varchar(10) not null,
	muCd varchar(10) not null,
	muname varchar(10) not null,
	mulength DOUBLE,
	muwidth DOUBLE,
	muheight DOUBLE,
	muvolume DOUBLE,
	muweight DOUBLE,
	munetweight DOUBLE,
	caseCd DOUBLE,
	casename DOUBLE,
	casequantity DOUBLE,
	caselength DOUBLE,
	casewidth DOUBLE,
	caseheight DOUBLE,
	caseweight DOUBLE,
	casenetweight DOUBLE,
	casevolume DOUBLE,
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id), UNIQUE KEY cd_UNIQUE (cd)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品表';

CREATE TABLE t_receiver_trans_location (
	id bigint not null AUTO_INCREMENT,
	receiverCd varchar(20) not null,
	translocationCd varchar(20) not null,
	operatorCd varchar(20) not null,
	operatorName varchar(20) not null,
	updDt datetime,
	addDt datetime not null,
	addBy bigint not null,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收发货方与运输地';

CREATE TABLE t_receiver (
	id bigint not null AUTO_INCREMENT,
	clientId bigint not null,
	cityId bigint not null,
	cd varchar(20) not null,
	name varchar(100) not null,
	likeman varchar(200),
	phone varchar(200),
	fax varchar(200),
	email varchar(200),
	postcode varchar(50),
	address varchar(255),
	updDt datetime,
	addDt datetime not null,
	addBy bigint not null,
	updBy bigint,
	descr varchar(255),
	st int not null default 0,
	PRIMARY KEY (id), UNIQUE KEY cd_UNIQUE (cd)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收发货方表';

CREATE TABLE t_trans_location (
	id bigint not null AUTO_INCREMENT,
	cityId bigint not null,
	platformId bigint not null,
	cd varchar(20) not null,
	name varchar(100) not null,
	shortname varchar(100) not null,
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id), UNIQUE KEY cd_UNIQUE (cd)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运输地表';