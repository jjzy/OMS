CREATE TABLE t_tempShipment (
	id bigint not null AUTO_INCREMENT,
	code varchar(255) not null,
	vehicleId bigint,
	platformId bigint not null,
	status varchar(45) not null,
	shipmentId bigint,
	quantity int,
	weight Double,
	volume Double,
	addBy bigint not null,
	updDt datetime,
	addDt datetime not null,
	updBy bigint,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='高级调度表';