CREATE TABLE sys_platform_menu (
  platformId bigint NOT NULL,
  menuId bigint NOT NULL,
  PRIMARY KEY (platformId, menuId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台菜单表';