CREATE TABLE sys_role (
	id bigint not null AUTO_INCREMENT,
	name varchar(100) not null,
	platformId bigint not null,
	remark varchar(255) comment '备注',
	sortNb int not null comment '排序值',
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';
