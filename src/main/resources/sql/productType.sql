CREATE TABLE t_product_type (
	id bigint not null AUTO_INCREMENT,
	clientId bigint not null,
	cd varchar(20) not null,
	name varchar(100) not null,
	typ varchar(100) not null,
	descr varchar(255),
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id), UNIQUE KEY cd_UNIQUE (cd)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品类型表';