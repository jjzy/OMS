CREATE TABLE sys_user_role (
  userId bigint NOT NULL,
  roleId bigint NOT NULL,
  PRIMARY KEY (userId, roleId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色表';