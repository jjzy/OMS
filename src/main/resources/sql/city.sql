CREATE TABLE sys_city (
	id bigint not null AUTO_INCREMENT,
	cd varchar(100) not null,
	name varchar(100) not null,
	province varchar(100) not null,
	descr varchar(255),
	sortNb int not null comment '排序值',
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='城市表';
