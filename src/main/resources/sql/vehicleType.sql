CREATE TABLE t_vehicleType (
	id bigint not null AUTO_INCREMENT,
	cd varchar(20) not null,
	name varchar(45) not null,
	descr varchar(255),
	sortNb int not null comment '排序值',
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id), UNIQUE KEY cd_UNIQUE (cd)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运输工具类型信息表';