CREATE TABLE t_order_type (
	id bigint not null AUTO_INCREMENT,
	clientId bigint not null,
	cd varchar(20) not null,
	name varchar(100) not null,
	typ varchar(10) not null,
	distCd varchar(50),
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id), UNIQUE KEY cd_UNIQUE (cd)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单类型表';