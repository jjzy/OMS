CREATE TABLE sys_menu (
	id bigint not null AUTO_INCREMENT,
	name varchar(100) not null,
	pid bigint,
	href varchar(255),
	target varchar(25),
	icon varchar(50),
	pemisi varchar(255) not null,
	ifShow int(1) not null,
	remark varchar(255) comment '备注',
	sortNb int not null comment '排序值',
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

insert into sys_menu(name, pid, href, target, icon, pemisi, ifShow, remark, sortNb, addDt, addBy, st) values('', '', '', '', '', '', '', '', '', '', '', '');