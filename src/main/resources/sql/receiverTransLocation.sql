CREATE TABLE t_receiver_trans_location (
	id bigint not null AUTO_INCREMENT,
	receiverCd varchar(20) not null,
	translocationCd varchar(20) not null,
	operatorCd varchar(20) not null,
	operatorName varchar(20) not null,
	updDt datetime,
	addDt datetime not null,
	addBy bigint not null,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收发货方与运输地';