CREATE TABLE sys_role_menu (
  roleId bigint NOT NULL,
  menuId bigint NOT NULL,
  PRIMARY KEY (roleId, menuId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色菜单表';