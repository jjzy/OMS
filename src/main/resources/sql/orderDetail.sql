CREATE TABLE t_order_detail (
	id bigint not null AUTO_INCREMENT,
	orderId bigint not null,
	productId bigint not null,
	lineno varchar(20) not null,
	lot varchar(20) not null,
	unit varchar(10) not null,
	quantity varchar(20) not null,
	weight varchar(20),
	volume varchar(20),
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单明细表';