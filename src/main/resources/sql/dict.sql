CREATE TABLE sys_dict (
	id bigint not null AUTO_INCREMENT,
	cd varchar(100) not null comment '类型',
	lb varchar(100) not null comment '标签名',
	val varchar(100) not null comment '数据值',
	descr varchar(100) comment '描述',
	remark varchar(255) comment '备注',
	sortNb int not null comment '排序值',
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';

insert into sys_dict(cd, lb, val, descr, remark, sortNb, addDt, addBy, updDt, updBy, st) values('', '', '', '', '', '', '', '', '', '', '');