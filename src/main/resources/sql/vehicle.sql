CREATE TABLE t_vehicle (
	id bigint not null AUTO_INCREMENT,
	cd varchar(100) not null,
	constractorId bigint not null,
	vehicleTypeId bigint not null,
	descr varchar(255),
	vehicleNo varchar(100) not null,
	volume Double,
	contractType varchar(100) not null,
	sortNb int not null comment '排序值',
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id), UNIQUE KEY cd_UNIQUE (cd)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运输工具类型信息表';