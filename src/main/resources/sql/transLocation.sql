CREATE TABLE t_trans_location (
	id bigint not null AUTO_INCREMENT,
	clientId bigint not null,
	platformId bigint not null,
	cd varchar(20) not null,
	name varchar(100) not null,
	shortname varchar(100) not null,
	addDt datetime not null,
	addBy bigint not null,
	updDt datetime,
	updBy bigint,
	st int not null default 0,
	PRIMARY KEY (id), UNIQUE KEY cd_UNIQUE (cd)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运输地表';