
var dt = {};
/**DataTables**/
dt.datatablesAll = function(id, url, columns){
	$('#'+id).dataTable( {
		"bJQueryUI": false,
		"bAutoWidth": false,
		"bSort": true,
		"aaSorting": [],
        "bProcessing": true,
        "bServerSide": false,
        "sAjaxSource": url,
        "aoColumns": columns,
         "oLanguage": {
        	 "sUrl": ctx+"/res/lib/dataTables/dataTables.cn.txt"
         },
         "aLengthMenu": [2, 10, 25, 50, 100],
         "scrollY": 300
    } );
};

dt.genOption = function(tableId, url, displayColumns, displayLength){
	var table = $("#"+tableId);
	var block = table.closest('[i-dt="block"]');
	//formId eq reqAttrId
	var form = block.find('[i-dt="form"] form');
	var msg = block.find('[i-dt="msg"]');
	var params = null;
	if(form.length>0){
		params = form.serializeArray();//define outside
	}
	
	var option = {
			"bAutoWidth": false,
			"bFilter": false,
			//"bSort": false,
			"aaSorting": [],
	        "bLengthChange": false,
	        "iDisplayLength": displayLength,
	        "oLanguage": {
	        	"sLengthMenu":   "显示 _MENU_ 项结果",
	            "sZeroRecords":  "没有匹配结果",
	            "sInfo":         "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
	            "sInfoEmpty":    "显示第 0 至 0 项结果，共 0 项",
	            "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
	            "sInfoPostFix":  "",
	            "sSearch":       "结果内搜索:",
	            "sUrl":          "",
	            "sEmptyTable":     "查询结果为空",
	            "sLoadingRecords": "载入中...",
	            "sInfoThousands":  ",",
	            "oPaginate": {
	                "sFirst":    "首页",
	                "sPrevious": "上页",
	                "sNext":     "下页",
	                "sLast":     "末页"
	            },
	            "oAria": {
	                "sSortAscending":  ": 以升序排列此列",
	                "sSortDescending": ": 以降序排列此列"
	            }
	         },
	        "bServerSide": true,
	        "bProcessing": false,
	        "bDestroy": true,
	        "sAjaxSource": url,
	        "fnServerData": function ( sSource, aoData, fnCallback ) {
	        	blockElement(block);
	        	if(params!=null){
	        		aoData = aoData.concat(params);
	        	}
	            $.ajax( {
	                "dataType": 'json',
	                "type": "POST",
	                "url": sSource,
	                "data": aoData,
	                "success": function(data){
	                	unBlockElement(block);
	                	//validate msg
	                	form.find(".form-group").removeClass("has-error");
	                	msg.html("");
	                	if(data.formFieldErrorMap!=null){
	                		var html = '<div class="alert alert-danger">';
	                		html += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
	                		html += '<p>';
		                	$.each(data.formFieldErrorMap, function(key, val){
		                		var ffDiv = $(key).closest(".form-group");
		                		ffDiv.addClass("has-error");
		                		var label = ffDiv.find("label").text();
		                		html += label;
		                		html += ': ';
		                		html += val;
		                		html += '<br/>';
		                	});
		                	html += '</p>';
	                		html += '</div>';
	                		
		                	msg.html(html);
	                	}else{
	                		fnCallback(data);
	                	}
	                },
	                "error": function(){
	                	//alert( "查詢出錯[1001]。" );
	                	unBlockElement(block);
	                },
	                "statusCode": {
		                404: function() {
		                  alert( "无此页面[1002]。" );
		                  unBlockElement(block);
		                },500: function() {
		                  alert( "查询出错[1003]。" );
		                  unBlockElement(block);
		                }
	                }
	            } );
	        },
	        "aoColumns": displayColumns,
	        "fnDrawCallback" : function(data) {
	        	unBlockElement(block);
	         }
	    };
	
	return option;
};

dt.genOptionCallBack = function(tableId, url, displayColumns, displayLength, callBack){
	var table = $("#"+tableId);
	var block = table.closest('[i-dt="block"]');
	//formId eq reqAttrId
	var form = block.find('[i-dt="form"] form');
	var msg = block.find('[i-dt="msg"]');
	var params = null;
	if(form.length>0){
		params = form.serializeArray();//define outside
	}
	
	var option = {
			"bAutoWidth": false,
			"bFilter": false,
			//"bSort": false,
			"aaSorting": [],
	        "bLengthChange": false,
	        "iDisplayLength": displayLength,
	        "oLanguage": {
	        	"sLengthMenu":   "显示 _MENU_ 项结果",
	            "sZeroRecords":  "没有匹配结果",
	            "sInfo":         "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
	            "sInfoEmpty":    "显示第 0 至 0 项结果，共 0 项",
	            "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
	            "sInfoPostFix":  "",
	            "sSearch":       "结果内搜索:",
	            "sUrl":          "",
	            "sEmptyTable":     "查询结果为空",
	            "sLoadingRecords": "载入中...",
	            "sInfoThousands":  ",",
	            "oPaginate": {
	                "sFirst":    "首页",
	                "sPrevious": "上页",
	                "sNext":     "下页",
	                "sLast":     "末页"
	            },
	            "oAria": {
	                "sSortAscending":  ": 以升序排列此列",
	                "sSortDescending": ": 以降序排列此列"
	            }
	         },
	        "bServerSide": true,
	        "bProcessing": false,
	        "bDestroy": true,
	        "sAjaxSource": url,
	        "fnServerData": function ( sSource, aoData, fnCallback ) {
	        	blockElement(block);
	        	if(params!=null){
	        		aoData = aoData.concat(params);
	        	}
	            $.ajax( {
	                "dataType": 'json',
	                "type": "POST",
	                "url": sSource,
	                "data": aoData,
	                "success": function(data){
	                	unBlockElement(block);
	                	//validate msg
	                	form.find(".form-group").removeClass("has-error");
	                	msg.html("");
	                	if(data.formFieldErrorMap!=null){
	                		var html = '<div class="alert alert-danger">';
	                		html += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
	                		html += '<p>';
		                	$.each(data.formFieldErrorMap, function(key, val){
		                		var ffDiv = $(key).closest(".form-group");
		                		ffDiv.addClass("has-error");
		                		var label = ffDiv.find("label").text();
		                		html += label;
		                		html += ': ';
		                		html += val;
		                		html += '<br/>';
		                	});
		                	html += '</p>';
	                		html += '</div>';
	                		
		                	msg.html(html);
	                	}else{
	                		fnCallback(data);
	                	}
	                },
	                "error": function(){
	                	//alert( "查詢出錯[1001]。" );
	                	unBlockElement(block);
	                },
	                "statusCode": {
		                404: function() {
		                  alert( "无此页面[1002]。" );
		                  unBlockElement(block);
		                },500: function() {
		                  alert( "查询出错[1003]。" );
		                  unBlockElement(block);
		                }
	                }
	            } );
	        },
	        "aoColumns": displayColumns,
	        "fnDrawCallback" : function(data) {
	        	unBlockElement(block);
	        	callBack();
	         },
	         "fnCreatedRow": function( nRow, aData, iDataIndex ) {
	             $(nRow).attr('dom-id', aData['id']);
	         }
	    };
	
	return option;
};

dt.genTable = function(tableId, option){
	var table = $("#"+tableId).dataTable(option);
	return table;
};

dt.setScrollX = function(option, x){
	option.bScrollCollapse = true;
	option.sScrollX = "100%";
	option.sScrollXInner = x;
};

dt.setScrollY = function(option, y){
	option.bScrollCollapse = true;
	option.sScrollY = y;
};
