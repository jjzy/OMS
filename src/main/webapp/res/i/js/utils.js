function httpLocation(url) {
	window.location = url;
}

function clearForm(formElement) {
	// iterate over all of the inputs for the form
	// element that was passed in
	$(':input', formElement).each(function() {
		var type = this.type;
		var tag = this.tagName.toLowerCase(); // normalize case
		// it's ok to reset the value attr of text inputs,
		// password inputs, and textareas
		if (type == 'text' || type == 'password' || type == 'number' || type == 'tel' || type == 'email' || tag == 'textarea')
			this.value = "";
		// checkboxes and radios need to have their checked state cleared
		// but should *not* have their 'value' changed
		else if (type == 'checkbox' || type == 'radio')
			this.checked = false;
		// select elements need to have their 'selectedIndex' property set to -1
		// (this works for both single and multiple select elements)
		else if (tag == 'select')
			this.selectedIndex = 0;
	});
};

function getDay(n) {
	var today = new Date();
	var yesterday = new Date(today);
	yesterday.setDate(today.getDate() + n); // setDate also supports negative values, which cause the month to rollover.

	var dd = yesterday.getDate();
	var mm = yesterday.getMonth() + 1; // January is 0!
	var yyyy = yesterday.getFullYear();

	if (dd < 10) {
		dd = '0' + dd;
	}
	if (mm < 10) {
		mm = '0' + mm;
	}

	yesterday = yyyy + '-' + mm + '-' + dd;

	return yesterday;
}

function date2Ymdhms(dateObject) {
	var d = new Date(dateObject);
	var yyyy = d.getFullYear();
	var mm = d.getMonth() + 1;
	var dd = d.getDate();

	var hh = d.getHours();
	var mi = d.getMinutes();
	var ss = d.getSeconds();

	if (dd < 10) {
		dd = "0" + dd;
	}
	if (mm < 10) {
		mm = "0" + mm;
	}
	if (hh < 10) {
		hh = "0" + hh;
	}
	if (mi < 10) {
		mi = "0" + mi;
	}
	if (ss < 10) {
		ss = "0" + ss;
	}
	var date = yyyy + "-" + mm + "-" + dd + " " + hh + ":" + mi + ":" + ss;

	return date;
};

function ymdhms2date(ymdhms) {
	return new Date(Date.parse(ymdhms.replace(/-/g, "/")));
};

function isNotNull(str) {
	return (typeof str !== "undefined" && str != '' && str != null && str != 'n/a' && str != 'N/A');
}
function isNull(str) {
	return (typeof str === "undefined" || str == '' || str == null || str == 'n/a' || str == 'N/A');
}