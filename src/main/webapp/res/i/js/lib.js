/** BlockUI* */
function blockElement(element) {
	if (element) {
		element.block({
			message : '<img src="' + ctx + '/res/i/imgs/loader.gif"/>',
			css : {
				border : 'none',
				padding : '15px',
				'-webkit-border-radius' : '10px',
				'-moz-border-radius' : '10px',
				opacity : .5,
				color : '#fff'
			}
		});
	} else {
		$.blockUI({
			message : '<img src="' + ctx + '/res/i/imgs/loader.gif"/>'
		});
	}
}
function unBlockElement(element) {
	if (element) {
		element.unblock();
	} else {
		$.unblockUI();
	}
}
/** BlockUI* */

/** toastr* */
function toastInfo(content) {
	toastr.options = {
		"closeButton" : true,
		"positionClass" : "toast-top-right",
		"timeOut" : "10000"
	};
	toastr.info(content);
}

function toastWarning(content) {
	toastr.options = {
		"closeButton" : true,
		"positionClass" : "toast-top-right",
		"timeOut" : "20000"
	};
	toastr.warning(content);
}

function toastSuccess(content) {
	toastr.options = {
		"closeButton" : true,
		"positionClass" : "toast-top-right",
		"timeOut" : "10000"
	};
	toastr.success(content);
}

function toastError(content) {
	toastr.options = {
		"closeButton" : true,
		"positionClass" : "toast-bottom-full-width",
		"timeOut" : "300000"
	};
	toastr.error(content);
}
/** toastr* */

/** bootbox* */
function dialogConfirm(content, callback) {
	bootbox.setDefaults({
		locale : "zh_CN"
	});
	bootbox.confirm(content, function(result) {
		if (result) {
			callback();
		}
	});
}
function alertErrMsg(msg) {
	var html = '<div class="alert alert-danger" role="alert" style="margin-top:20px;">';
	html += msg;
	html += '</div>';
	bootbox.setDefaults({
		locale : "zh_CN"
	});
	bootbox.alert(html, null);
};

function alertSusMsg(msg) {
	var html = '<div class="alert alert-success" role="alert" style="margin-top:20px;">';
	html += msg;
	html += '</div>';
	bootbox.setDefaults({
		locale : "zh_CN"
	});
	bootbox.alert(html, null);
};
/** bootbox* */

/** angular http ajax* */
function httpPost($http, cfg, onSuccess, onFail) {
	if (cfg.ifBlock)
		blockElement();
	var httpCfg = {};
	httpCfg.method = 'POST';
	httpCfg.url = cfg.url;
	if (cfg.params) {
		httpCfg.params = cfg.params;
	}
	if (cfg.data) {
		httpCfg.data = cfg.data;
	}

	$http(httpCfg).success(function(data, status, headers, config) {
		if (data.errCd == 0) {
			onSuccess(data);
		} else {
			console.log(data);
			onFail(data.errMsg);
		}
		if (cfg.ifBlock)
			unBlockElement();
	}).error(function(data, status, headers, config) {
		console.log(data);
		onFail("");
		if (cfg.ifBlock)
			unBlockElement();
	});
}
function httpGet($http, cfg, onSuccess, onFail) {
	if (cfg.ifBlock)
		blockElement();
	var httpCfg = {};
	httpCfg.method = 'GET';
	httpCfg.url = cfg.url;
	if (cfg.params) {
		httpCfg.params = cfg.params;
	}
	if (cfg.data) {
		httpCfg.data = cfg.data;
	}

	$http(httpCfg).success(function(data, status, headers, config) {
		if (data.errCd == 0) {
			onSuccess(data);
		} else {
			console.log(data);
			onFail(data.errMsg);
		}
		if (cfg.ifBlock)
			unBlockElement();
	}).error(function(data, status, headers, config) {
		console.log(data);
		onFail("");
		if (cfg.ifBlock)
			unBlockElement();
	});
}
/** angular http ajax* */

/** bootstrap modal* */
function openModal(element, option) {
	element.modal({
		backdrop : 'static'
	});
}
function closeModal(element) {
	element.modal('hide');
}
/** bootstrap modal* */
