$(function(){
	$('input[name="begDt"], input[name="endDt"]').datepicker({
		format: "yyyy-mm-dd",
	    todayBtn: "linked",
	    language: "zh-CN",
	    autoclose: true,
	    todayHighlight: true
	});

	var today = getDay(0);
	var yesterday = getDay(-1);
	$('input[name="begDt"]').val(yesterday);
	$('input[name="endDt"]').val(today);
});

var myApp = angular.module('myApp', [ 'ngGrid', 'ui.select2' ]);
myApp.controller('productCtrl', function($scope, $http) {
	//global variable
	$scope.selectedRows = [];
	//init page item
	$scope.init = function() {
		$scope.stList = [{"id":0, "lb":"在用"},{"id":1, "lb":"停用"}];
		
		httpPost($http, {"url":ctx+"/client/findListByFirst", "data": {"ffMap":{"st":0}}}, function(data){
			$scope.clientList = data.dt;
		}, function(){
			toastError("初始化客户列表失败。");
		});
		httpPost($http, {"url":ctx+"/dict/findListBy", "data": {"ffMap":{"cd":"uom","st":0}}}, function(data){
			$scope.dictList = data.dt;
		}, function(){
			toastError("初始化字典列表失败。");
		});
	};
	$scope.init();
	
	//grid
	$scope.pagingOptions = {
		pageSizes : [ 20, 50, 100 ],
		pageSize : 20,
		currentPage : 1,
		onQueryFlag : false
	};
	$scope.$watch('pagingOptions', function(newVal, oldVal) {
		if (newVal !== oldVal/* && newVal.currentPage !== oldVal.currentPage*/) {
			$scope.query();
		}
	}, true);
	$scope.gridOptions = {
		data : 'dt',
		totalServerItems : 'tt',
		
		columnDefs : [
		{
			field : "id",
			displayName : "ID", visible : false
		},
		{
			field : "clientName",
			displayName : "所属客户",width:250
		},
		{
			field : "typeName",
			displayName : "产品类型",width:120
		},
		{
			field : "cd",
			displayName : "代码",width:120
		},
		{
			field : "name",
			displayName : "名称",width:300
		},
		{
			field : "muname",
			displayName : "计量单位",width:100
		},
		{
			field : "mulength",
			displayName : "主单位长度",width:100
		},
		{
			field : "muwidth",
			displayName : "主单位宽度",width:120
		},
		{
			field : "muheight",
			displayName : "主单位高度",width:120
		},
		{
			field : "muvolume",
			displayName : "主单位体积",width:160
		},
		{
			field : "muweight",
			displayName : "主单位重量",width:160,
			cellFilter:'number:2'
		},
		{
			field : "munetweight",
			displayName : "主单位净重",width:120
		},
		{
			field : "casequantity",
			displayName : "包含主单位数量",width:120
		},
		{
			field : "caselength",
			displayName : "箱长",width:120
		},
		{
			field : "casewidth",
			displayName : "箱宽",width:120
		},
		{
			field : "caseheight",
			displayName : "箱高",width:120
		},
		{
			field : "caseweight",
			displayName : "箱重",width:120
		},
		{
			field : "casenetweight",
			displayName : "箱净重",width:120
		},
		{
			field : "casevolume",
			displayName : "箱体积",width:120
		},
		{
			field : "st",
			displayName : "状态",width:120
		}
		],
		enablePaging : true,
		pagingOptions : $scope.pagingOptions,
		enableCellEdit:true,
		//enableCellSelection:true,
		//enableCellEdit:true,
		//enableCellSelection:true,
		//showColumnMenu : true,
		//showGroupPanel : true,
		showSelectionCheckbox : true,
		selectedItems : $scope.selectedRows,
		plugins : [ /*new ngGridFlexibleHeightPlugin()*/ ],
		i18n: 'zh-cn', rowHeight : 20.2, footerRowHeight : 35,
		showFooter : true
	};

	$scope.query = function() {
		closeModal($("#queryFormModal"));
	
		var requestBody = {
			pageNb : $scope.pagingOptions.currentPage,
			pageSize : $scope.pagingOptions.pageSize,
			ffMap : $scope.queryForm
		};
		setTimeout(function() {
			httpPost($http, {"url":ctx + '/product/findListBy', "data":requestBody, "ifBlock":true}, function(data){
				$scope.tt = data.tt;
				$scope.dt = data.dt;
				$scope.gridOptions.$gridScope.toggleSelectAll(false);
			}, function(errMsg){
				toastError("查询失败。" + errMsg);
			});
		}, 100);
	};
	//trigger query when currentPage not change
	$scope.onQuery = function(){
		$scope.pagingOptions.onQueryFlag = !$scope.pagingOptions.onQueryFlag;
		$scope.pagingOptions.currentPage = 1;
		if($scope.queryForm.st==0){
			$scope.flag=false;
		}
		if($scope.queryForm.st==1){
			$scope.flag=true;
		}
	};
	$scope.refresh=function(){
		$scope.onQuery();	
	};

	//utils
	$scope.onClearSelectedRows = function() {
		$scope.gridOptions.$gridScope.toggleSelectAll(false);
	};
	
	//crud
	$scope.onOpenQueryFormModal = function() {
		openModal($("#queryFormModal"));
		if($scope.queryForm.st==undefined){
			$scope.queryForm.st=0;
		}
	};
	//导入
	$scope.onOpenImportFormModal = function(){
		$scope.importForm={};
		$scope.importForm.importUrl="/product/import";
		$("input[name='file']").css("width","200px").css("height","30px").css("padding-bottom","2px");
		$(".col-sm-3").width(30);
		openModal($("#importFormModal"));
	};
	
	$scope.onImport = function(){
		closeModal($("#importFormModal"));
		$("form[name='importFormForm']").submit();
	};
	//下载模板
	$scope.onExportTemplate = function(){
		if(isNull($scope.importForm) || isNull($scope.importForm.importUrl)){
			toastWarning("请选择导入项，以下载相应模板。");
			return;
		}
		window.open(ctx + $scope.importForm.importUrl + '/template');
	};
	//导出
	$scope.onOpenExportFormModal=function(){	
		var params;
		if($scope.queryForm==undefined){
			params="";
		}else{
			params = $scope.queryForm;
		}
		var str = jQuery.param(params);
		window.open(ctx  + '/product/export?'+str);
	};
	
	$scope.onOpenEditFormModal = function(type, id) {
		if(type=="add"){
			$scope.flagC=false;
				$("#e").change(function(){
					var clientId=$scope.editForm.clientId;
					httpPost($http, {"url":ctx+"/productType/clientId/"+clientId, "ifBlock":true}, function(data){
						$scope.producttypeList = data.dt;
					}, function(){
						toastError("初始化产品类型列表失败。");
					});
			});
				$("select[name='uom']").change(function(){
					var lb=$scope.editForm.uom;
					httpPost($http, {"url":ctx+"/dict/findListBy", "data": {"ffMap":{"lb":""+lb+"","st":0}}}, function(data){
						$scope.editForm.muCd=data.dt[0]["descr"];
						$scope.editForm.muname=data.dt[0]["val"];
					}, function(){
						toastError("初始化字典列表失败。");
					});});
			$scope.editForm = {};
			$scope.editForm.st = 0;
			$scope.editForm.uom = "PIECE";
			$scope.editForm.muCd="pcs";
			$scope.editForm.muname="件";
			$scope.editForm.caseCd="case";
			$scope.editForm.casename="箱";
			$scope.editForm.mulength="0.00";
			$scope.editForm.muwidth="0.00";
			$scope.editForm.muheight="0.00";
			$scope.editForm.muvolume="0.00";
			$scope.editForm.muweight="0.00";
			$scope.editForm.munetweight="0.00";
			$scope.editForm.casequantity="1";
			$scope.editForm.caselength="0.00";
			$scope.editForm.caseheight="0.00";
			$scope.editForm.casewidth="0.00";
			$scope.editForm.caseweight="0.00";
			$scope.editForm.casenetweight="0.00";
			$scope.editForm.casevolume="0.00";
			$("input[name='muwidth']").bind('input propertychange', function() {
				$scope.editForm.muwidth=$("input[name='muwidth']").val();
				$("input[name='muvolume']").val($scope.editForm.mulength*$scope.editForm.muwidth*$scope.editForm.muheight);
				$("input[name='casevolume']").val($scope.editForm.casequantity*$scope.editForm.mulength*$scope.editForm.muwidth*$scope.editForm.muheight);
				$scope.editForm.muvolume=$("input[name='muvolume']").val();
				$scope.editForm.casevolume=$("input[name='casevolume']").val();
			});
			$("input[name='muheight']").bind('input propertychange', function() {
				$scope.editForm.muheight=$("input[name='muheight']").val();
				$("input[name='muvolume']").val($scope.editForm.mulength*$scope.editForm.muwidth*$scope.editForm.muheight);
				$("input[name='casevolume']").val($scope.editForm.casequantity*$scope.editForm.mulength*$scope.editForm.muwidth*$scope.editForm.muheight);
				$scope.editForm.muvolume=$("input[name='muvolume']").val();
				$scope.editForm.casevolume=$("input[name='casevolume']").val();
			});
			$("input[name='mulength']").bind('input propertychange', function() {
				$scope.editForm.mulength=$("input[name='mulength']").val();
				$("input[name='muvolume']").val($scope.editForm.mulength*$scope.editForm.muwidth*$scope.editForm.muheight);
				$("input[name='casevolume']").val($scope.editForm.casequantity*$scope.editForm.mulength*$scope.editForm.muwidth*$scope.editForm.muheight);
				$scope.editForm.muvolume=$("input[name='muvolume']").val();
				$scope.editForm.casevolume=$("input[name='casevolume']").val();
			}); 
			$("input[name='casequantity']").bind('input propertychange', function() {
				$scope.editForm.casequantity=$("input[name='casequantity']").val();
				$("input[name='casevolume']").val($scope.editForm.casequantity*$scope.editForm.muvolume);
				$("input[name='casenetweight']").val($scope.editForm.casequantity*$scope.editForm.munetweight);
				$("input[name='caseweight']").val($scope.editForm.casequantity*$scope.editForm.muweight);
				$scope.editForm.casevolume=$("input[name='casevolume']").val();
				$scope.editForm.casenetweight=$("input[name='casenetweight']").val();
				$scope.editForm.caseweight=$("input[name='caseweight']").val();
			}); 
			$("input[name='muvolume']").bind('input propertychange', function() {
				$scope.editForm.muvolume=$("input[name='muvolume']").val();
				$("input[name='casevolume']").val($scope.editForm.casequantity*$scope.editForm.muvolume);
				$scope.editForm.casevolume=$("input[name='casevolume']").val();
			}); 
			$("input[name='munetweight']").bind('input propertychange', function() {
				$scope.editForm.munetweight=$("input[name='munetweight']").val();
				$("input[name='casenetweight']").val($scope.editForm.casequantity*$scope.editForm.munetweight);
				$scope.editForm.casenetweight=$("input[name='casenetweight']").val();
			}); 
			$("input[name='muweight']").bind('input propertychange', function() {
				$scope.editForm.muweight=$("input[name='muweight']").val();
				$("input[name='caseweight']").val($scope.editForm.casequantity*$scope.editForm.muweight);
				$scope.editForm.caseweight=$("input[name='caseweight']").val();
			}); 
		}else if(type=="upd"){
			$scope.flagC=true;
			if($scope.selectedRows.length != 1) { toastWarning("请选择一笔记录进行编辑。"); return; }
			httpGet($http, {"url":ctx+'/product/id/'+$scope.selectedRows[0]['id'], "ifBlock":true}, function(data){
				$scope.editForm = data.dt;
				var clientId=data.dt['clientId'];
				httpPost($http, {"url":ctx+"/productType/clientId/"+clientId, "ifBlock":true}, function(data){
					$scope.producttypeList = data.dt;
				}, function(){
					toastError("初始化产品类型列表失败。");
				});
				$("input[name='muwidth']").bind('input propertychange', function() {
					$scope.editForm.muwidth=$("input[name='muwidth']").val();
					$("input[name='muvolume']").val($scope.editForm.mulength*$scope.editForm.muwidth*$scope.editForm.muheight);
					$("input[name='casevolume']").val($scope.editForm.casequantity*$scope.editForm.mulength*$scope.editForm.muwidth*$scope.editForm.muheight);
					$scope.editForm.muvolume=$("input[name='muvolume']").val();
					$scope.editForm.casevolume=$("input[name='casevolume']").val();
				});
				$("input[name='muheight']").bind('input propertychange', function() {
					$scope.editForm.muheight=$("input[name='muheight']").val();
					$("input[name='muvolume']").val($scope.editForm.mulength*$scope.editForm.muwidth*$scope.editForm.muheight);
					$("input[name='casevolume']").val($scope.editForm.casequantity*$scope.editForm.mulength*$scope.editForm.muwidth*$scope.editForm.muheight);
					$scope.editForm.muvolume=$("input[name='muvolume']").val();
					$scope.editForm.casevolume=$("input[name='casevolume']").val();
				});
				$("input[name='mulength']").bind('input propertychange', function() {
					$scope.editForm.mulength=$("input[name='mulength']").val();
					$("input[name='muvolume']").val($scope.editForm.mulength*$scope.editForm.muwidth*$scope.editForm.muheight);
					$("input[name='casevolume']").val($scope.editForm.casequantity*$scope.editForm.mulength*$scope.editForm.muwidth*$scope.editForm.muheight);
					$scope.editForm.muvolume=$("input[name='muvolume']").val();
					$scope.editForm.casevolume=$("input[name='casevolume']").val();
				}); 
				$("input[name='casequantity']").bind('input propertychange', function() {
					$scope.editForm.casequantity=$("input[name='casequantity']").val();
					$("input[name='casevolume']").val($scope.editForm.casequantity*$scope.editForm.muvolume);
					$("input[name='casenetweight']").val($scope.editForm.casequantity*$scope.editForm.munetweight);
					$("input[name='caseweight']").val($scope.editForm.casequantity*$scope.editForm.muweight);
					$scope.editForm.casevolume=$("input[name='casevolume']").val();
					$scope.editForm.casenetweight=$("input[name='casenetweight']").val();
					$scope.editForm.caseweight=$("input[name='caseweight']").val();
				}); 
				$("input[name='muvolume']").bind('input propertychange', function() {
					$scope.editForm.muvolume=$("input[name='muvolume']").val();
					$("input[name='casevolume']").val($scope.editForm.casequantity*$scope.editForm.muvolume);
					$scope.editForm.casevolume=$("input[name='casevolume']").val();
				}); 
				$("input[name='munetweight']").bind('input propertychange', function() {
					$scope.editForm.munetweight=$("input[name='munetweight']").val();
					$("input[name='casenetweight']").val($scope.editForm.casequantity*$scope.editForm.munetweight);
					$scope.editForm.casenetweight=$("input[name='casenetweight']").val();
				}); 
				$("input[name='muweight']").bind('input propertychange', function() {
					$scope.editForm.muweight=$("input[name='muweight']").val();
					$("input[name='caseweight']").val($scope.editForm.casequantity*$scope.editForm.muweight);
					$scope.editForm.caseweight=$("input[name='caseweight']").val();
				}); 
			}, function(errMsg){
				toastWarning("查询此笔记录失败。");
			});
		}
		openModal($("#editFormModal"));
	};
	$scope.onSaveEditForm = function(){
		closeModal($("#editFormModal"));
		httpPost($http, {"url":ctx + '/product/save', "data":$scope.editForm, "ifBlock":true}, function(data){
			toastSuccess("保存成功");
			$scope.query();
		}, function(errMsg){
			toastError("保存失败,您输入的信息有误，请检查正确之后再保存。" + errMsg);
		});
	};
	$scope.onOpenDelFormModal = function() {
		var selectedRowsLength = $scope.selectedRows.length;
		if(selectedRowsLength < 1) { toastWarning("请至少选择一笔记录进行操作。"); return; }
		dialogConfirm("是否确认停用选择的"+selectedRowsLength+"笔记录？", function(){
			var selectedRowsId = [];
			$.each($scope.selectedRows, function(idx, obj){
				selectedRowsId.push(obj.id);
			});
			
			httpPost($http, {"url":ctx + '/product/updStByIds', "params":{"ids":selectedRowsId, "st":1}, "ifBlock":true}, function(data){
				toastSuccess("停用成功");
				$scope.query();
			}, function(errMsg){
				toastError("停用失败。" + errMsg);
			});
		});
	};
});