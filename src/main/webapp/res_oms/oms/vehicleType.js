$(function(){
	$('input[name="begDt"], input[name="endDt"]').datepicker({
		format: "yyyy-mm-dd",
	    todayBtn: "linked",
	    language: "zh-CN",
	    autoclose: true,
	    todayHighlight: true
	});

	var today = getDay(0);
	var yesterday = getDay(-1);
	$('input[name="begDt"]').val(yesterday);
	$('input[name="endDt"]').val(today);
});

var myApp = angular.module('myApp', [ 'ngGrid', 'ui.select2' ]);
myApp.controller('vehicleTypeCtrl', function($scope, $http) {
	//global variable
	$scope.selectedRows = [];
	//init page item
	$scope.init = function() {
		$scope.stList = [{"id":0, "lb":"在用"},{"id":1, "lb":"停用"}];
		httpPost($http, {"url":ctx+"/vehicleType/findListBy", "data": {"ffMap":{"st":0}}}, function(data){
			$scope.cdList = data.dt;
		}, function(){
			toastError("初始化承运商列表失败。");
		});
	};
	$scope.init();
	
	//grid
	$scope.pagingOptions = {
		pageSizes : [ 20, 50, 100 ],
		pageSize : 20,
		currentPage : 1,
		onQueryFlag : false
	};
	$scope.$watch('pagingOptions', function(newVal, oldVal) {
		if (newVal !== oldVal/* && newVal.currentPage !== oldVal.currentPage*/) {
			$scope.query();
		}
	}, true);
	$scope.gridOptions = {
		data : 'dt',
		totalServerItems : 'tt',
		
		columnDefs : [
		{
			field : "id",
			displayName : "ID", visible : false
		},
		{
			field : "cd",
			displayName : "代码"
		},
		{
			field : "name",
			displayName : "名称"
		},
		{
			field : "descr",
			displayName : "描述"
		},
		{
			field : "sortNb",
			displayName : "排序值"
		},
		{
			field : "st",
			displayName : "状态"
		}
		],
		enablePaging : true,
		pagingOptions : $scope.pagingOptions,
		enableCellEdit:true,
		//enableCellSelection:true,
		//enablePinning : true,
		//showColumnMenu : true,
		//showGroupPanel : true,
		showSelectionCheckbox : true,
		selectedItems : $scope.selectedRows,
		plugins : [ /*new ngGridFlexibleHeightPlugin()*/ ],
		i18n: 'zh-cn', rowHeight : 20.2, footerRowHeight : 35,
		showFooter : true
	};

	$scope.query = function() {
		closeModal($("#queryFormModal"));
	
		var requestBody = {
			pageNb : $scope.pagingOptions.currentPage,
			pageSize : $scope.pagingOptions.pageSize,
			ffMap : $scope.queryForm
		};
		setTimeout(function() {
			httpPost($http, {"url":ctx + '/vehicleType/findListBy', "data":requestBody, "ifBlock":true}, function(data){
				$scope.tt = data.tt;
				$scope.dt = data.dt;
				
				$scope.gridOptions.$gridScope.toggleSelectAll(false);
			}, function(errMsg){
				toastError("查询失败。" + errMsg);
			});
		}, 100);
	};
	//trigger query when currentPage not change
	$scope.onQuery = function(){
		$scope.pagingOptions.onQueryFlag = !$scope.pagingOptions.onQueryFlag;
		$scope.pagingOptions.currentPage = 1;
		if($scope.queryForm.st==0){
			$scope.flag=false;
		}
		if($scope.queryForm.st==1){
			$scope.flag=true;
		}
	};

	$scope.refresh=function(){
		$scope.onQuery();	
	};

	//utils
	$scope.onClearSelectedRows = function() {
		$scope.gridOptions.$gridScope.toggleSelectAll(false);
	};
	
	//crud
	$scope.onOpenQueryFormModal = function() {
		openModal($("#queryFormModal"));
		if($scope.queryForm.st==undefined){
			$scope.queryForm.st=0;
		}
	};
	
	$scope.onOpenEditFormModal = function(type, id) {
		if(type=="add"){
			$scope.editForm = {};
			$scope.editForm.st = 0;
		}else if(type=="upd"){
			if($scope.selectedRows.length != 1) { toastWarning("请选择一笔记录进行编辑。"); return; }
			httpGet($http, {"url":ctx+'/vehicleType/id/'+$scope.selectedRows[0]['id'], "ifBlock":true}, function(data){
				$scope.editForm = data.dt;
			}, function(errMsg){
				toastWarning("查询此笔记录失败。");
			});
		}
		openModal($("#editFormModal"));
	};
	$scope.onSaveEditForm = function(){
		closeModal($("#editFormModal"));
		httpPost($http, {"url":ctx + '/vehicleType/save', "data":$scope.editForm, "ifBlock":true}, function(data){
			toastSuccess("保存成功");
			$scope.query();
		}, function(errMsg){
			toastError("保存失败,您输入的信息有误，请检查正确之后再保存。" + errMsg);
		});
	};
	$scope.onOpenDelFormModal = function() {
		var selectedRowsLength = $scope.selectedRows.length;
		if(selectedRowsLength < 1) { toastWarning("请至少选择一笔记录进行删除。"); return; }
		dialogConfirm("是否确认删除选择的"+selectedRowsLength+"笔记录？", function(){
			var selectedRowsId = [];
			$.each($scope.selectedRows, function(idx, obj){
				selectedRowsId.push(obj.id);
			});
			
			httpPost($http, {"url":ctx + '/vehicleType/updStByIds', "params":{"ids":selectedRowsId, "st":1}, "ifBlock":true}, function(data){
				toastSuccess("删除成功");
				$scope.query();
			}, function(errMsg){
				toastError("删除失败。" + errMsg);
			});
		});
	};
});