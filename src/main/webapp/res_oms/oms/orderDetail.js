$(function(){
	$('input[name="begDt"], input[name="endDt"]').datepicker({
		format: "yyyy-mm-dd",
	    todayBtn: "linked",
	    language: "zh-CN",
	    autoclose: true,
	    todayHighlight: true
	});

	var today = getDay(0);
	var yesterday = getDay(-1);
	$('input[name="begDt"]').val(yesterday);
	$('input[name="endDt"]').val(today);
});

var myApp = angular.module('myApp', [ 'ngGrid', 'ui.select2' ]);
myApp.controller('orderDetailCtrl', function($scope, $http) {
	//global variable
	$scope.selectedRows = [];
	//init page item
	$scope.init = function() {
		$scope.stList = [{"id":0, "lb":"在用"},{"id":1, "lb":"停用"}];
		httpPost($http, {"url":ctx+"/client/findListByFirst", "data": {"ffMap":{"st":0}}}, function(data){
			$scope.clientList = data.dt;
		}, function(){
			toastError("初始化客户列表失败。");
		});
		httpPost($http, {"url":ctx+"/dict/findListBy", "data": {"ffMap":{"cd":"status","st":0}}}, function(data){
			$scope.statusList = data.dt;
		}, function(){
			toastError("初始化状态列表失败。");
		});
	};
	$scope.init();
	
	//grid
	$scope.pagingOptions = {
		pageSizes : [ 20, 50, 100 ],
		pageSize : 20,
		currentPage : 1,
		onQueryFlag : false
	};
	$scope.$watch('pagingOptions', function(newVal, oldVal) {
		if (newVal !== oldVal/* && newVal.currentPage !== oldVal.currentPage*/) {
			$scope.query();
		}
	}, true);
	$scope.gridOptions = {
		data : 'dt',
		totalServerItems : 'tt',
		
		columnDefs : [
		{
			field : "id",
			displayName : "ID", visible : false
		},
		{
			field : "orderCd",
			displayName : "订单号",width:120
		},
		{
			field : "typeName",
			displayName : "订单类型",width:120
		},
		{
			field : "clientName",
			displayName : "所属客户",width:120
		},
		{
			field : "ftranslocationName",
			displayName : "出发地",width:120
		},
		{
			field : "freceiverName",
			displayName : "发货方名称",width:120
		},
		{
			field : "freceiverCd",
			displayName : "发货方代码",width:120
		},
		{
			field : "treceiverCd",
			displayName : "收货方代码",width:120
		},
		{
			field : "treceiverName",
			displayName : "收货方名称",width:120
		},
		{
			field : "ttranslocationName",
			displayName : "目的地",width:120
		},
		{
			field : "treceiverAddress",
			displayName : "收货方地址",width:120
		},
		{
			field : "orderDt",
			displayName : "开单时间",width:160,
			cellFilter: "date:'yyyy-MM-dd HH:mm:ss'"
		},
		{
			field : "billingDt",
			displayName : "计费周期",width:160,
			cellFilter: "date:'yyyy-MM-dd HH:mm:ss'"
		},
		{
			field : "planlDt",
			displayName : "计划出发时间",width:160,
			cellFilter: "date:'yyyy-MM-dd HH:mm:ss'"
		},
		{
			field : "statusName",
			displayName : "状态",width:120
		},
		{
			field : "lineno",
			displayName : "行号",width:120
		},
		{
			field : "productCd",
			displayName : "物料代码",width:120
		},
		{
			field : "productName",
			displayName : "货物名称",width:120
		},
		{
			field : "productTypeName",
			displayName : "产品类型",width:120
		},
		{
			field : "lot",
			displayName : "批号",width:120
		},
		{
			field : "unit",
			displayName : "包装单位",width:120
		},
		{
			field : "quantity",
			displayName : "数量",width:120
		},
		{
			field : "weight",
			displayName : "重量",width:120
		},
		{
			field : "volume",
			displayName : "体积",width:120
		}
		],
		enablePaging : true,
		pagingOptions : $scope.pagingOptions,
		enableCellEdit:true,
		//enableCellSelection:true,
		//enablePinning : true,
		//showColumnMenu : true,
		//showGroupPanel : true,
		
		showSelectionCheckbox : true,
		selectedItems : $scope.selectedRows,
		plugins : [ /*new ngGridFlexibleHeightPlugin()*/ ],
		i18n: 'zh-cn', rowHeight : 20.2, footerRowHeight : 35,
		showFooter : true
	};
	$scope.query = function() {
		closeModal($("#queryFormModal"));
	
		var requestBody = {
			pageNb : $scope.pagingOptions.currentPage,
			pageSize : $scope.pagingOptions.pageSize,
			ffMap : $scope.queryForm
		};
		setTimeout(function() {
			httpPost($http, {"url":ctx + '/orderDetail/findListBy', "data":requestBody, "ifBlock":true}, function(data){
				$scope.tt = data.tt;
				$scope.dt = data.dt;
				
				$scope.gridOptions.$gridScope.toggleSelectAll(false);
			}, function(errMsg){
				toastError("查询失败。" + errMsg);
			});
		}, 100);
	};
	//trigger query when currentPage not change
	$scope.onQuery = function(){
		$scope.pagingOptions.onQueryFlag = !$scope.pagingOptions.onQueryFlag;
		$scope.pagingOptions.currentPage = 1;
	};

	$scope.refresh=function(){
		$scope.onQuery();	
	};

	//utils
	$scope.onClearSelectedRows = function() {
		$scope.gridOptions.$gridScope.toggleSelectAll(false);
	};
	
	//crud
	$scope.onOpenQueryFormModal = function() {
		openModal($("#queryFormModal"));
	};
	
	$scope.onOpenEditFormModal = function(type, id) {
		if(type=="add"){
			$scope.editForm = {};
		}else if(type=="upd"){
			if($scope.selectedRows.length != 1) { toastWarning("请选择一笔记录进行编辑。"); return; }
			httpGet($http, {"url":ctx+'/orderDetail/id/'+$scope.selectedRows[0]['id'], "ifBlock":true}, function(data){
				$scope.editForm = data.dt;
			}, function(errMsg){
				toastWarning("查询此笔记录失败。");
			});
		}
		openModal($("#editFormModal"));
	};
	$scope.onSaveEditForm = function(){
		closeModal($("#editFormModal"));
		httpPost($http, {"url":ctx + '/orderDetail/save', "data":$scope.editForm, "ifBlock":true}, function(data){
			toastSuccess("保存成功");
			$scope.query();
		}, function(errMsg){
			toastError("保存失败。" + errMsg);
		});
	};
	$scope.onOpenDelFormModal = function() {
		var selectedRowsLength = $scope.selectedRows.length;
		if(selectedRowsLength < 1) { toastWarning("请至少选择一笔记录进行删除。"); return; }
		dialogConfirm("是否确认删除选择的"+selectedRowsLength+"笔记录？", function(){
			var selectedRowsId = [];
			$.each($scope.selectedRows, function(idx, obj){
				selectedRowsId.push(obj.id);
			});
			
			httpPost($http, {"url":ctx + '/orderDetail/updStByIds', "params":{"ids":selectedRowsId, "st":1}, "ifBlock":true}, function(data){
				toastSuccess("删除成功");
				$scope.query();
			}, function(errMsg){
				toastError("删除失败。" + errMsg);
			});
		});
	};
});