var myApp = angular.module('myApp', [ 'ngGrid', 'ui.select2' ]);
myApp.controller('shipmentCtrl', function($scope, $http) {
	//global variable
	$scope.selectedRows = [];
	$scope.selectedRow = [];
	$scope.selectedRowL=[];
	//init page item
	$scope.init = function() {
		$('input[name="pcTime"],input[name="rkTime"],input[name="ckTime"],input[name="fyTime"],input[name="ydTime"],input[name="leavetime"],input[name="arrivetime"],input[name="planltime"],input[name="planatime"]').datetimepicker({
			minView: "month",
			format: "yyyy-mm-dd hh:ii",
		    todayBtn: "linked",
		    language: "zh-CN",
		    forceParse: false,
		    autoclose: true,
		    todayHighlight: true
		});
		$scope.stList = [{"id":0, "lb":"在用"},{"id":1, "lb":"停用"}];
		httpPost($http, {"url":ctx+"/constractor/findListBy", "data": {"ffMap":{"st":0}}}, function(data){
			$scope.constractorList = data.dt;
		}, function(){
			toastError("初始化承运商列表失败。");
		});
		httpPost($http, {"url":ctx+"/driver/findListBy", "data": {"ffMap":{"st":0}}}, function(data){
			$scope.driverList = data.dt;
		}, function(){
			toastError("初始化司机列表失败。");
		});
		
		httpPost($http, {"url":ctx+"/transLocation/findListByFirst", "data": {"ffMap":{"st":0}}}, function(data){
			$scope.ftranslocationList = data.dt;
		}, function(){
			toastError("初始化运输地列表失败。");
		});
		
		httpPost($http, {"url":ctx+"/transLocation/findListByFirst", "data": {"ffMap":{"st":0}}}, function(data){
			$scope.ttranslocationList = data.dt;
		}, function(){
			toastError("初始化运输地列表失败。");
		});
		
		httpPost($http, {"url":ctx+"/dict/findListBy", "data": {"ffMap":{"cd":"shipmentMethod","st":0}}}, function(data){
			$scope.shipment_methodList = data.dt;
		}, function(){
			toastError("初始化运输方式列表失败。");
		});
		httpPost($http, {"url":ctx+"/dict/findListBy", "data": {"ffMap":{"cd":"shipmentType","st":0}}}, function(data){
			$scope.shipmentTypeList = data.dt;
		}, function(){
			toastError("初始化配载方式列表失败。");
		});
		httpPost($http, {"url":ctx+"/dict/findListBy", "data": {"ffMap":{"cd":"vocation","st":0}}}, function(data){
			$scope.cdList = data.dt;	
		}, function(){
			toastError("初始化业务类型列表失败。");
		});
	};
	$scope.init();
	
	//grid
	$scope.pagingOptions = {
		pageSizes : [ 20, 50, 100 ],
		pageSize : 20,
		currentPage : 1,
		onQueryFlag : false
	};
	$scope.$watch('pagingOptions', function(newVal, oldVal) {
		if (newVal !== oldVal/* && newVal.currentPage !== oldVal.currentPage*/) {
			$scope.query();
		}
	}, true);
	$scope.gridOptions = {
		data : 'dt',
		totalServerItems : 'tt',
		
		columnDefs : [
		{
			field : "id",
			displayName : "ID", visible : false
		},
		{
			field : "vocationType",
			displayName : "vocationType", visible : false
		},
		{
			field : "cd",
			displayName : "调度单号",width:140
		},
		{
			field : "statusName",
			displayName : "调度单状态",width:120
		},
		{
			field : "vocationTypeName",
			displayName : "业务类型",width:120
		},
		{
			field : "ftranslocationName",
			displayName : "出发地",width:120
		},
		{
			field : "ttranslocationName",
			displayName : "目的地",width:120
		},
		{
			field : "constractorName",
			displayName : "承运商",width:160
		},		
		{
			field : "carNo",
			displayName : "车牌号",width:120
		},
		{
			field : "driverName",
			displayName : "司机",width:120
		},
		{
			field : "shipmentType",
			displayName : "配载方式",width:120
		},
		{
			field : "shipment_methodName",
			displayName : "运输方式",width:120
		},
		{
			field : "points",
			displayName : "送货点数",width:120
		},
		{
			field : "quantity",
			displayName : "件数",width:120
		},
		{
			field : "weight",
			displayName : "重量",width:120
		},
		{
			field : "volume",
			displayName : "体积",width:120
		},
		{
			field : "palletsum",
			displayName : "托数",width:120
		},
		{
			field : "case",
			displayName : "零数箱",width:120
		},
		{
			field : "planltime",
			displayName : "计划发车时间",width:140,
			cellFilter: "date:'yyyy-MM-dd HH:mm:ss'"
		},
		{
			field : "planatime",
			displayName : "预计到达时间",width:140,
			cellFilter: "date:'yyyy-MM-dd HH:mm:ss'"
		},
		{
			field : "leavetime",
			displayName : "出发时间",width:140,
			cellFilter: "date:'yyyy-MM-dd HH:mm:ss'"
		},
		{
			field : "arrivetime",
			displayName : "到达时间",width:140,
			cellFilter: "date:'yyyy-MM-dd HH:mm:ss'"
		},
		{
			field : "gzNo",
			displayName : "跟踪单号",width:120
		},
		{
			field : "descr",
			displayName : "描述",width:180
		}
		],
		enablePaging : true,
		pagingOptions : $scope.pagingOptions,
		enableCellEdit:true,
		//enableCellSelection:true,
		//enablePinning : true,
		//showColumnMenu : true,
		//showGroupPanel : true,
		showSelectionCheckbox : true,
		selectedItems : $scope.selectedRows,
		plugins : [ /*new ngGridFlexibleHeightPlugin()*/ ],
		i18n: 'zh-cn', rowHeight :20.2, footerRowHeight : 35,
		showFooter : true
	};

	$scope.query = function() {
		closeModal($("#queryFormModal"));
	
		var requestBody = {
			pageNb : $scope.pagingOptions.currentPage,
			pageSize : $scope.pagingOptions.pageSize,
			ffMap : $scope.queryForm
		};
		setTimeout(function() {
			httpPost($http, {"url":ctx + '/shipment/findListBy', "data":requestBody, "ifBlock":true}, function(data){
				$scope.tt = data.tt;
				$scope.dt = data.dt;
				
				$scope.gridOptions.$gridScope.toggleSelectAll(false);
			}, function(errMsg){
				toastError("查询失败。" + errMsg);
			});
		}, 100);
	};
	//trigger query when currentPage not change
	$scope.onQuery = function(){
		$scope.pagingOptions.onQueryFlag = !$scope.pagingOptions.onQueryFlag;
		$scope.pagingOptions.currentPage = 1;
	};

	$scope.refresh=function(){
		$scope.onQuery();	
	};

	//utils
	$scope.onClearSelectedRows = function() {
		$scope.gridOptions.$gridScope.toggleSelectAll(false);
	};
		
	//grid
	$scope.pagingOption = {
		pageSizes : [ 20, 50, 100 ],
		pageSize : 20,
		currentPage : 1,
		onQueryFlag : false
	};
	$scope.$watch('pagingOption', function(newVal, oldVal) {
		if (newVal !== oldVal/* && newVal.currentPage !== oldVal.currentPage*/) {
			$scope.queryl();
		}
	}, true);
	$scope.gridOption = {
		data : 'd',
		totalServerItems : 't',
		
		columnDefs : [
		{
			field : "id",
			displayName : "ID", visible : false
		},
		{
			field : "orderId",
			displayName : "orderId", visible : false
		},
		{
			field : "vocationType",
			displayName : "业务类型",visible : false
		},
		{
			field : "orderCd",
			displayName : "订单号",width:140
		},
		{
			field : "legs_no",
			displayName : "分段号",width:120
		},
		{
			field : "shipment_methodName",
			displayName : "运输方式",width:120
		},
		{
			field : "shipmentType",
			displayName : "配载方式",width:120
		},
		{
			field : "ftranslocationName",
			displayName : "出发地",width:160
		},
		{
			field : "ttranslocationName",
			displayName : "目的地",width:160
		},
		{
			field : "quantity",
			displayName : "件数",width:120
		},
		{
			field : "weight",
			displayName : "重量",width:120
		},
		{
			field : "volume",
			displayName : "体积",width:120
		},
		{
			field : "palletsum",
			displayName : "托数",width:120
		},
		{
			field : "formName",
			displayName : "发货方名称",width:160
		},
		{
			field : "toName",
			displayName : "收货方名称",width:160
		},
		{
			field : "toAddr",
			displayName : "收货方地址",width:160
		},
		{
			field : "planltime",
			displayName : "计划离开时间",width:160,
			cellFilter: "date:'yyyy-MM-dd HH:mm:ss'"
		},
		{
			field : "descr",
			displayName : "描述",width:180
		}
		],
		enablePaging : true,
		pagingOptions : $scope.pagingOption,
		enableCellEdit:true,
		//enablePinning : true,
		//showColumnMenu : true,
		//showGroupPanel : true,
		showSelectionCheckbox : true,
		selectedItems : $scope.selectedRow,
		plugins : [ /*new ngGridFlexibleHeightPlugin()*/ ],
		i18n: 'zh-cn', rowHeight : 21, footerRowHeight : 35,
		showFooter : true,
		afterSelectionChange : function(rowItem, event){
			if($scope.selectedRow.length>0){
				$scope.cancelflag=false;
			}else{
				$scope.cancelflag=true;
			}
		}
	};

	$scope.queryl = function() {
		closeModal($("#queryFormModal"));
	
		var requestBody = {
			pageNb : $scope.pagingOption.currentPage,
			pageSize : $scope.pagingOption.pageSize,
			ffMap : $scope.queryForml
		};
		setTimeout(function() {
			httpPost($http, {"url":ctx + '/legs/findListBy', "data":requestBody, "ifBlock":true}, function(data){
				$scope.t = data.tt;
				$scope.d = data.dt;
				$scope.gridOption.$gridScope.toggleSelectAll(false);
			}, function(errMsg){
				toastError("查询失败。" + errMsg);
			});
		}, 100);
	};
	//trigger query when currentPage not change
	$scope.onQueryl = function(){
		$scope.pagingOption.onQueryFlag = !$scope.pagingOption.onQueryFlag;
		$scope.pagingOption.currentPage = 1;
	};
	
	
	//grid
	$scope.pagingOptionL = {
		pageSizes : [ 20, 50, 100 ],
		pageSize : 20,
		currentPage : 1,
		onQueryFlag : false
	};
	$scope.$watch('pagingOptionL', function(newVal, oldVal) {
		if (newVal !== oldVal/* && newVal.currentPage !== oldVal.currentPage*/) {
			$scope.queryLg();
		}
	}, true);
	$scope.gridOptionL = {
		data : 'dl',
		totalServerItems : 'tl',
		
		columnDefs : [
		{
			field : "id",
			displayName : "ID", visible : false
		},
		{
			field : "orderCd",
			displayName : "订单号",width:140
		},
		{
			field : "legs_no",
			displayName : "调度单号",width:140
		},
		{
			field : "vocationName",
			displayName : "业务类型",width:120
		},
		{
			field : "billtime",
			displayName : "出发时间",width:140
		},
		{
			field : "clientName",
			displayName : "客户",width:120
		},
		{
			field : "quantity",
			displayName : "总件数",width:120
		},
		{
			field : "weight",
			displayName : "重量",width:130
		},
		{
			field : "volume",
			displayName : "体积",width:120
		},
		{
			field : "ftranslocationName",
			displayName : "出发地",width:120
		},
		{
			field : "fromAddr",
			displayName : "出发地地址",width:120
		},
		{
			field : "ttranslocationName",
			displayName : "目的地",width:120
		},
		{
			field : "toAddr",
			displayName : "目的地地址",width:120
		},
		{
			field : "planltime",
			displayName : "计划发车时间",width:140
		},
		{
			field : "planatime",
			displayName : "预计到达时间",width:140
		}
		],
		enablePaging : true,
		pagingOptions : $scope.pagingOptionL,
		enableCellEdit:true,
		//enablePinning : true,
		//showColumnMenu : true,
		//showGroupPanel : true,
		showSelectionCheckbox : true,
		selectedItems : $scope.selectedRowL,
		plugins : [ /*new ngGridFlexibleHeightPlugin()*/ ],
		i18n: 'zh-cn', rowHeight : 25, footerRowHeight : 35,
		showFooter : true
	};

	$scope.queryLg = function() {
		closeModal($("#queryFormModal"));
	
		var requestBody = {
			pageNb : $scope.pagingOptionL.currentPage,
			pageSize : $scope.pagingOptionL.pageSize,
			ffMap : $scope.queryFormlg
		};
		setTimeout(function() {
			httpPost($http, {"url":ctx + '/legs/findListBy', "data":requestBody, "ifBlock":true}, function(data){
				$scope.tl = data.tt;
				$scope.dl = data.dt;
				$scope.gridOptionL.$gridScope.toggleSelectAll(false);
			}, function(errMsg){
				toastError("查询失败。" + errMsg);
			});
		}, 100);
	};
	//trigger query when currentPage not change
	$scope.onQueryLg = function(){
		$scope.pagingOptionL.onQueryFlag = !$scope.pagingOptionL.onQueryFlag;
		$scope.pagingOptionL.currentPage = 1;
	};
	
	//派车确认
	$scope.onOpenEditFormModalPc = function() {
		$scope.editFormPc={};
		var date= (new Date());
		var now = date.getFullYear() + "-" + (parseInt(date.getMonth()) + 1) +"-"+((date.getDate() > 9) ? date.getDate() : "0" + date.getDate())+" "+((date.getHours()> 9) ? date.getHours() : "0" + date.getHours())+":"+((date.getMinutes() > 9) ? date.getMinutes() : "0" + date.getMinutes());
		$scope.editFormPc.pcTime=now;
		openModal($("#editFormModalPc"));
	};
	$scope.pcConfirm=function(){
		closeModal($("#editFormModalPc"));
		var selectedRowIds = [];
		var selectedShipmentIds = [];
		$.each($scope.selectedRows, function(idx, obj){
			selectedRowIds.push(obj.id);
			var shipmentId=$scope.selectedRows[idx]['shipmentId'];
			if(jQuery.inArray( shipmentId, selectedShipmentIds ) < 0){
				selectedShipmentIds.push(shipmentId);
			}
		});
		httpPost($http, {"url":ctx + '/legs/updTime',"data": {"ffMap":{"shipmentIds":selectedRowIds,"pcTime":$scope.editFormPc.pcTime}},"ifBlock":true}, function(data){
			httpPost($http, {"url":ctx + '/shipment/updStatusByIds',"params": {"ids":selectedRowIds,"status":"sendCar"},"ifBlock":true}, function(data){
				httpPost($http, {"url":ctx + '/legs/findByShipmentId',"params": {"ids":selectedRowIds,"status":"osendCar"},"ifBlock":true}, function(data){
					$scope.onQuery();
				});
			});
		});
	};
	//车辆入库
	$scope.onOpenEditFormModalIn = function() {
		$scope.editFormRk={};
		var date= (new Date());
		var now = date.getFullYear() + "-" + (parseInt(date.getMonth()) + 1) +"-"+((date.getDate() > 9) ? date.getDate() : "0" + date.getDate())+" "+((date.getHours()> 9) ? date.getHours() : "0" + date.getHours())+":"+((date.getMinutes() > 9) ? date.getMinutes() : "0" + date.getMinutes());
		$scope.editFormRk.rkTime=now;
		openModal($("#editFormModalIn"));
	};
	$scope.rkConfirm=function(){
		closeModal($("#editFormModalIn"));
		var selectedRowIds = [];
		var selectedShipmentIds = [];
		$.each($scope.selectedRows, function(idx, obj){
			selectedRowIds.push(obj.id);
			var shipmentId=$scope.selectedRows[idx]['shipmentId'];
			if(jQuery.inArray( shipmentId, selectedShipmentIds ) < 0){
				selectedShipmentIds.push(shipmentId);
			}
		});
		httpPost($http, {"url":ctx + '/legs/updTime',"data": {"ffMap":{"shipmentIds":selectedRowIds,"rkTime":$scope.editFormRk.rkTime}},"ifBlock":true}, function(data){
			httpPost($http, {"url":ctx + '/shipment/updStatusByIds',"params": {"ids":selectedRowIds,"status":"instorage"},"ifBlock":true}, function(data){
				httpPost($http, {"url":ctx + '/legs/findByShipmentId',"params": {"ids":selectedRowIds,"status":"oinstorage"},"ifBlock":true}, function(data){
					$scope.onQuery();
				});
			});
		});
	};
	//车辆出库
	$scope.onOpenEditFormModalOut = function() {
		$scope.editFormCk={};
		var date= (new Date());
		var now = date.getFullYear() + "-" + (parseInt(date.getMonth()) + 1) +"-"+((date.getDate() > 9) ? date.getDate() : "0" + date.getDate())+" "+((date.getHours()> 9) ? date.getHours() : "0" + date.getHours())+":"+((date.getMinutes() > 9) ? date.getMinutes() : "0" + date.getMinutes());
		$scope.editFormCk.ckTime=now;
		openModal($("#editFormModalOut"));
	};
	$scope.ckConfirm=function(){
		closeModal($("#editFormModalOut"));
		var selectedRowIds = [];
		var selectedShipmentIds = [];
		$.each($scope.selectedRows, function(idx, obj){
			selectedRowIds.push(obj.id);
			var shipmentId=$scope.selectedRows[idx]['shipmentId'];
			if(jQuery.inArray( shipmentId, selectedShipmentIds ) < 0){
				selectedShipmentIds.push(shipmentId);
			}
		});
		httpPost($http, {"url":ctx + '/legs/updTime',"data": {"ffMap":{"shipmentIds":selectedRowIds,"ckTime":$scope.editFormCk.ckTime}},"ifBlock":true}, function(data){
			httpPost($http, {"url":ctx + '/shipment/updStatusByIds',"params": {"ids":selectedRowIds,"status":"outstorage"},"ifBlock":true}, function(data){
				httpPost($http, {"url":ctx + '/legs/findByShipmentId',"params": {"ids":selectedRowIds,"status":"ooutstorage"},"ifBlock":true}, function(data){
					$scope.onQuery();
				});
			});
		});
	};
	
	//发运确认
	$scope.onOpenEditFormModalFy = function() {
		$scope.editFormFy={};
		var date= (new Date());
		var now = date.getFullYear() + "-" + (parseInt(date.getMonth()) + 1) +"-"+((date.getDate() > 9) ? date.getDate() : "0" + date.getDate())+" "+((date.getHours()> 9) ? date.getHours() : "0" + date.getHours())+":"+((date.getMinutes() > 9) ? date.getMinutes() : "0" + date.getMinutes());
		$scope.editFormFy.fyTime=now;
		openModal($("#editFormModalFy"));
	};
	$scope.fyConfirm=function(){
		closeModal($("#editFormModalFy"));
		var selectedRowIds = [];
		var selectedShipmentIds = [];
		$.each($scope.selectedRows, function(idx, obj){
			selectedRowIds.push(obj.id);
			var shipmentId=$scope.selectedRows[idx]['shipmentId'];
			if(jQuery.inArray( shipmentId, selectedShipmentIds ) < 0){
				selectedShipmentIds.push(shipmentId);
			}
		});
		httpPost($http, {"url":ctx + '/shipment/updTime',"data": {"ffMap":{"ids":selectedRowIds,"fyTime":$scope.editFormFy.fyTime}},"ifBlock":true}, function(data){
			
		});
		httpPost($http, {"url":ctx + '/legs/updTime',"data": {"ffMap":{"shipmentIds":selectedRowIds,"fyTime":$scope.editFormFy.fyTime}},"ifBlock":true}, function(data){
			httpPost($http, {"url":ctx + '/shipment/updStatusByIds',"params": {"ids":selectedRowIds,"status":"onway"},"ifBlock":true}, function(data){
				httpPost($http, {"url":ctx + '/legs/findByShipmentId',"params": {"ids":selectedRowIds,"status":"oonway"},"ifBlock":true}, function(data){
					$scope.onQuery();
				});
			});
			httpPost($http, {"url":ctx + '/legs/updStByShipmentIds',"params": {"ids":selectedRowIds,"st":"onway"},"ifBlock":true}, function(data){
				
			});
		});
	};
	
	//运抵确认
	$scope.onOpenEditFormModalYd = function() {
		$scope.editFormYd={};
		var date= (new Date());
		var now = date.getFullYear() + "-" + (parseInt(date.getMonth()) + 1) +"-"+((date.getDate() > 9) ? date.getDate() : "0" + date.getDate())+" "+((date.getHours()> 9) ? date.getHours() : "0" + date.getHours())+":"+((date.getMinutes() > 9) ? date.getMinutes() : "0" + date.getMinutes());
		$scope.editFormYd.ydTime=now;
		openModal($("#editFormModalYd"));
	};
	$scope.ydConfirm=function(){
		closeModal($("#editFormModalYd"));
		var selectedRowIds = [];
		var selectedShipmentIds = [];
		$.each($scope.selectedRows, function(idx, obj){
			selectedRowIds.push(obj.id);
			var shipmentId=$scope.selectedRows[idx]['shipmentId'];
			if(jQuery.inArray( shipmentId, selectedShipmentIds ) < 0){
				selectedShipmentIds.push(shipmentId);
			}
		});
		httpPost($http, {"url":ctx + '/shipment/updTime',"data": {"ffMap":{"ids":selectedRowIds,"ydTime":$scope.editFormYd.ydTime}},"ifBlock":true}, function(data){
			
		});
		httpPost($http, {"url":ctx + '/legs/updTime',"data": {"ffMap":{"shipmentIds":selectedRowIds,"ydTime":$scope.editFormYd.ydTime}},"ifBlock":true}, function(data){
			httpPost($http, {"url":ctx + '/shipment/updStatusByIds',"params": {"ids":selectedRowIds,"status":"arrive"},"ifBlock":true}, function(data){
				httpPost($http, {"url":ctx + '/legs/findByShipmentId',"params": {"ids":selectedRowIds,"status":"oarrive"},"ifBlock":true}, function(data){
					$scope.onQuery();
				});
			});
			httpPost($http, {"url":ctx + '/legs/updStByShipmentIds',"params": {"ids":selectedRowIds,"st":"arrive"},"ifBlock":true}, function(data){
				
			});
		});
	};
	
	$scope.cancel=function(){
		var selectedRowId = [];
		var orderId=[];
		var object=[];
		$.each($scope.selectedRow, function(idx, obj){
			selectedRowId.push(obj.id);
			orderId.push($scope.selectedRow[idx]['orderId']);
			object.push($scope.selectedRow[idx]);
		});

		httpPost($http, {"url":ctx + '/legs/updByIds',"data":object, "params":{"shipmentId":$scope.selectedRows[0]['id'],"ids":selectedRowId,"orderId":orderId,"vocationType":$scope.selectedRows[0]['vocationType']}, "ifBlock":true}, function(data){
			toastSuccess("取消成功");
			ids={"shipmentId":$scope.selectedRows[0]['id']};
			$scope.onQueryl = function(){
				$scope.pagingOption.onQueryFlag = !$scope.pagingOption.onQueryFlag;
				$scope.pagingOption.currentPage = 1;
				$scope.queryForml=ids;
			};	
			$scope.onQueryl();
			httpGet($http, {"url":ctx+'/shipment/id/'+$scope.selectedRows[0]['id'], "ifBlock":true}, function(data){
				$scope.editForm=data.dt;
				if($scope.editForm.planatime!=null){
					$scope.editForm.planatime = date2Ymdhms(data.dt.planatime);
				}
				if($scope.editForm.planltime!=null){
					$scope.editForm.planltime = date2Ymdhms(data.dt.planltime);
				}
					$scope.selectedRows[0]=data.dt;
			});
		}, function(errMsg){
			toastError("取消失败。" + errMsg);
		});	
	};
	//utils
	$scope.onClearSelectedRows = function() {
		$scope.gridOption.$gridScope.toggleSelectAll(false);
	};
	
	//crud
	$scope.onOpenQueryFormModal = function() {	
		openModal($("#queryFormModal"));
	};
	
	//添加分段订单
	$scope.onOpenEditFormModalL=function(){
		var vocationType=$scope.selectedRows[0]['vocationType'];
		httpPost($http, {"url":ctx + '/tempShipment/findListBy',"data": {"ffMap":{"code":"未配载","vocation":vocationType}},"ifBlock":true}, function(data){
			var id=data.dt[0]['id'];
			ids={"tempshipmentId":id};
			$scope.onQueryLg = function(){
				$scope.pagingOptionL.onQueryFlag = !$scope.pagingOptionL.onQueryFlag;
				$scope.pagingOptionL.currentPage = 1;
				$scope.queryFormlg=ids;
			};	
			$scope.onQueryLg();
		});
		openModal($("#editFormModalL"));
	};
	
	$scope.add=function(){
		closeModal($("#editFormModalL"));
		var selectedRowLId = [];
		var object=[];
		$.each($scope.selectedRowL, function(idx, obj){
			selectedRowLId.push(obj.id);
			object.push($scope.selectedRowL[idx]);
		});
		console.log(object)
		httpPost($http, {"url":ctx + '/legs/updByIds1',"data":object,"params":{"ids":selectedRowLId,"shipmentId":$scope.selectedRows[0]['id'],"constractorId":$scope.selectedRows[0]['constractorId'],"shipment_method":$scope.selectedRows[0]['shipment_method'],"vocationType":$scope.selectedRows[0]['vocationType']}, "ifBlock":true}, function(data){
			ids={"shipmentId":$scope.selectedRows[0]['id']};
			$scope.onQueryl = function(){
				$scope.pagingOption.onQueryFlag = !$scope.pagingOption.onQueryFlag;
				$scope.pagingOption.currentPage = 1;
				$scope.queryForml=ids;
			};	
			$scope.onQueryl();
			httpGet($http, {"url":ctx+'/shipment/id/'+$scope.selectedRows[0]['id'], "ifBlock":true}, function(data){
				$scope.editForm=data.dt;
				if($scope.editForm.planatime!=null){
					$scope.editForm.planatime = date2Ymdhms(data.dt.planatime);
				}
				if($scope.editForm.planltime!=null){
					$scope.editForm.planltime = date2Ymdhms(data.dt.planltime);
				}
				$scope.selectedRows[0]=data.dt;
			});
		});
	};
	$scope.onOpenEditFormModal = function(type, id) {
		if(type=="add"){
			$("#cd").css("display","none");
			$("#ft").css("margin-left","15px");
			$scope.addflag=true;
			$scope.cancelflag=true;
			$scope.editForm = {};
			$scope.editForm.shipment_method='TRUCK';
			$("#c").change(function(){
				var id=$scope.editForm.constractorId;
				httpGet($http, {"url":ctx+'/constractor/id/'+id, "ifBlock":true}, function(data){	
					httpPost($http, {"url":ctx+"/vehicle/findListBy", "data": {"ffMap":{"constractorId":id,"st":0}}}, function(data){
						$scope.vehicleList = data.dt;				
					}, function(){
						toastError("初始化运输工具列表失败。");
					});
				});	
			});
			$("#vh").click(function(){
				var vid=$scope.editForm.vehicleId;
				httpGet($http, {"url":ctx+'/vehicle/id/'+vid, "ifBlock":true}, function(data){
					$scope.editForm.carNo=data.dt['vehicleNo'];
				}, function(){
					toastError("初始化运输工具列表失败。");
				});
			});
			$("#d").change(function(){
				var id=$scope.editForm.driverId;
				httpGet($http, {"url":ctx+'/driver/id/'+id, "ifBlock":true}, function(data){
					$scope.editForm.driverName=data.dt['name'];
					$scope.editForm.idcard=data.dt['idcard'];
					$scope.editForm.phone=data.dt['phone'];
				}, function(){
					toastError("初始化运输工具列表失败。");
				});
			});
			ids={"shipmentId":0};
			$scope.onQueryl = function(){
				$scope.pagingOption.onQueryFlag = !$scope.pagingOption.onQueryFlag;
				$scope.pagingOption.currentPage = 1;
				$scope.queryForml=ids;
			};	
			$scope.onQueryl();
		}else if(type=="upd"){
			$scope.addflag=false;
			$scope.cancelflag=true;
			$("#cd").css("display","block");
			$("#ft").css("margin-left","0px");
			$scope.flagOfCd=true;
			if($scope.selectedRows.length != 1) { toastWarning("请选择一笔记录进行编辑。"); return; }
			httpGet($http, {"url":ctx+'/shipment/id/'+$scope.selectedRows[0]['id'], "ifBlock":true}, function(data){
				$scope.editForm = data.dt;
				if($scope.editForm.planatime!=null){
					$scope.editForm.planatime = date2Ymdhms(data.dt.planatime);
				}
				if($scope.editForm.planltime!=null){
					$scope.editForm.planltime = date2Ymdhms(data.dt.planltime);
				}
				var id=$scope.editForm['constractorId'];
				httpPost($http, {"url":ctx+"/vehicle/findListBy", "data": {"ffMap":{"constractorId":id,"st":0}}}, function(data){
					$scope.vehicleList = data.dt;				
				}, function(){
					toastError("初始化运输工具列表失败。");
				});
				$("#vh").click(function(){
					var vid=$scope.editForm.vehicleId;
					httpGet($http, {"url":ctx+'/vehicle/id/'+vid, "ifBlock":true}, function(data){
						$scope.editForm.carNo=data.dt['vehicleNo'];
					}, function(){
						toastError("初始化运输工具列表失败。");
					});
				});
				
			}, function(errMsg){
				toastWarning("查询此笔记录失败。");
			});
			var id=$scope.selectedRows[0]['id'];
			$scope.queryForml={};
			ids={"shipmentId":id};
			$scope.onQueryl = function(){
				$scope.pagingOption.onQueryFlag = !$scope.pagingOption.onQueryFlag;
				$scope.pagingOption.currentPage = 1;
				$scope.queryForml=ids;
			};	
			$scope.onQueryl();
			$(".close").click(function(){
				$scope.query();
			})
			$("#btn").click(function(){
				$scope.query();
			})
		}
		openModal($("#editFormModal"));
		$(".modal-body").css("padding","5px").css("overflow","visible");
		$(".modal-body").css("overflow","hidden");
		$("#mainDatas .ngViewport").height(180);
	};
	$scope.onSaveEditForm = function(){
		closeModal($("#editFormModal"));
		if($scope.editForm.planatime!=undefined){
			$scope.editForm.planatime = ymdhms2date($scope.editForm.planatime);
		}else{
			$scope.editForm.planatime=null;
		}
		if($scope.editForm.planltime!=undefined){
			$scope.editForm.planltime = ymdhms2date($scope.editForm.planltime);		
		}else{
			$scope.editForm.planltime=null;
		}
		httpPost($http, {"url":ctx + '/shipment/save', "data":$scope.editForm, "ifBlock":true}, function(data){
			toastSuccess("保存成功");
			$scope.query();
		}, function(errMsg){
			toastError("保存失败,您输入的信息有误，请检查正确之后再保存。" + errMsg);
		});
	};
	$scope.onOpenDelFormModal = function() {
		var selectedRowsLength = $scope.selectedRows.length;
		if(selectedRowsLength < 1) { toastWarning("请至少选择一笔记录进行删除。"); return; }
		dialogConfirm("是否确认删除选择的"+selectedRowsLength+"笔记录？", function(){
			var selectedRowsId = [];
			$.each($scope.selectedRows, function(idx, obj){
				selectedRowsId.push(obj.id);
			});
			
			httpPost($http, {"url":ctx + '/shipment/updStByIds', "params":{"ids":selectedRowsId, "st":1}, "ifBlock":true}, function(data){
				toastSuccess("删除成功");
				$scope.query();
			}, function(errMsg){
				toastError("删除失败。" + errMsg);
			});
		});
	};
	
	
});