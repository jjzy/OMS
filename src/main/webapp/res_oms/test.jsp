<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>Admin</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<link href="${ctx}/res/lib/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="${ctx}/res/lib/font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet" />
<link href="${ctx}/res/lib/jquery-ui/jquery-ui.min.css" type="text/css" rel="stylesheet" />

<link href="${ctx}/res/lib/toastr/toastr.min.css" rel="stylesheet">

<link href="${ctx}/res_oms/theme-b/smartadmin-production.min.css" type="text/css" rel="stylesheet" />
<link href="${ctx}/res_oms/theme-b/smartadmin-skins.min.css" type="text/css" rel="stylesheet" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="${ctx}/res/lib/common/html5shiv.js"></script>
  <script src="${ctx}/res/lib/common/respond.min.js"></script>
<![endif]-->

<!--[if lte IE 8]>
  <script src="${ctx}/res/lib/common/json2.js"></script>
<![endif]-->
<!--[if lte IE 8]>
  <script>
    document.createElement('ng-include');
    document.createElement('ng-pluralize');
    document.createElement('ng-view');

    // Optionally these for CSS
    document.createElement('ng:include');
    document.createElement('ng:pluralize');
    document.createElement('ng:view');
  </script>
<![endif]-->

<link rel="shortcut icon" href="${ctx}/res/favicon.png">
<script>
	var ctx = "<c:out value="${pageContext.request.contextPath}" />"
</script>
<script>
	var uri = "<c:out value="${requestScope['javax.servlet.forward.request_uri']}" />"
</script>

<style>
.ui-tabs {
	position: relative;
	padding: .2em .2em 0 .2em;
}

.ui-tabs .ui-tabs-nav {
	margin: 0;
	padding: 0;
}

.ui-tabs .ui-tabs-panel {
	display: block;
	border-width: 0;
	padding: 3px 3px 0 3px;
	background: none;
}
#left-panel{
	background: #EBE5E5;
}
.menu-on-top .menu-item-parent{
	font-size: 14px;
	font-family: "微软雅黑";
	font-weight: bold;
}
</style>
</head>

<body ng-controller="MyCtrl" class="desktop-detected menu-on-top smart-style-2">
	<aside id="left-panel">

		<!-- User info -->
		<span class="pull-left" style="margin:10px 10px 0 10px;">
			<a href="${ctx}/" id="show-shortcut" data-action="toggleShortcut">
				<img src="${ctx}/res_oms/theme-b/imgs/logo.png" style="height: 50px;">
			</a>
		</span>
		<nav>
			<ul id="test">
				<c:forEach var="sidebarZTreeItem" items="${sidebarZTree}" varStatus="status">
					<li class="">
						<a href="javascript:addTab('${sidebarZTreeItem.id}', '${sidebarZTreeItem.name}', '${sidebarZTreeItem.url}')" title="Dashboard">
							<c:if test="${not empty sidebarZTreeItem.icon}">
								<i class="${sidebarZTreeItem.icon}"></i>
							</c:if>
							<span class="menu-item-parent">${sidebarZTreeItem.name}</span>
						</a>
						<c:if test="${not empty sidebarZTreeItem.children}">
							<ul>
								<c:forEach var="sidebarZTree1Children" items="${sidebarZTreeItem.children}" varStatus="status">
									<li>
										<a href="javascript:addTab('${sidebarZTree1Children.id}', '${sidebarZTree1Children.name}', '${sidebarZTree1Children.url}')">
											<c:if test="${not empty sidebarZTree1Children.icon}">
												<i class="${sidebarZTree1Children.icon}"></i>
											</c:if>
											${sidebarZTree1Children.name}
										</a>
										<c:if test="${not empty sidebarZTree1Children.children}">
											<ul>
												<c:forEach var="sidebarZTree2Children" items="${sidebarZTree1Children.children}" varStatus="status">
													<li>
														<a href="javascript:addTab('${sidebarZTree2Children.id}', '${sidebarZTree2Children.name}', '${sidebarZTree2Children.url}')">
															<c:if test="${not empty sidebarZTree2Children.icon}">
																<i class="${sidebarZTree2Children.icon}"></i>
															</c:if>
															${sidebarZTree2Children.name}
														</a>
														<c:if test="${not empty sidebarZTree2Children.children}">
															<ul>
																<c:forEach var="sidebarZTree3Children" items="${sidebarZTree2Children.children}" varStatus="status">
																	<li>
																		<a href="javascript:addTab('${sidebarZTree3Children.id}', '${sidebarZTree3Children.name}', '${sidebarZTree3Children.url}')">
																			<c:if test="${not empty sidebarZTree3Children.icon}">
																				<i class="${sidebarZTree3Children.icon}"></i>
																			</c:if>
																			${sidebarZTree3Children.name}
																		</a>
																		<c:if test="${not empty sidebarZTree3Children.children}">
																			<ul>
																				<c:forEach var="sidebarZTree4Children" items="${sidebarZTree3Children.children}" varStatus="status">
																					<li>
																						<a href="javascript:addTab('${sidebarZTree4Children.id}', '${sidebarZTree4Children.name}', '${sidebarZTree4Children.url}')">
																							<c:if test="${not empty sidebarZTree4Children.icon}">
																								<i class="${sidebarZTree4Children.icon}"></i>
																							</c:if>
																							${sidebarZTree4Children.name}
																						</a>
																						<c:if test="${not empty sidebarZTree4Children.children}">
																							<ul>
																								<c:forEach var="sidebarZTree5Children" items="${sidebarZTree4Children.children}" varStatus="status">
																									<li>
																										<a href="javascript:addTab('${sidebarZTree5Children.id}', '${sidebarZTree5Children.name}', '${sidebarZTree5Children.url}')">
																											<c:if test="${not empty sidebarZTree5Children.icon}">
																												<i class="${sidebarZTree5Children.icon}"></i>
																											</c:if>
																											${sidebarZTree5Children.name}
																										</a>
																										<c:if test="${not empty sidebarZTree5Children.children}">
																											<ul>
																												<c:forEach var="sidebarZTree6Children" items="${sidebarZTree5Children.children}" varStatus="status">
																													<li>
																														<a href="javascript:addTab('${sidebarZTree6Children.id}', '${sidebarZTree6Children.name}', '${sidebarZTree6Children.url}')">
																															<c:if test="${not empty sidebarZTree6Children.icon}">
																																<i class="${sidebarZTree6Children.icon}"></i>
																															</c:if>
																															${sidebarZTree6Children.name}
																														</a>
																													</li>
																												</c:forEach>
																											</ul>
																										</c:if>
																									</li>
																								</c:forEach>
																							</ul>
																						</c:if>
																					</li>
																				</c:forEach>
																			</ul>
																		</c:if>
																	</li>
																</c:forEach>
															</ul>
														</c:if>
													</li>
												</c:forEach>
											</ul>
										</c:if>
									</li>
								</c:forEach>
							</ul>
						</c:if>
					</li>
				</c:forEach>

				<li class="pull-right" style="margin-right:95px;">
					<a href="#">
						<i class="fa fa-lg fa-fw fa-user"></i>
						<span class="menu-item-parent">${i.name}</span>
					</a>
					<ul>
						<li>
							<a href="${ctx}/logout">
								<i class="fa fa-sign-out"></i>
								注销
							</a>
						</li>
					</ul>
				</li>
				<li class="pull-right">
					<a href="javascript:void(0);">
						<i class="fa fa-lg fa-fw fa-sitemap"></i>
						<span class="menu-item-parent">${i.platformName}</span>
					</a>
					<ul ng-if="platformList!=null&&platformList.length>1">
						<li ng-repeat="platform in platformList">
							<a href="${ctx}/switchPlatform/{{platform.id}}">
								{{platform.name}}
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</nav>
	</aside>
	<!-- END NAVIGATION -->

	<!-- #MAIN PANEL -->
	<div id="main" role="main" style="padding: 5px 5px 0 5px;">
		<div id="tabs">
			<ul>
				<li>
					<a href="#tabs-0" style="display: none">主页</a>
				</li>
			</ul>
			<div id="tabs-0">
			</div>
		</div>
	</div>
</body>

<script src="${ctx}/res/lib/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="${ctx}/res/lib/jquery/jquery-migrate-1.2.1.js" type="text/javascript"></script>
<script src="${ctx}/res/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<script src="${ctx}/res_oms/theme-b/bootstrap.min.js" type="text/javascript"></script>

<script src="${ctx}/res/lib/angularjs/angular.min.js"></script>

<script src="${ctx}/res/lib/angular-ui/ng-grid/ng-grid-2.0.11.min.js"></script>
<script src="${ctx}/res/lib/angular-ui/ng-grid/zh-cn.js"></script>
<script src="${ctx}/res/lib/angular-ui/ng-grid/plugins/ng-grid-flexible-height.js"></script>

<script src="${ctx}/res/lib/select2/select2.js"></script>
<script src="${ctx}/res/lib/select2/select2_locale_zh-CN.js"></script>
<script src="${ctx}/res/lib/angular-ui/ui-select2/select2.js"></script>

<script src="${ctx}/res/lib/toastr/toastr.min.js"></script>
<script src="${ctx}/res/lib/jquery.blockUI.js" type="text/javascript"></script>

<script src="${ctx}/res/i/js/lib.js" type="text/javascript"></script>
<script src="${ctx}/res/i/js/utils.js" type="text/javascript"></script>

</html>

<script>
	var tabs = $("#tabs").tabs();
	var tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span><a href='r{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; r{label}</a></li>";

	function addTab(tabId, tabTitle, tabHref) {
		var tabCounter = $(".ui-tabs-nav li").length;
		tabCounter++;
		//validate
		var maxTabCounter = 5;
		if (tabCounter > maxTabCounter) {
			toastWarning("请不要打开超过" + maxTabCounter + "个标签页。");
			return;
		}
		if (tabHref.indexOf("javascript:") >= 0) {
			return;
		}
		var ifExistsTab = false;
		$.each($(".ui-tabs-panel"), function(idx, val) {
			if (("tabs-" + tabId) == val.id) {
				tabs.tabs({
					active : idx
				});
				console.log("ifExistsTab true");
				ifExistsTab = true;
				return false;
			}
		});
		if (ifExistsTab) {
			return;
		}
		//new tab
		tabId = "tabs-" + tabId;
		var li = $(tabTemplate.replace(/r\{href\}/g, "#" + tabId).replace(/r\{label\}/g, tabTitle));
		var websiteframe = '<iframe src="' + tabHref + '" width="100%" height="100%" allowtransparency="true" frameBorder="0">Your browser does not support IFRAME</iframe>';
		tabs.find(".ui-tabs-nav").append(li);
		tabs.append("<div align='center' id='" + tabId + "'>" + websiteframe + "</div>");
		tabs.tabs("refresh");
		//active last tab
		tabs.addClass("ui-tabs-vertical ui-helper-clearfix");
		$("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
		tabs.tabs({
			active : tabCounter - 1
		});

		resizeIframe();
	}

	function resizeIframe() {
		var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		$("iframe").attr("height", windowHeight - 135);
		//$("#tabs-25").height(windowHeight - 100);
	}
	$(function() {
		resizeIframe();
		$(window).resize(function() {
			resizeIframe();
		});

		tabs.delegate("span.delete-tab", "click", function() {
			var panelId = $(this).closest("li").remove().attr("aria-controls");
			$("#" + panelId).remove();
			tabs.tabs("refresh");
		});
	});

	var uid = "${i.id}";
	var myApp = angular.module('myApp', [ 'ui.select2' ]);
	myApp.controller('MyCtrl', function($scope, $http) {
		if (uid) {
			httpGet($http, {"url":ctx+"/user/role/platforms"}, function(data){
				$scope.platformList = data.dt;
				console.log(data.dt);
			}, function(errMsg){
				toastError("获取平台列表失败。");
			});
		}
	});
</script>
