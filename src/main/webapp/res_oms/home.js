$(function() {
	resizeIframe();
	$(window).resize(function() {
		resizeIframe();
	});
	tabs.delegate("span.delete-tab", "click", function() {
		var panelId = $(this).closest("li").remove().attr("aria-controls");
		$("#" + panelId).remove();
		tabs.tabs("refresh");
	});
});

var myApp = angular.module('myApp', [ 'ui.select2' ]);
myApp.controller('MyCtrl', function($scope, $http) {
	if (uid) {
		httpGet($http, {
			"url" : ctx + "/user/role/platforms"
		}, function(data) {
			$scope.platformList = data.dt;
		}, function(errMsg) {
			toastError("获取平台列表失败。");
		});

		httpGet($http, {
			"url" : ctx + "/user/role/menu/tree/" + uid + "/" + platformId
		}, function(data) {
			var html = buildMenu(data.dt);
			$("#topMenu").before(html);
			$("nav>ul>li>a").css("font-size","14px").css("line-height","30px"); 
			$("nav ul li ul ").css("width","125px");
			$("nav ul li ul li a").css("font-size","12px");
		}, function(errMsg) {
			toastError("获取菜单列表失败。" + errMsg);
		});

		httpGet($http, {
			"url" : ctx + '/findImportList',
			"ifBlock" : true
		}, function(data) {
			$scope.importList = data.dt;
		}, function(msg) {
			toastError("初始化导入列表失败。" + msg);
		});
	}

	$scope.openChangePasswordFormModal = function() {
		$scope.changePasswordForm = {};
		openModal($("#changePasswordFormModal"));
	};
	$scope.onSaveChangePasswordForm = function() {
		closeModal($("#changePasswordFormModal"));
		httpPost($http, {
			"url" : ctx + '/changepassword',
			"data" : $scope.changePasswordForm,
			"ifBlock" : true
		}, function(data) {
			toastSuccess("保存成功");
		}, function(errMsg) {
			toastError("保存失败。" + errMsg);
		});
	};
});

var tabs = $("#mainTabs").tabs();
var tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:7px; left:7px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span><a href='r{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; r{label}</a></li>";

function addTab(tabId, tabTitle, tabHref) {
	var tabCounter = $(".ui-tabs-nav li").length;
	tabCounter++;
	// validate
	var maxTabCounter=6 ;
	if (tabCounter >maxTabCounter) {
		toastWarning("请不要打开超过" + 5 + "个标签页。");
		return;
	}
	if (tabHref.indexOf("javascript:") >= 0) {
		return;
	}
	var ifExistsTab = false;
	$.each($(".ui-tabs-panel"), function(idx, val) {
		if (("tabs-" + tabId) == val.id) {
			tabs.tabs({
				active : idx
			});
			ifExistsTab = true;
			return false;
		}
	});
	if (ifExistsTab) {
		return;
	}
	// new tab
	tabId = "tabs-" + tabId;
	var li = $(tabTemplate.replace(/r\{href\}/g, "#" + tabId).replace(/r\{label\}/g, tabTitle));
	var websiteframe = '<iframe src="' + tabHref + '" width="100%" height="100%" allowtransparency="true" frameBorder="0">Your browser does not support IFRAME</iframe>';
	tabs.find(".ui-tabs-nav").append(li);
	tabs.append("<div align='center' id='" + tabId + "'>" + websiteframe + "</div>");
	tabs.tabs("refresh");
	// active last tab
	tabs.addClass("ui-tabs-vertical ui-helper-clearfix");
	$("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
	tabs.tabs({
		active : tabCounter - 1
	});
	resizeIframe();
}

function resizeIframe() {
	var windowHeight = $(window).height();
	var northHeight = $("#left-panel").height();
	var tabsHeight = $("#mainTabs ul").height();
	$("iframe").attr("height", windowHeight - northHeight - tabsHeight - 20);
}

function buildMenu(data) {
	var html = '';
	html += '<ul>';
	$.each(data, function(idx, row) {
		html += '<li>';
		html += '<a href="javascript:addTab(' + row.id + ', \'' + row.name + '\', \'' + row.url + '\')">';
		if (row.icon) {
			html += '<i class="' + row.icon + '"></i>';
		}
		html += row.name;
		html += '</a>';
		if (row.children) {
			html += buildMenu(row.children);
		}
		html += '</li>';
	});
	html += '</ul>';
	return html;
}