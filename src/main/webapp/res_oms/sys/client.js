$(function(){
	$('input[name="begDt"], input[name="endDt"]').datepicker({
		format: "yyyy-mm-dd",
	    todayBtn: "linked",
	    language: "zh-CN",
	    autoclose: true,
	    todayHighlight: true
	});

	var today = getDay(0);
	var yesterday = getDay(-1);
	$('input[name="begDt"]').val(yesterday);
	$('input[name="endDt"]').val(today);
});

var myApp = angular.module('myApp', [ 'ngGrid', 'ui.select2' ]);
myApp.controller('clientCtrl', function($scope, $http) {
	//global variable
	$scope.selectedRows = [];
	//init page item
	$scope.init = function() {
		$scope.stList = [{"id":0, "lb":"在用"},{"id":1, "lb":"停用"}];
		httpGet($http, {"url":ctx+"/user/role/platforms"}, function(data){
			$scope.platformList = data.dt;
		}, function(){
			toastError("初始化平台列表失败。");
		});
		
		httpPost($http, {"url":ctx+"/city/findListByFirst", "data": {"ffMap":{"st":0}}}, function(data){
			$scope.cityList = data.dt;
		}, function(){
			toastError("初始化城市列表失败。");
		});
		
		httpPost($http, {"url":ctx+"/client/findListBy", "data": {"ffMap":{"st":0}}}, function(data){
			$scope.cdList = data.dt;
		}, function(){
			toastError("初始化客户列表失败。");
		});
	};
	$scope.init();
	
	//grid
	$scope.pagingOptions = {
		pageSizes : [ 20, 50, 100 ],
		pageSize : 20,
		currentPage : 1,
		onQueryFlag : false
	};
	$scope.$watch('pagingOptions', function(newVal, oldVal) {
		if (newVal !== oldVal/* && newVal.currentPage !== oldVal.currentPage*/) {
			$scope.query();
		}
	}, true);
	$scope.gridOptions = {
		data : 'dt',
		totalServerItems : 'tt',
		
		columnDefs : [
		{
			field : "id",
			displayName : "ID", visible : false
		},
		{
			field : "cd",
			displayName : "代码",width:120
		},
		{
			field : "name",
			displayName : "名称",width:250
		},
		{
			field : "shortName",
			displayName : "缩写",width:140
		},
		{
			field : "cityName",
			displayName : "城市"
		},
		{
			field : "descr",
			displayName : "描述"
		},
		{
			field : "linkMan",
			displayName : "联系人"
		},
		{
			field : "phone",
			displayName : "电话"
		},
		{
			field : "email",
			displayName : "邮件"
		},
		{
			field : "addr",
			displayName : "地址",width:200
		},
		{
			field : "postCd",
			displayName : "邮编"
		},
		{
			field : "st",
			displayName : "状态"
		}
		],
		enablePaging : true,
		pagingOptions : $scope.pagingOptions,
		enableCellEdit:true,
		//enablePinning : true,
		//showColumnMenu : true,
		//showGroupPanel : true,
		showSelectionCheckbox : true,
		selectedItems : $scope.selectedRows,
		i18n: 'zh-cn', rowHeight : 20.2, footerRowHeight : 35, //plugins : [/*new ngGridFlexibleHeightPlugin()*/ ],
		showFooter : true
	};

	$scope.query = function() {
		closeModal($("#queryFormModal"));
		var requestBody = {
			pageNb : $scope.pagingOptions.currentPage,
			pageSize : $scope.pagingOptions.pageSize,
			ffMap : $scope.queryForm
		};
		setTimeout(function() {
			httpPost($http, {"url":ctx + '/client/findListBy', "data":requestBody, "ifBlock":true}, function(data){
				$scope.tt = data.tt;
				$scope.dt = data.dt;				
				$scope.gridOptions.$gridScope.toggleSelectAll(false);
			}, function(errMsg){
				toastError("查询失败。" + errMsg);
			});
		}, 100);
	};
	//trigger query when currentPage not change
	$scope.onQuery = function(){
		$scope.pagingOptions.onQueryFlag = !$scope.pagingOptions.onQueryFlag;
		$scope.pagingOptions.currentPage = 1;
		if($scope.queryForm.st==0){
			$scope.flag=false;
		}
		if($scope.queryForm.st==1){
			$scope.flag=true;
		}
	};

	$scope.refresh=function(){
		$scope.onQuery();
	};

	//utils
	$scope.onClearSelectedRows = function() {
		$scope.gridOptions.$gridScope.toggleSelectAll(false);
	};
	
	//crud
	$scope.onOpenQueryFormModal = function() {
		openModal($("#queryFormModal"));
		if($scope.queryForm==undefined){
			$scope.queryForm={};
			$scope.queryForm.st=0;
		}
	};
	
	//导入
	$scope.onOpenImportFormModal = function(){
		$scope.importForm={};
		$scope.importForm.importUrl="/client/import";
		$("input[name='file']").css("width","200px").css("height","30px").css("padding-bottom","2px");
		$(".col-sm-3").width(30);
		openModal($("#importFormModal"));
	};
	
	$scope.onImport = function(){
		closeModal($("#importFormModal"));
		$("form[name='importFormForm']").submit();
	};
	//下载模板
	$scope.onExportTemplate = function(){
		if(isNull($scope.importForm) || isNull($scope.importForm.importUrl)){
			toastWarning("请选择导入项，以下载相应模板。");
			return;
		}
		window.open(ctx + $scope.importForm.importUrl + '/template');
	};
	//导出
	$scope.onOpenExportFormModal=function(){	
		var params;
		if($scope.queryForm==undefined){
			params="";
		}else{
			params = $scope.queryForm;
		}
		var str = jQuery.param(params);
		window.open(ctx  + '/client/export?'+str);
	};
	
	$scope.onOpenEditFormModal = function(type, id) {
		if(type=="add"){
			$scope.editForm = {};
			$scope.editForm.st = 0;
		}else if(type=="upd"){
			if($scope.selectedRows.length != 1) { toastWarning("请选择一笔记录进行编辑。"); return; }
			httpGet($http, {"url":ctx+'/client/id/'+$scope.selectedRows[0]['id'], "ifBlock":true}, function(data){
				$scope.editForm = data.dt;
			}, function(errMsg){
				toastWarning("查询此笔记录失败。");
			});
		}
		openModal($("#editFormModal"));
	};
	$scope.onSaveEditForm = function(){
		closeModal($("#editFormModal"));
		httpPost($http, {"url":ctx + '/client/save', "data":$scope.editForm, "ifBlock":true}, function(data){
			toastSuccess("保存成功");
			$scope.query();
		}, function(errMsg){
			toastError("保存失败,您输入的信息有误，请检查正确之后再保存。" + errMsg);
		});
	};
	$scope.onOpenDelFormModal = function() {
		var selectedRowsLength = $scope.selectedRows.length;
		if(selectedRowsLength < 1) { toastWarning("请至少选择一笔记录进行操作。"); return; }
		dialogConfirm("是否确认停用选择的"+selectedRowsLength+"笔记录？", function(){
			var selectedRowsId = [];
			$.each($scope.selectedRows, function(idx, obj){
				selectedRowsId.push(obj.id);
			});
			
			httpPost($http, {"url":ctx + '/client/updStByIds', "params":{"ids":selectedRowsId, "st":1}, "ifBlock":true}, function(data){
				toastSuccess("停用成功");
				$scope.query();
			}, function(errMsg){
				toastError("停用失败。" + errMsg);
			});
		});
	};
});