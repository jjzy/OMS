$(function(){
	
});

var myApp = angular.module('myApp', [ 'ngGrid', 'ui.select2' ]);
myApp.controller('MyCtrl', function($scope, $http) {
	//global variable
	$scope.selectedRows = [];
	//init page item
	$scope.init = function() {
		$scope.stList = [{"id":0, "lb":"在用"},{"id":1, "lb":"停用"}];
	};
	$scope.init();
	
	//grid
	$scope.pagingOptions = {
		pageSizes : [ 20, 50, 100 ],
		pageSize : 20,
		currentPage : 1,
		onQueryFlag : false
	};
	$scope.$watch('pagingOptions', function(newVal, oldVal) {
		if (newVal !== oldVal/* && newVal.currentPage !== oldVal.currentPage*/) {
			$scope.query();
		}
	}, true);
	$scope.gridOptions = {
		data : 'dt',
		totalServerItems : 'tt',
		
		columnDefs : [
		{
			field : "id",
			displayName : "ID", visible : false
		},
		{
			field : "account",
			displayName : "登陆名"
		},
		{
			field : "password",
			displayName : "密码"
		},
		{
			field : "name",
			displayName : "名称"
		},
		{
			field : "email",
			displayName : "邮箱"
		},
		{
			field : "homePage",
			displayName : "个人主页"
		},
		{
			field : "remark",
			displayName : "备注"
		},
		{
			field : "st",
			displayName : "状态"
		},
		{
			field : "sortNb",
			displayName : "排序"
		}
		],
		enablePaging : true,
		pagingOptions : $scope.pagingOptions,
		enableCellEdit:true,
		//enableCellSelection:true,
		//enablePinning : true,
		//showColumnMenu : true,
		//showGroupPanel : true,
		showSelectionCheckbox : true,
		selectedItems : $scope.selectedRows,
		i18n: 'zh-cn', rowHeight : 20.2, footerRowHeight : 35, //plugins : [ngGridDoubleClick ],
		showFooter : true
	};

	$scope.query = function() {
		closeModal($("#queryFormModal"));
		
		var requestBody = {
			pageNb : $scope.pagingOptions.currentPage,
			pageSize : $scope.pagingOptions.pageSize,
			ffMap : $scope.queryForm
		};
		setTimeout(function() {
			httpPost($http, {"url":ctx + '/user/findListBy', "data":requestBody, "ifBlock":true}, function(data){
				$scope.tt = data.tt;
				$scope.dt = data.dt;
				$scope.gridOptions.$gridScope.toggleSelectAll(false);
			}, function(errMsg){
				toastError("查询失败。" + errMsg);
			});
		}, 100);
	};
	//trigger query when currentPage not change
	$scope.onQuery = function(){
		$scope.pagingOptions.onQueryFlag = !$scope.pagingOptions.onQueryFlag;
		$scope.pagingOptions.currentPage = 1;
		if($scope.queryForm.st==0){
			$scope.flag=false;
		}
		if($scope.queryForm.st==1){
			$scope.flag=true;
		}
	};
	
	$scope.refresh=function(){
		$scope.onQuery();	
	};

	//utils
	$scope.onClearSelectedRows = function() {
		$scope.gridOptions.$gridScope.toggleSelectAll(false);
	};
	
	//crud
	$scope.onOpenQueryFormModal = function() {
		openModal($("#queryFormModal"));
		if($scope.queryForm==undefined){
			$scope.queryForm={};
			$scope.queryForm.st=0;
		}
	};
	
	$scope.onOpenEditFormModal = function(type, id) {
		if(type=="add"){
			$scope.editForm = {};
			$scope.editForm.st = 0;
		}else if(type=="upd"){
			if($scope.selectedRows.length != 1) { toastWarning("请选择一笔记录进行编辑。"); return; }
			httpGet($http, {"url":ctx+'/user/id/'+$scope.selectedRows[0]['id'], "ifBlock":true}, function(data){
				$scope.editForm = data.dt;
			}, function(errMsg){
				toastWarning("查询此笔记录失败。");
			});
		}
		openModal($("#editFormModal"));
	};
	$scope.onSaveEditForm = function(){
		closeModal($("#editFormModal"));
		console.log($scope.editForm);
		httpPost($http, {"url":ctx + '/user/save', "data":$scope.editForm, "ifBlock":true}, function(data){
			toastSuccess("保存成功");
			$scope.query();
		}, function(errMsg){
			toastError("保存失败,您输入的信息有误，请检查正确之后再保存。" + errMsg);
		});
	};
	$scope.onOpenDelFormModal = function() {
		var selectedRowsLength = $scope.selectedRows.length;
		if(selectedRowsLength < 1) { toastWarning("请至少选择一笔记录进行操作。"); return; }
		dialogConfirm("是否确认停用选择的"+selectedRowsLength+"笔记录？", function(){
			var selectedRowsId = [];
			$.each($scope.selectedRows, function(idx, obj){
				selectedRowsId.push(obj.id);
			});
			
			httpPost($http, {"url":ctx + '/user/updStByIds', "params":{"ids":selectedRowsId, "st":1}, "ifBlock":true}, function(data){
				toastSuccess("停用成功");
				$scope.query();
			}, function(errMsg){
				toastError("停用失败。" + errMsg);
			});
		});
	};
	
	$scope.onOpenUserRoleFormModal = function() {
		if($scope.selectedRows.length != 1) { toastWarning("请选择一笔记录进行编辑。"); return; }
		
		var userId = $scope.selectedRows[0].id;
		setTimeout(function() {
			httpGet($http, {"url":ctx+"/user/role/select/"+userId, "ifBlock":true}, function(data){
				$scope.userRoles = data.dt;
				 // selected fruits
				$scope.userRoleSelection = [];
				$.each(data.dt, function(idx, val) {
					var roleId = val.id;
					if(isNotNull(val.userId)){
						$scope.userRoleSelection.push(roleId);
					}
			    });
				
				openModal($("#userRoleFormModal"));
			}, function(msg){
				toastError("获取用户角色列表失败。"+msg);
			});
		}, 100);
	};
	$scope.toggleUserRoleSelection = function(roleId) {
	    var idx = $scope.userRoleSelection.indexOf(roleId);
	    if (idx > -1) {// is currently selected
	      $scope.userRoleSelection.splice(idx, 1);
	    } else {// is newly selected
	      $scope.userRoleSelection.push(roleId);
	    }
	};
	
	$scope.onSaveUserRoleForm = function(){
		closeModal($("#userRoleFormModal"));
		
		var userId = $scope.selectedRows[0].id;
		var roleIds = $scope.userRoleSelection;
		
		
		httpPost($http, {"url":ctx + '/user/role/save', "params":{"userId":userId, "roleIds": roleIds}, "ifBlock":true}, function(data){
			toastSuccess("保存用户角色成功");
			$scope.query();
		}, function(errMsg){
			toastError("保存用户角色失败。" + errMsg);
		});
	};
	
	$scope.onOpenUserClientFormModal = function() {
		if($scope.selectedRows.length != 1) { toastWarning("请选择一笔记录进行编辑。"); return; }
		
		var userId = $scope.selectedRows[0].id;
		setTimeout(function() {
			httpGet($http, {"url":ctx+"/user/client/select/"+userId, "ifBlock":true}, function(data){
				$scope.userClients = data.dt;
				 // selected fruits
				$scope.userClientSelection = [];
				$.each(data.dt, function(idx, val) {
					var clientId = val.id;
					if(isNotNull(val.userId)){
						$scope.userClientSelection.push(clientId);
					}
			    });
				
				openModal($("#userClientFormModal"));
			}, function(msg){
				toastError("获取用户客户列表失败。"+msg);
			});
		}, 100);
	};
	$scope.toggleUserClientSelection = function(clientId) {
	    var idx = $scope.userClientSelection.indexOf(clientId);
	    if (idx > -1) {// is currently selected
	      $scope.userClientSelection.splice(idx, 1);
	    } else {// is newly selected
	      $scope.userClientSelection.push(clientId);
	    }
	};
	
	$scope.onSaveUserClientForm = function(){
		closeModal($("#userClientFormModal"));
		
		var userId = $scope.selectedRows[0].id;
		var clientIds = $scope.userClientSelection;
		
		
		httpPost($http, {"url":ctx + '/user/client/save', "params":{"userId":userId, "clientIds": clientIds}, "ifBlock":true}, function(data){
			toastSuccess("保存用户客户成功");
			$scope.query();
		}, function(errMsg){
			toastError("保存用户客户失败。" + errMsg);
		});
	};
});