var zTree;
var pid = null;

function initTree($scope, $http, pid){
	var setting = {
		callback:{
			onClick:function(event, treeId, treeNode){
				var selectedId = treeNode.id;
				$scope.editForm.pid = selectedId;
				event.preventDefault();
			}
		}
	};
	httpGet($http, {"url":ctx+"/menu/tree"}, function(data){
		zTree = $.fn.zTree.init($("#menuTree"), setting, data.dt);
		//zTree.expandAll(true);
		if(pid!=null){
			var selectedNode = zTree.getNodeByParam("id", pid, null);
			zTree.selectNode(selectedNode);
			zTree.expandNode(selectedNode, true);
		}
	}, function(msg){
		toastError("获取树节点失败。"+msg);
	});
}

$(function(){
	
});

var myApp = angular.module('myApp', [ 'ngGrid', 'ui.select2' ]);
myApp.controller('MyCtrl', function($scope, $http) {
	//global variable
	$scope.selectedRows = [];
	//init page item
	$scope.init = function() {
		$scope.stList = [{"id":0, "lb":"在用"},{"id":1, "lb":"停用"}];
		$scope.ifShowList = [{"id":1, "lb":"是"},{"id":0, "lb":"否"}];
		$scope.targetList = ["_self","_blank"];
	};
	$scope.init();
	
	$scope.clearPid = function(){
		pid = null;
		zTree.cancelSelectedNode(zTree.getNodeByParam("id", pid, null));
		$scope.editForm.pid = null;
		console.log($scope.editForm);
		$("input[name='pid']").val(null);
	};
	
	//grid
	$scope.pagingOptions = {
		pageSizes : [ 20, 50, 100 ],
		pageSize : 20,
		currentPage : 1,
		onQueryFlag : false
	};
	$scope.$watch('pagingOptions', function(newVal, oldVal) {
		if (newVal !== oldVal/* && newVal.currentPage !== oldVal.currentPage*/) {
			$scope.query();
		}
	}, true);
	$scope.gridOptions = {
		data : 'dt',
		totalServerItems : 'tt',
		
		columnDefs : [
		{
			field : "id",
			displayName : "ID", visible : false
		},
		{
			field : "name",
			displayName : "名称"
		},
		{
			field : "pid",
			displayName : "Parent ID", visible : false
		},
		{
			field : "pidName",
			displayName : "父菜单"
		},
		{
			field : "href",
			displayName : "菜单链接"
		},
		{
			field : "target",
			displayName : "链接方式"
		},
		{
			field : "icon",
			displayName : "icon"
		},
		{
			field : "pemisi",
			displayName : "权限标识"
		},
		{
			field : "ifShow",
			displayName : "是否菜单"
		},
		{
			field : "remark",
			displayName : "备注"
		},
		{
			field : "st",
			displayName : "状态"
		},
		{
			field : "sortNb",
			displayName : "排序"
		}
		],
		enablePaging : true,
		pagingOptions : $scope.pagingOptions,
		enableCellEdit:true,
		//enableCellSelection:true,
		//enablePinning : true,
		//showColumnMenu : true,
		//showGroupPanel : true,
		showSelectionCheckbox : true,
		selectedItems : $scope.selectedRows,
		i18n: 'zh-cn', rowHeight : 20.2, footerRowHeight : 35, //plugins : [/*new ngGridFlexibleHeightPlugin()*/ ],
		showFooter : true
	};

	$scope.query = function() {
		closeModal($("#queryFormModal"));
		
		var requestBody = {
			pageNb : $scope.pagingOptions.currentPage,
			pageSize : $scope.pagingOptions.pageSize,
			ffMap : $scope.queryForm
		};
		setTimeout(function() {
			httpPost($http, {"url":ctx + '/menu/findListBy', "data":requestBody, "ifBlock":true}, function(data){
				$scope.tt = data.tt;
				$scope.dt = data.dt;
				
				$scope.gridOptions.$gridScope.toggleSelectAll(false);
			}, function(errMsg){
				toastError("查询失败。" + errMsg);
			});
		}, 100);
	};
	//trigger query when currentPage not change
	$scope.onQuery = function(){
		$scope.pagingOptions.onQueryFlag = !$scope.pagingOptions.onQueryFlag;
		$scope.pagingOptions.currentPage = 1;
		if($scope.queryForm.st==0){
			$scope.flag=false;
		}
		if($scope.queryForm.st==1){
			$scope.flag=true;
		}
	};

	$scope.refresh=function(){
		$scope.onQuery();	
	};

	//utils
	$scope.onClearSelectedRows = function() {
		$scope.gridOptions.$gridScope.toggleSelectAll(false);
	};
	
	//crud
	$scope.onOpenQueryFormModal = function() {
		openModal($("#queryFormModal"));
		if($scope.queryForm==undefined){
			$scope.queryForm={};
			$scope.queryForm.st=0;
		}
	};
	
	$scope.onOpenEditFormModal = function(type, id) {
		if(type=="add"){
			$scope.editForm = {};
			$scope.editForm.st = 0;
			$("input[name='pid']").val(null);
			initTree($scope, $http, null);
		}else if(type=="upd"){
			if($scope.selectedRows.length != 1) { toastWarning("请选择一笔记录进行编辑。"); return; }
			httpGet($http, {"url":ctx+'/menu/id/'+$scope.selectedRows[0]['id'], "ifBlock":true}, function(data){
				$scope.editForm = data.dt;
				initTree($scope, $http, $scope.editForm.pid);
			}, function(errMsg){
				toastWarning("查询此笔记录失败。");
			});
		}
		openModal($("#editFormModal"));
	};
	$scope.onSaveEditForm = function(){
		delete $scope.editForm["pidName"];
		closeModal($("#editFormModal"));
		httpPost($http, {"url":ctx + '/menu/save', "data":$scope.editForm, "ifBlock":true}, function(data){
			toastSuccess("保存成功");
			$scope.query();
		}, function(errMsg){
			toastError("保存失败,您输入的信息有误，请检查正确之后再保存。" + errMsg);
		});
	};
	$scope.onOpenDelFormModal = function() {
		var selectedRowsLength = $scope.selectedRows.length;
		if(selectedRowsLength < 1) { toastWarning("请至少选择一笔记录进行操作。"); return; }
		dialogConfirm("是否确认停用选择的"+selectedRowsLength+"笔记录？", function(){
			var selectedRowsId = [];
			$.each($scope.selectedRows, function(idx, obj){
				selectedRowsId.push(obj.id);
			});
			
			httpPost($http, {"url":ctx + '/menu/updStByIds', "params":{"ids":selectedRowsId, "st":1}, "ifBlock":true}, function(data){
				toastSuccess("停用成功");
				$scope.query();
			}, function(errMsg){
				toastError("停用失败。" + errMsg);
			});
		});
	};
});