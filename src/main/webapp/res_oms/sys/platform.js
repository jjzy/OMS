$(function() {

});

var myApp = angular.module('myApp', [ 'ngGrid', 'ui.select2' ]);
myApp.controller('MyCtrl', function($scope, $http) {
	// global variable
	$scope.selectedRows = [];
	// init page item
	$scope.init = function() {
		$scope.stList = [ {
			"id" : 0,
			"lb" : "在用"
		}, {
			"id" : 1,
			"lb" : "停用"
		} ];
		httpPost($http, {
			"url" : ctx + "/city/findListByFirst",
			"data" : {
				"ffMap" : {
					"st" : 0
				}
			}
		}, function(data) {
			$scope.cityList = data.dt;
		}, function() {
			toastError("初始化城市列表失败。");
		});
	};
	$scope.init();

	// grid
	$scope.pagingOptions = {
		pageSizes : [ 20, 50, 100 ],
		pageSize : 20,
		currentPage : 1,
		onQueryFlag : false
	};
	$scope.$watch('pagingOptions', function(newVal, oldVal) {
		if (newVal !== oldVal/* && newVal.currentPage !== oldVal.currentPage */) {
			$scope.query();
		}
	}, true);
	$scope.gridOptions = {
		data : 'dt',
		totalServerItems : 'tt',
		columnDefs : [ {
			field : "id",
			displayName : "ID",
			visible : false
		}, {
			field : "cd",
			displayName : "代码"
		}, {
			field : "name",
			displayName : "名称"
		}, {
			field : "shortName",
			displayName : "缩写"
		}, {
			field : "cityId",
			displayName : "城市",
			visible : false
		}, {
			field : "cityName",
			displayName : "城市"
		}, {
			field : "descr",
			displayName : "描述"
		}, {
			field : "linkMan",
			displayName : "联系人"
		}, {
			field : "phone",
			displayName : "电话"
		}, {
			field : "fax",
			displayName : "传真"
		}, {
			field : "email",
			displayName : "邮件"
		}, {
			field : "st",
			displayName : "状态"
		}, {
			field : "sortNb",
			displayName : "排序"
		} ],
		enablePaging : true,
		pagingOptions : $scope.pagingOptions,
		enableCellEdit:true,
		//enableCellSelection:true,
		// enablePinning : true,
		// showColumnMenu : true,
		// showGroupPanel : true,
		showSelectionCheckbox : true,
		selectedItems : $scope.selectedRows,
		i18n : 'zh-cn',
		rowHeight : 20.2,
		footerRowHeight : 35,
		plugins : [],
		showFooter : true
	};

	$scope.query = function() {
		closeModal($("#queryFormModal"));

		var requestBody = {
			pageNb : $scope.pagingOptions.currentPage,
			pageSize : $scope.pagingOptions.pageSize,
			ffMap : $scope.queryForm
		};
		setTimeout(function() {
			httpPost($http, {
				"url" : ctx + '/platform/findListBy',
				"data" : requestBody,
				"ifBlock" : true
			}, function(data) {
				$scope.tt = data.tt;
				$scope.dt = data.dt;

				$scope.gridOptions.$gridScope.toggleSelectAll(false);
			}, function(errMsg) {
				toastError("查询失败。" + errMsg);
			});
		}, 100);
	};
	// trigger query when currentPage not change
	$scope.onQuery = function() {
		$scope.pagingOptions.onQueryFlag = !$scope.pagingOptions.onQueryFlag;
		$scope.pagingOptions.currentPage = 1;
		if($scope.queryForm.st==0){
			$scope.flag=false;
		}
		if($scope.queryForm.st==1){
			$scope.flag=true;
		}
	};

	$scope.refresh=function(){
		$scope.onQuery();	
	};

	// utils
	$scope.onClearSelectedRows = function() {
		$scope.gridOptions.$gridScope.toggleSelectAll(false);
	};

	// crud
	$scope.onOpenQueryFormModal = function() {
		openModal($("#queryFormModal"));
		if($scope.queryForm.st==undefined){
			$scope.queryForm.st=0;
		}
	};

	$scope.onOpenEditFormModal = function(type, id) {
		if (type == "add") {
			$scope.editForm = {};
			$scope.editForm.st = 0;
		} else if (type == "upd") {
			if($scope.selectedRows.length != 1) { toastWarning("请选择一笔记录进行编辑。"); return; }
			httpGet($http, {
				"url" : ctx + '/platform/id/'+$scope.selectedRows[0]['id'],
				"ifBlock" : true
			}, function(data) {
				$scope.editForm = data.dt;
			}, function(errMsg) {
				toastWarning("查询此笔记录失败。");
			});
		}
		openModal($("#editFormModal"));
	};
	$scope.onSaveEditForm = function() {
		closeModal($("#editFormModal"));
		httpPost($http, {
			"url" : ctx + '/platform/save',
			"data" : $scope.editForm,
			"ifBlock" : true
		}, function(data) {
			toastSuccess("保存成功");
			$scope.query();
		}, function(errMsg) {
			toastError("保存失败,您输入的信息有误，请检查正确之后再保存。" + errMsg);
		});
	};

	$scope.onOpenDelFormModal = function() {
		var selectedRowsLength = $scope.selectedRows.length;
		if (selectedRowsLength < 1) {
			toastWarning("请至少选择一笔记录进行操作。");
			return;
		}
		dialogConfirm("是否确认停用选择的" + selectedRowsLength + "笔记录？", function() {
			var selectedRowsId = [];
			$.each($scope.selectedRows, function(idx, obj) {
				selectedRowsId.push(obj.id);
			});

			httpPost($http, {
				"url" : ctx + '/platform/updStByIds',
				"params" : {
					"ids" : selectedRowsId,
					"st" : 1
				},
				"ifBlock" : true
			}, function(data) {
				toastSuccess("停用成功");
				$scope.query();
			}, function(errMsg) {
				toastError("停用失败。" + errMsg);
			});
		});
	};

	$scope.onOpenPlatformMenuFormModal = function() {
		if ($scope.selectedRows.length != 1) {
			toastWarning("请选择一笔记录进行编辑。");
			return;
		}

		var platformId = $scope.selectedRows[0].id;
		var setting = {
			check : {
				enable : true
			}
		};
		httpGet($http, {
			"url" : ctx + "/platform/menu/tree/" + platformId
		}, function(data) {
			// console.log(data);
			zTree = $.fn.zTree.init($("#platformMenuTree"), setting, data.dt);
			zTree.expandAll(true);

			openModal($("#platformMenuFormModal"));
		}, function(msg) {
			toastError("获取平台对应菜单列表失败。" + msg);
		});
	};

	$scope.onSavePlatformMenuForm = function() {
		closeModal($("#platformMenuFormModal"));

		var platformId = $scope.selectedRows[0].id;
		var menuIds = [];
		var checkedNodes = zTree.getCheckedNodes(true);
		$.each(checkedNodes, function(idx) {
			menuIds.push(this.id);
		});

		httpPost($http, {
			"url" : ctx + '/platform/menu/save',
			"params" : {
				"platformId" : platformId,
				"menuIds" : menuIds
			},
			"ifBlock" : true
		}, function(data) {
			toastSuccess("保存成功");
			$scope.query();
		}, function(errMsg) {
			toastError("保存失败。" + errMsg);
		});
	};
});