var $border_color = "#efefef";
var $grid_color = "#ddd";
var $default_black = "#666";
var $primary = "#575348";
var $secondary = "#A4DB79";
var $orange = "#F38733";

// SparkLine Bar
$(function () {
  $('#orders').sparkline([23213, 63216, 82234, 29341, 61789, 88732, 11234, 54328, 33245], {
    type: 'bar',
    barColor: [$orange, $secondary],
    barWidth: 6,
    height: 18,
  });

  $('#currentBalance').sparkline([33251, 98123, 54312, 88763, 12341, 55672, 87904, 23412, 17632], {
    type: 'bar',
    barColor: [$secondary, $primary],
    barWidth: 6,
    height: 18,
  });
  
});

//Date Range Picker
$(document).ready(function() {
	
});


//Tooltip
$('a').tooltip('hide');

//Popover
$('button').popover('hide');

