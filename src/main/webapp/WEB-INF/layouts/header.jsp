<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>

<style>
.dropdown-submenu {
	position: relative;
}

.dropdown-submenu>.dropdown-menu {
	top: 0;
	left: 100%;
	margin-top: -6px;
	margin-left: -1px;
	-webkit-border-radius: 0 6px 6px 6px;
	-moz-border-radius: 0 6px 6px 6px;
	border-radius: 0 6px 6px 6px;
}
/*.dropdown-submenu:hover>.dropdown-menu{display:block;}*/
.dropdown-submenu>a:after {
	display: block;
	content: " ";
	float: right;
	width: 0;
	height: 0;
	border-color: transparent;
	border-style: solid;
	border-width: 5px 0 5px 5px;
	border-left-color: #cccccc;
	margin-top: 5px;
	margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
	border-left-color: #ffffff;
}

.dropdown-submenu.pull-left {
	float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
	left: -100%;
	margin-left: 10px;
	-webkit-border-radius: 6px 0 6px 6px;
	-moz-border-radius: 6px 0 6px 6px;
	border-radius: 6px 0 6px 6px;
}
</style>

<!-- Header start -->
<header class="navbar" role="navigation">
	<div class="navbar-header">
		<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse" data-original-title="" title="">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a href="#" class="navbar-brand" data-original-title="" title="">&nbsp;</a>
	</div>
	<nav class="navbar-collapse  collapse" role="navigation" style="height: 1px;">
		<ul class="nav navbar-nav">
			<c:forEach var="sidebarZTreeItem" items="${sidebarZTree}" varStatus="status">
				<li class="menu-item<c:if test="${not empty sidebarZTreeItem.children}"> dropdown</c:if>">
					<a href="javascript:addTab('${sidebarZTreeItem.id}', '${sidebarZTreeItem.name}', '${sidebarZTreeItem.url}')" <c:if test="${not empty sidebarZTreeItem.children}"> class="dropdown-toggle" data-toggle="dropdown"</c:if> data-original-title="" title="">${sidebarZTreeItem.name}</a>
					<c:if test="${not empty sidebarZTreeItem.children}">
						<ul class="dropdown-menu">
							<c:forEach var="sidebarZTree1Children" items="${sidebarZTreeItem.children}" varStatus="status">
								<li class="menu-item<c:if test="${not empty sidebarZTree1Children.children}"> dropdown dropdown-submenu</c:if>">
									<a href="javascript:addTab('${sidebarZTree1Children.id}', '${sidebarZTree1Children.name}', '${sidebarZTree1Children.url}')" <c:if test="${not empty sidebarZTree1Children.children}"> class="dropdown-toggle" data-toggle="dropdown"</c:if> data-original-title="" title="">${sidebarZTree1Children.name}</a>
									<c:if test="${not empty sidebarZTree1Children.children}">
										<ul class="dropdown-menu">
											<c:forEach var="sidebarZTree2Children" items="${sidebarZTree1Children.children}" varStatus="status">
												<li class="menu-item<c:if test="${not empty sidebarZTree2Children.children}"> dropdown dropdown-submenu</c:if>">
													<a href="javascript:addTab('${sidebarZTree2Children.id}', '${sidebarZTree2Children.name}', '${sidebarZTree2Children.url}')" <c:if test="${not empty sidebarZTree2Children.children}"> class="dropdown-toggle" data-toggle="dropdown"</c:if> data-original-title="" title="">${sidebarZTree2Children.name}</a>
													<c:if test="${not empty sidebarZTree2Children.children}">
														<ul class="dropdown-menu">
															<c:forEach var="sidebarZTree3Children" items="${sidebarZTree2Children.children}" varStatus="status">
																<li class="menu-item<c:if test="${not empty sidebarZTree3Children.children}"> dropdown dropdown-submenu</c:if>">
																	<a href="javascript:addTab('${sidebarZTree3Children.id}', '${sidebarZTree3Children.name}', '${sidebarZTree3Children.url}')" <c:if test="${not empty sidebarZTree3Children.children}"> class="dropdown-toggle" data-toggle="dropdown"</c:if> data-original-title="" title="">${sidebarZTree3Children.name}</a>
																	<c:if test="${not empty sidebarZTree3Children.children}">
																		<ul class="dropdown-menu">
																			<c:forEach var="sidebarZTree4Children" items="${sidebarZTree3Children.children}" varStatus="status">
																				<li class="menu-item<c:if test="${not empty sidebarZTree4Children.children}"> dropdown dropdown-submenu</c:if>">
																					<a href="javascript:addTab('${sidebarZTree4Children.id}', '${sidebarZTree4Children.name}', '${sidebarZTree4Children.url}')" <c:if test="${not empty sidebarZTree4Children.children}"> class="dropdown-toggle" data-toggle="dropdown"</c:if> data-original-title="" title="">${sidebarZTree4Children.name}</a>
																					<c:if test="${not empty sidebarZTree4Children.children}">
																						<ul class="dropdown-menu">
																							<c:forEach var="sidebarZTree5Children" items="${sidebarZTree4Children.children}" varStatus="status">
																								<li class="menu-item<c:if test="${not empty sidebarZTree5Children.children}"> dropdown dropdown-submenu</c:if>">
																									<a href="javascript:addTab('${sidebarZTree5Children.id}', '${sidebarZTree5Children.name}', '${sidebarZTree5Children.url}')" <c:if test="${not empty sidebarZTree5Children.children}"> class="dropdown-toggle" data-toggle="dropdown"</c:if> data-original-title="" title="">${sidebarZTree5Children.name}</a>
																									<c:if test="${not empty sidebarZTree5Children.children}">
																										<ul class="dropdown-menu">
																											<c:forEach var="sidebarZTree6Children" items="${sidebarZTree5Children.children}" varStatus="status">
																												<li class="menu-item<c:if test="${not empty sidebarZTree6Children.children}"> dropdown dropdown-submenu</c:if>">
																													<a href="javascript:addTab('${sidebarZTree6Children.id}', '${sidebarZTree6Children.name}', '${sidebarZTree6Children.url}')" <c:if test="${not empty sidebarZTree6Children.children}"> class="dropdown-toggle" data-toggle="dropdown"</c:if> data-original-title="" title="">${sidebarZTree6Children.name}</a>
																												</li>
																											</c:forEach>
																										</ul>
																									</c:if>
																								</li>
																							</c:forEach>
																						</ul>
																					</c:if>
																				</li>
																			</c:forEach>
																		</ul>
																	</c:if>
																</li>
															</c:forEach>
														</ul>
													</c:if>
												</li>
											</c:forEach>
										</ul>
									</c:if>
								</li>
							</c:forEach>
						</ul>
					</c:if>
				</li>
			</c:forEach>
		</ul>

		<!-- Mini navigation start -->
		<ul class="nav navbar-nav navbar-right hidden-sm hidden-xs">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-original-title="" title="">
					<i class="fa fa-user"></i> ${i.name} <b class="caret"></b>
				</a>
				<ul class="dropdown-menu">
					<li>
						<a href="${ctx}/logout" data-original-title="" title="">Logout</a>
					</li>
				</ul>
			</li>
		</ul>
		<!-- Mini navigation end -->

	</nav>
</header>
<!-- Header end -->