<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>

<c:set var="uri"  value="${requestScope['javax.servlet.forward.request_uri']}"/>

<!-- Top bar start -->
<div class="top-bar">
  
  <div class="logo">
    OMS<sup><i class="fa fa-cloud"></i></sup>
  </div>

  <!-- Icon nav start -->
  <ul id="icon-nav">
    <li>
      <a href="http://trisandhya.com/cloud/blankpage.html#" data-original-title="" title="">
        <i class="fa fa-bell"></i>
        <span class="count-label"></span>
      </a>
    </li>
    <li>
      <a href="http://trisandhya.com/cloud/blankpage.html#" data-original-title="" title="">
        <i class="fa fa-comment-o"></i>
        <span class="count-label count-lb-yellow"></span>
      </a>
    </li>
    <li>
      <a href="http://trisandhya.com/cloud/blankpage.html#" data-original-title="" title="">
        <i class="fa fa-envelope"></i>
        <span class="count-label count-lb-green"></span>
      </a>
    </li>
  </ul>
  <!-- Icon nav end -->


  <!-- Search bar start -->
  <div id="topbarPlatformListElem" style="margin:0 10px 0 12px; float:right;">
  	
  </div>
  <!-- Search bar end -->

</div>
<!-- Top bar end -->
