<%@ page contentType="text/html;charset=UTF-8"%>

<link href="${ctx}/res/lib/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="${ctx}/res/lib/font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet" />

<link href="${ctx}/res/lib/bootstrap-modal/css/bootstrap-modal-bs3patch.css" type="text/css" rel="stylesheet" />
<link href="${ctx}/res/lib/bootstrap-modal/css/bootstrap-modal.css" type="text/css" rel="stylesheet" />
<link href="${ctx}/res/lib/i/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<link href="${ctx}/res/lib/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link href="${ctx}/res_oms/theme-b/smartadmin-production.min.css" type="text/css" rel="stylesheet" />
<link href="${ctx}/res_oms/theme-b/smartadmin-skins.min.css" type="text/css" rel="stylesheet" />

<link href="${ctx}/res/lib/angular-ui/ng-grid/ng-grid.min.css" rel="stylesheet">

<link href="${ctx}/res/lib/toastr/toastr.min.css" rel="stylesheet">
<link href="${ctx}/res_oms/theme/jquery.contextmenu.css" rel="stylesheet">
<!-- Main CSS -->

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="${ctx}/res/lib/common/html5shiv.js"></script>
  <script src="${ctx}/res/lib/common/respond.min.js"></script>
<![endif]-->

<!--[if lte IE 8]>
  <script src="${ctx}/res/lib/common/json2.js"></script>
<![endif]-->
<!--[if lte IE 8]>
  <script>
    document.createElement('ng-include');
    document.createElement('ng-pluralize');
    document.createElement('ng-view');

    // Optionally these for CSS
    document.createElement('ng:include');
    document.createElement('ng:pluralize');
    document.createElement('ng:view');
  </script>
<![endif]-->

<script>
	var ctx = "<c:out value="${pageContext.request.contextPath}" />";
</script>
<script>
	var uri = "<c:out value="${requestScope['javax.servlet.forward.request_uri']}" />";
</script>

<style>
#mainDataBlock {
	height: 100%;
	border: 1px solid rgb(212, 212, 212);
}

#funcBlock {
	padding: 5px;
	margin-top: 5px;
}
.ngCellText{
	border-left:1px solid #F1E7E7;
}
.ngHeaderText{
	border-left:1px solid #F1E7E7;
}
.modal11 {
	top: 30%;
}
.select2-hidden-accessible {
	display: none;
}

.ngFooterSelectedItems {
	padding-bottom: 5px;
	display: none;
}
.ngTopPanel{
	height: 25px;
}
.ngTotalSelectContainer {
	float: left;
	margin: 5px;
}
.modal-footer .btn{
	height:30px;
	padding:3px 8px;
}
.ngCellText{
	padding:2px;
}
.form-group{
	margin-bottom: 5px;
}
.form-group .col-sm-4{
	padding-right: 1px;
}
.ngHeaderScroller{
	background: #D6EEDE;
}
.bootbox{
	width:300px;
	border:2px solid #9CB4C5;
}
.bootbox .modal-body{
	height: 20px;
}
.bootbox .modal-footer{
	height: 20px;
	padding: 1px 8px 32px;
}
.bootbox .modal-body .close{
	border:1px solid #aaa;
	width:20px;
}
.ngRow.selected {
	border-top: 1px solid white;
}
#mainDatas .modal-body{
	overflow: hidden;
}
.ngSelectionCell{
	margin-top:3px;
	margin-left: 1px;
}
.today{
	text-align: center;
}
</style>