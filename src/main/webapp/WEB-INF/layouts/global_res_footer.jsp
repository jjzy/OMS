<%@ page contentType="text/html;charset=UTF-8"%>
<script src="${ctx}/res/lib/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="${ctx}/res/lib/jquery/jquery-migrate-1.2.1.js" type="text/javascript"></script>
<script src="${ctx}/res/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="${ctx}/res/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${ctx}/res/lib/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="${ctx}/res/lib/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="${ctx}/res/lib/bootbox/bootbox.js" type="text/javascript"></script>
<script src="${ctx}/res/lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="${ctx}/res/lib/bootstrap-datetimepicker/js/locale/bootstrap-datetimepicker.zh-CN.js" type="text/javascript"></script>
<script src="${ctx}/res/lib/angularjs/angular.min.js"></script>
<script src="${ctx}/res/lib/i/angularjs/ui-bootstrap-tpls-0.10.0.min.js"></script>
<script src="${ctx}/res/lib/i/angularjs/i-angular.js"></script>
<script src="${ctx}/res/lib/angular-ui/ng-grid/ng-grid-2.0.11.min.js"></script>
<script src="${ctx}/res/lib/angular-ui/ng-grid/zh-cn.js"></script>
<script src="${ctx}/res/lib/angular-ui/ng-grid/plugins/ng-grid-flexible-height.js"></script>

<script src="${ctx}/res/lib/select2/select2.js"></script>
<script src="${ctx}/res/lib/select2/select2_locale_zh-CN.js"></script>
<script src="${ctx}/res/lib/angular-ui/ui-select2/select2.js"></script>

<script src="${ctx}/res/lib/toastr/toastr.min.js"></script>

<script src="${ctx}/res/lib/jquery.blockUI.js" type="text/javascript"></script>

<script src="${ctx}/res/i/js/lib.js" type="text/javascript"></script>
<script src="${ctx}/res/i/js/utils.js" type="text/javascript"></script>
<script src="${ctx}/res_oms/theme/jquery.contextmenu.js" type="text/javascript"></script>

<script>
$(function() {
	resizeMainDataBlock();
	$(window).resize(function() {
		resizeMainDataBlock();
	});
	$(".ngPagerContainer select").addClass("form-control");
	$(".form-control").css("padding","2px 4px");
	$("input[name='file']").css("padding","0px");
	$(".ngPagerContainer").css("margin-top","5px");
	$(".modal-header").css("background","#9CB4C5").css("height","35px").css("padding","6px");
	$(".modal-footer").css("height","35px").css("padding","2px").css("margin-top","2px");
	$(".modal").css("border","2px solid #9CB4C5");
	$(".close").css("border","1px solid #eee").css("width","40px").css("height","25px");
	$("body").css("font-size","12px");
	$(".ngTopPanel").css("height","25px");
	$(".ngLabel").css("margin-top","5px");
	$("#funcBlock").css("margin-top","1px").css("padding","2px");
	$(".modal-body").css("padding","10px").css("overflow","hidden");
	$("h4").css("font-size","14px");
	$("#mainDataBlock").css("text-align","center");
	$("#mainDataBlocks").css("text-align","center");
	$("#mainData").css("text-align","center");
	$(".ngHeaderContainer").css("height","25px");
});

function resizeMainDataBlock(){
	var windowHeight = $(window).height();
	var funcBlockHeight = $("#funcBlock").height();
	var footheight=$(".ngFooterPanel").height();
	var topheight=$(".ngTopPanel").height();
	$("#mainDataBlock").height(windowHeight - funcBlockHeight-13);
	$("#mainDataBlock").css("margin-right","1px");
	$(".ngViewport").height(windowHeight - funcBlockHeight - 10-topheight-footheight);
	$("#mainDataBlocks .ngViewport").height(windowHeight - funcBlockHeight - 44-topheight-footheight);
	$("#mainDatas .ngViewport").height(170);
	$("#mainDataL .ngViewport").height(320);
	}
$(function(){
		$("input[class!='login']").css("height","25px");
		$("select").css("height","25px");
		$(".ngSelectionHeader").css("top","1px");
		$("input").css("font-size","12px");
		$("input").removeAttr("placeholder");
		$("select").removeAttr("data-placeholder");
		$("input[class!='login']").focus(function(){
			$(this).css("border-left","4px solid #aaa");
			$(this).css("border-top","3px solid #aaa");
			$(this).css("border-right","1px solid #fff");
			$(this).css("border-bottom","1px solid #fff");
			$(this).css("background","#F2F3F4");
			$(this).css("color","#1808D3");
	});	
		$("input[name!='submit']").blur(function(){
			$(this).css("border","1px solid #CCC");
			$(this).css("background","#FFF");
			$(this).css("color","#333");
		});
		
});
</script>
 <script>
     // $(function() {
       // $('#mainDataBlock').contextPopup({
         // items: [
          //  {label:'增加',     icon:'',              action:function() {$('#add').click();}},
          //  {label:'查询', 	  icon:'',           	action:function() { openModal($("#queryFormModal")) ;} },
           // {label:'刷新',     icon:'',               action:function() {$('#bt').click();}},
         // ]
       // });
      //});
    </script>
