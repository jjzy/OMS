<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>平台管理</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
<link href="${ctx}/res/lib/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" />
</head>

<body ng-controller="MyCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span>
				查询
			</button>
			<shiro:hasPermission name="sys:platform:add">
				<button type="button" class="btn btn-default" id="add" ng-click="onOpenEditFormModal('add')">
					<span class="glyphicon glyphicon-plus"></span>
					增加
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:platform:del">
				<button type="button" class="btn btn-default" ng-click="onOpenDelFormModal()" ng-disabled="flag">
					<span class="glyphicon glyphicon-minus"></span>
					停用
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:platform:upd">
				<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('upd')">
					<span class="glyphicon glyphicon-pencil"></span>
					编辑
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:platform:menu">
				<button type="button" class="btn btn-default" ng-click="onOpenPlatformMenuFormModal()">
					<span class="glyphicon glyphicon-list"></span>
					菜单
				</button>
			</shiro:hasPermission>
			<button type="button" class="btn btn-default" ng-click="refresh()"  id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
		</div>
	</div>
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="400px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group">
						<label for="cityId">城市</label>
						<select name="cityId" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.cityId" style="width: 100%" class="select2" data-placeholder="城市">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="city in cityList" value="{{city.id}}">{{city.name}}</option>
						</select>
					</div>
						<div class="form-group" style="margin-top: 5px">
						<label for="cd">代码</label> 
						<input type="text"  name="cd" ng-model="queryForm.cd" class="form-control" placeholder="产品标识" style="width:150px;">		
					</div>	
					<div class="form-group">
						<label for="name">名称</label>
						<input style="width: 150px;" type="text" name="name" ng-model="queryForm.name" class="form-control" placeholder="名称">
					</div>
					
					<div class="form-group" style="margin-top: 5px">
						<label for="st">状态</label>
						<select name="st" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.st" style="width: 100%" class="select2" data-placeholder="状态">
							<option value=""></option>
							<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
						</select>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal" tabindex="-1" data-width="900px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title" style="font-weight: bold;">平台管理</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" role="form" novalidate>
					<input type="hidden" name="id" ng-model="editForm.id" class="form-control">
					<div class="row">
					
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 3px;margin-top: 2px;"><span style="font-size: 18px;color:red">*</span>代码</div>
							<input style="width: 200px;" type="text" name="cd" ng-model="editForm.cd" required class="form-control input-sm">
						</div>
						
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 3px;margin-top: 2px;"><span style="font-size: 18px;color:red">*</span>名称</div>
							<input style="width: 200px;" type="text" name="name" ng-model="editForm.name" required class="form-control input-sm">
						</div>
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 3px;margin-top: 2px;"><span style="font-size: 18px;color:red">*</span>城市</div>
							<select name="cityId" ui-select2="{width:'200px', allowClear:'true'}" ng-required="true" ng-model="editForm.cityId" style="width: 100%" class="select2">
								<option value=""></option>
								<option ng-repeat="city in cityList" value="{{city.id}}">{{city.name}}</option>
							</select>
						</div>						
					</div>

					<div class="row" style="margin-top:5px;">
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 3px;margin-top: 2px;"><span style="font-size: 18px;color:red">*</span>缩写</div>
							<input style="width: 200px;" type="text" name="shortName" ng-model="editForm.shortName" required class="form-control input-sm">
						</div>
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 8px;margin-top: 7px;">描述</div>
							<input style="width: 200px;" type="text" name="descr" ng-model="editForm.descr" class="form-control input-sm">
						</div>
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;margin-top: 7px;">联系人</div>
							<input style="width: 200px;" type="text" name="linkMan" ng-model="editForm.linkMan" class="form-control input-sm">
						</div>
					</div>

					<div class="row" style="margin-top:5px;">
					<div class="col-sm-4">
					<div style="width:50px;float: left;font-size: 12px;padding-left: 10px;margin-top: 7px;">电话</div>
							<input style="width: 200px;" type="text" name="phone" ng-model="editForm.phone" class="form-control input-sm">
						</div>
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 8px;margin-top: 7px;">传真</div>
							<input style="width: 200px;" type="text" name="fax" ng-model="editForm.fax" class="form-control input-sm">
						</div>
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 10px;margin-top: 7px;">邮件</div>
							<input style="width: 200px;" type="text" name="email" ng-model="editForm.email" class="form-control input-sm">
						</div>
					</div>

					<div class="row" style="margin-top:5px;">
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 2px;"><span style="font-size: 18px;color:red">*</span>状态</div>
							<select name="st" ui-select2="{width:'200px', allowClear:'true'}" ng-required="true" ng-model="editForm.st" style="width: 100%" class="select2">
								<option value=""></option>
								<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
							</select>
						</div>
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 3px;margin-top: 2px;"><span style="font-size: 18px;color:red">*</span>排序</div>
							<input style="width: 200px;" type="text" name="sortNb" ng-model="editForm.sortNb" class="form-control input-sm">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div> 
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<div id="platformMenuFormModal" class="modal" tabindex="-1" data-width="500px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">编辑</h4>
	</div>
	<div class="modal-body">
		<div class="zTreeDemoBackground left">
			<ul id="platformMenuTree" class="ztree"></ul>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSavePlatformMenuForm()" class="btn btn-default">提交</button>
	</div>
</div>

<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res/lib/ztree/js/jquery.ztree.all-3.5.min.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/sys/platform.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script> 
</html>
