<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>用户管理</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="MyCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span> 查询
			</button>
			<shiro:hasPermission name="sys:user:add">
				<button type="button" class="btn btn-default" id="add" ng-click="onOpenEditFormModal('add')">
					<span class="glyphicon glyphicon-plus"></span>
					增加
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:user:del">
				<button type="button" class="btn btn-default" ng-click="onOpenDelFormModal()" ng-disabled="flag">
					<span class="glyphicon glyphicon-minus"></span>
					停用
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:user:upd">
				<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('upd')">
					<span class="glyphicon glyphicon-pencil"></span>
					编辑
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:user:role">
				<button type="button" class="btn btn-default" ng-click="onOpenUserRoleFormModal()">
					<span class="glyphicon glyphicon-pencil"></span>
					角色
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:user:client">
				<button type="button" class="btn btn-default" ng-click="onOpenUserClientFormModal()">
					<span class="glyphicon glyphicon-pencil"></span>
					客户
				</button>
			</shiro:hasPermission>
			<button type="button" class="btn btn-default" ng-click="refresh()"  id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
		</div>
	</div>
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="400px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title" style="font-weight: bold;">用户管理</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group">
						<label for="account">登录名</label> 
						<input type="text" name="account" style="width: 150px;" ng-model="queryForm.account" class="form-control" placeholder="登录名">
					</div>
					<div class="form-group">
						<label for="st">状态</label>
						<select name="st" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.st" style="width: 100%" class="select2" data-placeholder="状态">
							<option value=""></option>
							<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
						</select>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal" tabindex="-1" data-width="500px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title" style="font-weight: bold;">用户管理</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" class="form-horizontal" role="form" novalidate>
					<div class="form-group hidden">
						<label for="id" class="col-sm-4 control-label">ID</label>
						<div class="col-sm-5">
							<input type="hidden" name="id" ng-model="editForm.id" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="account" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>登陆名</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="account" ng-model="editForm.account" required class="form-control" placeholder="登陆名">
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>密码</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="password" name="password" ng-model="editForm.password" required class="form-control" placeholder="密码">
						</div>
					</div>
					<div class="form-group">
						<label for="name" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>名称</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="name" ng-model="editForm.name" required class="form-control" placeholder="名称">
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-sm-4 control-label">邮箱</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="email" ng-model="editForm.email" class="form-control" placeholder="邮箱">
						</div>
					</div>
					<div class="form-group">
						<label for="homePage" class="col-sm-4 control-label">个人主页</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="homePage" ng-model="editForm.homePage" class="form-control" placeholder="个人主页">
						</div>
					</div>
					<div class="form-group">
						<label for="remark" class="col-sm-4 control-label">备注</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="remark" ng-model="editForm.remark" class="form-control" placeholder="备注">
						</div>
					</div>
					<div class="form-group">
						<label for="st" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>状态</label>
						<div class="col-sm-5">
							<select name="st" ui-select2="{width:'200px', allowClear:'true'}" ng-required="true" ng-model="editForm.st" style="width: 100%" class="select2" data-placeholder="状态">
								<option value=""></option>
								<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="sortNb" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>排序</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="sortNb" ng-model="editForm.sortNb" required class="form-control" placeholder="排序">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<div id="userRoleFormModal" class="modal" tabindex="-1" data-width="500px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">编辑</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<div style="margin-bottom: 10px;">
					<form name="userRoleFormForm" class="form-inline" role="form">
						<div class="form-group">
							<label ng-repeat="userRole in userRoles">
								<input type="checkbox" name="userRoleCheckbox" value="{{userRole.id}}" ng-checked="userRoleSelection.indexOf(userRole.id) > -1" ,
							    ng-click="toggleUserRoleSelection(userRole.id)">
								{{userRole.name}}
							</label>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveUserRoleForm()" class="btn btn-default">提交</button>
	</div>
</div>

<div id="userClientFormModal" class="modal" tabindex="-1" data-width="500px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">编辑</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<div style="margin-bottom: 10px;">
					<form name="userClientFormForm" class="form-inline" role="form">
						<div class="form-group">
							<label ng-repeat="userClient in userClients">
								<input type="checkbox" name="userClientCheckbox" value="{{userClient.id}}" ng-checked="userClientSelection.indexOf(userClient.id) > -1" ,
							    ng-click="toggleUserClientSelection(userClient.id)">
								{{userClient.name}}
							</label>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveUserClientForm()" class="btn btn-default">提交</button>
	</div>
</div>

<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/sys/user.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script> 
</html>
