<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>城市管理</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="MyCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span> 查询
			</button>
			<shiro:hasPermission name="sys:city:add">
				<button type="button" class="btn btn-default" id="add" ng-click="onOpenEditFormModal('add')">
					<span class="glyphicon glyphicon-plus"></span>
					增加
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:city:del">
				<button type="button" class="btn btn-default" ng-click="onOpenDelFormModal()" ng-disabled="flag">
					<span class="glyphicon glyphicon-minus"></span>
					停用
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:city:upd">
				<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('upd')">
					<span class="glyphicon glyphicon-pencil"></span>
					编辑
				</button>
			</shiro:hasPermission>
			<button type="button" class="btn btn-default" ng-click="refresh()" id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
			<shiro:hasPermission name="sys:city:import">
			<button type="button" class="btn btn-default" ng-click="onOpenImportFormModal()">
				<span class="glyphicon glyphicon-import"></span> 导入
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:city:export">
			<button type="button" class="btn btn-default" ng-click="onOpenExportFormModal()">
				<span class="glyphicon glyphicon-export"></span> 导出
			</button>
			</shiro:hasPermission>
		</div>
	</div>
	<!-- Row end -->
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="400px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group">
						<label for="cd">代码</label>
						<select name="cd" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.cd" style="width: 100%;" class="select2" data-placeholder="标识">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="cd in cdList" value="{{cd.cd}}">{{cd.cd}}</option>
						</select>
					</div>
					<div class="form-group">
						<label for="name">名称</label> 
						<input type="text" name="name" ng-model="queryForm.name" class="form-control" placeholder="名称" style="width:150px;">		
					</div>
					<div class="form-group" style="margin-top: 5px;">
						<label for="name">省份</label> 
						<input type="text" name="province" ng-model="queryForm.province" class="form-control" placeholder="省份" style="width:150px;">		
					</div>	
					<div class="form-group" style="margin-top: 5px;">
						<label for="st">状态</label>
						<select name="st" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.st" style="width: 100%" class="select2" data-placeholder="状态">
							<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
						</select>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal" tabindex="-1" data-width="500px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title" style="font-weight: bold;">城市管理</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" class="form-horizontal" role="form" novalidate>
					<div class="form-group hidden">
						<label for="id" class="col-sm-4 control-label">ID</label>
						<div class="col-sm-5">
							<input style="width: 180px;" type="hidden" name="id" ng-model="editForm.id" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="cd" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>代码</label>
						<div class="col-sm-5">
							<input style="width: 180px;" type="text" name="cd" ng-model="editForm.cd" required class="form-control" placeholder="标识">
						</div>
					</div>
					<div class="form-group">
						<label for="name" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>名称</label>
						<div class="col-sm-5">
							<input style="width: 180px;" type="text" name="name" ng-model="editForm.name" required class="form-control" placeholder="名称">
						</div>
					</div>
					<div class="form-group">
						<label for="province" class="col-sm-4 control-label" style="margin-top: 5px;">省份</label>
						<div class="col-sm-5">
							<input onClick="WdatePicker({})" class="Wdate" style="width: 180px;" type="text" name="province" ng-model="editForm.province"  placeholder="省份">
						</div>
					</div>
					<div class="form-group">
						<label for="descr" class="col-sm-4 control-label">描述</label>
						<div class="col-sm-5">
							<input style="width: 180px;" type="text" name="descr" ng-model="editForm.descr" class="form-control" placeholder="描述">
						</div>
					</div>
					<div class="form-group">
						<label for="st" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>状态</label>
						<div class="col-sm-5">
							<select name="st" ui-select2="{width:'180px', allowClear:'true'}" ng-required="true" ng-model="editForm.st" style="width: 100%" class="select2" data-placeholder="状态">
								<option value=""></option>
								<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="sortNb" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>排序</label>
						<div class="col-sm-5">
							<input style="width: 180px;" type="text" name="sortNb" ng-model="editForm.sortNb" class="form-control" placeholder="排序">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<div id="importFormModal" class="modal" tabindex="-1" data-width="400px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">导入</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="importFormForm" action="{{importForm.importUrl}}" target="_blank" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
					<div class="form-group">
						<div class="col-sm-8">
						<input name="importUrl" id="importUrl" ng-required="true" ng-model="importForm.importUrl" class="form-control" placeholder="导入" style="display: none;">
						</div>
					</div>

					<div class="form-group">
						<label for="file" class="col-sm-3 control-label">文件</label>
						<div style="float: left;margin-right: 10px;">
							<input type="file" name="file" class="form-control">
						</div>
						<div style="float: left;" >
								<button ng-click="onExportTemplate()" type="button" class="btn btn-success" style="height: 30px;padding: 4px 6px;">下载模板</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-disabled="importFormForm.$invalid" ng-click="onImport()" class="btn btn-primary">导入</button>
	</div>
</div>

<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/sys/city.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script> 
<script src="${ctx}/res/lib/datepicker/js/WdatePicker.js" type="text/javascript"></script>
</html>
