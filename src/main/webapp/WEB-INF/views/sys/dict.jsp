<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>字典管理</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="MyCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span> 查询
			</button>
			<shiro:hasPermission name="sys:dict:upd">
				<button type="button" class="btn btn-default" id="add" ng-click="onOpenEditFormModal('add')">
					<span class="glyphicon glyphicon-plus"></span>
					增加
				</button>
			</shiro:hasPermission>
			<button type="button" class="btn btn-default" id="add" ng-click="onOpenEditFormModal('add1')">
					<span class="glyphicon glyphicon-plus"></span>
					增加域
				</button>
			<shiro:hasPermission name="sys:dict:upd">
				<button type="button" class="btn btn-default" ng-click="onOpenDelFormModal()" ng-disabled="flag">
					<span class="glyphicon glyphicon-minus"></span>
					停用
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:dict:upd">
				<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('upd')">
					<span class="glyphicon glyphicon-pencil"></span>
					编辑
				</button>
			</shiro:hasPermission>
			<button type="button" class="btn btn-default" ng-click="refresh()" id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
		</div>
	</div>
	<!-- Row end -->
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="400px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group">
						<label for="cd" >代码</label>
						<select name="cd" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.cd" style="width: 100%" class="select2" placeholder="">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="cd in cdList" value="{{cd}}">{{cd}}</option>
						</select>
					</div>
					<div class="form-group">
						<label for="st">状态</label>
						<select name="st" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.st" style="width: 100%" class="select2" data-placeholder="状态">
							<option value="" style="height: 10px;"></option>
							<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
						</select>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal" tabindex="-1" data-width="500px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title" style="font-weight: bold;">字典管理</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" class="form-horizontal" role="form" novalidate>
					<div class="form-group hidden">
						<label for="id" class="col-sm-4 control-label">ID</label>
						<div class="col-sm-5">
							<input type="hidden" name="id" ng-model="editForm.id" class="form-control">
						</div>
					</div>
					<div class="form-group" id="cd1">
						<label for="cd" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>域</label>
						<div class="col-sm-5">
							<select name="cd" ui-select2="{width:'200px', allowClear:'true'}"  ng-model="editForm.cd" style="width: 100%" class="select2" placeholder="域">
								<option ng-repeat="cd in cdList" value="{{cd}}">{{cd}}</option>
							</select>
						</div>
					</div>
					<div class="form-group" id="cd2">
						<label for="cd" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>域</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="cd" ng-model="editForm.cd"  class="form-control" placeholder="域">
						</div>
					</div>
					<div class="form-group">
						<label for="lb" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>标签</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="lb" ng-model="editForm.lb" required class="form-control" placeholder="标签">
						</div>
					</div>
					<div class="form-group">
						<label for="val" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>值</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="val" ng-model="editForm.val" required class="form-control" placeholder="值">
						</div>
					</div>
					<div class="form-group">
						<label for="descr" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>描述</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="descr" ng-model="editForm.descr" required class="form-control" placeholder="描述">
						</div>
					</div>
					<div class="form-group">
						<label for="remark" class="col-sm-4 control-label">备注</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="remark" ng-model="editForm.remark" class="form-control" placeholder="注解">
						</div>
					</div>
					<div class="form-group">
						<label for="st" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>状态</label>
						<div class="col-sm-5">
							<select name="st" ui-select2="{width:'200px', allowClear:'true'}" ng-model="editForm.st" style="width: 100%" class="select2" placeholder="状态">
								<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="sortNb" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>排序</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="sortNb" ng-model="editForm.sortNb" class="form-control" placeholder="排序">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/sys/dict.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script> 
</html>
