<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>菜单管理</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
<link href="${ctx}/res/lib/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" />
</head>

<body ng-controller="MyCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span> 查询
			</button>
			<shiro:hasPermission name="sys:menu:add">
				<button type="button" class="btn btn-default" id="add" ng-click="onOpenEditFormModal('add')">
					<span class="glyphicon glyphicon-plus"></span>
					增加
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:menu:del">
				<button type="button" class="btn btn-default" ng-click="onOpenDelFormModal()" ng-disabled="flag">
					<span class="glyphicon glyphicon-minus"></span>
					停用
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:menu:upd">
				<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('upd')">
					<span class="glyphicon glyphicon-pencil"></span>
					编辑
				</button>
			</shiro:hasPermission>
			<button type="button" class="btn btn-default" ng-click="refresh()"  id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
		</div>
	</div>
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="400px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group">
						<label for="name">名称</label>
						<input style="150px;" type="text" name="name" ng-model="queryForm.name" class="form-control" placeholder="名称">
					</div>					
					<div class="form-group">
						<label for="st">状态</label>
						<select name="st" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.st" style="width: 100%" class="select2" data-placeholder="状态">
							<option value=""></option>
							<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
						</select>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal" tabindex="-1" data-width="700px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title" style="font-weight: bold;">菜单管理</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<form name="editFormForm" class="form-horizontal" role="form" novalidate>
				<input type="hidden" id="pid" name="pid" ng-model="editForm.pid" />
				<div class="col-md-6">
					<label>上级菜单</label>
					<p>
						<a href="#" ng-click="clearPid()" class="btn">==无（设为顶级）==</a>
					</p>
					<div class="zTreeDemoBackground left">
						<ul id="menuTree" class="ztree"></ul>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group hidden">
						<label for="id" class="col-sm-4 control-label">ID</label>
						<div class="col-sm-5">
							<input type="hidden" name="id" ng-model="editForm.id" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="name" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>名称</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="name" ng-model="editForm.name" required class="form-control" placeholder="名称">
						</div>
					</div>
					<div class="form-group hidden">
						<label for="pid" class="col-sm-4 control-label">Parent ID</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="hidden" name="pid" ng-model="editForm.pid" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="href" class="col-sm-4 control-label" style="">菜单链接</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="href" ng-model="editForm.href" class="form-control" placeholder="菜单链接">
						</div>
					</div>
					<div class="form-group">
						<label for="target" class="col-sm-4 control-label">链接方式</label>
						<div class="col-sm-5">
							<select name="target" ui-select2="{width:'200px', allowClear:'true'}" ng-model="editForm.target" style="width: 100%" class="select2" data-placeholder="请选择链接方式">
								<option value=""></option>
								<option ng-repeat="row in targetList" value="{{row}}">{{row}}</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="icon" class="col-sm-4 control-label">icon</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="icon" ng-model="editForm.icon" class="form-control" placeholder="icon">
						</div>
					</div>
					<div class="form-group">
						<label for="pemisi" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>权限标识</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="pemisi" ng-model="editForm.pemisi" required class="form-control" placeholder="权限标识">
						</div>
					</div>
					<div class="form-group">
						<label for="ifShow" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>是否菜单</label>
						<div class="col-sm-5">
							<select name="ifShow" ui-select2="{width:'200px', allowClear:'true'}" ng-required="true" ng-model="editForm.ifShow" style="width: 100%" class="select2" data-placeholder="请选择是否菜单">
								<option value=""></option>
								<option ng-repeat="row in ifShowList" value="{{row.id}}">{{row.lb}}</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="remark" class="col-sm-4 control-label">备注</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="remark" ng-model="editForm.remark" class="form-control" placeholder="备注">
						</div>
					</div>
					<div class="form-group">
						<label for="st" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>状态</label>
						<div class="col-sm-5">
							<select name="st" ui-select2="{width:'200px', allowClear:'true'}" ng-required="true" ng-model="editForm.st" style="width: 100%" class="select2" data-placeholder="状态">
								<option value=""></option>
								<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="sortNb" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>排序</label>
						<div class="col-sm-5">
							<input style="width: 200px;" type="text" name="sortNb" ng-model="editForm.sortNb" required class="form-control" placeholder="排序">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res/lib/ztree/js/jquery.ztree.all-3.5.min.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/sys/menu.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script> 
</html>
