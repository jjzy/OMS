<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>客户管理</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="clientCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span> 查询
			</button>
			<shiro:hasPermission name="sys:client:add">
				<button type="button" class="btn btn-default" id="add" ng-click="onOpenEditFormModal('add')">
					<span class="glyphicon glyphicon-plus"></span>
					增加
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:client:add">
				<button type="button" class="btn btn-default" ng-click="onOpenDelFormModal()" ng-disabled="flag">
					<span class="glyphicon glyphicon-minus"></span>
					停用
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:client:add">
				<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('upd')">
					<span class="glyphicon glyphicon-pencil"></span>
					编辑
				</button>
			</shiro:hasPermission>
			<button type="button" class="btn btn-default" ng-click="refresh()"  id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
			<shiro:hasPermission name="sys:client:import">
			<button type="button" class="btn btn-default" ng-click="onOpenImportFormModal()">
				<span class="glyphicon glyphicon-import"></span> 导入
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:client:export">
			<button type="button" class="btn btn-default" ng-click="onOpenExportFormModal()">
				<span class="glyphicon glyphicon-export"></span> 导出
			</button>
			</shiro:hasPermission>
		</div>
	</div>
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="400px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group">
						<label for="cd">代码</label> 
						<input type="text" name="cd" ng-model="queryForm.cd" class="form-control" placeholder="代码" style="width: 150px;">		
					</div>
					<div class="form-group">
						<label for="name">名称</label> 
						<input type="text" name="name" ng-model="queryForm.name" class="form-control" placeholder="名称" style="width: 150px;">		
					</div>
					<div class="form-group" style="margin-top: 5px">
						<label for="shortName">缩写</label> 
						<input type="text" name="shortName" ng-model="queryForm.shortName" class="form-control" placeholder="名称" style="width: 150px;">		
					</div>		
					<div class="form-group" style="margin-top: 5px">
						<label for="st">状态</label>
						<select name="st" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.st" style="width: 100%" class="select2" data-placeholder="状态">
							<option value="" style="height: 10px;"></option>
							<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
						</select>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal" tabindex="-1" data-width="900px" style="display: none;">
	<div class="modal-header" id="jd_dialog_m_h">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title" style="font-weight: bold;">客户管理</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" role="form" novalidate>
					<input type="hidden" name="id" ng-model="editForm.id"
						class="form-control">
					<div class="row">
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 8px;"><span style="font-size: 18px;color:red">*</span>代码</div>
							<input type="text" style="width: 180px;" name="cd" ng-model="editForm.cd" required class="form-control" placeholder="标识">
						</div>
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left:4px;"><span style="font-size: 18px;color:red">*</span>名称</div>
							<input type="text" style="width: 180px; margin-left: 50px;" name="name" ng-model="editForm.name" required class="form-control" placeholder="名称">
						</div>
					
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 4px;"><span style="font-size: 18px;color:red">*</span>缩写</div>
							<input type="text" style="width: 180px;" name="shortName" ng-model="editForm.shortName" required class="form-control" placeholder="缩写">
						</div>
					</div>
					<div class="row">
					
						
					<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-top:12px;padding-left: 16px;">城市 </div>
							<select name="cityId" id="e" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.cityId"  style="width: 100%;margin-top: 5px" class="select2" data-placeholder="城市">								<option value=""></option>
								<option ng-repeat="city in cityList" value="{{city.id}}">{{city.name}}</option>
							</select>
						</div>
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 4px;"><span style="font-size: 18px;color:red">*</span>状态</div>
							<select style="margin-top: 5px" name="st" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.st"  style="width: 100%；;margin-top: 5px" class="select2" data-placeholder="状态">								
								<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
							</select>
						</div>
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 10px;margin-top: 7px;">描述</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="descr" ng-model="editForm.descr" class="form-control" placeholder="描述">
						</div>
					
					</div>
					<div class="row">
					
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 3px;margin-top: 7px;">联系人</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="linkMan" ng-model="editForm.linkMan" class="form-control" placeholder="联系人">
						</div>
					
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 10px;margin-top: 7px;">电话</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="phone" ng-model="editForm.phone" class="form-control" placeholder="电话">
						</div>
					
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 10px;margin-top: 7px;">传真</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="fax" ng-model="editForm.fax" class="form-control" placeholder="传真">
						</div>
					</div>
					<div class="row">
					
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 14px;margin-top: 7px;">邮箱</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="email" ng-model="editForm.email" class="form-control" placeholder="邮件">
						</div>
					
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 10px;margin-top: 7px;">地址</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="addr" ng-model="editForm.addr" class="form-control" placeholder="地址">
						</div>
					
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 10px;margin-top: 7px;">邮编</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="postCd" ng-model="editForm.postCd" class="form-control" placeholder="邮编">
						</div>
					</div>
					<div class="row">														
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>
<div id="importFormModal" class="modal" tabindex="-1" data-width="400px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">导入</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="importFormForm" action="{{importForm.importUrl}}" target="_blank" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
					<div class="form-group">
						<div class="col-sm-8">
						<input name="importUrl" id="importUrl" ng-required="true" ng-model="importForm.importUrl" class="form-control" placeholder="导入" style="display: none;">
						</div>
					</div>

					<div class="form-group">
						<label for="file" class="col-sm-3 control-label">文件</label>
						<div style="float: left;margin-right: 10px;">
							<input type="file" name="file" class="form-control">
						</div>
						<div style="float: left;" >
								<button ng-click="onExportTemplate()" type="button" class="btn btn-success" style="height: 30px;padding: 4px 6px;">下载模板</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-disabled="importFormForm.$invalid" ng-click="onImport()" class="btn btn-primary">导入</button>
	</div>
</div>
<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/sys/client.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script> 
</html>
