<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body style="overflow: auto;font-size: 13px;">
	<div class="container" style="margin-top: 150px">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<c:forEach var="err" items="${msg}">
					${err.errMsg}<br>
				</c:forEach>
			</div>
		</div>
	</div>
</body>
</html>
