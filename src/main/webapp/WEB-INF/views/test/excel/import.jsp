<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>导入</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="MyCtrl">
	<div class="container">
		<%@ include file="/WEB-INF/layouts/topbar.jsp"%>
		<%@ include file="/WEB-INF/layouts/header.jsp"%>

		<!-- Row start -->
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<i class="fa fa-calendar"></i>
						<h3 class="panel-title">导入</h3>
					</div>
					<div class="panel-body">
						<div style="margin-bottom: 10px;">
							<form action="${ctx}/productType/import" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
								
								<div class="form-group">
									<label for="importBean" class="col-sm-4 control-label">模型</label> 
									<div class="col-sm-5">
										<select name="importBean" id="importBean" ui-select2="{width:'200px', allowClear:'true'}" ng-required="true" ng-model="importForm.importBean" class="form-control" data-placeholder="模型">
											<option value=""></option>
											<option ng-repeat="dict in dictList" value="{{dict.val}}">{{dict.lb}}</option>
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<label for="file" class="col-sm-4 control-label">文件</label> 
									<div class="col-sm-5">
										<input type="file" name="file" class="form-control">
									</div>
								</div>
								
								<button type="submit" class="btn btn-info">提交</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Row end -->
	</div>
	<!-- Container end -->

	<%@ include file="/WEB-INF/layouts/footer.jsp"%>
</body>

<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
</html>

<script>
$(function(){
	
});

var myApp = angular.module('myApp', [ 'ui.select2' ]);
myApp.controller('MyCtrl', function($scope, $http) {
	$scope.init = function() {
		var requestBody = {
			pageNb : 1,
			pageSize : 9999,
			ffMap : {"cd":"oms:import:url", "st":0}
		};
		httpPost($http, {"url":ctx + '/dict/findListBy', "data":requestBody, "ifBlock":true}, function(data){
			$scope.dictList = data.dt;
		}, function(msg){
			toastError("初始化导入列表失败。"+msg);
		});
	};
	$scope.init();
});
</script>
