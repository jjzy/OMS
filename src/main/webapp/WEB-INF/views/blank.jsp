<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body>
	<div id="wrapper">
		<%@ include file="/WEB-INF/layouts/topbar.jsp"%>
		<%@ include file="/WEB-INF/layouts/header.jsp"%>
		
		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Blank</h1>
                </div>
            </div>
        </div>
	</div>
	<%@ include file="/WEB-INF/layouts/footer.jsp"%>
</body>
<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script>
	$(function(){
		
	});
</script>
</html>
