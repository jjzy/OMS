<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>O M S 系   统</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>

<style>
.ui-tabs {
	position: relative;
	padding: .2em .2em 0 .2em;
}

.ui-tabs .ui-tabs-nav {
	margin: 0;
	padding: 0;
}

.ui-tabs .ui-tabs-panel {
	display: block;
	border-width: 0;
	padding: 3px 3px 0 3px;
	background: none;
}



.menu-on-top .menu-item-parent {
	color: #5B5B5B;
	font-size: 13px;
	font-family: Microsoft YaHei, helvetica, tahoma, arial, SimSun;
}
nav ul li a{
	font-size: 13px;
}
</style>
</head>

<body ng-controller="MyCtrl" class="desktop-detected menu-on-top smart-style-2">
	<aside id="left-panel">
		<!-- User info -->
		<span class="pull-left" style="margin: 10px 10px 0 10px;">
			<a href="${ctx}/" id="show-shortcut" data-action="toggleShortcut">
				<img src="${ctx}/res_oms/theme-b/imgs/logo.png" style="height: 35px;margin-top: 4px;">
			</a>
		</span>
		<nav>
			<ul id="topMenu">
				<li class="pull-right" style="margin-right: 25px; margin-left: 20px;">
					<a href="#">
						<i></i>
						<span class="menu-item-parent">${i.name}</span>
					</a>
					<ul>
						<li>
							<a href="${ctx}/logout">
								<i class="fa fa-sign-out"></i>
								注销
							</a>
						</li>
						<li>
							<a href="#" ng-click="openChangePasswordFormModal()">
								<i class="fa fa-pencil"></i>
								修改密码
							</a>
						</li>
					</ul>
				</li>
				<li class="pull-right">
					<a href="javascript:void(0);">
						<i></i>
						<span class="menu-item-parent">${i.platformName}</span>
					</a>
					<ul ng-if="platformList!=null&&platformList.length>1">
						<li ng-repeat="platform in platformList">
							<a href="${ctx}/switchPlatform/{{platform.id}}"> {{platform.name}} </a>
						</li>
					</ul>
				</li>
			</ul>
		</nav>
	</aside>
	<!-- END NAVIGATION -->

	<div id="main" role="main" style="padding: 5px 5px 0 5px;">
		<div id="mainTabs">
			<ul>
				<li>
					<a href="#tabs-0" style="display: none">主页</a>
				</li>
			</ul>
			<div id="tabs-0"></div>
		</div>
	</div>
</body>


<div id="changePasswordFormModal" class="modal" tabindex="-1" data-width="500px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">修改密码</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="changePasswordForm" class="form-horizontal" role="form" novalidate>
					<div class="form-group">
						<label for="currentPassword" class="col-sm-4 control-label">当前密码</label>
						<div class="col-sm-5">
							<input type="password" name="currentPassword" ng-model="changePasswordForm.currentPassword" required class="form-control" placeholder="当前密码">
						</div>
					</div>
					<div class="form-group">
						<label for="newPassword" class="col-sm-4 control-label">新密码</label>
						<div class="col-sm-5">
							<input type="password" name="newPassword" ng-model="changePasswordForm.newPassword" required class="form-control" placeholder="新密码">
						</div>
					</div>
					<div class="form-group">
						<label for="confirmNewPassword" class="col-sm-4 control-label">确认新密码</label>
						<div class="col-sm-5">
							<input type="password" name="confirmNewPassword" ng-model="changePasswordForm.confirmNewPassword" required class="form-control" placeholder="再次输入新密码">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveChangePasswordForm()" ng-disabled="changePasswordFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<script src="${ctx}/res/lib/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="${ctx}/res/lib/jquery/jquery-migrate-1.2.1.js" type="text/javascript"></script>
<script src="${ctx}/res/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<script src="${ctx}/res_oms/theme-b/bootstrap.min.js" type="text/javascript"></script>

<script src="${ctx}/res/lib/angularjs/angular.min.js"></script>

<script src="${ctx}/res/lib/angular-ui/ng-grid/ng-grid-2.0.11.min.js"></script>
<script src="${ctx}/res/lib/angular-ui/ng-grid/zh-cn.js"></script>
<script src="${ctx}/res/lib/angular-ui/ng-grid/plugins/ng-grid-flexible-height.js"></script>

<script src="${ctx}/res/lib/select2/select2.js"></script>
<script src="${ctx}/res/lib/select2/select2_locale_zh-CN.js"></script>
<script src="${ctx}/res/lib/angular-ui/ui-select2/select2.js"></script>

<script src="${ctx}/res/lib/toastr/toastr.min.js"></script>
<script src="${ctx}/res/lib/jquery.blockUI.js" type="text/javascript"></script>

<script src="${ctx}/res/i/js/lib.js" type="text/javascript"></script>
<script src="${ctx}/res/i/js/utils.js" type="text/javascript"></script>

<script>
	var uid = "${i.id}";
	var platformId = "${i.platformId}";
</script>
<script src="${ctx}/res_oms/home.js" type="text/javascript"></script>
</html>