<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>调度单管理</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="shipmentCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions" class="demo1"></div>
	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span> 查询
			</button>
			<shiro:hasPermission name="sys:shipment:add">
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('add')">
				<span class="glyphicon glyphicon-plus"></span> 增加
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:shipment:del">
			<button type="button" class="btn btn-default" ng-click="onOpenDelFormModal()">
				<span class="glyphicon glyphicon-minus"></span> 停用
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="sys:shipment:upd">
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('upd')">
				<span class="glyphicon glyphicon-pencil"></span> 编辑
			</button>
			</shiro:hasPermission>
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModalPc()">
				<span class="glyphicon glyphicon-refresh"></span> 派车确认
			</button>
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModalIn()">
				<span class="glyphicon glyphicon-refresh"></span> 车辆入库
			</button>
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModalOut()">
				<span class="glyphicon glyphicon-refresh"></span> 车辆出库
			</button>
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModalFy()">
				<span class="glyphicon glyphicon-refresh"></span> 发运确认
			</button>
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModalYd()">
				<span class="glyphicon glyphicon-refresh"></span> 运抵确认
			</button>
			<button type="button" class="btn btn-default" ng-click="refresh()" id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
		</div>
	</div>
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="450px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">	
					<div class="form-group">
						<label for="cd">调度单号</label> 
						<input style="width:150px;" type="text" name="cd" ng-model="queryForm.cd" class="form-control" placeholder="调度单号">		
					</div>								
					<div class="form-group" style="margin-left: 12px;">
						<label for="st">状态</label>
						<select name="st" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.st" style="width: 100%" class="select2" data-placeholder="状态">
							<option value=""></option>
							<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
						</select>
					</div>
					<div class="form-group" style="margin-left: 12px;margin-top: 5px;">
						<label for="constractorId">承运商</label>
						<select name="constractorId" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.constractorId" style="width: 100%;" class="select2" data-placeholder="承运商">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="constractor in constractorList" value="{{constractor.id}}">{{constractor.name}}</option>
						</select>
					</div>
					<div class="form-group" style="margin-top: 5px;">
						<label for="carNo">车牌号</label> 
						<input style="width:150px;" type="text" name="carNo" ng-model="queryForm.carNo" class="form-control" placeholder="车牌号">		
					</div>
				</form>
			</div>
		</div>	
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal" tabindex="-1" data-width="1050" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">调度单管理</h4>
	</div>
<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" role="form" novalidate>
					<input type="hidden" name="id" ng-model="editForm.id" class="form-control">
					<div class="row">
						<div class="col-sm-3" id="cd">
							<div style="width:65px;float: left;font-size: 12px;padding-left: 10px;margin-top: 12px;margin-left: 15px;">调度单号</div>
							<input type="text" name="cd" style="width: 150px;margin-top: 5px;" ng-disabled="flagOfCd"  ng-model="editForm.cd"  class="form-control" placeholder="调度单号">		
						</div>
						<div class="col-sm-3">
							<div style="width:65px;float: left;font-size: 12px;margin-left: 15px;padding-left: 14px;margin-top: 7px;"><span style="font-size: 18px;color:red">*</span>承运商</div>
							<select name="constractorId" id="c" ng-disabled="flagOfClientCd"  ng-required="true" ui-select2="{width:'150px', allowClear:'true'}" ng-model="editForm.constractorId" style="width: 100%;margin-top: 5px;" class="select2" data-placeholder="承运商">
								<option value=""></option>
								<option ng-repeat="constractor in constractorList" value="{{constractor.id}}">{{constractor.name}}</option>
							</select>						
						</div>	
						<div class="col-sm-3">
							<div style="width:65px;float: left;font-size: 12px;padding-left: 14px;margin-top: 7px;" id="ft"><span style="font-size: 18px;color:red">*</span>出发地</div>
							<select name="ftranslocationId"  ng-required="true" ui-select2="{width:'150px', allowClear:'true'}" ng-model="editForm.ftranslocationId" style="width: 100%;margin-top: 5px;" class="select2" data-placeholder="出发地">
								<option value=""></option>
								<option ng-repeat="ftranslocation in ftranslocationList" value="{{ftranslocation.id}}">{{ftranslocation.name}}</option>
							</select>						
						</div>	
						<div class="col-sm-3">
							<div style="width:65px;float: left;font-size: 12px;padding-left: 14px;margin-top: 7px;"><span style="font-size: 18px;color:red">*</span>目的地</div>
							<select name="ttranslocationId"  ng-required="true" ui-select2="{width:'150px', allowClear:'true'}" ng-model="editForm.ttranslocationId" style="width: 100%;margin-top: 5px;" class="select2" data-placeholder="目的地">
								<option value=""></option>
								<option ng-repeat="ttranslocation in ttranslocationList" value="{{ttranslocation.id}}">{{ttranslocation.name}}</option>
							</select>						
						</div>				
					
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div style="width:65px;float: left;font-size: 12px;margin-top: 12px;margin-left: 15px;padding-left: 10px">运输工具</div>
							<select name="vehicleId"  id="vh" ui-select2="{width:'150px', allowClear:'true'}" ng-model="editForm.vehicleId" style="width: 100%;margin-top: 5px;" class="select2" data-placeholder="运输工具">
								<option value=""></option>
								<option ng-repeat="vehicle in vehicleList" value="{{vehicle.id}}">{{vehicle.cd}}</option>
							</select>						
						</div>
						<div class="col-sm-3">
							<div style="width:60px;float: left;font-size: 12px;padding-left: 16px;margin-top: 12px;margin-left: 20px;">车牌号</div>
							<input type="text" name="carNo" style="width: 150px;margin-top: 5px;" ng-model="editForm.carNo"  class="form-control" placeholder="车牌号">		
						</div>
						<div class="col-sm-3">
							<div style="width:65px;float: left;font-size: 12px;margin-top: 12px;padding-left: 8px;">运输方式</div>
							<select name="shipment_method" ui-select2="{width:'150px', allowClear:'true'}"  ng-model="editForm.shipment_method"  style="width: 100%;margin-top: 5px;" class="select2" data-placeholder="运输方式">
								<option value=""></option>
								<option ng-repeat="shipment_method in shipment_methodList" value="{{shipment_method.descr}}">{{shipment_method.val}}</option>
							</select>		
						</div>						
						<div class="col-sm-3">
							<div style="width: 80px; float: left; font-size: 12px; padding-left: 14px; margin-left: -15px;margin-top: 5px">
								<span style="font-size: 18px; color: red">*</span>
								业务类型
							</div>
							<select name="vocationType" ui-select2="{width:'150px', allowClear:'true'}" ng-required="true" ng-model="editForm.vocationType" style="width: 100%;margin-top: 5px" class="select2" data-placeholder="业务类型">
								<option value=""></option>
								<option ng-repeat="cd in cdList" value="{{cd.descr}}">{{cd.val}}</option>
							</select>
						</div>						
					</div>
						<div class="row">
						<div class="col-sm-3">
							<div style="width:80px;float: left;font-size: 12px;margin-top: 12px;">计划发车时间</div>
							<input type="text" name="planltime" style="width: 150px;margin-top: 5px;" ng-model="editForm.planltime" class="form-control" placeholder="计划发车时间">		
						</div>
						<div class="col-sm-3">
							<div style="width:80px;float: left;font-size: 12px;margin-top: 12px;">预计到达时间</div>
							<input type="text" name="planatime" style="width: 150px;margin-top: 5px;" ng-model="editForm.planatime" class="form-control" placeholder="预计到达时间">		
						</div>
						<div class="col-sm-3">
							<div style="width:65px;float: left;font-size: 12px;margin-top: 12px;padding-left: 8px;">跟踪单号</div>
							<input type="text" name="gzNo" style="width: 150px;margin-top: 5px;" ng-model="editForm.gzNo" class="form-control" placeholder="跟踪单号">		
						</div>
						<div class="col-sm-3">
							<div style="width:65px;float: left;font-size: 12px;margin-top: 12px;padding-left: 8px;">配载方式</div>
							<select name="shipmentType" ui-select2="{width:'150px', allowClear:'true'}"  ng-model="editForm.shipmentType"  style="width: 100%;margin-top: 5px;" class="select2" data-placeholder="配载方式">
								<option value=""></option>
								<option value=""></option>
								<option ng-repeat="type in shipmentTypeList" value="{{type.descr}}">{{type.val}}</option>
							</select>		
						</div>
					</div>
					<div class="row">
					<div class="col-sm-3">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 16px;margin-top: 12px;margin-left: 30px;">司机</div>
							<select name="driverId"  id="d" ui-select2="{width:'150px', allowClear:'true'}" ng-model="editForm.driverId" style="width: 100%;margin-top: 5px;" class="select2" data-placeholder="司机">
								<option value=""></option>
								<option ng-repeat="driver in driverList" value="{{driver.id}}">{{driver.name}}</option>
							</select>
						</div>
						<div class="col-sm-3">
							<div style="width:65px;float: left;font-size: 12px;margin-top: 12px;margin-left: 15px;padding-left: 8px">司机名称</div>
							<input type="text" name="driverName" style="width: 150px;margin-top: 5px;" ng-model="editForm.driverName" class="form-control" placeholder="司机名称">		
						</div>
					<div class="col-sm-3">
						<div style="width:65px;float: left;font-size: 12px;margin-top: 12px;padding-left: 8px;">联系电话</div>
							<input type="text" name="phone" style="width: 150px;margin-top: 5px;" ng-model="editForm.phone" class="form-control" placeholder="联系电话">		
						</div>
						<div class="col-sm-3">
						<div style="width:65px;float: left;font-size: 12px;margin-top: 12px;padding-left: 18px;">身份证</div>
							<input type="text" name="idcard" style="width: 150px;margin-top: 5px;" ng-model="editForm.idcard" class="form-control" placeholder="身份证">		
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div style="width:65px;float: left;font-size: 12px;margin-top: 12px;margin-left: 15px;padding-left: 8px;">送货点数</div>
							<input type="text" name="points" style="width: 150px;margin-top: 5px;" ng-model="editForm.points" class="form-control" placeholder=">送货点数">		
						</div>
						<div class="col-sm-3">
							<div style="width:65px;float: left;font-size: 12px;margin-top: 12px;margin-left: 15px;padding-left: 8px;">预计耗时</div>
							<input type="text" name="timeconsuming" style="width: 150px;margin-top: 5px;" ng-model="editForm.timeconsuming" class="form-control" placeholder="预计耗时">		
						</div>					
						<div class="col-sm-3">
							<div style="width:60px;float: left;font-size: 12px;margin-top: 12px;margin-left: 5px;padding-left: 14px;">一口价</div>
							<input type="text" name="expense" style="width: 150px;margin-top: 5px;" ng-model="editForm.expense" class="form-control" placeholder="一口价">		
						</div>
						<div class="col-sm-3">
							<div style="width:60px;float: left;font-size: 12px;margin-top: 12px;margin-left: 6px;">预计费用</div>
							<input type="text" name="expectMoney" style="width: 150px;margin-top: 5px;" ng-model="editForm.expectMoney" class="form-control" placeholder="体积">		
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 16px;margin-top: 12px;margin-left: 30px">件数</div>
							<input type="text" name="quantity" style="width: 150px;margin-top: 5px;" ng-model="editForm.quantity" class="form-control" placeholder="件数">		
						</div>
						<div class="col-sm-3">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 14px;margin-top: 12px;margin-left: 30px;">重量</div>
							<input type="text" name="weight" style="width: 150px;margin-top: 5px;" ng-model="editForm.weight" class="form-control" placeholder="重量">		
						</div>
						<div class="col-sm-3">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 14px;margin-top: 12px;margin-left: 15px">体积</div>
							<input type="text" name="volume" style="width: 150px;margin-top: 5px;" ng-model="editForm.volume" class="form-control" placeholder="体积">		
						</div>
						<div class="col-sm-3">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 14px;margin-top: 12px;margin-left: 15px">描述</div>
							<input type="text" name="descr" style="width: 150px;margin-top: 5px;" ng-model="editForm.descr" class="form-control" placeholder="描述">		
						</div>
					</div>		
			</form>
			</div>
		</div>
	</div>
	<div style="width: 1045px;">
		<div id="mainDatas" ng-grid="gridOption" style="width: 100%;"></div>
	</div>
	<div class="modal-footer">
	<div style="float: left;margin-right: 800px;margin-left: 10px;">
		<button type="button" ng-click="onOpenEditFormModalL()" class="btn btn-default" ng-disabled="addflag">添加</button>
		<button type="button" ng-click="cancel()" class="btn btn-default" ng-disabled="cancelflag">取消调度</button>
	</div>
	<div style="float: left">
		<button type="button" data-dismiss="modal" id="btn" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
	</div>
</div>

<div id="editFormModalL" class="modal" tabindex="-1" data-width="900"style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">调度单管理</h4>
	</div>
		<div id="mainDataL" ng-grid="gridOptionL" style="width: 895px;"></div>
	<div class="modal-footer">
		<button type="button" ng-click="add()" class="btn btn-default">添加</button>
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
	</div>
</div>

<div id="editFormModalPc" class="modal" tabindex="-1" data-width="300"style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">派车确认</h4>
	</div>
	<div style="height: 40px;margin-left: 20px;margin-top: 15px;">
	<form name="editFormFormPc" class="form-horizontal" role="form" novalidate>
		派车确认：<input type="text" name="pcTime" style="width: 150px;display: inline;" ng-model="editFormPc.pcTime" class="form-control" placeholder="派车确认">		
	</form>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-click="pcConfirm()" class="btn btn-default">确认</button>
	</div>
</div>
<div id="editFormModalIn" class="modal" tabindex="-1" data-width="300"style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">车辆入库</h4>
	</div>
	<div style="height: 40px;margin-left: 20px;margin-top: 15px;">
		<form name="editFormFormrk" class="form-horizontal" role="form" novalidate>
		车辆入库：<input type="text" name="rkTime" style="width: 150px;display: inline;" ng-model="editFormRk.rkTime" class="form-control" placeholder="车辆入库">		
		</form>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-click="rkConfirm()" class="btn btn-default">确认</button>
	</div>
</div>
<div id="editFormModalOut" class="modal" tabindex="-1" data-width="300"style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">车辆出库</h4>
	</div>
	<div style="height: 40px;margin-left: 20px;margin-top: 15px;">
		<form name="editFormFormck" class="form-horizontal" role="form" novalidate>
		车辆出库：<input type="text" name="ckTime" style="width: 150px;display: inline;" ng-model="editFormCk.ckTime" class="form-control" placeholder="车辆出库">		
		</form>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-click="ckConfirm()" class="btn btn-default">确认</button>
	</div>
</div>
<div id="editFormModalFy" class="modal" tabindex="-1" data-width="300"style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">发运确认</h4>
	</div>
	<div style="height: 40px;margin-left: 20px;margin-top: 15px;">
		<form name="editFormFormfy" class="form-horizontal" role="form" novalidate>
		发运确认：<input type="text" name="fyTime" style="width: 150px;display: inline;" ng-model="editFormFy.fyTime" class="form-control" placeholder="发运确认">		
		</form>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-click="fyConfirm()" class="btn btn-default">确认</button>
	</div>
</div>
<div id="editFormModalYd" class="modal" tabindex="-1" data-width="300"style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">运抵确认</h4>
	</div>
	<div style="height: 40px;margin-left: 20px;margin-top: 15px;">
		<form name="editFormFormyd" class="form-horizontal" role="form" novalidate>
		运抵确认：<input type="text" name="ydTime" style="width: 150px;display: inline;" ng-model="editFormYd.ydTime" class="form-control" placeholder="运抵确认">		
		</form>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-click="ydConfirm()" class="btn btn-default">确认</button>
	</div>
</div>
<!--  
<div class="contextMenu" id="myMenu1">
	<ul>
		<li id="save">派车确认</li>
		<li id="open">车辆入库</li>
		<li id="email">车辆出库</li>
		<li id="delete">发运确认</li>
	</ul>
</div>
--> 
<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/oms/shipment.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/jquery.contextmenu.r2.js"></script>
</html>
