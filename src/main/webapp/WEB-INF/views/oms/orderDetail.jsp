<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>订单明细</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="orderDetailCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span> 查询
			</button>
			<button type="button" class="btn btn-default" ng-click="refresh()" id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
		</div>
	</div>
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="500px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group" style="margin-left: 24px;">
						<label for="clientId">客户</label>
						<select name=clientId" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.clientId" style="width: 100%" class="select2" data-placeholder="客户">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="client in clientList" value="{{client.id}}">{{client.shortName}}</option>
						</select>
					</div>
					<div class="form-group">
						<label for="orderId">订单号</label> 
						<input type="text" name="orderCd" ng-model="queryForm.orderCd" class="form-control" placeholder="订单标识" style="width:150px;">		
					</div>	
					<div class="form-group">
						<label for="productCd">物料代码</label> 
						<input type="text" name="productCd" ng-model="queryForm.productCd" class="form-control" placeholder="物料标识" style="width: 150px;">		
					</div>
					<div class="form-group" style="margin-top: 5px;margin-left: 12px;">
						<label for="status">状态</label>
						<select name="status" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.status" style="width: 100%" class="select2" data-placeholder="状态">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="status in statusList" value="{{status.descr}}">{{status.val}}</option>
						</select>
					</div>	
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal" tabindex="-1" data-width="500px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">编辑</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" class="form-horizontal" role="form" novalidate>
					<div class="form-group hidden">
						<label for="id" class="col-sm-4 control-label">ID</label> 
						<div class="col-sm-5">
							<input type="hidden" name="id" ng-model="editForm.id" class="form-control">		
						</div>
					</div>
					<div class="form-group">
						<label for="orderId" class="col-sm-4 control-label">订单号</label> 
						<div class="col-sm-5">
							<input type="text" name="orderId" ng-model="editForm.orderId" required class="form-control" placeholder="订单标识">		
						</div>
					</div>
					<div class="form-group">
						<label for="productId" class="col-sm-4 control-label">物料代码</label> 
						<div class="col-sm-5">
							<input type="text" name="productId" ng-model="editForm.productId" required class="form-control" placeholder="物料标识">		
						</div>
					</div>
					<div class="form-group">
						<label for="lineno" class="col-sm-4 control-label">行号</label> 
						<div class="col-sm-5">
							<input type="text" name="lineno" ng-model="editForm.lineno" required class="form-control" placeholder="行号">		
						</div>
					</div>
					<div class="form-group">
						<label for="lot" class="col-sm-4 control-label">批号</label> 
						<div class="col-sm-5">
							<input type="text" name="lot" ng-model="editForm.lot" required class="form-control" placeholder="批号">		
						</div>
					</div>
					<div class="form-group">
						<label for="unit" class="col-sm-4 control-label">包装单位</label> 
						<div class="col-sm-5">
							<input type="text" name="unit" ng-model="editForm.unit" required class="form-control" placeholder="包装单位">		
						</div>
					</div>
					<div class="form-group">
						<label for="quantity" class="col-sm-4 control-label">数量</label> 
						<div class="col-sm-5">
							<input type="text" name="quantity" ng-model="editForm.quantity" required class="form-control" placeholder="数量">		
						</div>
					</div>
					<div class="form-group">
						<label for="weight" class="col-sm-4 control-label">重量</label> 
						<div class="col-sm-5">
							<input type="text" name="weight" ng-model="editForm.weight" class="form-control" placeholder="重量">		
						</div>
					</div>
					<div class="form-group">
						<label for="volume" class="col-sm-4 control-label">体积</label> 
						<div class="col-sm-5">
							<input type="text" name="volume" ng-model="editForm.volume" class="form-control" placeholder="体积">		
						</div>
					</div>
					<div class="form-group">
						<label for="st" class="col-sm-4 control-label">状态</label> 
						<div class="col-sm-5">
							<select name="st" ng-required="true" ng-model="editForm.st" class="form-control" data-placeholder="状态">
								<option value=""></option>
								<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
							</select>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/oms/orderDetail.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script> 
</html>
