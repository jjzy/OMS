<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>高级调度</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
<style type="text/css">
	#container{
		border: 3px solid #99BBE8;
	}
	#left{
		border-right:2px solid #99BBE8;
		width:22%;
		height: 100%;
		float: left;
	}
	#lefttop{
		width:100%;
		height: 5%;
		background: #CDDEF3;
		border-bottom: 1px solid #E5F0FB;
		padding: 3px 3px;
		color: #15429D;
	}
	#style1{
		height: 28px;
		background:#E1EBF7;
		border-bottom:2px solid #99BBE8;
	}
	#style2{
		height: 340px;
		border-bottom: 2px solid #99BBE8;
		overflow: auto;
		padding-right:200px;
	}
	#style3{
		height:45px;
		border-bottom: 1px solid #99BBE8;
	}
	#right{
		width:78%;
		float: left;
	}
	#tree li{
		width:-moz-max-content;
		padding-right:10px;
	}
	#righttop{
		height: 26px;
		border-bottom: 2px solid #99BBE8;
		background: #CFDFF3;
		padding: 4px;
	}
	#righttop span{
		margin-right: 5px;
	}
	#rightmiddle{
		border-bottom: 1px solid #99BBE8;
		padding:5px;
	}
	#p{
		width:16%;
		height:100%;
		float: left;
		padding: 3px;
	}
	#o{
		width:16%;
		height:100%;
		float: left;
		padding: 3px;
	}
	#j{
		width:22%;
		height:100%;
		float: left;
		padding: 3px;
	}
	#w{
		width:24%;
		height:100%;
		float: left;
		padding: 3px;
	}
	#v{
	width:22%;
		height:100%;
		float: left;
		padding: 3px;
	}
	a{
		text-decoration: none;
	}
	a:hover{
		text-decoration: none;
	}
	a:actived{
		text-decoration: none;
	}
	li{
		list-style: none;
	}
	#car .ngViewport{
		height: 110px;
	}
	#c .ngViewport{
		height: 110px;
	}
</style>
</head>
<body ng-controller="tempShipmentCtrl">
	<div id="container">
		<div id="left">
			<div id="lefttop">配载列表</div>
			<div id="leftmiddle">
				<div id="style1">
        		<label style="margin-left: 137px;">业务类型</label>
        		<select name="vocationType"  id="c" style="width: 100px;margin-top: 2px">
						<option style="display: none"></option>
						<option value="NORMAL">正常配送</option>
						<option value="SPLIT">短驳运输</option>
						<option value="SWITCH">转仓运输</option>
				</select>
				</div>
				<div id="style2">
					<ul id="tree">
					</ul>
				</div>
			</div>
			<div id="leftfooter">
				<div id="style3">
					<div id="p" style="color: #0000FF">配载数 
						<span style="text-align: center;display: block;">0</span>
					</div>
					<div id="o" style="color: #008000">订单数
						<span style="text-align: center;display: block;">0</span>
					</div>
					<div id="j" style="color: #800080">总件数(件)
						<span style="text-align: center;display: block;">0</span>	
					</div>
					<div id="w" style="color: #043460">总重量(KG)
						<span style="text-align: center;display: block;">0</span>	
					</div>
					<div id="v" style="color: #B33B3B">总体积(方)
						<span style="text-align: center;display: block;">0</span>			
					</div>
				</div>
				<div id="style4">	
				<div class="style">
        			<div style="margin-top: 10px;margin-left: 20px;float: left;"><img src="${ctx}/res_oms/theme-b/imgs/e01.gif" style="width:20px;"/><span style="font-size: 12px;margin-top:-18px; display: block;margin-left: 55px;margin-right: 40px;">待配载</span></div>
        			<div style="margin-top: 10px;margin-left: 20px;float: left;"><img src="${ctx}/res_oms/theme-b/imgs/e03.gif" style="width:20px;"><span style="font-size: 12px;margin-top:-18px; display: block;margin-left:55px">已配载</span></div>
        		</div>
        		<div class="style">
        		<div style="margin-top: 10px;margin-left: 20px;float: left;"><img src="${ctx}/res_oms/theme-b/imgs/e02.gif" style="width:20px;"/><span style="font-size: 12px;margin-left: 45px;margin-top:-18px; display: block;margin-right: 25px;">指定承运商</span></div>
        			<div style="margin-top: 10px;margin-left: 20px;float: left;"><img src="${ctx}/res_oms/theme-b/imgs/e04.gif" style="width:20px;"><span style="font-size: 12px;margin-left: 50px;margin-top:-18px; display: block;">指定车辆</span></div>
        		</div>				
				</div>
			</div>
		</div>
		<div id="right">
			<div id="righttop">
			<span style="display: none" class='id'></span>	配载号：<span class="p"></span> 单数：<span class="o">0</span> 送货点：<span class="s">0</span> 箱数：<span class="c">0</span> 零散数：<span class="l">0</span> 件数：<span class="j">0</span> 重量：<span class="w">0</span> 体积：<span class="v">0</span>
			</div>
			<div id="rightfooter">
			<div id="mainDataBlocks" ng-grid="gridOptions"></div>
			<div id="funcBlock" class="alert alert-info">
				<button type="button" class="btn btn-default" ng-click="onQuery()" style="font-size: 12px" id="open">
				<span class="glyphicon glyphicon-search"></span> 打开
				</button> 
				<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()" style="font-size: 12px">
				<span class="glyphicon glyphicon-filter"></span> 过滤
				</button>
				<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('add')" style="font-size: 12px" ng-disabled="addflag">
				<span class="glyphicon glyphicon-plus"></span> 创建配载
				</button> 
				<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal1()" style="font-size: 12px" id="btnc">
				<span class="glyphicon glyphicon-check"></span> 登记配车
				</button>
				<button type="button" class="btn btn-default" ng-click="cancel()" style="font-size: 12px" id="btnq">
				<span class="glyphicon glyphicon-remove"></span> 取消配载
				</button>
				<button type="button" class="btn btn-default" ng-click="confirmp()" style="font-size: 12px" id="btnp">
				<span class="glyphicon glyphicon-ok"></span> 确定配载
				</button>
			</div>
			</div>
		</div>
	</div>
</body>
<div id="editFormModal" class="modal" tabindex="-1" data-width="350px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">创建配载</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" class="form-horizontal" role="form" novalidate>
					<div class="form-group hidden">
						<label for="id" class="col-sm-4 control-label">ID</label> 
						<div class="col-sm-5">
							<input type="hidden" name="id" ng-model="editForm.id" class="form-control">		
						</div>
					</div>
					<div class="form-group hidden">
						<label for="quantity" class="col-sm-4 control-label">quantity</label> 
						<div class="col-sm-5">
							<input type="hidden" name="quantity" ng-model="editForm.quantity" class="form-control">		
						</div>
					</div>
					<div class="form-group hidden">
						<label for="weight" class="col-sm-4 control-label">weight</label> 
						<div class="col-sm-5">
							<input type="hidden" name="weight" ng-model="editForm.weight" class="form-control">		
						</div>
					</div>
					<div class="form-group hidden">
						<label for="volume" class="col-sm-4 control-label">volume</label> 
						<div class="col-sm-5">
							<input type="hidden" name="volume" ng-model="editForm.volume" class="form-control">		
						</div>
					</div>
					<div class="form-group hidden">
						<label for="points" class="col-sm-4 control-label">points</label> 
						<div class="col-sm-5">
							<input type="hidden" name="points" ng-model="editForm.points" class="form-control">		
						</div>
					</div>
					<div class="form-group">
						<label for="code" class="col-sm-4 control-label">配载号</label> 
						<div class="col-sm-5">
							<input type="text" name="code" ng-model="editForm.code" required class="form-control" placeholder="配载号" style="width: 150px;">		
						</div>
					</div>
				
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<div id="editFormModal1" class="modal" tabindex="-1" data-width="900px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">登记配车</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm1" role="form" novalidate>
					<input type="hidden" name="id" ng-model="editForm1.id"
						class="form-control">
					<div class="row">
						<div class="col-sm-4">
							<div style="width:70px;float: left;font-size: 12px;padding-left: 6px; margin-top: 2px;"><span style="font-size: 18px;color:red">*</span>服务产品 </div>
							<select name="constractorId" id="e" ng-required="true" ng-disabled="flagC"
								ui-select2="{width:'180px', allowClear:'true'}"
								ng-model="editForm1.constractorId" style="width: 100%" class="select2"
								data-placeholder="服务产品">
								<option value=""></option>
								<option ng-repeat="constractor in constractorList" value="{{constractor.id}}">{{constractor.shortName}}</option>
							</select>
						</div>
						<div class="col-sm-4">
						<div style="width:55px;float: left;margin-left:-5px; font-size: 12px;padding-left:2px "><span style="font-size: 18px;color:red">*</span>承运商</div>
							<input type="text" style="width: 180px;" name="name"
								ng-model="editForm1.name" required class="form-control"
								placeholder="承运商">
						</div>
						<div class="col-sm-4">
						<div style="width:70px;float: left;font-size: 12px;padding-left: 4px; margin-top: 2px;margin-left: -20px;"><span style="font-size: 18px;color:red">*</span>运输方式 </div>
							<select name="shipment_method" ng-required="true"
								ui-select2="{width:'180px', allowClear:'true'}"
								ng-model="editForm1.shipment_method" style="width: 100%" class="select2"
								data-placeholder="运输方式">
								<option value=""></option>
								<option value=""></option>
								<option ng-repeat="shipment_method in shipment_methodList" value="{{shipment_method.descr}}">{{shipment_method.val}}</option>
							</select>
						</div>			
					</div>
					<div class="row">
							<div class="col-sm-4">
							<div style="width:70px;float: left;font-size: 12px;padding-left: 12px; margin-top: 5px;"><span style="font-size: 18px;color:red">*</span>车辆编号</div>
							<select name="vehicleId" id="vh" ng-required="true"
								ui-select2="{width:'180px', allowClear:'true'}"
								ng-model="editForm1.vehicleId" style="width: 100%; margin-top: 5px" class="select2"
								data-placeholder="车辆编号">
								<option value=""></option>
								<option value=""></option>
								<option ng-repeat="vehicle in vehicleList" value="{{vehicle.id}}">{{vehicle.cd}}</option>
							</select>
						</div>
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 3px;margin-top: 7px;">车牌号</div>
							<input type="text" style="width: 180px; margin-top: 5px"
								name="vehicleNo" ng-model="editForm1.vehicleNo" class="form-control"
								placeholder="车牌号">
						</div>
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 14px; margin-top: 12px;">车型</div>
							<select name="vehicleTypeId"
								ui-select2="{width:'180px', allowClear:'true'}"
								ng-model="editForm1.vehicleTypeId" style="width: 100%; margin-top: 5px" class="select2"
								data-placeholder="车型">
								<option value=""></option>
								<option value=""></option>
								<option ng-repeat="vehicleType in vehicleTypeList" value="{{vehicleType.id}}">{{vehicleType.name}}</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div style="width:70px;float: left;font-size: 12px;padding-left: 32px; margin-top: 12px;">司机</div>
							<select name="driverId" 
								ui-select2="{width:'180px', allowClear:'true'}"
								ng-model="editForm1.driverId" style="width: 100%; margin-top: 5px" class="select2"
								data-placeholder="司机">
								<option value=""></option>
								<option value=""></option>
								<option ng-repeat="driver in driverList" value="{{driver.id}}">{{driver.name}}</option>
							</select>
						</div>													
						
						<div class="col-sm-4">
							<div style="width: 90px; float: left; font-size: 12px; padding-left: 6px; margin-top: 12px; margin-left: -40px">计划出发时间</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="planlDt" ng-model="editForm1.planlDt" class="form-control" placeholder="计划出发时间" id="planlDt">
						</div>
						<div class="col-sm-4">
							<div style="width: 90px; float: left; font-size: 12px; padding-left: 6px; margin-top: 12px;margin-left: -40px">预计到达时间</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="planaDt" ng-model="editForm1.planaDt" class="form-control" placeholder="预计到达时间" id="planaDt">
						</div>						
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div style="width: 66px; float: left; font-size: 12px; padding-left: 17px; margin-top: 12px; margin-left: 4px;">一口价</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="expense" ng-model="editForm1.expense" class="form-control" placeholder="一口价">
						</div>
						<div class="col-sm-4">
							<div style="width: 60px; float: left; font-size: 12px;  margin-top: 12px; margin-left: -10px;">预计费用</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="bunget" ng-model="editForm1.bunget" class="form-control" placeholder="预计费用">
						</div>
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 14px;padding-top:4px; margin-top: 7px;">描述 </div>
							<input type="text" style="width: 180px; margin-top: 5px"
								name="descr" ng-model="editForm1.descr" class="form-control"
								placeholder="描述">
						</div>					
					</div>					
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSave()" ng-disabled="editFormForm1.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="450px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">过滤</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">	
					<div class="form-group">
						<label for="clientId">客户</label> <select
							name=clientId " ui-select2="{width:'150px', allowClear:'true'}"
							ng-model="queryForm.clientId" style="width: 100%" class="select2"
							data-placeholder="客户">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="client in clientList" value="{{client.id}}">{{client.shortName}}</option>
						</select>
					</div>	
					<div class="form-group" style="margin-left: 12px;">
						<label for="orderCd">订单号</label> <input type="text"
							name="orderCd" ng-model="queryForm.orderCd" class="form-control"
							placeholder="订单号" style="width:150px;">
					</div>						
					<div class="form-group" style="margin-top: 5px">
						<label for="cityId">城市</label> <select
							name=cityId " ui-select2="{width:'150px', allowClear:'true'}"
							ng-model="queryForm.cityId" style="width: 100%" class="select2"
							data-placeholder="城市">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="city in cityList" value="{{city.id}}">{{city.name}}</option>
						</select>
					</div>	
					<div class="form-group" style="margin-top: 5px">
						<label for="shipment_method">运输方式</label> <select
							name=shipment_method " ui-select2="{width:'150px', allowClear:'true'}"
							ng-model="queryForm.shipment_method" style="width: 100%" class="select2"
							data-placeholder="运输方式">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="cd in cdList" value="{{cd.descr">{{cd.val}}</option>
						</select>
					</div>	
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onFilter()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>



<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/oms/tempShipment.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script> 
<script type="text/javascript">
	$(function(){
		var windowHeight = $(window).height();
		$("#container").height(windowHeight-15);
		$("#style1 select").height(20);
	});
	
</script>


</html>
