<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp"
	xmlns:ng="http://angularjs.org">
<head>
<title>收发货方</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="receiverCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default"
				ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span> 查询
			</button>
			<shiro:hasPermission name="oms:receiver:add">
				<button type="button" class="btn btn-default" id="add" 
					ng-click="onOpenEditFormModal('add')">
					<span class="glyphicon glyphicon-plus"></span> 增加
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:receiver:del">
				<button type="button" class="btn btn-default"
					ng-click="onOpenDelFormModal()" ng-disabled="flag">
					<span class="glyphicon glyphicon-minus"></span> 停用
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:receiver:upd">
				<button type="button" class="btn btn-default"
					ng-click="onOpenEditFormModal('upd')">
					<span class="glyphicon glyphicon-pencil"></span> 编辑
				</button>
			</shiro:hasPermission>
			<button type="button" class="btn btn-default" ng-click="refresh()" id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
			<shiro:hasPermission name="oms:receiver:import">
			<button type="button" class="btn btn-default" ng-click="onOpenImportFormModal()" id="bt">
				<span class="glyphicon glyphicon-import"></span> 导入
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:receiver:export">
			<button type="button" class="btn btn-default" ng-click="onOpenExportFormModal()">
				<span class="glyphicon glyphicon-export"></span> 导出
			</button>
			</shiro:hasPermission>
		</div>
	</div>
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="400px"
	style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group">
						<label for="clientId">客户</label> <select
							name=clientId " ui-select2="{width:'150px', allowClear:'true'}"
							ng-model="queryForm.clientId" style="width: 100%" class="select2"
							data-placeholder="客户">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="client in clientList" value="{{client.id}}">{{client.shortName}}</option>
						</select>
					</div>
					<div class="form-group">
						<label for="cd">代码</label> <input type="text"
							name="cd" ng-model="queryForm.cd" class="form-control"
							placeholder="收发货方标识" style="width:150px;">
					</div>
					<div class="form-group" style="margin-top: 5px;">
						<label for="name">名称</label> <input type="text"
							name="name" ng-model="queryForm.name" class="form-control"
							placeholder="名称" style="width:150px;">
					</div>
					<div class="form-group" style="margin-top: 5px;">
						<label for="st">状态</label>
						<select name="st" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.st" style="width: 100%" class="select2" data-placeholder="状态">
							<option value=""></option>
							<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
						</select>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid"
			class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal"  data-width="900px"
	style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">×</button>
		<h4 class="modal-title" style="font-weight: bold;">收发货方信息管理</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" role="form" novalidate>
					<input type="hidden" name="id" ng-model="editForm.id"
						class="form-control">
					<div class="row">
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 8px; margin-top: 2px;"><span style="font-size: 18px;color:red">*</span>客户 </div>
							<select name="clientId" id="e" ng-required="true" ng-disabled="flagC"
								ui-select2="{width:'180px', allowClear:'true'}"
								ng-model="editForm.clientId" style="width: 100%" class="select2"
								data-placeholder="客户">
								<option value=""></option>
								<option ng-repeat="client in clientList" value="{{client.id}}">{{client.shortName}}</option>
							</select>
						</div>
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 8px; margin-top: 2px;"><span style="font-size: 18px;color:red">*</span>城市 </div>
							<select name="cityId" id="e" ng-required="true"
								ui-select2="{width:'180px', allowClear:'true'}"
								ng-model="editForm.cityId" style="width: 100%" class="select2"
								data-placeholder="城市">
								<option value=""></option>
								<option ng-repeat="city in cityList" value="{{city.id}}">{{city.name}}</option>
							</select>
						</div>
						<div class="col-sm-4">
						<div style="width:90px;float: left;margin-left:-40px; font-size: 12px;"><span style="font-size: 18px;color:red">*</span>收发货方代码 </div>
							<input type="text" style="width: 180px;" name="cd"
								ng-model="editForm.cd" required class="form-control"
								placeholder="收发货方标识">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 8px;padding-top:4px; margin-top: 2px;"><span style="font-size: 18px;color:red">*</span>名称 </div>
							<input type="text" style="width: 180px; margin-top: 5px"
								name="name" ng-model="editForm.name" required
								class="form-control" placeholder="名称">
						</div>
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 3px;margin-top: 7px;">联系人 </div>
							<input type="text" style="width: 180px; margin-top: 5px"
								name="likeman" ng-model="editForm.likeman" class="form-control"
								placeholder="联系人">
						</div>
						<div class="col-sm-4">
						<div style="width:65px;float: left;margin-left:-15px; font-size: 12px;margin-top: 5px;padding-top:4px;padding-left:4px;">联系电话 </div>
							<input type="text" style="width: 180px; margin-top: 5px"
								name="phone" ng-model="editForm.phone" class="form-control"
								placeholder="联系电话">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 14px;padding-top:4px; margin-top: 7px;">传真 </div>
							<input type="text" style="width: 180px; margin-top: 5px"
								name="fax" ng-model="editForm.fax" class="form-control"
								placeholder="传真">
						</div>
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 14px;padding-top:4px; margin-top: 7px;">邮件 </div>
							<input type="text" style="width: 180px; margin-top: 5px"
								name="email" ng-model="editForm.email" class="form-control"
								placeholder="邮件">
						</div>
						<div class="col-sm-4">
						<div style="width:65px;float: left;margin-left:-15px; font-size: 12px;margin-top: 7px;padding-top:4px;padding-left:4px">联系地址 </div>
							<input type="text" style="width: 180px; margin-top: 5px"
								name="address" ng-model="editForm.address" class="form-control"
								placeholder="联系地址">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 14px;padding-top:4px; margin-top: 7px;">邮编 </div>
							<input type="text" style="width: 180px; margin-top: 5px"
								name="postcode" ng-model="editForm.postcode"
								class="form-control" placeholder="邮编">
						</div>
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 14px;padding-top:4px; margin-top: 7px;">描述 </div>
							<input type="text" style="width: 180px; margin-top: 5px"
								name="descr" ng-model="editForm.descr" class="form-control"
								placeholder="描述">
						</div>
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 4px;padding-top:4px; "><span style="font-size: 18px;color:red">*</span>状态 </div>
						<select style="width: 180px;margin-top: 5px" name="st" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.st"  style="width: 100%；;margin-top: 5px" class="select2" data-placeholder="状态">
								<option value=""></option>
								<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
							</select>
						</div>
					</div>
						<div class="row">
						<div class="col-sm-4">
						<div style="width:60px;float: left;font-size: 12px;padding-left: 6px;padding-top:4px; margin-top: 2px;margin-left: -10px;"><span style="font-size: 18px;color:red">*</span>运输地 </div>
						<select style="width: 180px;margin-top: 5px;" name="translocationId" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.translocationId"  style="width: 100%；;margin-top: 5px" class="select2" data-placeholder="运输地">
								<option ng-repeat="translocation in translocationList" value="{{translocation.id}}">{{translocation.name}}</option>
							</select>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()"
			ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<div id="importFormModal" class="modal" tabindex="-1" data-width="400px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">导入</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="importFormForm" action="{{importForm.importUrl}}" target="_blank" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
					<div class="form-group">
						<div class="col-sm-8">
						<input name="importUrl" id="importUrl" ng-required="true" ng-model="importForm.importUrl" class="form-control" placeholder="导入" style="display: none;">
						</div>
					</div>

					<div class="form-group">
						<label for="file" class="col-sm-3 control-label">文件</label>
						<div style="float: left;margin-right: 10px;">
							<input type="file" name="file" class="form-control">
						</div>
						<div style="float: left;">
								<button ng-click="onExportTemplate()" type="button" class="btn btn-success" style="height: 30px;padding: 4px 6px;">下载模板</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-disabled="importFormForm.$invalid" ng-click="onImport()" class="btn btn-primary">导入</button>
	</div>
</div>


<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/oms/receiver.js"></script> 
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script> 
</html>
