<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>分段订单查询</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="legsCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span> 查询
			</button>
			<button type="button" class="btn btn-default" ng-click="refresh()" id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModalPc()">
				<span class="glyphicon glyphicon-refresh"></span> 派车确认
			</button>
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModalIn()">
				<span class="glyphicon glyphicon-refresh"></span> 车辆入库
			</button>
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModalOut()">
				<span class="glyphicon glyphicon-refresh"></span> 车辆出库
			</button>
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModalFy()">
				<span class="glyphicon glyphicon-refresh"></span> 发运确认
			</button>
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModalYd()">
				<span class="glyphicon glyphicon-refresh"></span> 运抵确认
			</button>
		</div>
	</div>
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="450px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group" style="margin-left: 12px;">
						<label for="orderCd">订单号</label>
						<input style="width: 150px;" type="text" name="orderCd" ng-model="queryForm.orderCd" class="form-control" placeholder="订单号">
					</div>
					<div class="form-group" style="margin-left: 23px;">
						<label for="clientId">客户</label>
						<select name="clientId" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.clientId" style="width: 100%" class="select2" data-placeholder="客户">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="client in clientList" value="{{client.id}}">{{client.shortName}}</option>
						</select>
					</div>
					<div class="form-group" style="margin-top: 5px;margin-left: 12px">
						<label for="constractorId">承运商</label>
						<select name="constractorId" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.constractorId" style="width: 100%" class="select2" data-placeholder="承运商">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="constractor in constractorList" value="{{constractor.id}}">{{constractor.name}}</option>
						</select>
					</div>
					<div class="form-group" style="margin-top: 5px;margin-left: 23px;">
						<label for="st">状态</label>
						<select name="st" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.st" style="width: 100%" class="select2" data-placeholder="状态">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="st in stList" value="{{st.descr}}">{{st.val}}</option>
						</select>
					</div>
					<div class="form-group" style="margin-top: 5px;">
						<label for="status">订单状态</label>
						<select name="status" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.status" style="width: 100%" class="select2" data-placeholder="订单状态">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="status in statusList" value="{{status.descr}}">{{status.val}}</option>
						</select>
					</div>
					<div class="form-group" style="margin-top: 5px;">
						<label for="status">运输方式</label>
						<select name="shipment_method" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.shipment_method" style="width: 100%" class="select2" data-placeholder="运输方式">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="shipment_method in shipment_methodList" value="{{shipment_method.descr}}">{{shipment_method.val}}</option>
						</select>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal" tabindex="-1" data-width="500px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">编辑</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" class="form-horizontal" role="form" novalidate>
					<div class="form-group hidden">
						<label for="id" class="col-sm-4 control-label">ID</label> 
						<div class="col-sm-5">
							<input type="hidden" name="id" ng-model="editForm.id" class="form-control">		
						</div>
					</div>
					<div class="form-group">
						<label for="orderId" class="col-sm-4 control-label">订单</label> 
						<div class="col-sm-5">
							<input type="text" name="orderId" ng-model="editForm.orderId" required class="form-control" placeholder="订单">		
						</div>
					</div>
					<div class="form-group">
						<label for="orderCd" class="col-sm-4 control-label">订单编号</label> 
						<div class="col-sm-5">
							<input type="text" name="orderCd" ng-model="editForm.orderCd" required class="form-control" placeholder="订单编号">		
						</div>
					</div>
					<div class="form-group">
						<label for="legs_no" class="col-sm-4 control-label">分段订单号</label> 
						<div class="col-sm-5">
							<input type="text" name="legs_no" ng-model="editForm.legs_no" required class="form-control" placeholder="分段订单号">		
						</div>
					</div>
					<div class="form-group">
						<label for="status" class="col-sm-4 control-label">订单状态</label> 
						<div class="col-sm-5">
							<input type="text" name="status" ng-model="editForm.status" required class="form-control" placeholder="订单状态">		
						</div>
					</div>
					<div class="form-group">
						<label for="st" class="col-sm-4 control-label">状态</label> 
						<div class="col-sm-5">
							<input type="text" name="st" ng-model="editForm.st" required class="form-control" placeholder="状态">		
						</div>
					</div>
					<div class="form-group">
						<label for="platformId" class="col-sm-4 control-label">平台</label> 
						<div class="col-sm-5">
							<input type="text" name="platformId" ng-model="editForm.platformId" required class="form-control" placeholder="平台">		
						</div>
					</div>
					<div class="form-group">
						<label for="clientId" class="col-sm-4 control-label">客户</label> 
						<div class="col-sm-5">
							<input type="text" name="clientId" ng-model="editForm.clientId" required class="form-control" placeholder="客户">		
						</div>
					</div>
					<div class="form-group">
						<label for="constractorId" class="col-sm-4 control-label">承运商</label> 
						<div class="col-sm-5">
							<input type="text" name="constractorId" ng-model="editForm.constractorId" class="form-control" placeholder="承运商">		
						</div>
					</div>
					<div class="form-group">
						<label for="vehicleId" class="col-sm-4 control-label">运输工具</label> 
						<div class="col-sm-5">
							<input type="text" name="vehicleId" ng-model="editForm.vehicleId" class="form-control" placeholder="运输工具">		
						</div>
					</div>
					<div class="form-group">
						<label for="shipment_method" class="col-sm-4 control-label">运输方式</label> 
						<div class="col-sm-5">
							<input type="text" name="shipment_method" ng-model="editForm.shipment_method" class="form-control" placeholder="运输方式">		
						</div>
					</div>
					<div class="form-group">
						<label for="formCd" class="col-sm-4 control-label">出发地</label> 
						<div class="col-sm-5">
							<input type="text" name="formCd" ng-model="editForm.formCd" required class="form-control" placeholder="出发地">		
						</div>
					</div>
					<div class="form-group">
						<label for="toCd" class="col-sm-4 control-label">目的地</label> 
						<div class="col-sm-5">
							<input type="text" name="toCd" ng-model="editForm.toCd" required class="form-control" placeholder="目的地">		
						</div>
					</div>
					<div class="form-group">
						<label for="formName" class="col-sm-4 control-label">发货方名称</label> 
						<div class="col-sm-5">
							<input type="text" name="formName" ng-model="editForm.formName" required class="form-control" placeholder="发货方名称">		
						</div>
					</div>
					<div class="form-group">
						<label for="formlikeName" class="col-sm-4 control-label">发货方联系人</label> 
						<div class="col-sm-5">
							<input type="text" name="formlikeName" ng-model="editForm.formlikeName" class="form-control" placeholder="发货方联系人">		
						</div>
					</div>
					<div class="form-group">
						<label for="formPhone" class="col-sm-4 control-label">发货方电话</label> 
						<div class="col-sm-5">
							<input type="text" name="formPhone" ng-model="editForm.formPhone" class="form-control" placeholder="发货方电话">		
						</div>
					</div>
					<div class="form-group">
						<label for="formAddr" class="col-sm-4 control-label">发货方地址</label> 
						<div class="col-sm-5">
							<input type="text" name="formAddr" ng-model="editForm.formAddr" class="form-control" placeholder="发货方地址">		
						</div>
					</div>
					<div class="form-group">
						<label for="toName" class="col-sm-4 control-label">收货方名称</label> 
						<div class="col-sm-5">
							<input type="text" name="toName" ng-model="editForm.toName" required class="form-control" placeholder="收货方名称">		
						</div>
					</div>
					<div class="form-group">
						<label for="tolikeName" class="col-sm-4 control-label">收货方联系人</label> 
						<div class="col-sm-5">
							<input type="text" name="tolikeName" ng-model="editForm.tolikeName" class="form-control" placeholder="收货方联系人">		
						</div>
					</div>
					<div class="form-group">
						<label for="toPhone" class="col-sm-4 control-label">收货方电话</label> 
						<div class="col-sm-5">
							<input type="text" name="toPhone" ng-model="editForm.toPhone" class="form-control" placeholder="收货方电话">		
						</div>
					</div>
					<div class="form-group">
						<label for="toAddr" class="col-sm-4 control-label">收货方地址</label> 
						<div class="col-sm-5">
							<input type="text" name="toAddr" ng-model="editForm.toAddr" class="form-control" placeholder="收货方地址">		
						</div>
					</div>
					<div class="form-group">
						<label for="shipmentId" class="col-sm-4 control-label">调度单号</label> 
						<div class="col-sm-5">
							<input type="text" name="shipmentId" ng-model="editForm.shipmentId" class="form-control" placeholder="调单单号">		
						</div>
					</div>
					<div class="form-group">
						<label for="shipmentType" class="col-sm-4 control-label">配载方式</label> 
						<div class="col-sm-5">
							<input type="text" name="shipmentType" ng-model="editForm.shipmentType" class="form-control" placeholder="配载方式">		
						</div>
					</div>
					<div class="form-group">
						<label for="quantity" class="col-sm-4 control-label">件数</label> 
						<div class="col-sm-5">
							<input type="text" name="quantity" ng-model="editForm.quantity" class="form-control" placeholder="件数">		
						</div>
					</div>
					<div class="form-group">
						<label for="weight" class="col-sm-4 control-label">重量</label> 
						<div class="col-sm-5">
							<input type="text" name="weight" ng-model="editForm.weight" class="form-control" placeholder="重量">		
						</div>
					</div>
					<div class="form-group">
						<label for="volume" class="col-sm-4 control-label">体积</label> 
						<div class="col-sm-5">
							<input type="text" name="volume" ng-model="editForm.volume" class="form-control" placeholder="体积">		
						</div>
					</div>
					<div class="form-group">
						<label for="palletsum" class="col-sm-4 control-label">托数</label> 
						<div class="col-sm-5">
							<input type="text" name="palletsum" ng-model="editForm.palletsum" class="form-control" placeholder="托数">		
						</div>
					</div>
					<div class="form-group">
						<label for="expense" class="col-sm-4 control-label">一口价</label> 
						<div class="col-sm-5">
							<input type="text" name="expense" ng-model="editForm.expense" class="form-control" placeholder="一口价">		
						</div>
					</div>
					<div class="form-group">
						<label for="bag" class="col-sm-4 control-label">零箱数</label> 
						<div class="col-sm-5">
							<input type="text" name="bag" ng-model="editForm.bag" class="form-control" placeholder="零箱数">		
						</div>
					</div>
					<div class="form-group">
						<label for="ordertime" class="col-sm-4 control-label">开单时间</label> 
						<div class="col-sm-5">
							<input type="text" name="ordertime" ng-model="editForm.ordertime" class="form-control" placeholder="开单时间">		
						</div>
					</div>
					<div class="form-group">
						<label for="billtime" class="col-sm-4 control-label">计费周期</label> 
						<div class="col-sm-5">
							<input type="text" name="billtime" ng-model="editForm.billtime" class="form-control" placeholder="计费周期">		
						</div>
					</div>
					<div class="form-group">
						<label for="planltime" class="col-sm-4 control-label">计划离开时间</label> 
						<div class="col-sm-5">
							<input type="text" name="planltime" ng-model="editForm.planltime" class="form-control" placeholder="计划离开时间">		
						</div>
					</div>
					<div class="form-group">
						<label for="leavetime" class="col-sm-4 control-label">出发时间</label> 
						<div class="col-sm-5">
							<input type="text" name="leavetime" ng-model="editForm.leavetime" class="form-control" placeholder="出发时间">		
						</div>
					</div>
					<div class="form-group">
						<label for="arrivetime" class="col-sm-4 control-label">到达时间</label> 
						<div class="col-sm-5">
							<input type="text" name="arrivetime" ng-model="editForm.arrivetime" class="form-control" placeholder="到达时间">		
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<div id="editFormModalPc" class="modal" tabindex="-1" data-width="300"style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">派车确认</h4>
	</div>
	<div style="height: 40px;margin-left: 20px;margin-top: 15px;">
	<form name="editFormFormPc" class="form-horizontal" role="form" novalidate>
		派车确认：<input type="text" name="pcTime" style="width: 150px;display: inline;" ng-model="editFormPc.pcTime" class="form-control" placeholder="派车确认">		
	</form>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-click="pcConfirm()" class="btn btn-default">确认</button>
	</div>
</div>
<div id="editFormModalIn" class="modal" tabindex="-1" data-width="300"style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">车辆入库</h4>
	</div>
	<div style="height: 40px;margin-left: 20px;margin-top: 15px;">
		<form name="editFormFormrk" class="form-horizontal" role="form" novalidate>
		车辆入库：<input type="text" id="rk" name="rkTime" style="width: 150px;display: inline;" ng-model="editFormRk.rkTime" class="form-control" placeholder="车辆入库">		
		</form>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-click="rkConfirm()" class="btn btn-default">确认</button>
	</div>
</div>
<div id="editFormModalOut" class="modal" tabindex="-1" data-width="300"style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">车辆出库</h4>
	</div>
	<div style="height: 40px;margin-left: 20px;margin-top: 15px;">
		<form name="editFormFormck" class="form-horizontal" role="form" novalidate>
		车辆出库：<input type="text" name="ckTime" style="width: 150px;display: inline;" ng-model="editFormCk.ckTime" class="form-control" placeholder="车辆出库">		
		</form>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-click="ckConfirm()" class="btn btn-default">确认</button>
	</div>
</div>
<div id="editFormModalFy" class="modal" tabindex="-1" data-width="300"style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">发运确认</h4>
	</div>
	<div style="height: 40px;margin-left: 20px;margin-top: 15px;">
		<form name="editFormFormfy" class="form-horizontal" role="form" novalidate>
		发运确认：<input type="text" name="fyTime" style="width: 150px;display: inline;" ng-model="editFormFy.fyTime" class="form-control" placeholder="发运确认">		
		</form>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-click="fyConfirm()" class="btn btn-default">确认</button>
	</div>
</div>
<div id="editFormModalYd" class="modal" tabindex="-1" data-width="300"style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">运抵确认</h4>
	</div>
	<div style="height: 40px;margin-left: 20px;margin-top: 15px;">
		<form name="editFormFormyd" class="form-horizontal" role="form" novalidate>
		运抵确认：<input type="text" name="ydTime" style="width: 150px;display: inline;" ng-model="editFormYd.ydTime" class="form-control" placeholder="运抵确认">		
		</form>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-click="ydConfirm()" class="btn btn-default">确认</button>
	</div>
</div>
<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/oms/legs.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script>
</html>
