<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>承运商管理</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="constractorCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span> 查询
			</button>
			<shiro:hasPermission name="oms:constractor:add">
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('add')">
				<span class="glyphicon glyphicon-plus"></span> 增加
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:constractor:del">
			<button type="button" class="btn btn-default" ng-click="onOpenDelFormModal()" ng-disabled="flag">
				<span class="glyphicon glyphicon-minus"></span> 停用
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:constractor:upd">
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('upd')">
				<span class="glyphicon glyphicon-pencil"></span> 编辑
			</button>
			</shiro:hasPermission>
			<button type="button" class="btn btn-default" ng-click="refresh()"  id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
		</div>
	</div>
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="400px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group">
						<label for="cd">代码</label>
						<select name="cd" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.cd" style="width: 100%;" class="select2" data-placeholder="标识">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="cd in cdList" value="{{cd.cd}}">{{cd.cd}}</option>
						</select>
					</div>	
					<div class="form-group">
						<label for="name">名称</label> 
						<input style="width:150px;" type="text" name="name" ng-model="queryForm.name" class="form-control" placeholder="名称">		
					</div>	
					<div class="form-group">
						<label for="cityId">城市</label>
						<select name=cityId" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.cityId" style="width: 100%" class="select2" data-placeholder="城市">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="city in cityList" value="{{city.id}}">{{city.name}}</option>
						</select>
					</div>	
					<div class="form-group" style="margin-top: 5px;">
						<label for="st">状态</label>
						<select name="st" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.st" style="width: 100%" class="select2" data-placeholder="状态">
							<option value="" style="height: 10px;"></option>
							<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
						</select>
					</div>		
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal" tabindex="-1" data-width="900px" style="display: none;">
	<div class="modal-header" id="jd_dialog_m_h">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title" style="font-weight: bold;">承运商管理</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" role="form" novalidate>
					<input type="hidden" name="id" ng-model="editForm.id"
						class="form-control">
					<div class="row">
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 10px;margin-top: 2px;"><span style="font-size: 18px;color:red">*</span>代码</div>
							<input type="text" style="width: 180px;" name="cd" ng-model="editForm.cd" required class="form-control" placeholder="代码">
						</div>
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 10px;margin-top: 2px;"><span style="font-size: 18px;color:red">*</span>名称</div>
							<input type="text" style="width: 180px; margin-left: 50px;" name="name" ng-model="editForm.name" required class="form-control" placeholder="名称">
						</div>
					
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 10px;margin-top: 2px;"><span style="font-size: 18px;color:red">*</span>简称</div>
							<input type="text" style="width: 180px;" name="shortName" ng-model="editForm.shortName" required class="form-control" placeholder="简称">
						</div>
					</div>
					<div class="row">
					<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 10px;margin-top: 2px;"><span style="font-size: 18px;color:red">*</span>城市 </div>
							<select name="cityId" id="e" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.cityId"  style="width: 100%;margin-top: 5px" class="select2" data-placeholder="城市">								<option value=""></option>
								<option ng-repeat="city in cityList" value="{{city.id}}">{{city.name}}</option>
							</select>
						</div>
						<div class="col-sm-4">
						<div style="width:80px;float: left;font-size: 12px;padding-top:4px;padding-left: 14px;margin-top: 2px;margin-left: -30px;"><span style="font-size: 18px;color:red">*</span>运输方式</div>
							<select name="shipment_method" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.shipment_method"  style="width: 100%;margin-top: 5px;" class="select2" data-placeholder="运输方式">
								<option value=""></option>
								<option ng-repeat="cd in list" value="{{cd.descr}}">{{cd.val}}</option>
							</select>
						</div>
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 14px;margin-top: 7px;">描述</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="descr" ng-model="editForm.descr" class="form-control" placeholder="描述">
						</div>											
					</div>
					<div class="row">
					
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 4px;margin-top: 7px;">联系人</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="linkMan" ng-model="editForm.linkMan" class="form-control" placeholder="联系人">
						</div>
					
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 14px;margin-top: 7px;">电话</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="phone" ng-model="editForm.phone" class="form-control" placeholder="电话">
						</div>
					
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 14px;margin-top: 7px;">传真</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="fax" ng-model="editForm.fax" class="form-control" placeholder="传真">
						</div>
					</div>
					<div class="row">
					
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 14px;margin-top: 7px;">邮箱</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="email" ng-model="editForm.email" class="form-control" placeholder="邮件">
						</div>
					
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 14px;margin-top: 7px;">地址</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="addr" ng-model="editForm.addr" class="form-control" placeholder="地址">
						</div>
					
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 14px;margin-top: 7px;">邮编</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="postCd" ng-model="editForm.postCd" class="form-control" placeholder="邮编">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-top:4px;padding-left: 7px;margin-top: 2px;"><span style="font-size: 18px;color:red">*</span>状态</div>
							<select style="margin-top: 5px" name="st" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.st"  style="width: 100%；;margin-top: 5px" class="select2" data-placeholder="状态">								<option value=""></option>
								<option value=""></option>
								<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
							</select>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/oms/constractor.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script> 
</html>
