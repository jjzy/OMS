<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>订单类型</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="orderTypeCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span> 查询
			</button>
			<shiro:hasPermission name="oms:orderType:add">
			<button type="button" class="btn btn-default" id="add" ng-click="onOpenEditFormModal('add')">
				<span class="glyphicon glyphicon-plus"></span> 增加
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:orderType:del">
			<button type="button" class="btn btn-default" ng-click="onOpenDelFormModal()" ng-disabled="flag">
				<span class="glyphicon glyphicon-minus"></span> 停用
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:orderType:upd">
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('upd')">
				<span class="glyphicon glyphicon-pencil"></span> 编辑
			</button>
			</shiro:hasPermission>
			<button type="button" class="btn btn-default" ng-click="refresh()" id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
			<shiro:hasPermission name="oms:orderType:export">
			<button type="button" class="btn btn-default" ng-click="onOpenExportFormModal()">
				<span class="glyphicon glyphicon-export"></span> 导出
			</button>
			</shiro:hasPermission>
		</div>
	</div>
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="400px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group">
						<label for="clientId">客户</label>
						<select name=clientId" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.clientId" style="width: 100%" class="select2" data-placeholder="客户">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="client in clientList" value="{{client.id}}">{{client.shortName}}</option>
						</select>
					</div>	
					<div class="form-group">
						<label for="cd">代码</label> 
						<input style="width:150px;" type="text" name="cd" ng-model="queryForm.cd" class="form-control" placeholder="订单标识">		
					</div>	
					<div class="form-group" style="margin-top: 5px;">
						<label for="name">名称</label> 
						<input style="width:150px;" type="text" name="name" ng-model="queryForm.name" class="form-control" placeholder="名称">		
					</div>	
					<div class="form-group" style="margin-top: 5px;">
						<label for="st">状态</label>
						<select name="st" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.st" style="width: 100%" class="select2" data-placeholder="状态">
								<option value="0">在用</option>
								<option value="1">停用</option>
						</select>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal" tabindex="-1" data-width="500px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title" style="font-weight: bold;">订单类型管理</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" class="form-horizontal" role="form" novalidate>
					<div class="form-group hidden">
						<label for="id" class="col-sm-4 control-label">ID</label> 
						<div class="col-sm-5">
							<input type="hidden" name="id" ng-model="editForm.id" class="form-control">		
						</div>
					</div>
					<div class="form-group">
						<label for="clientId" class="col-sm-4 control-label" style="margin-top: -5px;padding-right: 2px;"><span style="font-size: 18px;color:red">*</span>客户</label>
						<div class="col-sm-5">
							<select ng-disabled="flagC" name="clientId" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.clientId"  style="width: 100%" class="select2" data-placeholder="客户">
								<option value=""></option>
								<option ng-repeat="client in clientList" value="{{client.id}}">{{client.shortName}}</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="cd" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>代码</label> 
						<div class="col-sm-5">
							<input type="text" style="width: 180px;" name="cd" ng-model="editForm.cd" required class="form-control" placeholder="订单标识">		
						</div>
					</div>
					<div class="form-group">
						<label for="name" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>名称</label> 
						<div class="col-sm-5">
							<input type="text" style="width: 180px; name="name" ng-model="editForm.name" required class="form-control" placeholder="名称">		
						</div>
					</div>
					<div class="form-group">
						<label for="typ" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>类型</label> 
						<div class="col-sm-5">
							<select style="margin-top: 5px" name="typ" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.typ"  style="width: 100%；;margin-top: 5px" class="select2" data-placeholder="状态">								<option value=""></option>
								<option>直发</option>
								<option>收货</option>
								<option>发货</option>
							</select>	
						</div>
					</div>
					<div class="form-group">
						<label for="distId" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>运输方式</label>
						<div class="col-sm-5">
							<select name="shipment_method" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.shipment_method"  style="width: 100%" class="select2" data-placeholder="运输方式">
								<option value=""></option>
								<option ng-repeat="cd in cdList" value="{{cd.descr}}">{{cd.val}}</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="st" class="col-sm-4 control-label" style="margin-top: -5px;"><span style="font-size: 18px;color:red">*</span>状态</label> 
						<div class="col-sm-5">
							<select style="margin-top: 5px" name="st" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.st"  style="width: 100%；;margin-top: 5px" class="select2" data-placeholder="状态">								<option value=""></option>
								<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
							</select>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/oms/ordertype.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script> 
</html>
