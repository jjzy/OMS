<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>订单</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="orderHeadCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span>
				查询
			</button>
			<shiro:hasPermission name="oms:orderHead:add">
				<button type="button" class="btn btn-default" id="add" ng-click="onOpenEditFormModal('add')">
					<span class="glyphicon glyphicon-plus"></span>
					增加
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:orderHead:upd">
				<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('upd')">
					<span class="glyphicon glyphicon-pencil"></span>
					编辑
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:orderHead:confirm">
			<button type="button" class="btn btn-default" ng-click="onChangeSt('SIGNED', '审核')" ng-disabled="flagOfConfirmOrderBtn">
				<span class="glyphicon glyphicon-check"></span>
				录入确认
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:orderHead:arrived">
			<button type="button" name="arr" ng-disabled="flagOfEnableOrderBtn" class="btn btn-default" ng-click="onChangeSt('ARRIVED', '生效')">
				<span class="glyphicon glyphicon-flag"></span>
				生效
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:orderHead:back">
				<button type="button" class="btn btn-default" ng-disabled="flagOfDisableOrderBtn" ng-click="onChangeSt('INPUT', '失效')">
					<span class="glyphicon glyphicon-minus"></span>
					失效
				</button>
			</shiro:hasPermission>
			<button type="button" class="btn btn-default" ng-click="refresh()" id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
			<shiro:hasPermission name="oms:orderHead:import">
			<button type="button" class="btn btn-default" ng-click="onOpenImportFormModal()" id="bt">
				<span class="glyphicon glyphicon-import"></span>
				导入
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:orderHead:export">
			<button type="button" class="btn btn-default" ng-click="onOpenExportFormModal()">
				<span class="glyphicon glyphicon-export"></span>
				导出
			</button>
			</shiro:hasPermission>
			
		</div>
	</div>
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="480px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group" style="margin-left: 48px;">
						<label for="clientId">客户</label>
						<select name="clientId" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.clientId" style="width: 100%" class="select2" data-placeholder="客户">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="client in clientList" value="{{client.id}}">{{client.shortName}}</option>
						</select>
					</div>
					<div class="form-group">
						<label for="cd">订单号</label>
						<input style="width: 150px;" type="text" name="cd" ng-model="queryForm.cd" class="form-control" placeholder="订单标识">
					</div>
					<div class="form-group" style="margin-top: 5px;margin-left: 24px;">
						<label for="orderTypeId">订单类型</label>
						<select name="orderTypeId" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.orderTypeId" style="width: 100%" class="select2" data-placeholder="订单类型">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="orderType in orderTypeList" value="{{orderType.id}}">{{orderType.name}}</option>
						</select>
					</div>
					<div class="form-group" style="margin-top: 5px; margin-left: 12px;">
						<label for="status">状态</label>
						<select name="status" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.status" style="width: 100%" class="select2" data-placeholder="状态">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="status in statusList" value="{{status.descr}}">{{status.val}}</option>
						</select>
					</div>
					<div class="form-group" style="margin-top: 5px; ">
						<label for="status">计划到达时间</label>
						<input type="text" style="width: 150px;" name="planaDt1" ng-model="queryForm.planaDt1" placeholder="开单时间" />								
					</div>
					<div class="form-group" style="margin-top: 5px; margin-left: 24px;">
						<label for="status">至</label>
						<input type="text" style="width: 150px;" name="planaDt2" ng-model="queryForm.planaDt2" placeholder="开单时间" />								
					</div>
					<div class="form-group" style="margin-top: 5px;margin-left: 24px">
						<label for="status">开单日期</label>
						<input type="text" style="width: 150px;" name="orderDt1" ng-model="queryForm.orderDt1" placeholder="开单时间" />								
					</div>
					<div class="form-group" style="margin-top: 5px; margin-left: 24px;">
						<label for="status">至</label>
						<input type="text" style="width: 150px;" name="orderDt2" ng-model="queryForm.orderDt2" placeholder="开单时间" />								
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button id="cx" ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal" tabindex="-1" data-width="1050" style="display: none; margin-left: 0px;">
	<div class="modal-header" id="or">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title" style="font-weight: bold;">订单信息管理</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" role="form" novalidate>
					<input type="hidden" name="id" ng-model="editForm.id" class="form-control">
					<input type="hidden" name="shipment_method" ng-model="editForm.shipment_method" class="form-control">
					<div class="row">
						<div class="col-sm-3">
							<div style="width: 50px; float: left; font-size: 12px; padding-left: 8px; margin-left: 12px">
								<span style="font-size: 18px; color: red">*</span>
								客户
							</div>
							<select name="clientId" ng-disabled="flagOfClientCd" id="e" ng-required="true" ui-select2="{width:'150px', allowClear:'true'}" ng-model="editForm.clientId" style="width: 100%" class="select2" data-placeholder="客户">
								<option value=""></option>
								<option ng-repeat="client in clientList" value="{{client.id}}">{{client.shortName}}</option>
							</select>

						</div>
						<div class="col-sm-3">
							<div>
								<div style="width: 60px; float: left; font-size: 12px; padding-left: 4px; margin-left: -20px">
									<span style="font-size: 18px; color: red">*</span>
									订单号
								</div>
								<input type="text" ng-disabled="flagOfCd" name="cd" style="width: 150px" ng-model="editForm.cd" required class="form-control" placeholder="订单号">
							</div>
						</div>
						<div class="col-sm-3">
							<div style="width: 80px; float: left; font-size: 12px; padding-left: 10px; margin-left: -30px">
								<span style="font-size: 18px; color: red">*</span>
								业务类型
							</div>
							<select name="vocation" ui-select2="{width:'150px', allowClear:'true'}" ng-model="editForm.vocation" style="width: 100%" class="select2" data-placeholder="业务类型">
								<option value=""></option>
								<option ng-repeat="cd in cdList" value="{{cd.descr}}">{{cd.val}}</option>
							</select>
						</div>
						<div class="col-sm-3">
							<div style="width: 80px; float: left; font-size: 12px; padding-left: 10px; margin-left: -30px">
								<span style="font-size: 18px; color: red">*</span>
								订单类型
							</div>
							<select name="orderTypeId" ng-required="true" ui-select2="{width:'150px', allowClear:'true'}" ng-model="editForm.orderTypeId" style="width: 100%" class="select2" data-placeholder="订单类型" id="ot">
								<option value=""></option>
								<option ng-repeat="orderType in orderTypeList" value="{{orderType.id}}">{{orderType.name}}</option>
							</select>
						</div>
					</div>
					<hr style="margin-top: 5px; margin-bottom: 5px; border-color: #aaa;">
					<div class="row">
						<div class="col-sm-3">
							<div style="width: 70px; float: left; font-size: 12px; padding-left: 10px; margin-top: 7px; margin-left: -8px">开单时间</div>

							<div class="input-append date">
								<input type="text" style="width: 150px;" name="orderDt" ng-model="editForm.orderDt" placeholder="开单时间" />								
							</div>
						</div>
						<div class="col-sm-3">
							<div style="width: 70px; float: left; font-size: 12px; padding-left: 10px; margin-top: 7px; margin-left: -30px">计费周期</div>
							<input type="text" style="width: 150px;" name="billingDt" ng-model="editForm.billingDt" class="form-control" placeholder="计费周期" id="billingDt">
						</div>
						<div class="col-sm-3">
							<div style="width: 100px; float: left; font-size: 12px; padding-left: 10px; margin-top: 7px; margin-left: -50px">计划出发时间</div>
							<input type="text" style="width: 150px;" name="planlDt" ng-model="editForm.planlDt" class="form-control" placeholder="计划出发时间" id="planlDt">
						</div>

						<div class="col-sm-3">
							<div style="width: 100px; float: left; font-size: 12px; padding-left: 10px; margin-top: 7px; margin-left: -50px">计划到达时间</div>
							<input type="text" style="width: 150px;" name="planaDt" ng-model="editForm.planaDt" class="form-control" placeholder="计划到达时间" id="planaDt">
						</div>
					</div>
					<hr style="margin-top: 5px; margin-bottom: 5px; border-color: #aaa;">
					<div class="row">
						<div class="col-sm-3">
							<div style="width: 50px; float: left; font-size: 12px; padding-left: 8px; margin-top: 7px; margin-left: 14px">数量</div>
							<input type="text" style="width: 150px;" name="quantity" ng-model="editForm.quantity" class="form-control" placeholder="数量">
						</div>
						<div class="col-sm-3">
							<div style="width: 50px; float: left; font-size: 12px; padding-left: 12px; margin-top: 7px; margin-left: -10px">重量</div>
							<input type="text" style="width: 150px;" name="weight" ng-model="editForm.weight" class="form-control" placeholder="重量">
						</div>
						<div class="col-sm-3">
							<div style="width: 50px; float: left; font-size: 12px; padding-left: 10px; margin-top: 7px;">体积</div>
							<input type="text" style="width: 150px;" name="volume" ng-model="editForm.volume" class="form-control" placeholder="体积">
						</div>
						<div class="col-sm-3">
							<div style="width: 50px; float: left; font-size: 12px; padding-left: 10px; margin-top: 7px;">托数</div>
							<input type="text" style="width: 150px;" name="palletsum" ng-model="editForm.palletsum" class="form-control" placeholder="托数">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div style="width: 60px; float: left; font-size: 12px; padding-left: 8px; margin-top: 12px; margin-left: 4px;">一口价</div>
							<input type="text" style="width: 150px; margin-top: 5px" name="expense" ng-model="editForm.expense" class="form-control" placeholder="一口价">
						</div>
						<div class="col-sm-3">
							<div style="width: 80px; float: left; font-size: 12px; padding-left: 18px; margin-left: -40px;margin-top: 12px;">			
								运输方式
							</div>
							<select name="shipment_method" ui-select2="{width:'150px', allowClear:'true'}" ng-model="editForm.shipment_method" style="width: 100%;margin-top: 5px;" class="select2" data-placeholder="业务类型">
								<option value=""></option>
								<option value=""></option>
								<option ng-repeat="shipment_method in shipment_methodList" value="{{shipment_method.descr}}">{{shipment_method.val}}</option>
							</select>
						</div>
						<div class="col-sm-3" style="width: 510px;">
							<div style="width: 50px; float: left; font-size: 12px; padding-left: 10px; margin-top: 12px; ">备注</div>
							<input type="text" style="width: 417px; margin-top: 5px; margin-left: 40px;" name="descr" ng-model="editForm.descr" class="form-control" placeholder="描述">
						</div>

					</div>
					<hr style="margin-top: 5px; margin-bottom: 5px; border-color: #aaa;">
					<div class="row">
						<div class="col-sm-4">
							<div style="width: 90px; float: left; font-size: 12px; padding-left: 10px;">
								<span style="font-size: 18px; color: red">*</span>
								发货方代码
							</div>
							<select name="freceiverId" ng-required="true" id="f" ui-select2="{width:'150px', allowClear:'true'}" ng-model="editForm.freceiverId" style="width: 100%" class="select2" data-placeholder="发货方标识">
								<option value=""></option>
								<option ng-repeat="receiver in receiverList" value="{{receiver.id}}">{{receiver.cd}}</option>
							</select>
						</div>
						<div class="col-sm-4">
							<div style="width: 90px; float: left; font-size: 12px; padding-left: 14px; margin-top: 7px; margin-left: -10px">发货方名称</div>
							<input type="text" style="width: 150px;" name="freceiverName" ng-model="editForm.freceiverName" class="form-control" placeholder="发货方名称">
						</div>
						<div class="col-sm-4">
							<div style="width: 60px; float: left; font-size: 12px; margin-left: -10px">
								<span style="font-size: 18px; color: red">*</span>
								出发地
							</div>
							<select name="ftranslocationId" ng-required="true" ui-select2="{width:'150px', allowClear:'true'}" ng-model="editForm.ftranslocationId" style="width: 100%" class="select2" data-placeholder="出发地">
								<option value=""></option>
								<option ng-repeat="translocation in translocationList" value="{{translocation.id}}">{{translocation.name}}</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div style="width: 100px; float: left; font-size: 12px; padding-left: 14px; margin-top: 12px; margin-left: -10px">发货方联系人</div>
							<input type="text" style="width: 150px; margin-top: 5px" name="freceiverLikename" ng-model="editForm.freceiverLikename" class="form-control" placeholder="发货方联系人">
						</div>

						<div class="col-sm-4">
							<div style="width: 110px; float: left; font-size: 12px; padding-left: 10px; margin-top: 12px; margin-left: -30px">发货方联系电话</div>
							<input type="text" style="width: 150px; margin-top: 5px" name="freceiverPhone" ng-model="editForm.freceiverPhone" class="form-control" placeholder="发货方联系电话">
						</div>

						<div class="col-sm-4">
							<div style="width: 90px; float: left; font-size: 12px; padding-left: 14px; margin-top: 12px; margin-left: -40px">发货方传真</div>
							<input type="text" style="width: 150px; margin-top: 5px" name="freceiverFax" ng-model="editForm.freceiverFax" class="form-control" placeholder="发货方传真">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div style="width: 90px; float: left; font-size: 12px; padding-left: 14px; margin-top: 12px;">发货方邮箱</div>
							<input type="text" style="width: 150px; margin-top: 5px" name="freceiverEmail" ng-model="editForm.freceiverEmail" class="form-control" placeholder="发货方邮箱">
						</div>
						<div class="col-sm-4">
							<div style="width: 90px; float: left; font-size: 12px; padding-left: 14px; margin-top: 12px; margin-left: -10px">发货方邮编</div>
							<input type="text" style="width: 150px; margin-top: 5px" name="freceiverPostcode" ng-model="editForm.freceiverPostcode" class="form-control" placeholder="发货方邮编">
						</div>

						<div class="col-sm-4">
							<div style="width: 90px; float: left; font-size: 12px; padding-left: 12px; margin-top: 12px; margin-left: -40px">发货方地址</div>
							<input type="text" style="width: 150px; margin-top: 5px" name="freceiverAddress" ng-model="editForm.freceiverAddress" class="form-control" placeholder="发货方地址">
						</div>
					</div>
					<hr style="margin-top: 5px; margin-bottom: 5px; border-color: #aaa;">
					<div class="row">
						<div class="col-sm-4">
							<div style="width: 90px; float: left; font-size: 12px; padding-left: 6px; margin-top: 2px;">
								<span style="font-size: 18px; color: red">*</span>
								收货方代码
							</div>
							<select name="treceiverId" id="s" ng-required="true" ui-select2="{width:'150px', allowClear:'true'}" ng-model="editForm.treceiverId" style="width: 100%" class="select2" data-placeholder="收货方标识">
								<option value=""></option>
								<option ng-repeat="receiver in receiverList" value="{{receiver.id}}">{{receiver.cd}}</option>
							</select>
						</div>

						<div class="col-sm-4">
							<div style="width: 90px; float: left; font-size: 12px; padding-left: 10px; margin-top: 7px; margin-left: -10px">收货方名称</div>
							<input type="text" style="width: 150px;" name="treceiverName" ng-model="editForm.treceiverName" class="form-control" placeholder="收货方名称">
						</div>
						<div class="col-sm-4">
							<div style="width: 65px; float: left; font-size: 12px; padding-left: 2px; margin-top: 2px; margin-left: -14px">
								<span style="font-size: 18px; color: red">*</span>
								目的地
							</div>
							<select name="ttranslocationId" ng-required="true" ui-select2="{width:'150px', allowClear:'true'}" ng-model="editForm.ttranslocationId" style="width: 100%" class="select2" data-placeholder="出发地">
								<option value=""></option>
								<option ng-repeat="translocation in translocationList" value="{{translocation.id}}">{{translocation.name}}</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div style="width: 100px; float: left; font-size: 12px; padding-left: 10px; margin-top: 12px; margin-left: -10px">收货方联系人</div>
							<input type="text" style="width: 150px; margin-top: 5px" name="treceiverLikename" ng-model="editForm.treceiverLikename" class="form-control" placeholder="收货方联系人">
						</div>

						<div class="col-sm-4">
							<div style="width: 110px; float: left; font-size: 12px; padding-left: 10px; margin-top: 12px; margin-left: -30px">收货方联系电话</div>
							<input type="text" style="width: 150px; margin-top: 5px" name="treceiverPhone" ng-model="editForm.treceiverPhone" class="form-control" placeholder="收货方联系电话">
						</div>

						<div class="col-sm-4">
							<div style="width: 90px; float: left; font-size: 12px; padding-left: 12px; margin-top: 12px; margin-left: -40px">收货方传真</div>
							<input type="text" style="width: 150px; margin-top: 5px" name="treceiverFax" ng-model="editForm.treceiverFax" class="form-control" placeholder="收货方传真">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div style="width: 90px; float: left; font-size: 12px; padding-left: 10px; margin-top: 12px;">收货方邮箱</div>
							<input type="text" style="width: 150px; margin-top: 5px" name="treceiverEmail" ng-model="editForm.treceiverEmail" class="form-control" placeholder="收货方邮箱">
						</div>
						<div class="col-sm-4">
							<div style="width: 90px; float: left; font-size: 12px; padding-left: 10px; margin-top: 12px; margin-left: -10px">收货方邮编</div>
							<input type="text" style="width: 150px; margin-top: 5px" name="treceiverPostcode" ng-model="editForm.treceiverPostcode" class="form-control" placeholder="收货方邮编">
						</div>

						<div class="col-sm-4">
							<div style="width: 90px; float: left; font-size: 12px; padding-left: 12px; margin-top: 12px; margin-left: -40px">收货方地址</div>
							<input type="text" style="width: 150px; margin-top: 5px" name="treceiverAddress" ng-model="editForm.treceiverAddress" class="form-control" placeholder="收货方地址">
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
	<div style="width: 1045px;">
		<div id="mainData" ng-grid="gridOption" style="width: 100%;"></div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="disabledSubmitBtnOfEditForm" class="btn btn-default">提交</button>
	</div>
</div>

<div id="importFormModal" class="modal" tabindex="-1" data-width="400px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">导入</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="importFormForm" action="{{importForm.importUrl}}" target="_blank" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
					<div class="form-group">
						<div class="col-sm-8">
							<input name="importUrl" id="importUrl" ng-required="true" ng-model="importForm.importUrl" class="form-control" placeholder="导入" style="display: none;">
						</div>
					</div>

					<div class="form-group">
						<label for="file" class="col-sm-3 control-label" style="width: 70px;">文件</label>
						<div style="float: left; margin-right: 10px;">
							<input type="file" name="file" class="form-control">
						</div>
						<div style="float: left;">
							<button ng-click="onExportTemplate()" type="button" class="btn btn-success" style="height: 30px; padding: 4px 6px;">下载模板</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-disabled="importFormForm.$invalid" ng-click="onImport()" class="btn btn-primary">导入</button>
	</div>
</div>

<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/oms/orderHead.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script>
</html>
