<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>运输工具类型信息管理</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="vehicleCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span> 查询
			</button>
			<shiro:hasPermission name="oms:vehicle:add">
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('add')">
				<span class="glyphicon glyphicon-plus"></span> 增加
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:vehicle:del">
			<button type="button" class="btn btn-default" ng-click="onOpenDelFormModal()" ng-disabled="flag">
				<span class="glyphicon glyphicon-minus"></span> 停用
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:vehicle:upd">
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('upd')">
				<span class="glyphicon glyphicon-pencil"></span> 编辑
			</button>
			</shiro:hasPermission>
			<button type="button" class="btn btn-default" ng-click="refresh()" id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
		</div>
	</div>
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="450px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group" style="margin-left: 12px;">
						<label for="cd">代码</label>
						<select name="cd" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.cd" style="width: 100%;" class="select2" data-placeholder="标识">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="cd in cdList" value="{{cd.cd}}">{{cd.cd}}</option>
						</select>
					</div>					
					<div class="form-group">
						<label for="contractType">承包类型</label>
							<select name="contractType" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.contractType" style="width: 100%" class="select2" data-placeholder="状态">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="contractType in contractTypeList" value="{{contractType.descr}}">{{contractType.val}}</option>
						</select>
					</div>	
					<div class="form-group" style="margin-top: 5px;">
						<label for="name">车牌号</label> 
						<input style="width:150px;" type="text" name="vehicleNo" ng-model="queryForm.vehicleNo" class="form-control" placeholder="名称">		
					</div>	
					<div class="form-group" style="margin-top: 5px;margin-left: 25px;">
						<label for="st">状态</label>
						<select name="st" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.st" style="width: 100%" class="select2" data-placeholder="状态">
							<option value=""></option>
							<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
						</select>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal" tabindex="-1" data-width="900px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">运输工具信息管理</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" class="form-horizontal" role="form" novalidate>
							<input type="hidden" name="id" ng-model="editForm.id" class="form-control">		
					<div class="row">
						<div class="col-sm-4">
							<div style="width:55px;float: left;font-size: 12px;padding-left: 4px;"><span style="font-size: 18px;color:red">*</span>承运商 </div>
							<select name="constractorId"  ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.constractorId"  style="width: 100%;" class="select2" data-placeholder="承运商">								<option value=""></option>
								<option ng-repeat="constractor in constractorList" value="{{constractor.id}}">{{constractor.name}}</option>
							</select>		
						</div>
						<div class="col-sm-4">
							<div style="width:70px;float: left;font-size: 12px;padding-left: 8px;"><span style="font-size: 18px;color:red">*</span>工具类型 </div>
							<select name="vehicleTypeId"  ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.vehicleTypeId"  style="width: 100%;" class="select2" data-placeholder="运输工具类型">								<option value=""></option>
								<option ng-repeat="vehicleType in vehicleTypeList" value="{{vehicleType.id}}">{{vehicleType.name}}</option>
							</select>			
						</div>
						<div class="col-sm-4">
							<div style="width:70px;float: left;font-size: 12px;padding-left: 8px;"><span style="font-size: 18px;color:red">*</span>车辆编号</div>
							<input style="width: 180px;" type="text" name="cd" ng-model="editForm.cd" required class="form-control" placeholder="车辆编号">								
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div style="width:55px;float: left;font-size: 12px;padding-left: 4px;margin-top: 5px;"><span style="font-size: 18px;color:red">*</span>车牌号</div>
							<input style="width: 180px;margin-top: 5px;" type="text" name="vehicleNo" ng-model="editForm.vehicleNo" required class="form-control" placeholder="车牌号">	
						</div>
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 10px;margin-left: 20px;margin-top: 5px;"><span style="font-size: 18px;color:red">*</span>体积</div>
							<input style="width: 180px;margin-top: 5px;" type="text" name="volume" ng-model="editForm.volume" class="form-control" placeholder="体积">		
						</div>
						<div class="col-sm-4">
							<div style="width:70px;float: left;font-size: 12px;padding-left: 8px;margin-top: 5px;"><span style="font-size: 18px;color:red">*</span>承包类型 </div>
							<select  name="contractType" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.contractType"  style="width: 100%;margin-top: 5px;" class="select2" data-placeholder="承包类型">						
								<option value=""></option>
								<option ng-repeat="contractType in contractTypeList" value="{{contractType.descr}}">{{contractType.val}}</option>
							</select>			
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 16px;margin-left: 5px;margin-top: 12px;">司机</div>
							<select style="margin-top: 5px" name="driverId" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.driverId"  style="width: 100%；;margin-top: 5px" class="select2" data-placeholder="司机">														
								<option value=""></option>
								<option ng-repeat="driver in driverList" value="{{driver.id}}">{{driver.name}}</option>
							</select>			
						</div>
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 16px;margin-left: 20px;margin-top: 12px;">描述</div>
							<input style="width: 180px;margin-top: 5px;" type="text" name="descr" ng-model="editForm.descr" class="form-control" placeholder="描述">		
						</div>
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 10px;margin-left: 20px;margin-top: 5px;"><span style="font-size: 18px;color:red">*</span>状态</div>
							<select style="margin-top: 5px" name="st" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.st"  style="width: 100%；;margin-top: 5px" class="select2" data-placeholder="状态">						
								<option value=""></option>
								<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
							</select>			
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/oms/vehicle.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script> 
</html>
