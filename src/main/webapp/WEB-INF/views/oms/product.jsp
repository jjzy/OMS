<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>产品</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="productCtrl">
	<div id="mainDataBlock" ng-grid="gridOptions"></div>

	<div id="funcBlock" class="alert alert-info">
		<div class="btn-group btn-group-sm">
			<button type="button" class="btn btn-default" ng-click="onOpenQueryFormModal()">
				<span class="glyphicon glyphicon-search"></span> 查询
			</button>
			<shiro:hasPermission name="oms:product:add">
			<button type="button" class="btn btn-default" id="add" ng-click="onOpenEditFormModal('add')">
				<span class="glyphicon glyphicon-plus"></span> 增加
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:product:del">
			<button type="button" class="btn btn-default" ng-click="onOpenDelFormModal()" ng-disabled="flag">
				<span class="glyphicon glyphicon-minus"></span> 停用
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:product:upd">
			<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('upd')">
				<span class="glyphicon glyphicon-pencil"></span> 编辑
			</button>
			</shiro:hasPermission>
			<button type="button" class="btn btn-default" ng-click="refresh()" id="bt">
				<span class="glyphicon glyphicon-refresh"></span> 刷新
			</button>
			<shiro:hasPermission name="oms:product:import">
			<button type="button" class="btn btn-default" ng-click="onOpenImportFormModal()">
				<span class="glyphicon glyphicon-import"></span> 导入
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="oms:product:export">
			<button type="button" class="btn btn-default" ng-click="onOpenExportFormModal()">
				<span class="glyphicon glyphicon-export"></span> 导出
			</button>
			</shiro:hasPermission>
		</div>
	</div>
</body>

<div id="queryFormModal" class="modal" tabindex="-1" data-width="400px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">查询</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group">
						<label for="clientId">客户</label>
						<select name=clientId" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.clientId" style="width: 100%" class="select2" data-placeholder="客户">
							<option value=""></option>
							<option value=""></option>
							<option ng-repeat="client in clientList" value="{{client.id}}">{{client.shortName}}</option>
						</select>
					</div>	
					<div class="form-group">
						<label for="cd">代码</label> 
						<input type="text" name="cd" ng-model="queryForm.cd" class="form-control" placeholder="产品标识" style="width:150px;">		
					</div>	
					<div class="form-group" style="margin-top: 5px;">
						<label for="name">名称</label> 
						<input type="text" name="name" ng-model="queryForm.name" class="form-control" placeholder="名称" style="width:150px;">		
					</div>
					<div class="form-group" style="margin-top: 5px;">
						<label for="st">状态</label>
						<select name="st" ui-select2="{width:'150px', allowClear:'true'}" ng-model="queryForm.st" style="width: 100%" class="select2" data-placeholder="状态">
							<option value="" style="height: 10px;"></option>
							<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
						</select>
					</div>	
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onQuery()" ng-disabled="queryFormForm.$invalid" class="btn btn-default">查询</button>
	</div>
</div>

<div id="editFormModal" class="modal" tabindex="-1" data-width="75%" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title" style="font-weight: bold;">产品信息管理</h4>
	</div>
<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" role="form" novalidate>
					<input type="hidden" name="id" ng-model="editForm.id"
						class="form-control">
					<div class="row">
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 6px;margin-left: 20px;"><span style="font-size: 18px;color:red">*</span>客户</div>
							<select ng-disabled="flagC" name="clientId" id="e" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.clientId"  style="width: 100%" class="select2" data-placeholder="客户">
								<option value=""></option>
								<option ng-repeat="client in clientList" value="{{client.id}}">{{client.shortName}}</option>
							</select>
						</div>
					
						<div class="col-sm-4">
						<div style="width:70px;float: left;font-size: 12px;padding-left:14px;margin-left: 5px;margin-top: 7px;">产品类型</div>
							<select name="producttypeId" ui-select2="{width:'180px', allowClear:'true'}" ng-model="editForm.producttypeId" style="width: 100%;margin-left: 5px;" class="select2" data-placeholder="产品类型">
								<option value=""></option>
								<option ng-repeat="producttype in producttypeList" value="{{producttype.id}}">{{producttype.name}}</option>
							</select>
						</div>
					<div class="col-sm-4">
							<div style="width:65px;float: left;margin-left:5px;margin-right:2px; font-size: 12px;p"><span style="font-size: 18px;color:red">*</span>计量单位</div>
							<select name="uom" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}" ng-model="editForm.uom" style="width: 100%" class="select2" data-placeholder="计量单位">
								<option value=""></option>
								<option ng-repeat="dict in dictList" value="{{dict.lb}}">{{dict.val}}</option>
							</select>		
						</div>
					</div>
					<div class="row">
					<div class="col-sm-4">
						<div style="width:50px;float: left;font-size: 12px;padding-left: 6px;margin-top: 5px;margin-left: 20px;"><span style="font-size: 18px;color:red">*</span>代码</div>
							<input type="text" name="cd" style="width: 180px;margin-top: 5px" ng-model="editForm.cd" required class="form-control" placeholder="标识">		
						</div>
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 10px;margin-top: 5px;margin-left: 25px;margin-right: 5px;"><span style="font-size: 18px;color:red">*</span>名称</div>
							<input type="text" style="width: 180px;margin-top: 5px;" name="name" ng-model="editForm.name" required class="form-control" placeholder="名称">		
						</div>
						</div>
					<hr style="margin-top: 5px;margin-bottom: 5px;border-color:#aaa;">
					<div class="row">
						<div class="col-sm-4">
							<div style="width:80px;float: left;margin-left:-10px; font-size: 12px;"><span style="font-size: 18px;color:red">*</span>主单位代码</div>
							<input type="text" style="width: 180px;" name="muCd" ng-model="editForm.muCd" required class="form-control" placeholder="主单位标识">		
						</div>
						<div class="col-sm-4">
							<div style="width:75px;float: left;font-size: 12px;margin-right: 5px;"><span style="font-size: 18px;color:red">*</span>主单位名称</div>
							<input type="text" style="width: 180px;" name="muname" ng-model="editForm.muname" required class="form-control" placeholder="主单位名称">		
						</div>
				 	</div>
				 	<div class="row">
						<div class="col-sm-4">
							<div style="width:75px;float: left;margin-left:-5px; font-size: 12px;margin-top: 12px;">主单位长度</div>
							<input type="text" style="width: 180px;margin-top: 5px" name="mulength" ng-model="editForm.mulength" class="form-control" placeholder="主单位长度">		
						</div>
					 
						<div class="col-sm-4">
							<div style="width:75px;float: left;margin-left:5px; font-size: 12px;margin-top: 12px;">主单位宽度</div>
							<input type="text" style="width: 180px;margin-top: 5px" name="muwidth" ng-model="editForm.muwidth" class="form-control" placeholder="主单位宽度">		
						</div>
				
						<div class="col-sm-4">
							<div style="width:75px;float: left;margin-left:-3px;padding-left:2px; font-size: 12px;margin-top: 12px;">主单位高度</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="muheight" ng-model="editForm.muheight" class="form-control" placeholder="主单位高度">		
						</div>
					 </div>
					 <div class="row">
						<div class="col-sm-4">
							<div style="width:75px;float: left;margin-left:-5px; font-size: 12px;margin-top: 10px;">主单位体积</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="muvolume" ng-model="editForm.muvolume" class="form-control" placeholder="主单位体积">		
						</div>
					
						<div class="col-sm-4">
							<div style="width:75px;float: left;margin-left:5px; font-size: 12px;margin-top: 10px;">主单位重量</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="muweight" ng-model="editForm.muweight" class="form-control" placeholder="主单位重量">		
						</div>					 
						<div class="col-sm-4">
							<div style="width:75px;float: left;margin-left:-3px;padding-left:2px; font-size: 12px;margin-top: 10px;">主单位净重</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="munetweight" ng-model="editForm.munetweight" class="form-control" placeholder="主单位净重">		
						</div>
					</div>
					<hr style="margin-top: 5px;margin-bottom: 5px;border-color:#aaa;">
					<div class="row">
						<div class="col-sm-4">
							<div style="width:75px;float: left;margin-left:-5px; font-size: 12px;margin-top: 7px;">箱单位代码</div>
							<input type="text" style="width: 180px;" name="caseCd" ng-model="editForm.caseCd" class="form-control" placeholder="箱单位标识">		
						</div>
					
						<div class="col-sm-4">
							<div style="width:75px;float: left;margin-left:5px; font-size: 12px;margin-top: 7px;">箱单位名称</div>
							<input type="text" style="width: 180px;" name="casename" ng-model="editForm.casename" class="form-control" placeholder="箱单位名称">		
						</div>
						<div class="col-sm-4">
							<div style="width:105px;float: left;margin-left:-35px;padding-left:8px; margin-right:2px;font-size: 12px;margin-top: 12px;">包含主单位数量</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="casequantity" ng-model="editForm.casequantity" class="form-control" placeholder="包含主单位数量">		
						</div>
						</div>
				 <div class="row">										 
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 10px;margin-top: 12px;margin-left:20px;">箱长</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="caselength" ng-model="editForm.caselength" class="form-control" placeholder="箱长">		
						</div>
					 
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 10px;margin-top: 12px;margin-left:30px;">箱宽</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="casewidth" ng-model="editForm.casewidth" class="form-control" placeholder="箱宽">		
						</div>
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 12px;margin-top: 12px;margin-left:20px;margin-right: 2px;">箱高</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="caseheight" ng-model="editForm.caseheight" class="form-control" placeholder="箱高">		
						</div>
						</div>
					<div class="row">				
				
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 10px;margin-top: 12px;margin-left:20px;">箱重</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="caseweight" ng-model="editForm.caseweight" class="form-control" placeholder="箱重">		
						</div>
				 
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-top:4px;margin-top: 7px;margin-left:30px;">箱净重</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="casenetweight" ng-model="editForm.casenetweight" class="form-control" placeholder="箱净重">		
						</div>
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-top:4px;margin-top: 7px;margin-left:20px;margin-right: 2px;">箱体积</div>
							<input type="text" style="width: 180px; margin-top: 5px" name="casevolume" ng-model="editForm.casevolume" class="form-control" placeholder="箱体积">		
						</div>
						</div>
						<div class="row">							
						<div class="col-sm-4">
							<div style="width:50px;float: left;font-size: 12px;padding-left: 2px;margin-top: 5px;margin-left:20px;"><span style="font-size: 18px;color:red">*</span>状态</div>
							<select style="margin-top: 5px" name="st" ng-required="true" ui-select2="{width:'180px', allowClear:'true'}"  ng-model="editForm.st"  style="width: 100%; margin-top:5px" class="select2" data-placeholder="状态">								<option value=""></option>
								<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
							</select>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>
<div id="importFormModal" class="modal" tabindex="-1" data-width="400px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">导入</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="importFormForm" action="{{importForm.importUrl}}" target="_blank" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
					<div class="form-group">
						<div class="col-sm-8">
						<input name="importUrl" id="importUrl" ng-required="true" ng-model="importForm.importUrl" class="form-control" placeholder="导入" style="display: none;">
						</div>
					</div>

					<div class="form-group">
						<label for="file" class="col-sm-3 control-label">文件</label>
						<div style="float: left;margin-right: 10px;">
							<input type="file" name="file" class="form-control">
						</div>
						<div style="float: left;" >
								<button ng-click="onExportTemplate()" type="button" class="btn btn-success" style="height: 30px;padding: 4px 6px;">下载模板</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button type="button" ng-disabled="importFormForm.$invalid" ng-click="onImport()" class="btn btn-primary">导入</button>
	</div>
</div>
<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/oms/product.js"></script>
<script type="text/javascript" src="${ctx}/res_oms/drag.js"></script>
</html>
