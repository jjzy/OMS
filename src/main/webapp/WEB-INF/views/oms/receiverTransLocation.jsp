<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html class="ng-app:myApp" id="ng-app" ng-app="myApp" xmlns:ng="http://angularjs.org">
<head>
<title>收发货方与运输地</title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
</head>

<body ng-controller="receiverTransLocationCtrl">
			<div style="margin-bottom: 5px;">
				<form name="queryFormForm" class="form-inline" role="form">
					<div class="form-group">
						<label for="receiverCd" class="sr-only">客户代码</label> 
						<input type="text" name="receiverCd" ng-model="queryForm.receiverCd" class="form-control" placeholder="客户代码">		
					</div>	
					<button type="submit" class="btn btn-info" ng-click="onQuery()" ng-disabled="queryFormForm.$invalid">查询</button>
				</form>
			</div>

			<div id="mainDataBlock" style="height:400px; border:1px solid rgb(212, 212, 212);" ng-grid="gridOptions"></div>

			<div class="alert alert-warning" style="padding: 5px; margin-top: 5px;">
				<div class="btn-group btn-group-sm">
					<shiro:hasPermission name="sys:receiverTransLocation:add">
					<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('add')">
						<span class="glyphicon glyphicon-plus"></span> 增加
					</button>
					</shiro:hasPermission>
					<shiro:hasPermission name="sys:receiverTransLocation:add">
					<button type="button" class="btn btn-default" ng-click="onOpenDelFormModal()">
						<span class="glyphicon glyphicon-minus"></span> 删除
					</button>
					</shiro:hasPermission>
					<shiro:hasPermission name="sys:receiverTransLocation:add">
					<button type="button" class="btn btn-default" ng-click="onOpenEditFormModal('upd')">
						<span class="glyphicon glyphicon-pencil"></span> 编辑
					</button>
					</shiro:hasPermission>
				</div>
				<button type="button" class="btn btn-default" ng-click="reflash()" id="bt">
				<span class="glyphicon glyphicon-pencil"></span> 刷新
			</button>
			</div>
</body>


<div id="editFormModal" class="modal" tabindex="-1" data-width="500px" style="display: none;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h4 class="modal-title">编辑</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<form name="editFormForm" class="form-horizontal" role="form" novalidate>
					<div class="form-group hidden">
						<label for="id" class="col-sm-4 control-label">ID</label> 
						<div class="col-sm-5">
							<input type="hidden" name="id" ng-model="editForm.id" class="form-control">		
						</div>
					</div>
					<div class="form-group">
						<label for="receiverCd" class="col-sm-4 control-label">收发货方标识</label> 
						<div class="col-sm-5">
							<input type="text" name="receiverCd" ng-model="editForm.receiverCd" required class="form-control" placeholder="收发货方标识">		
						</div>
					</div>
					<div class="form-group">
						<label for="translocationCd" class="col-sm-4 control-label">出发地标识</label> 
						<div class="col-sm-5">
							<input type="text" name="translocationCd" ng-model="editForm.translocationCd" required class="form-control" placeholder="出发地标识">		
						</div>
					</div>
					<div class="form-group">
						<label for="operatorCd" class="col-sm-4 control-label">用户标识</label> 
						<div class="col-sm-5">
							<input type="text" name="operatorCd" ng-model="editForm.operatorCd" required class="form-control" placeholder="用户标识">		
						</div>
					</div>
					<div class="form-group">
						<label for="operatorName" class="col-sm-4 control-label">客户名称</label> 
						<div class="col-sm-5">
							<input type="text" name="operatorName" ng-model="editForm.operatorName" required class="form-control" placeholder="用户名称">		
						</div>
					</div>
					<div class="form-group">
						<label for="st" class="col-sm-4 control-label">状态</label> 
						<div class="col-sm-5">
							<select name="st" ng-required="true" ng-model="editForm.st" class="form-control" data-placeholder="状态">
								<option value=""></option>
								<option ng-repeat="st in stList" value="{{st.id}}">{{st.lb}}</option>
							</select>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
		<button ng-click="onSaveEditForm()" ng-disabled="editFormForm.$invalid" class="btn btn-default">提交</button>
	</div>
</div>

<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script type="text/javascript" src="${ctx}/res_oms/oms/receiverTransLocation.js"></script>
</html>
