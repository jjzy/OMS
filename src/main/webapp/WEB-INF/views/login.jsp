<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro" %>

<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ page import="org.apache.shiro.authc.ExcessiveAttemptsException"%>
<%@ page import="org.apache.shiro.authc.IncorrectCredentialsException"%>

<c:set var="ctx" value="${pageContext.request.contextPath}" />

<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/common/meta.jsp"%>
<%@ include file="/WEB-INF/layouts/global_res_header.jsp"%>
<style type="text/css">
	body{
		text-align: center;
	}
	.account-container{
		height:350px;
		width:50%;
		background-image: url("${ctx}/res_oms/theme-b/imgs/login.png");
		background-repeat:no-repeat;  
		background-position:center center;
	}
</style>
</head>

<body>
	<div class="account-container" id="login">
	   <div class="form-content clearfix" >
		<form id="loginForm" action="${ctx}/login" method="post">
				
			<div class="login-fields">
                <!-- <p style="color:#ffffff; padding-left:20px; margin-

bottom:20px; margin-top:10px;">  >>欢迎登陆中外运VMS管理系统</p> -->
	<p style="margin-top: 25px;margin-left:30px; font-size: 30px;color: white;font-weight: bold;font-family:微软雅黑;">翔 越 OMS 系 统</p>
                <div>
                    <input class="login" type="text" id="username" style="margin-top:93px;margin-left: 40px;width: 160px;" name="username" value="" placeholder="Username" class="login username-field" required="">
                </div>
                 <div>
                    <input class="login" type="password" id="password" name="password" value="" style="margin-top:10px; margin-left: 40px;width: 160px;" placeholder="Password" class="login password-field" required="">
				</div>
			</div> 
			<div class="login-actions">
				<input class="login" type="submit" name="submit" value="  登 录   " class="btn-signin btn btn-warning btn-large" style="margin-left: 140px;height:33px;margin-top: 3px;color: #FF5400">                
			</div>
			<%String error = (String) request.getAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
			  if (error != null) {%>
			  	 <div class="row">
					<div class="alert alert-danger" style="width: 1020px;margin-top: 85px;margin-left: 20px;">
						<%if ("org.apache.shiro.authc.IncorrectCredentialsException".equals(error)) {%>
						您的帐号密码不正确，请重试。
						<%} else {%>
						登录失败，请重试。
						<%}%>
					</div>
				</div>
			<%}%>
		  </form>
	   </div> <!-- form-content -->
    </div> <!-- account-container -->
</body>
<%@ include file="/WEB-INF/layouts/global_res_footer.jsp"%>
<script>
	$(function(){
		 $("#login").css("position","absolute");  
         var dw = $(window).width();  
         var ow = $("#login").outerWidth();  
         var dh = $(window).height();  
         var oh = $("#login").outerHeight();  
         var l = (dw - ow) / 2;  
         var t = (dh - oh) / 2 > 0 ? (dh - oh) / 2 : 10;  
         var lDiff = $("#login").offset().left - $("#login").position().left;  
         var tDiff = $("#login").offset().top - $("#login").position().top;  
         l = l + $(window).scrollLeft() - lDiff;  
         t = t + $(window).scrollTop() - tDiff;  
         $("#login").css("left",l + "px");  
         $("#login").css("top",t + "px");
	});
</script>
</html>
